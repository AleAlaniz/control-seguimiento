angular.module("controlSeguimiento")
    .controller("Cshipments",Cshipments);
    function Cshipments($scope) {
        let timePress;

        $scope.shipmentType = "";
        $scope.category = "";
        $scope.operatorsSearched = [];
        $scope.operatorsAdded = [];
        $scope.filtered = [];
        $scope.suppliesFinded = [];
        $scope.suppliesAdded = [];
        $scope.suppliesAssigned = [];
        $scope.boxes = [];
        $scope.requestsFinded = [];
        $scope.requestsAdded = [];
        $scope.sended = false;


        $scope.selectShipmentType = function () {
            $scope.category = "";
            $scope.operatorsSearched = [];
            $scope.operatorsAdded = [];
            $scope.filtered = [];
            $scope.suppliesFinded = [];
            $scope.suppliesAdded = [];
            $scope.suppliesAssigned = [];
            $scope.boxes = [];
            $scope.requestsFinded = [];
            $scope.requestSelected = {};

            if($scope.shipmentType == $scope.refund){
                getOperatorsWithSupplies();
            }
            else if($scope.shipmentType == $scope.delivery){
                getRequestsToSend();
            }
        }

        // envio de insumos

        //obtencion de solicitudes 
        function getRequestsToSend() {
            $scope.loadingRequests = true;
            $.ajax({
            method 	: 'POST',
            url 	: `${FOLDERADD}/Suppliesrequests/getRequestsToSend`,
            dataType: 'JSON'
            }).done(function(data) {
                $scope.$apply(function () {
                    $scope.requestsFinded = data;
                    $scope.loadingRequests = false;
                })
            });
        }

        $scope.addRequest = function (request) {
            $scope.requestsAdded = [];
            $scope.requestsAdded.push(request);
            $scope.operatorsAdded.pop();
            $('#input_search').val(' ');
            $('#input_search').attr('disabled','disabled');
            let operator = {
                operatorId  : request.operatorId,
                name        : request.name
            }
            $scope.operatorsAdded.push(operator);
            $scope.requestSelected = request;
            $('#searchRequest').show();
        }

        $scope.searchRequests = function () {
            $('#searchRequest').hide();
            $.ajax({
            method 	: 'POST',
            url 	: `${FOLDERADD}suppliesrequests/searchRequests`,
            data 	: $scope.requestsAdded[0],
            dataType: 'JSON'
            }).done(function(data) {
                $scope.$apply(function(){
                    $scope.requestsSearched = data;
                });
            });
        }

        $scope.addRequestSearched = function (request) {
            $scope.requestsAdded.push(request);
            $scope.requestsSearched = $scope.requestsSearched.filter(el => el.requestId != request.requestId)
        }

        $scope.deleteRequestSearched = function (request) {
            $scope.requestsAdded = $scope.requestsAdded.filter(el => el.requestId != request.requestId)
            if($scope.requestsAdded.length == 0){
                $scope.operatorsAdded.pop();
            }
        }

        // busqueda de usuarios
        $('#input_search').on('keyup paste', function()
        {
            $('#loadingSpinner').css('display', 'block');

            if($('#input_search').val().length > 0)
            {
                clearTimeout(timePress);
                timePress = setTimeout(searchUser, 300);
            }
            else
                searchUser();
        });

        function searchUser() {

            // $("#search_result").empty();
            var searchText = $('#input_search').val().toLowerCase().trim();

            if (searchText.length > 0){

                $.ajax({   
                    type:     'POST',
                    url:      `${FOLDERADD}operators/searchOperators`,
                    data:     {'search':searchText},
                    dataType: 'JSON',                
                    success:  ajaxResponse
                })
            }
            else{
                $scope.$apply(function(){
                    $scope.filtered = [];
                })
                $('#loadingSpinner').css('display', 'none');
            }

            function ajaxResponse(data)
            {
                $scope.operatorsSearched = data;
                let filtered = $scope.operatorsSearched.filter(function (el) {
                    return !$scope.operatorsAdded.includes(el.operatorId)
                })
                $scope.$apply(function(){
                    $scope.filtered = filtered;
                })

                $('#loadingSpinner').css('display', 'none');
            }

        }

        $scope.addOperator = function (operator) {
            $scope.operatorsAdded.pop();
            $scope.operatorsAdded.push(operator);
            $('#input_search').val(' ');
            $('#input_search').attr('disabled','disabled');
            $scope.filtered = [];
        }

        $scope.deleteOperator = function (operator) {
            $scope.operatorsAdded.pop();
            $scope.requestsAdded = [];
            $('#input_search').removeAttr('disabled');
        }
        // fin de busqueda de usuarios

        //busqueda de insumos por categoria
        $scope.changedCategory = function () {
            var url = `${FOLDERADD}supplies/getsuppliesbycategory`;
            if($scope.category != " "){
                $scope.loadingSupplies = true;
                $.ajax({
                method 	: "POST",
                url 	: url,
                data 	: {'categoryId' : $scope.category},
                dataType: 'JSON'
                }).done(function(data) {
                    $scope.loadingSupplies = false;                    
                    $scope.$apply(function () {
                        $scope.suppliesFinded = data;
                    });
                    // $scope.suppliesAdded = $scope.suppliesFinded.filter( function( el ) {
                    //     return !$scope.suppliesAdded.includes( el.supplyId );
                    // });                    
                });
            }
            else{
                // $('#suppliesSearch').hide("fast");
            }
        }
        //fin de busqueda de insumos

        $scope.addSupply = function (supply) {
            $scope.suppliesAdded.push(supply);
            $scope.suppliesFinded = $scope.suppliesFinded.filter(el => el.supplyId != supply.supplyId)
        }

        $scope.deleteSupply = function (supply) {
            $scope.suppliesFinded.push(supply);
            $scope.suppliesAdded = $scope.suppliesAdded.filter(el => el.supplyId != supply.supplyId)
        }

        // fin de envio de insumos


        //retiro de insumos
        $('#siteId').change(function(){
            $scope.loadingBoxes = true;
            var url = `${FOLDERADD}boxes/getboxesbysite`;
            $.ajax({
            method 	: "POST",
            url 	: url,
            data 	: {'siteId' : this.value},
            dataType : 'JSON'
            }).done(function(data) {
                $scope.$apply(function () {
                    $scope.boxes = data;
                    $scope.loadingBoxes = false;
                })
            });
        })

        function getOperatorsWithSupplies(){
            $scope.loadingOperators = true;
            let url = `${FOLDERADD}operators/getoperatorswithsupplies`;
            $.ajax({
                method 	: "POST",
                url 	: url,
                }).done(function(data) {
                    $scope.loadingOperators = false;
                    let res = JSON.parse(data);
                    $scope.$apply(function(){
                        $scope.operatorsSearched = res;
                    })
                });
        }

        $scope.showSupplies = function(operator) {
            $scope.operatorsAdded.push(operator);
            $scope.loadingAssignedSupplies = true;

            var url = `${FOLDERADD}supplies/getsuppliesbyoperator` ;
            $.ajax({
                method 	    : "POST",
                url 	    : url,
                data 	    : {'operatorId' : operator.operatorId},
                dataType    : 'JSON'
                }).done(function(data) {
                    $scope.loadingAssignedSupplies = false;
                    $scope.$apply(function () {
                        $scope.suppliesFinded = data;
                    })
                });
        }
      
        // fin de retiro de insumos

        $scope.submitShipment = function () {
            let user_operators = $scope.operatorsAdded.map(element=>{
                return element.operatorId;
            })
            let suppliesAdded = $scope.suppliesAdded.map(element=>{
                return element.supplyId
            })

            let formData = {
                "type"                  : $scope.shipmentType,
                "user_operators[]"      : user_operators,
                "suppliesAdded[]"       : suppliesAdded
            }
            
            if($scope.shipmentType == $scope.refund){
                formData.boxId = $scope.boxId;
                formData.observation = $scope.observation;
                formData.refundreasonId = $scope.refundreasonId;
            }
            else{
                let requestsAdded = $scope.requestsAdded.map(el => el.requestId);
                if(requestsAdded.length > 0){
                    formData["requestsAdded[]"] = requestsAdded;
                    formData.dateFile = $scope.requestsAdded[0].dateFile;
                    formData.dateCheck = $scope.requestsAdded[0].dateCheck;
                }
            }

            $scope.sended = true;
            $.ajax({
            method 	: "POST",
            url 	: `${FOLDERADD}shipments/create`,
            data 	: formData
            }).done(function(data) {
                if(data == "success"){
                    window.location.href = FOLDERADD;
                }
            });
        }
    }