<?php 
if(!defined('BASEPATH')) exit('No direct scrript access allowed');

if(!function_exists('asset_url()'))
{
	function asset_url()
	{
		return base_url('assets/');
	}

	function getTurns()
	{
		$turns = array('Mañana','Tarde','Noche','Tiempo completo');
		return $turns;
	}

	function getDiscs()
	{
		$turns = array('256gb','500gb','1TB');
		return $turns;
	}

	function getProcesors()
	{
		$turns = array('Dual Core','I3','I5','I7','Pentium 4');
		return $turns;
	}

	function getMemories()
	{
		$turns = array('2gb','4gb','8gb','6gb');
		return $turns;
	}

	//estados de los insumos
	function getStates()
	{
		$states = array('Disponible','En reparación','Roto','Sin stock');
		return $states;
	}

	function getShipmentType()
	{
		$types = array('Envío de insumos','Retiro de insumos');
		return $types;
	}

	function getShipmentState()
	{
		$types = array('Entregado','En tránsito','No entregado');
		return $types;
	}

	function getValidationStates()
	{
		$states = array('Funcionando','No funcionando');
		return $states;
	}

	function getProvinces()
	{
		$provinces = array('Buenos Aires', 'Catamarca', 'Chaco', 'Chubut', 'Córdoba', 'Corrientes', 'Entre Ríos', 'Formosa', 'Jujuy', 'La Pampa', 'La Rioja', 'Mendoza', 'Misiones', 'Neuquén', 'Río Negro', 'Salta', 'San Juan', 'San Luis', 'Santa Cruz', 'Santa Fe', 'Santiago del Estero', 'Tierra del Fuego','Tucumán');
		return $provinces;
	}

	function getBrokenReason()
	{
		return "NO FUNCIONA";
	}

	function getSuppliesRequestState()
	{
		$states = array('Aceptada','Denegada','Enviada');
		return $states;
	}

	function insert_audit_logs($table,$action,$content)
	{
		$CI =& get_instance();
		$messageInsert = array(
			'userId'    => $CI->session->UserId,
			'table'     => $table,
			'action'    => $action,
			'content'   => json_encode($content),
			'timestamp' => time()
		);
		
		$CI->db->insert('audits', $messageInsert);
	}
}
