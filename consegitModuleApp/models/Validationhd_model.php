<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validationhd_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function Create($validationInsert)
    {
        $this->db->insert('validations', $validationInsert);
        $this->session->set_flashdata('validationMessage', 'create');
        return "success";
    }

    function GetValidations($filters)
    {
        $query = "SELECT CONCAT(o.nombre,' ', o.apellido) completeName, o.legajo, s.nombre, s.numero_serie, c.name category, validationDate, v.state, u.userName
        FROM validations v
        JOIN operators o ON v.operatorId = o.operatorId
        JOIN supplies s ON v.supplyId = s.supplyId
        JOIN categories c ON s.categoryId = c.categoryId
        JOIN users u ON v.userId = u.userId";

        $countQuery = "SELECT COUNT(validationId) validations
        FROM validations v
        JOIN operators o ON v.operatorId = o.operatorId
        JOIN supplies s ON v.supplyId = s.supplyId
        JOIN categories c ON s.categoryId = c.categoryId";

        if ($filters->search_value){
            $query      = $query." WHERE o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." WHERE o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";

            $query      = $query." OR o.apellido LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR o.apellido LIKE '%".htmlspecialchars($filters->search_value)."%' ";
            
            $query      = $query." OR o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' "; 

            $query      = $query." OR s.numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR s.numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' "; 
        }

        if(isset($filters->state))
        {
            {
                $query = $query." AND v.state = '".$filters->state."'";
                $countQuery = $countQuery." AND v.state = '".$filters->state."'";
            }
        }

        if ($filters->startDate && $filters->endDate){
            $mysqlStartDate = str_replace('/','-',$filters->startDate);
            $mysqlStartDate = date("Y-m-d", strtotime($mysqlStartDate));

            $mysqlEndDate = str_replace('/','-',$filters->endDate);
            $mysqlEndDate = date("Y-m-d", strtotime($mysqlEndDate));

            $query      = $query." AND v.validationDate >=  '".$mysqlStartDate."' AND v.validationDate <= '".$mysqlEndDate."'";
            $countQuery = $countQuery." AND v.validationDate >=  '".$mysqlStartDate."' AND v.validationDate <= '".$mysqlEndDate."'";  
        }

        $query      .= ' ORDER BY validationDate DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';
        $countQuery .= ' ORDER BY validationDate DESC  ';

        $supplies = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();

        $data = array();

        for ($i=0; $i < count($supplies); $i++) { 

            $nestedData = array();

            $nestedData[] = $supplies[$i]->completeName;
            $nestedData[] = $supplies[$i]->legajo;
            $nestedData[] = $supplies[$i]->numero_serie;
            $nestedData[] = $supplies[$i]->nombre;
            $nestedData[] = $supplies[$i]->category;
            $nestedData[] = $supplies[$i]->validationDate;
            $nestedData[] = $supplies[$i]->state;
            $nestedData[] = $supplies[$i]->userName;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->validations,
            "recordsFiltered"   => $countData->validations,
            "data"              => $data
        );

        echo json_encode($json_data); 
    }
}

/* End of file Validationhd_model.php */

?>