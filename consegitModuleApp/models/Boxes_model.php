<?php 
Class Boxes_model extends CI_Model {

	function GetBoxesData()
	{
		$sql = 'SELECT b.boxId,b.name as box_name, s.name as site_name
        FROM boxes b 
        INNER JOIN sites s ON s.siteId = b.siteId
        WHERE b.active = 1  AND s.active = 1';

        return $this->db->query($sql)->result();
    }
    
    function GetBoxData($boxId)
    {
        $sql = 'SELECT b.boxId,b.name as box_name, s.name as site_name
        FROM boxes b 
        INNER JOIN sites s ON s.siteId = b.siteId
        WHERE b.active = 1 AND b.boxId = ? AND s.active = 1';

        return $this->db->query($sql,$boxId)->row();
    }

    function GetBoxesBySite($siteId)
    {
        $sql = 'SELECT b.boxId,b.name as box_name
        FROM boxes b 
        INNER JOIN sites s ON s.siteId = b.siteId
        WHERE s.siteid = ? AND b.active = 1  AND s.active = 1';

        return $this->db->query($sql,$siteId)->result();
    }

	function Create($box)
    {
        if(isset($box))
        {
            $this->db->insert('boxes', $box);
            return "success";
        }
        return "error";
    }

    function Edit($boxId,$box)
    {
        if(isset($box))
        {
            $this->db->where('boxId', $boxId);
            $this->db->update('boxes',$box);
            return "success";
        }
        return "error";
    }

    public function Delete($box)
    {
        if(isset($box))
        {
            $this->db->where('boxId', $box['boxId']);
            $this->db->update('boxes', array('active' => 0));
            return "success";
        }
        return "error";
    }















}
