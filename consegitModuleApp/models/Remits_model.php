<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Remits_model extends CI_Model {
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Shipments_model','Shipments');
        }

        public function getRemits()
        {
            $query = "SELECT r.remitId, r.deliveryDate, r.name, d.name delivery
            FROM remits r
            JOIN deliveries d
            ON r.deliveryId = d.deliveryId
            ORDER BY r.remitId DESC";
            return $this->db->query($query)->result();
        }

        public function Create($remit,$shipments)
        {
            if(isset($remit))
            {
                $this->db->insert('remits', $remit);
                $remitId = $this->db->insert_id();
                
                for ($i=0; $i < count($shipments); $i++) { 
                    
                    $objectInsert = array(
                        'remitId'       => $remitId,
                        'shipmentId'    => $shipments[$i]['shipmentId']
                    );
                    if(array_key_exists('observation',$shipments[$i]))
                    {
                        $objectInsert['observation'] = $shipments[$i]['observation'];
                    }
                    $this->db->insert('shipmentsinremit', $objectInsert);
                    
                }
                return "success";
            }
            return "error";
        }

        public function GetRemitById($remitId)
        {
            $res = new StdClass();
            $query = "SELECT r.remitId, DATE_FORMAT(r.deliveryDate,'%d-%c') deliveryDate, r.name, d.name delivery, DATE_FORMAT(r.extensionDate,'%d/%c/%Y') extensionDate
            FROM remits r
            JOIN deliveries d
            ON r.deliveryId = d.deliveryId
            WHERE remitId = ?";
            $res->remit = $this->db->query($query,$remitId)->row();

            //buscar los operadores con envíos, que esos envíos esten en shipment in remit
            $query = "SELECT o.operatorId, o.dni, CONCAT(o.nombre,' ',o.apellido) completeName, CONCAT(calle,' ',altura,' ',localidad,' ',codigo_postal,' ',provincia,' ',piso_depto) domicilio, celular, o.empresa
            FROM operators o
            JOIN shipments s
            ON o.operatorId = s.operatorId
            LEFT JOIN shipmentsinremit sr
            ON s.shipmentId = sr.shipmentId
            WHERE o.active = 1
            AND sr.remitId = ?
            GROUP BY o.operatorId,s.shipmentId";
            $res->operators = $this->db->query($query,$res->remit->remitId)->result();

            for ($i=0; $i < count($res->operators); $i++) { 
                $operator = $res->operators[$i];

                $query = "SELECT s.shipmentId, s.supplies, s.type, sr.observation
                FROM shipments s
                JOIN shipmentsinremit sr
                ON s.shipmentId = sr.shipmentId
                WHERE operatorId = ?";
                $operator->shipments = $this->db->query($query,$operator->operatorId)->result();

                for ($j=0; $j < count($operator->shipments); $j++) { 
                    $shipment = $operator->shipments[$j];

                    $supplies = $this->Shipments->obtainSuppliesFromText($shipment->supplies);
                    $shipment->supplies = array();
                    for ($k=0; $k < count($supplies)-1 ; $k++) {
                        $query = "SELECT s.supplyId, c.name category, numero_serie, s.nombre
                        FROM supplies s 
                        JOIN categories c
                        ON s.categoryId = c.categoryId
                        WHERE supplyId = ?";
                        $supply = $this->db->query($query,$supplies[$k])->row();
                        $shipment->supplies[] = $supply;
                    }
                }
            }
            return $res;
        }
    }
?>