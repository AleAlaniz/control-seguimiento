<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aperturesite_model extends CI_Model {
    
    function GetFiltersData($filterName)
    {
        $query = "SELECT DISTINCT $filterName as keyDb FROM operators where $filterName <> ''";
        return $this->db->query($query)->result();
    }

    function AssignPosition($object)
    {
        $this->db->insert('assignedpositions', $object);
        return "success";
    }

}

/* End of file Aperturesite_model.php */
?>