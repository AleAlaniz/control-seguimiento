<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipments_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Supplies_model','Supplies');
        $this->load->model('Operators_model','Operators');
        $this->load->model('Shipments_model','Shipments');
    }
    
    function GetShipments()
    {
        $query = "SELECT shipmentId, shipmentDate, deliveryDate, supplies, type, state, s.operatorId, CONCAT(o.nombre,' ',o.apellido) completeName
        FROM shipments s
        JOIN operators o ON s.operatorId = o.operatorId 
        WHERE s.active = 1";

        $shipments = $this->db->query($query)->result();
        return $shipments;
    }

    function GetShipmentById($id)
    {
        $query = "SELECT shipmentId, shipmentDate, deliveryDate, supplies, type, state, s.operatorId, CONCAT(o.nombre,' ',o.apellido) completeName, boxId, refundReasonId
        FROM shipments s
        JOIN operators o ON s.operatorId = o.operatorId
        WHERE s.shipmentId = ? AND s.active = 1";

        $shipment = $this->db->query($query,$id)->row();
        $supplies = $this->obtainSuppliesFromText($shipment->supplies);

        $shipment->supplies = array();
        for ($i=0; $i < count($supplies)-1; $i++) { 
            $query = "SELECT s.supplyId, s.nombre, s.numero_serie, ss.name site, b.name box, c.name category
            FROM supplies s
            LEFT JOIN assignedsupplies a ON s.supplyId = a.supplyId
            JOIN categories c ON s.categoryId = c.categoryId
            LEFT JOIN boxes b ON s.boxId = b.boxId
            LEFT JOIN sites ss ON ss.siteId = b.siteId
            WHERE s.supplyId = ?";
            $shipment->supplies[] = $this->db->query($query,$supplies[$i])->row();
        }

        return $shipment;
    }

    public function Exists($id)
    {
        $query = "SELECT shipmentId, supplies, operatorId, dateFile
        FROM shipments s
        WHERE s.active = 1 AND s.shipmentId = ?";

        return $this->db->query($query,$id)->row();
    }

    public function searchShipments($filters)
    {
        $query = "SELECT shipmentId, dateFile, dateCheck, shipmentDate, deliveryDate, type, state, o.operatorId, CONCAT(o.nombre,' ',o.apellido) completeName
        FROM shipments s
        JOIN operators o ON s.operatorId = o.operatorId 
        WHERE s.active = 1";

        $countQuery = "SELECT COUNT(shipmentId) as envios
        FROM shipments s
        JOIN operators o ON s.operatorId = o.operatorId 
        WHERE s.active = 1";

        if(isset($filters->state))
        {
            {
                $query = $query." AND state = '".$filters->state."'";
                $countQuery = $countQuery." AND state = '".$filters->state."'";
            }
        }

        if ($filters->startDate && $filters->endDate){
            $mysqlStartDate = str_replace('/','-',$filters->startDate);
            $mysqlStartDate = date("Y-m-d", strtotime($mysqlStartDate));

            $mysqlEndDate = str_replace('/','-',$filters->endDate);
            $mysqlEndDate = date("Y-m-d", strtotime($mysqlEndDate));

            $query      = $query." AND s.shipmentDate >=  '".$mysqlStartDate."' AND s.shipmentDate <= '".$mysqlEndDate."'";
            $countQuery = $countQuery." AND s.shipmentDate >=  '".$mysqlStartDate."' AND s.shipmentDate <= '".$mysqlEndDate."'";  
        }

        if ($filters->search_value){

            $query      = $query." AND concat_ws(o.nombre,' ',o.apellido) LIKE '%".htmlspecialchars($filters->search_value)."%'";  
            $countQuery = $countQuery." AND concat_ws(o.nombre,' ',o.apellido) LIKE '%".htmlspecialchars($filters->search_value)."%'";  

        }
        $query .= ' ORDER BY shipmentId DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';
        $countQuery .= ' ORDER BY shipmentId DESC  ';

        $shipments = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        $data = array();

        for ($i=0; $i < count($shipments); $i++) { 
            $nestedData = array();
            
            $to = new DateTime($shipments[$i]->dateFile);
            if(isset($shipments[$i]->dateFile))
            {
                if(isset($shipments[$i]->deliveryDate))
                {
                    $from = new DateTime($shipments[$i]->deliveryDate);
                }
                else{
                    
                    $from = new DateTime();
                    $from->sub(new DateInterval("PT3H"));
                }
                $stat = $to->diff($from); // DateInterval object
                $hours   = $stat->days * 24 + $stat->h;
                $minutes = $stat->i;
                $actualDate = $hours."h ".$minutes."m";
            }
            else{
                $actualDate = 0;
            }

            $nestedData[] = $shipments[$i]->completeName;
            $nestedData[] = $shipments[$i]->shipmentId;
            $nestedData[] = $shipments[$i]->dateFile;
            $nestedData[] = $shipments[$i]->dateCheck;
            $nestedData[] = $shipments[$i]->shipmentDate;
            $nestedData[] = $shipments[$i]->deliveryDate;
            $nestedData[] = $actualDate;
            $nestedData[] = $shipments[$i]->type;
            $nestedData[] = $shipments[$i]->state;
            $nestedData[] = $shipments[$i]->operatorId;
            
            $data[] = $nestedData;

        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->envios,
            "recordsFiltered"   => $countData->envios,
            "data"              => $data
        );
        echo json_encode($json_data);

    }
    
    function Create($shipment)
    {
        if(isset($shipment))
        {
            $this->db->insert('shipments', $shipment);
            $shipmentId = $this->db->insert_id();
            insert_audit_logs("shipments","CREATE",$shipmentId);

            //envio de tipo entrega de insumos
            if($shipment['type'] == getShipmentType()[0]){
                $supplies = $this->obtainSuppliesFromText($shipment['supplies']);
                for ($i=0; $i < count($supplies)-1; $i++) { 
                    $supplyAsigned = array(
                        'operatorId' => $shipment['operatorId'],
                        'supplyId'   => $supplies[$i],
                    );
                    insert_audit_logs("assignedsupplies","CREATE",$supplyAsigned);

                    $this->db->insert('assignedsupplies', $supplyAsigned);
                    
                    $this->db->select('*');
                    $oldSupply = $this->db->get_where('supplies',array('supplyId' => $supplies[$i]))->row();
                    insert_audit_logs("supplies","UPDATE",$oldSupply);

                    $this->db->set('stock',0);
                    $this->db->set('state',getStates()[3]);
                    $this->db->where('supplyId', $supplies[$i]);
                    $this->db->update('supplies');
                }
            }

            return "success";
        }
        return "error";
    }

    public function Edit($shipment,$state,$date,$operatorId)
    {
        $date = str_replace('/','-',$date);
        $date = date("Y-m-d H:i:s", strtotime($date));
        
        //si es de tipo envio de insumos
        if($shipment->type == getShipmentType()[0])
        {
            //si el estado es no entregado 
            if($state == getShipmentState()[2]){
                for ($i=0; $i < count($shipment->supplies); $i++) { 
                    $supply = $shipment->supplies[$i];
                    // borro los insumos asignadas
                    $assignedSupply = $this->db->get_where('assignedsupplies',array('supplyId' => $supply->supplyId))->row();
                    insert_audit_logs("assignedsupplies","DELETE",$assignedSupply);
                    $this->db->delete('assignedsupplies', array('supplyId' => $supply->supplyId));

                    $supplyUpdated = $this->db->get_where('supplies',array('supplyId' => $supply->supplyId))->row();
                    insert_audit_logs("supplies","UPDATE",$supplyUpdated);
                    
                    // actualizo el stock a 1
                    $this->db->set('stock',1);
                    $this->db->where('supplyId', $supply->supplyId);
                    $this->db->update('supplies');      
                    
                }
            }
            //en cambio si el estado es entregado
            else if($state == getShipmentState()[0]){
                insert_audit_logs("shipments","UPDATE",$shipment);

                $this->db->set('deliveryDate',$date);
                $this->db->where('shipmentId', $shipment->shipmentId);
                $this->db->update('shipments');

                for ($i=0; $i < count($shipment->supplies); $i++) { 
                    $supply = $shipment->supplies[$i];

                    $recordInsert = array(
                        'date'          => $date,
                        'type'          => $shipment->type,
                        'operatorId'    => $shipment->operatorId,
                        'supplyId'      => $supply->supplyId,
                    );
                    insert_audit_logs("suppliesrecords","CREATE",$recordInsert);
                    $this->db->insert('suppliesrecords', $recordInsert);
                }
            }
        }
        // en cambio si el tipo de envio es retiro de insumos
        else if($shipment->type == getShipmentType()[1]){
            
            // verifico si el estado es entregado
            if($state == getShipmentState()[0]){
                // $date = date("Y-m-d H:i:s");

                insert_audit_logs("shipments","UPDATE",$shipment);

                $this->db->set('deliveryDate',$date);
                $this->db->where('shipmentId', $shipment->shipmentId);
                $this->db->update('shipments');

                for ($i=0; $i < count($shipment->supplies); $i++) { 
                    $supply = $shipment->supplies[$i];
                    
                    // borro los insumos asignadas
                    $assignedSupply = $this->db->get_where('assignedsupplies',array('supplyId' => $supply->supplyId))->row();
                    insert_audit_logs("assignedsupplies","DELETE",$assignedSupply);
                    $this->db->delete('assignedsupplies', array('supplyId' => $supply->supplyId));

                    $updatedSupply = $this->db->get_where('supplies',array('supplyId' => $supply->supplyId))->row();
                    insert_audit_logs("supplies","UPDATE",$updatedSupply);                  
                    if($shipment->refundReasonId == 3){
                        $this->db->set('stock',0);
                        $this->db->set('state',getStates()[1]);
                    }
                    else{
                        // actualizo el stock a 1
                        $this->db->set('stock',1);
                    }
                    $this->db->where('supplyId', $supply->supplyId);
                    $this->db->update('supplies');      
                    
                    //inserto el historial de retiro de insumos
                    $recordInsert = array(
                        'date'          => $date,
                        'type'          => $shipment->type,
                        'boxId'         => $shipment->boxId,
                        'supplyId'      => $supply->supplyId,
                        'operatorId'    => $operatorId
                    );
                    insert_audit_logs("suppliesrecords","CREATE",$recordInsert);
                    $this->db->insert('suppliesrecords', $recordInsert);
                }
            }
        }
        insert_audit_logs("shipments","UPDATE",$shipment);

        $this->db->set('state',$state);
        $this->db->where('shipmentId', $shipment->shipmentId);
        $this->db->update('shipments');
        
        return "success";
    }

    public function Delete($shipment)
    {   
        if(isset($shipment))
        {
            $supplies = $this->obtainSuppliesFromText($shipment->supplies);
            for ($i=0; $i < count($supplies)-1; $i++) { 
                $supply = $supplies[$i];
                $supplyDeleted = $this->db->get_where('assignedsupplies',array('supplyId' => $supply))->row();
                insert_audit_logs("assignedsupplies","DELETE",$supplyDeleted);
                $this->db->delete('assignedsupplies', array('supplyId' => $supply));

                $this->db->set('stock', 1);
                $this->db->set('state', getStates()[0]);
                $this->db->where('supplyId', $supply);
                $this->db->update('supplies');


                $this->db->set('state', getSuppliesRequestState()[0]);
                $this->db->where('state', getSuppliesRequestState()[2]);
                $this->db->where('operatorId', $shipment->operatorId);
                $this->db->where('dateFile', $shipment->dateFile);
                $this->db->update('suppliesrequests');

            }
            $shipmentDeleted = $this->db->get_where('shipments',array('shipmentId' => $shipment->shipmentId))->row();
            insert_audit_logs("shipments","DELETE",$shipmentDeleted);
            
            $this->db->where('shipmentId', $shipment->shipmentId);
            $this->db->update('shipments', array('active' => 0));
            return "success";
        }
        return "error";
    }

    public function obtainSuppliesFromText($text)
    {
        return explode(',',$text);
    }

    
    public function GetShipmentsInTransitWithSupplies()
    {
        $res = new StdClass();
        $query = "SELECT shipmentId, DATE_FORMAT(shipmentDate,'%d-%m-%Y') as shipmentDate, deliveryDate, type, CONCAT(o.nombre,' ',o.apellido,'(',o.dni,')' ) completeName, CONCAT_WS(' ',o.localidad,o.calle,o.altura) address, o.codigo_postal
        FROM shipments s
        JOIN operators o ON s.operatorId = o.operatorId 
        WHERE s.active = 1
        AND state = ? 
        AND shipmentId NOT IN (SELECT shipmentId FROM shipmentsinremit)";

        $res->shipments = $this->db->query($query,getShipmentState()[1])->result();
        
        return $res;
    }

    // public function ProcessData()
    // {
    //     // SCRIPT QUE RELACIONA LOS INSUMOS CON LOS OPERADORES EN LOS ENVIOS.
    //     $query = "SELECT operatorId 
    //     FROM shipment_ex
    //     GROUP BY operatorId";
    //     $shipments = $this->db->query($query)->result();

    //     for ($i=0; $i < count($shipments); $i++) { 
    //         $shipment = $shipments[$i];
    //         $res = $this->Operators->GetOperatorByDni($shipment->operatorId);
    //         if(isset($res) > 0){
    //             $this->db->set('operatorId',$res->operatorId);
    //             $this->db->where('operatorId', $shipment->operatorId);
    //             $this->db->update('shipment_ex');
    //         }
    //     }
        
    //     //busco en la tabla externa
    //     $query = "SELECT * FROM shipment_ex";
    //     $shipments = $this->db->query($query)->result();

    //     for ($i=0; $i < count($shipments); $i++) { 
    //         $shipment = $shipments[$i];

    //         //por cada registro, busco el id de categoria
    //         $sql = "SELECT categoryId FROM categories WHERE name = ?";
    //         $category = $this->db->query($sql,$shipment->category)->row();

    //         //si no existe la catgoria la agrego y obtengo el ID
    //         if(is_null($category)){
    //             // $object = array(
    //             //     'name'  => $shipment->category
    //             // );
                
    //             // $this->db->insert('categories', $object);
    //             // $categoryId = $this->db->insert_id();
    //         }
    //         else{
    //             $categoryId = $category->categoryId;
    //         }
    //         // $this->db->set('category', $category->categoryId);
    //         // $this->db->where('category', $shipment->category);
    //         // $this->db->update('shipment_ex');

    //         $query = "SELECT supplyId FROM supplies WHERE numero_serie = ? AND categoryId = ?";
    //         $res = $this->db->query($query,array($shipment->supplies,$categoryId))->row();
    //         if(!isset($res))
    //         {
    //             // inserto un insumo con los datos obtenidos
    //             $supplyInsert = array(
    //                 'numero_serie' => $shipment->supplies." ".$shipment->category,
    //                 'categoryId'   => $categoryId,
    //                 'stock'        => 0,
    //                 'active'       => 1
    //             );
    //             print_r($supplyInsert);print_r("<br>");
    //             $this->db->insert('supplies', $supplyInsert);
    //             $supplyId = $this->db->insert_id();
    //         }
    //         else{
    //             $supplyId = $res->supplyId;
    //             print_r($supplyId);print_r("<br>");
    //         }

    //         $record = array(
    //             'date'          => $shipment->shipmentDate,
    //             'supplyId'      => $supplyId,
    //             'type'          => 'Envío de insumos',
    //             'operatorId'    => $shipment->operatorId
    //         );
    //         $this->db->insert('suppliesrecords', $record);

    //         $object = array(
    //             'operatorId'    => $shipment->operatorId,
    //             'supplyId'      => $supplyId,
    //         );
    //         $this->db->insert('assignedsupplies', $object);

    //         $object = array(
    //             'shipmentDate'  => $shipment->shipmentDate, 
    //             'deliveryDate'  => $shipment->deliveryDate, 
    //             'supplies'      => $supplyId, 
    //             'type'          => 'Envío de insumos',
    //             'state'         => 'Entregado',
    //             'observation'   => '',
    //             'operatorId'    => $shipment->operatorId
    //         );
            
    //         $this->db->insert('shipments', $object);
    //     }
    //     return "success";

    //     //script que cambia el box por el boxId en los insumos (en caso de que manden texto)
    //     // $query = "SELECT boxId FROM supplies";
    //     // $supplies = $this->db->query($query)->result();
    //     // for ($i=0; $i < count($supplies); $i++) { 
    //     //     $supply = $supplies[$i];
    //     //     $query2 = "SELECT boxId FROM boxes WHERE name = ? LIMIT 1";
    //     //     $box = $this->db->query($query2,$supply->boxId)->row();
    //     //     if(isset($box))
    //     //     {
    //     //         $query3 = "UPDATE supplies SET boxId = ? WHERE boxId = ?";
    //     //         echo $this->db->query($query3,array($box->boxId,$supply->boxId));
    //     //     }
    //     // }

    //     // script que desglosar el domicilio
    //     // $query = "SELECT operatorId, domicilio FROM operators";
    //     // $res = $this->db->query($query)->result();
    //     // for ($i=0; $i < count($res); $i++) { 
    //     //     $element = $res[$i];
    //     //     $domicilio = explode('/',$element->domicilio);

    //     //     $calle = explode(' ',$domicilio[0]);
    //     //     if(array_key_exists(1,$calle)){
    //     //         $altura = $calle[1];
    //     //         $domicilio[0] = str_replace($altura,' ',$domicilio[0]);
    //     //     }
    //     //     else{
    //     //         $altura = '';
    //     //     }

    //     //     $this->db->set('calle',array_key_exists(0,$domicilio)? $domicilio[0] : '');
    //     //     $this->db->set('altura',$altura);
    //     //     $this->db->set('localidad',array_key_exists(1,$domicilio)? $domicilio[1] : '');
    //     //     $this->db->set('codigo_postal',array_key_exists(2,$domicilio)? $domicilio[2] : '');
    //     //     $this->db->set('provincia',array_key_exists(3,$domicilio)? $domicilio[3] : '');
    //     //     $this->db->where('operatorId', $element->operatorId);
    //     //     $this->db->update('operators');
    //     // }
    // }

    // public function FixShipment()
    // {
    //     $query = "SELECT shipmentId, shipmentDate, supplies, type, operatorId  FROM `shipments` WHERE state = 'En tránsito'";
    //     $shipments = $this->db->query($query)->result();
    //     for ($i=0; $i < count($shipments); $i++) { 
    //         $shipment = $shipments[$i];

    //         $this->db->set('deliveryDate',$shipment->shipmentDate);
    //         $this->db->set('state','Entregado');
    //         $this->db->where('shipmentId', $shipment->shipmentId);
    //         $this->db->update('shipments');
            
    //         $supplies = explode(',',$shipment->supplies);

    //         for ($j=0; $j < count($supplies)-1; $j++) { 
    //             $supply = $supplies[$j];
    
    //             $recordInsert = array(
    //                 'date'          => $shipment->shipmentDate,
    //                 'type'          => $shipment->type,
    //                 'operatorId'    => $shipment->operatorId,
    //                 'supplyId'      => $supply
    //             );
    //             $this->db->insert('suppliesrecords', $recordInsert);
    //             print_r("records:");print_r($recordInsert);print_r("<br>");
    //         }
    //         print_r("envios:");print_r($shipment); print_r("<br>");
    //     }
    //     return "success";
    // }

}

/* End of file Shipment_model.php */
