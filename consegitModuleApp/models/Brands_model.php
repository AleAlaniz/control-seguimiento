<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function getBrands()
    {
        $query = "SELECT name FROM brands";
        $result = $this->db->query($query)->result();
        return $result;
    }
    
}

?>