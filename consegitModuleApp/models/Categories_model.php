<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Categories_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getCategories()
        {
            $sql = "SELECT categoryId,name FROM categories";
            $categories = $this->db->query($sql)->result();

            return $categories;
        }

        public function getCategory($categoryId)
        {
            $sql = "SELECT * FROM categories WHERE categoryId = ?";
            $res = $this->db->query($sql,$categoryId)->row();
            return $res;
        }
        
        function Create($name)
        {
            if(isset($name))
            {
                $this->db->insert('categories', $name);
                return "success";
            }
            return "error";
        }

        public function Edit($category)
        {
            if(isset($category))
            {
                $this->db->where('categoryId', $category['categoryId']);
                $this->db->update('categories',$category);
                return "success";
            }
            return "error";
        }

        public function Delete($category)
        {
            if(isset($category))
            {
                $this->db->where('categoryId', $category['categoryId']);
                $this->db->delete('categories', $category);
                return "success";
            }
            return "error";
        }

    }

    /* End of file TeamLeaders_model.php */
?>