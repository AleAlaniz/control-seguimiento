<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Owners_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getOwners()
        {
            $sql = "SELECT ownerId,name FROM owners WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getOwner($ownerId)
        {
            $sql = "SELECT * FROM owners WHERE ownerId = ? AND active = 1";
            $res = $this->db->query($sql,$ownerId)->row();
            return $res;
        }
        
        function Create($owner)
        {
            if(isset($owner))
            {
                $this->db->insert('owners', $owner);
                return "success";
            }
            return "error";
        }

        public function Edit($owner)
        {
            if(isset($owner))
            {
                $this->db->where('ownerId', $owner['ownerId']);
                $this->db->update('owners',$owner);
                return "success";
            }
            return "error";
        }

        public function Delete($owner)
        {
            if(isset($owner))
            {
                $this->db->where('ownerId', $owner['ownerId']);
                $this->db->update('owners', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file owners_model.php */
?>