<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function GetUsers()
    {
        $sql = "SELECT u.userId,userName,password, u.name, lastName, turn, cuil, createDate, cuil, r.name role
        FROM users u
        LEFT JOIN roles r
        ON (u.roleId = r.roleId)
        WHERE u.active = 1";

        $users = $this->db->query($sql)->result();
        return $users;
    }

    public function Create($user)
    {
        if(isset($user))
        {
            $this->db->insert('users', $user);
            return $this->db->insert_id();
        }
        return null;
    }

    public function getFormData()
    {
        $data = new StdClass();
        
        $sql = "SELECT roleId,name FROM roles";
        $data->roles = $this->db->query($sql)->result();
        
        $data->turns = getTurns();

        return $data;
    }

    public function Edit($userId,$user)
    {
        if(isset($user))
        {
            $this->db->where('userId', $userId);
            $this->db->update('users',$user);
            return "success";
        }
        return "error";
    }

    public function EditPass($userId,$user)
    {
        if(isset($user))
        {
            $this->db->where('userId', $userId);
            $this->db->update('users',$user);
            return "success";
        }
        return "error";
    }

    public function GetUserData($userId)
    {
        $data = new StdClass();
        
        $sql = "SELECT u.userId, userName, name, lastName, cuil, roleId 
        FROM users u
        WHERE u.userId = ? and u.active = 1";
        $data->user = $this->db->query($sql,$userId)->row();

        $sql = "SELECT roleId,name FROM roles";
        $data->roles = $this->db->query($sql)->result();
        
        return $data;
    }

    public function Delete($user)
    {   
        if(isset($user))
        {
            $this->db->set('active', 0);
            $this->db->where('userId', $user['userId']);
            $this->db->update('users');

            return "success";
        }
        return "error";
    }

    public function GetDetailDeleteData($userId)
    {
        $sql = "SELECT u.userId,userName,password, u.name, lastName, turn, cuil, createDate, cuil, r.name role
        FROM users u
        LEFT JOIN roles r
        ON (u.roleId = r.roleId)
        WHERE u.userId = ? AND u.active = 1";
        $res = $this->db->query($sql,$userId)->row();

        return $res;
    }

    public function SearchUsers($searchValue)
    {
        $sql = 'SELECT userId, CONCAT(name, " ", lastName) as "completeName"
        FROM users
        WHERE LOWER(CONCAT(name, " ", lastName)) LIKE ? AND active = 1
        ORDER BY completeName
        LIMIT 5';

        return $this->db->query($sql, '%'.$searchValue.'%')->result();
    }
}

/* End of file Users_model.php */


?>