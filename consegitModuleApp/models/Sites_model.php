<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Sites_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getSites()
        {
            $sql = "SELECT siteId,name FROM sites WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getSite($siteId)
        {
            $sql = "SELECT * FROM sites WHERE siteId = ? AND active = 1";
            $res = $this->db->query($sql,$siteId)->row();
            return $res;
        }
        
        function Create($site)
        {
            if(isset($site))
            {
                $this->db->insert('sites', $site);
                return "success";
            }
            return "error";
        }

        public function Edit($site)
        {
            if(isset($site))
            {
                $this->db->where('siteId', $site['siteId']);
                $this->db->update('sites',$site);
                return "success";
            }
            return "error";
        }

        public function Delete($site)
        {
            if(isset($site))
            {
                $this->db->where('siteId', $site['siteId']);
                $this->db->update('sites', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file sites_model.php */
?>