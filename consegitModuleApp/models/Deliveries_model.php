<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliveries_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function getDeliveries()
    {
        $query = "SELECT deliveryId,name FROM deliveries";
        $result = $this->db->query($query)->result();
        return $result;
    }
    
}

?>