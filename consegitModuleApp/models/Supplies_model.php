<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplies_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    function GetSupplies()
    {
        $query = "SELECT supplyId, numero_serie as serial_number, ss.nombre as name, marca as brand, state, sophos_fecha, disco, memoria, procesador, id_teamviewer,imgs, c.categoryId, c.name category, s.siteId, s.name site, b.name box, o.name owner
        FROM supplies ss
        JOIN categories c ON ss.categoryId = c.categoryId
        LEFT JOIN boxes  b ON ss.boxId = b.boxId
        LEFT JOIN sites s ON b.siteId = s.siteId 
        LEFT JOIN owners o ON ss.ownerId = o.ownerId
        WHERE ss.active = 1";

        $supplies = $this->db->query($query)->result();
        return $supplies;
    }

    function GetSuppliesByCategory($categoryId)
    {
        $sql = 'SELECT s.supplyId,s.nombre,s.numero_serie,b.name box, c.name category
        FROM supplies s
        INNER JOIN categories c ON s.categoryId = c.categoryId
        LEFT JOIN boxes b ON s.boxId = b.boxId
        WHERE s.categoryId = ? AND stock = 1 AND s.active = 1  AND c.active = 1';
        return $this->db->query($sql,$categoryId)->result();
    }

    function GetSupplyById($id)
    {
        $sql = "SELECT supplyId, numero_serie, nombre, purchaseDate, marca, CONCAT_WS('-',areacode,number) phone, state, sophos_fecha, disco, memoria, procesador, id_teamviewer, imgs, stock, c.categoryId, c.name category, s.siteId, s.name site, b.name box, o.ownerId, o.name owner, p.providerId, p.name provider
        FROM supplies ss
        JOIN categories c ON ss.categoryId = c.categoryId
        LEFT JOIN boxes  b ON ss.boxId = b.boxId
        LEFT JOIN sites s ON b.siteId = s.siteId 
        LEFT JOIN owners o ON ss.ownerId = o.ownerId
        LEFT JOIN providers p ON ss.providerId = p.providerId
        WHERE ss.supplyId = ? AND ss.active = 1";
        
        $res = $this->db->query($sql,$id)->row();
        return $res;
    }

    function GetSuppliesWithFilters($filters)
    {

        $query = "SELECT supplyId, numero_serie as serial_number, ss.nombre as name, marca as brand, state, c.name category, s.name site, b.name box, o.name as owner
        FROM supplies ss
        LEFT JOIN categories c ON ss.categoryId = c.categoryId
        LEFT JOIN boxes  b ON ss.boxId = b.boxId
        LEFT JOIN sites s ON b.siteId = s.siteId 
        LEFT JOIN owners o ON ss.ownerId = o.ownerId
        WHERE ss.active = 1";

        $countQuery = "SELECT COUNT(ss.supplyId) AS insumos FROM supplies ss
        JOIN categories c ON ss.categoryId = c.categoryId
        LEFT JOIN boxes  b ON ss.boxId = b.boxId
        LEFT JOIN sites s ON b.siteId = s.siteId 
        LEFT JOIN owners o ON ss.ownerId = o.ownerId
        WHERE ss.active = 1";

        if ($filters->site){

            $query      = $query." AND b.siteId = ".htmlspecialchars($filters->site)." ";  
            $countQuery = $countQuery." AND b.siteId = ".htmlspecialchars($filters->site)." ";  
        }

        if ($filters->box){

            $query      = $query." AND b.boxId = ".htmlspecialchars($filters->box)." ";  
            $countQuery = $countQuery." AND b.boxId = ".htmlspecialchars($filters->box)." ";  
        }

        if ($filters->category){

            $query      = $query." AND c.categoryId = ".htmlspecialchars($filters->category)." ";  
            $countQuery = $countQuery." AND c.categoryId = ".htmlspecialchars($filters->category)." ";  
        }

        if ($filters->owner){

            $query      = $query." AND o.ownerId = ".htmlspecialchars($filters->owner)." ";  
            $countQuery = $countQuery." AND o.ownerId = ".htmlspecialchars($filters->owner)." ";  
        }

        if ($filters->state){

            $query      = $query." AND ss.state = '".htmlspecialchars($filters->state)."' ";  
            $countQuery = $countQuery." AND ss.state = '".htmlspecialchars($filters->state)."' ";  
        }

        if ($filters->search_value){

            $query      = $query." AND (numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." AND (numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' ";  

            $query      = $query." OR nombre LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
            $countQuery = $countQuery." OR nombre LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
        }        

        $query      .= ' ORDER BY supplyId DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';
        $countQuery .= ' ORDER BY supplyId DESC  ';

        $supplies = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();

        $data = array();
        
        for ($i=0; $i < count($supplies); $i++) { 

            $nestedData = array();

            $nestedData[] = $supplies[$i]->site;
            $nestedData[] = $supplies[$i]->box;
            $nestedData[] = $supplies[$i]->category;
            $nestedData[] = $supplies[$i]->brand;
            $nestedData[] = $supplies[$i]->serial_number;
            $nestedData[] = $supplies[$i]->name;
            $nestedData[] = $supplies[$i]->state;
            $nestedData[] = $supplies[$i]->supplyId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->insumos,
            "recordsFiltered"   => $countData->insumos,
            "data"              => $data
        );

        echo json_encode($json_data); 

    }
    
    function Create($supply)
    {
        if(isset($supply))
        {
            $this->db->insert('supplies', $supply);
            $supply['supplyId'] = $this->db->insert_id();
            insert_audit_logs('supplies','CREATE',$supply);
            return "success";
        }
        return "error";
    }

    public function Edit($supply)
    {
        if(isset($supply))
        {
            $query = "SELECT * FROM supplies WHERE supplyId = ?";
            $res = $this->db->query($query,$supply['supplyId'])->row();
            insert_audit_logs("supplies","EDIT",$res);

            $this->db->where('supplyId', $supply['supplyId']);
            $this->db->update('supplies',$supply);
            return "success";
        }
        return "error";
    }

    public function Delete($supply)
    {
        if(isset($supply))
        {

            $query = "SELECT * FROM supplies s LEFT JOIN assignedsupplies a ON s.supplyId = a.supplyId WHERE s.supplyId = ?";
            $res = $this->db->query($query,$supply)->row();
            insert_audit_logs("supplies","DELETE",$res);

            $this->db->where('supplyId', $supply);
            $this->db->update('supplies', array('active' => 0));
            
            $this->db->set('active', 0);
            $this->db->where('supplyId', $supply);
            $this->db->update('assignedsupplies');

            return "success";
        }
        return "error";
    }

    function getSuppliesByOperator($operatorId)
    {
        $sql = 'SELECT s.supplyId, s.nombre, s.numero_serie, ss.name site, b.name box, c.name category
        FROM supplies s
        LEFT JOIN assignedsupplies a ON s.supplyId = a.supplyId
        JOIN categories c ON s.categoryId = c.categoryId
        LEFT JOIN boxes b ON s.boxId = b.boxId
        LEFT JOIN sites ss ON ss.siteId = b.siteId
        WHERE a.operatorId = ? AND s.active = 1  AND a.active = 1';
        return $this->db->query($sql,$operatorId)->result();
    }

    public function GetSupplyRecord($supplyId)
    {
        $sql = "SELECT date, type, o.operatorId, CONCAT(o.nombre,' ',o.apellido,'(',o.dni,')') as completeName, b.boxId, b.name
        FROM suppliesrecords s
        LEFT JOIN operators o ON o.operatorId = s.operatorId
        LEFT JOIN boxes b ON b.boxId = s.boxId
        WHERE s.supplyId = ? ORDER BY date DESC";
        return $this->db->query($sql,$supplyId)->result();
    }
    
    public function GetSupplyByCustomId($supplyId)
    {
        $this->db->select('supplyId');
        $this->db->where('numero_serie', $supplyId);
        $this->db->or_where('nombre', $supplyId);
        return $this->db->get('supplies',1)->row();
    }

    public function GetColorCounters($categoryId=NULL)
    {
        $query = "SELECT (";
        $query.= "select count(a.supplyId) FROM assignedsupplies a JOIN supplies s ON a.supplyId = s.supplyId WHERE s.active = 1 ";
        if(!is_null($categoryId)){
            $query.= "AND categoryId = ?";
        }
        $query.= ") assigned,";

        $query.= "(select count(supplyId) from supplies where state = 'En reparación' AND stock = 0 AND supplyId NOT IN(select supplyId FROM assignedsupplies) AND active = 1 ";
        if(!is_null($categoryId)){
            $query.= "AND categoryId = ?";
        }
        $query.=") repair,";

        $query.="(select count(supplyId) from supplies where state = 'Disponible' AND stock = 1 AND supplyId NOT IN(select supplyId FROM assignedsupplies) AND active = 1 ";
        if(!is_null($categoryId)){
            $query.="AND categoryId = ?";
        }
        $query.=") available,";

        $query.="(select count(supplyId) from supplies where state = 'Roto' AND stock = 0 AND supplyId NOT IN(select supplyId FROM assignedsupplies) AND active = 1 ";
        if(!is_null($categoryId)){
            $query.="AND categoryId = ?";
        }
        $query.=") broken";
        if(is_null($categoryId)){
            $counters = $this->db->query($query)->row();
        }
        else{
            $counters = $this->db->query($query,array($categoryId,$categoryId,$categoryId,$categoryId))->row();
        }
        return $counters;
    }

    public function GetGeneralCountersBsAs()
    {
        $data = new StdClass();

        // insumos afuera bs as
        $query = $this->CounterInOutSuppliesByCategory("OUT",1);
        $data->Out = $this->db->query($query)->row();

        //insumos en bs as
        $query = $this->CounterInOutSuppliesByCategory("IN",1);
        $data->In = $this->db->query($query)->row();
        
        //cantidad de insumos por campaña por cada categoría
        $total = array();
        $customers = $this->db->get('customers')->result();
        $categories = $this->db->get('categories')->result();

        foreach ($customers as $c ) {
            $query = "SELECT
            ? as customerId,
            ? as name,";
            foreach ($categories as $category ) {
                $query.= "sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
            }
            $query = substr($query,0,-1);
            $query.=" FROM operators ope ,assignedsupplies asup, categories c, supplies_BA slp
            WHERE ope.operatorId = asup.operatorId
            AND slp.categoryId = c.categoryId
            AND slp.supplyId = asup.supplyId
            AND slp.active = 1
            AND asup.active = 1
            AND ope.customerId = ? 
            GROUP BY ";
            foreach ($categories as $category ) {
                $query.= "'$category->name'";
            }
            $counter = $this->db->query($query,array($c->customerId,$c->name,$c->customerId))->row();
            if(isset($counter))
            {
                //esto hay que sacarlo luego
                $counterArray  = (array)$counter;
                foreach ($counterArray as $key => $value) {
                    if(array_key_exists($key,$total)) {
                        $total[$key] += intval($value);
                    }
                    else{
                        $total[$key] = 0;
                        $total[$key] += intval($value);
                    }
                }
                //hasta aca

                $data->customerCounters[] = $counter;
            }
            else{
                $arr = array(
                    'customerId'=>$c->customerId,
                    'name'=>$c->name
                );
                
                foreach ($categories as $category ) {
                    $arr[$category->name] = 0;
                }
                $arr = (object)$arr;
                $data->customerCounters[] = $arr;
            }
        }
        
        //SE AGREGA LA FILA SIN ASIGNAR (CUSTOMERID IS NOT NULL)
        //esto debe sacarse
        $noAssigned = array(
            'customerId' => 0,
            'name'       => 'SIN ASIGNAR'
        );
        $Out = (array)$data->Out;
        foreach ($categories as $category) {
            if(array_key_exists($category->name,$noAssigned)){
                $noAssigned[$category->name] = $Out[$category->name] - $total[$category->name];
            }
            else{
                $noAssigned[$category->name] = 0;
                $noAssigned[$category->name] = $Out[$category->name] - $total[$category->name];
            }
        }
        $noAssigned = (object)$noAssigned;
        $data->customerCounters[] = $noAssigned;
        //hasta aca

        //esto hay que ponerlo de nuevo mejorando la query
        // $query = "SELECT
        // 0 as customerId,
        // 'SIN ASIGNAR' as name,";
        // foreach ($categories as $category ) {
        //     $query.= "sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
        // }
        // $query = substr($query,0,-1);
        // $query.=" FROM operators ope ,assignedsupplies asup, categories c, supplies_BA slp
        // WHERE ope.operatorId = asup.operatorId
        // AND slp.categoryId = c.categoryId
        // AND slp.supplyId = asup.supplyId
        // AND slp.active = 1
        // AND asup.active = 1
        // AND ope.customerId IS NULL 
        // GROUP BY ";
        // foreach ($categories as $category ) {
        //     $query.= "'$category->name'";
        // }
        
        // $counter = $this->db->query($query)->row();
        // if(isset($counter))
        // {
        //     $data->customerCounters[] = $counter;
        // }
        // else{
        //     $arr = array(
        //         'name'=>"SIN ASIGNAR"
        //     );
            
        //     foreach ($categories as $category ) {
        //         $arr[$category->name] = 0;
        //     }
        //     $arr = (object)$arr;
        //     $data->customerCounters[] = $arr;
        // }
        // hasta aca

        return $data;
    }

    public function GetGeneralCountersLP()
    {
        $data = new StdClass();

        // insumos afuera La punta
        $query = $this->CounterInOutSuppliesByCategory("OUT",13);
        $data->Out = $this->db->query($query)->row();

        //insumos en La Punta
        $query = $this->CounterInOutSuppliesByCategory("IN",13);
        $data->In = $this->db->query($query)->row();

        //cantidad de insumos por campaña por cada categoría
        $customers = $this->db->get('customers')->result();
        foreach ($customers as $c ) {
            $categories = $this->db->get('categories')->result();
            $query = "SELECT
            ? as customerId,
            ? as name,";
            foreach ($categories as $category ) {
                $query.= "sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
            }
            $query = substr($query,0,-1);
            $query.=" FROM operators ope ,assignedsupplies asup, categories c, supplies_LP slp
            WHERE ope.operatorId = asup.operatorId
            AND slp.categoryId = c.categoryId
            AND slp.supplyId = asup.supplyId
            AND slp.active = 1
            AND asup.active = 1
            AND ope.customerId = ? 
            GROUP BY ";
            foreach ($categories as $category ) {
                $query.= "'$category->name'";
            }
            $counter = $this->db->query($query,array($c->customerId,$c->name,$c->customerId))->row();
            if(isset($counter))
            {
                $data->customerCounters[] = $counter;
            }
            else{
                $arr = array(
                    'customerId'=>$c->customerId,
                    'name'=>$c->name
                );
                
                foreach ($categories as $category ) {
                    $arr[$category->name] = 0;
                }
                $arr = (object)$arr;
                $data->customerCounters[] = $arr;
            }
        }

        //SE AGREGA LA FILA SIN ASIGNAR (CUSTOMERID IS NOT NULL)
        $query = "SELECT
        0 as customerId,
        'SIN ASIGNAR' as name,";
        foreach ($categories as $category ) {
            $query.= "sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
        }
        $query = substr($query,0,-1);
        $query.=" FROM operators ope ,assignedsupplies asup, categories c, supplies_LP slp
        WHERE ope.operatorId = asup.operatorId
        AND slp.categoryId = c.categoryId
        AND slp.supplyId = asup.supplyId
        AND slp.active = 1
        AND asup.active = 1
        AND ope.customerId IS NULL 
        GROUP BY ";
        foreach ($categories as $category ) {
            $query.= "'$category->name'";
        }
        
        $counter = $this->db->query($query)->row();
        if(isset($counter))
        {
            $data->customerCounters[] = $counter;
        }
        else{
            $arr = array(
                'name'=>"SIN ASIGNAR"
            );
            
            foreach ($categories as $category ) {
                $arr[$category->name] = 0;
            }
            $arr = (object)$arr;
            $data->customerCounters[] = $arr;
        }
        return $data;
    }

    function CounterInOutSuppliesByCategory($type,$site)
    {
        $categories = $this->db->get('categories')->result();
        $query = "";

        //insumos adentro
        if($type == "IN"){
            $query = "SELECT";

            //SAN LUIS
            if($site == 13)
            {
                // $query.= "(SELECT count(slp.supplyId) FROM supplies_LP slp LEFT JOIN assignedsupplies asup ON slp.supplyId = asup.supplyId WHERE assignedId IS NULL AND slp.active = 1 AND slp.state <> 'Roto' AND slp.state <> 'En reparación' GROUP BY slp.categoryId HAVING categoryId = $category->categoryId )'$category->name', ";
                foreach ($categories as $category ) {
                    $query.= " sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
                }
                $query = substr($query,0,-1);
                $query.= " FROM supplies_LP slp, categories c
                WHERE slp.categoryId = c.categoryId
                AND slp.state = 'Disponible'
                AND slp.active = 1
                GROUP BY ";
                foreach ($categories as $category ) {
                    $query.= "'$category->name'";
                }
            }

            // BSAS
            else
            {
                // $query.= "(SELECT count(slp.supplyId) FROM supplies_BA slp LEFT JOIN assignedsupplies asup ON slp.supplyId = asup.supplyId WHERE assignedId IS NULL AND slp.active = 1 AND slp.state <> 'Roto' AND slp.state <> 'En reparación' GROUP BY slp.categoryId HAVING categoryId = $category->categoryId )'$category->name', ";
                foreach ($categories as $category ) {
                    $query.= " sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
                }
                $query = substr($query,0,-1);
                $query.= " FROM supplies_BA slp, categories c
                WHERE slp.categoryId = c.categoryId
                AND slp.state = 'Disponible'
                AND slp.active = 1
                GROUP BY ";
                foreach ($categories as $category ) {
                    $query.= "'$category->name'";
                }
                
            }
        }

        //insumos fuera
        else
        { 
            $query = "SELECT";
            //INSUMOS FUERA DE SAN LUIS

            if($site == 13){
                foreach ($categories as $category ) {
                    $query.= " sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
                }
                $query = substr($query,0,-1);
                $query.= " FROM supplies_LP slp,assignedsupplies asup, categories c
                WHERE slp.supplyId = asup.supplyId
                AND slp.categoryId = c.categoryId
                AND assignedId IS NOT NULL
                AND slp.active = 1
                AND asup.active = 1
                GROUP BY ";
                foreach ($categories as $category ) {
                    $query.= "'$category->name'";
                }
            }

            //INSUMOS FUERA DE BUENOS AIRES
            else{
                foreach ($categories as $category ) {
                    $query.= " sum(CASE WHEN c.categoryId = $category->categoryId THEN 1 ELSE 0 END) AS '$category->name',";
                }
                $query = substr($query,0,-1);
                $query.= " FROM supplies_BA slp,assignedsupplies asup, categories c
                WHERE slp.supplyId = asup.supplyId
                AND slp.categoryId = c.categoryId
                AND assignedId IS NOT NULL
                AND slp.active = 1
                AND asup.active = 1
                GROUP BY ";
                foreach ($categories as $category ) {
                    $query.= "'$category->name'";
                }
                
            }
        }
        return $query;
    }

    public function GetSuppliesForValidation($filters)
    {
        $state = getShipmentState()[0];
        $type  = getShipmentType()[1];
        
        $query = "SELECT s.supplyId, s.nombre, s.numero_serie, c.name category, CONCAT(o.nombre,' ', o.apellido) completeName, o.legajo, o.operatorId, sh.shipmentId
        FROM supplies s
        JOIN suppliesrecords sr ON s.supplyId = sr.supplyId
        JOIN categories c ON s.categoryId = c.categoryId
        JOIN shipments sh ON sh.operatorId = sr.operatorId
        JOIN operators o ON sh.operatorId = o.operatorId
        WHERE sh.refundreasonId = 3
        AND CONCAT_WS(' ',o.operatorId,s.supplyId) NOT IN (SELECT CONCAT_WS(' ',operatorid,supplyId) FROM validations)
        AND sh.state = '$state'
        AND sr.type = '$type'";

        $countQuery = "SELECT COUNT(s.supplyId) as insumos
        FROM supplies s
        JOIN suppliesrecords sr ON s.supplyId = sr.supplyId
        JOIN categories c ON s.categoryId = c.categoryId
        JOIN shipments sh ON sh.operatorId = sr.operatorId
        JOIN operators o ON sh.operatorId = o.operatorId
        WHERE sh.refundreasonId = 3
        AND CONCAT_WS(' ',o.operatorId,s.supplyId) NOT IN (SELECT CONCAT_WS(' ',operatorid,supplyId) FROM validations)
        AND sh.state = '$state'
        AND sr.type = '$type'";

        if ($filters->search_value){

            $query      = $query." AND (numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." AND (numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' ";  

            $query      = $query." OR o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR o.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            
            $query      = $query." OR s.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR s.nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  

            $query      = $query." OR s.numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
            $countQuery = $countQuery." OR s.numero_serie LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
        }        

        $query      .= ' ORDER BY o.operatorId DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';
        $countQuery .= ' ORDER BY o.operatorId DESC  ';

        $supplies = $this->db->query($query,getShipmentState()[0])->result();
        $countData = $this->db->query($countQuery,getShipmentState()[0])->row();

        $data = array();

        for ($i=0; $i < count($supplies); $i++) { 

            $nestedData = array();

            $nestedData[] = $supplies[$i]->completeName;
            $nestedData[] = $supplies[$i]->legajo;
            $nestedData[] = $supplies[$i]->numero_serie;
            $nestedData[] = $supplies[$i]->nombre;
            $nestedData[] = $supplies[$i]->category;
            // $nestedData[] = $supplies[$i]->state;
            // $nestedData[] = $supplies[$i]->refundreasonId;
            $nestedData[] = $supplies[$i]->operatorId;
            $nestedData[] = $supplies[$i]->supplyId;
            $nestedData[] = $supplies[$i]->shipmentId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->insumos,
            "recordsFiltered"   => $countData->insumos,
            "data"              => $data
        );

        echo json_encode($json_data); 
    }
}

/* End of file Supplies_model.php */
