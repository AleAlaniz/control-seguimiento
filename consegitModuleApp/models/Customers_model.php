<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Customers_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getCustomers()
        {
            $sql = "SELECT customerId,name FROM customers WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getCustomer($customerId)
        {
            $sql = "SELECT * FROM customers WHERE customerId = ? AND active = 1";
            $res = $this->db->query($sql,$customerId)->row();
            return $res;
        }
        
        function Create($customer)
        {
            if(isset($customer))
            {
                $this->db->insert('customers', $customer);
                return "success";
            }
            return "error";
        }

        public function Edit($customer)
        {
            if(isset($customer))
            {
                $this->db->where('customerId', $customer['customerId']);
                $this->db->update('customers',$customer);
                return "success";
            }
            return "error";
        }

        public function Delete($customer)
        {
            if(isset($customer))
            {
                $this->db->where('customerId', $customer['customerId']);
                $this->db->update('customers', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file customers_model.php */
?>