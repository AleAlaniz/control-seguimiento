<?php 
Class Identity_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}
	
	public function Autenticate()
	{
		if($this->input->post('userName') && $this->input->post('password')){
			$sql = "SELECT userId, name, lastName, roleId, password FROM users WHERE userName = ? AND active = 1 LIMIT 1";
			$result = $this->db->query($sql,$this->input->post('userName'))->row();
			
			// $hashed = crypt($this->input->post('password'),CRYPT_EXT_DES);
			if(isset($result) && (password_verify($this->input->post('password'),$result->password)))
			{
				$sessionData = array(
					'Loged'       => TRUE, 
					'UserId'      => $result->userId,
					'fullName'    => $result->name.' '.$result->lastName,
					'RoleId'      => $result->roleId
				);

				$this->session->set_userdata($sessionData);
				return 'success';
			}
			else
			{
				return $this->lang->line("login_error_message");
			}
		}
		else{
			show_404();
		}
	}

	public function Validate($permissionName)
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			$validateRole = FALSE;

			$sql = 'SELECT u.userId
			FROM (SELECT userId, roleId FROM users WHERE userId = ? AND active = 1) u JOIN roles r
			ON(u.roleId = r.roleId)
			JOIN rolepermissions rp
			ON(r.roleId = rp.roleId)
			JOIN permissions p
			ON(rp.permissionId = p.permissionId)
			WHERE p.name = ?';

			$validateRole = $this->db->query($sql, array($this->session->UserId, $permissionName))->row();
			return isset($validateRole);
		}
		else
		{
			return FALSE;
		}
	}

	public function ValidateOne($permissionName)
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			$validateRole = FALSE;

			$sql = 'SELECT u.userId
			FROM (SELECT userId, roleId FROM users WHERE userId = ? AND active = 1) u JOIN roles r
			ON(u.roleId = r.roleId)
			JOIN rolepermissions rp
			ON(r.roleId = rp.roleId)
			JOIN permissions p
			ON(rp.permissionId = p.permissionId)
			WHERE p.name = ?
			AND r.roleId NOT IN (SELECT roleId FROM rolepermissions where roleId = ? AND permissionId <> p.permissionId)';

			$validateRole = $this->db->query($sql, array($this->session->UserId, $permissionName,$this->session->RoleId))->row();
			return isset($validateRole);
		}
		else
		{
			return FALSE;
		}
	}

	public function Logout()
	{
		$this->session->sess_destroy();
		header('Location:/'.FOLDERADD.'/');
	}

	
}