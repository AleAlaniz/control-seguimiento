<?php 
Class Role_model extends CI_Model
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function getRoles()
    {
        $sql = "SELECT * FROM roles";
    	return $this->db->query($sql)->result();
    }

    public function getRole($roleId)
    {

    	$res = new StdClass();
    	$sql = 
    	"SELECT roles.roleId,roles.name
		FROM roles 
        WHERE roles.roleId = ?
        LIMIT 1";
		$res = $this->db->query($sql,$roleId)->row();

        if(isset($res)){

            $sql = 
            "SELECT permissions.permissionId,permissions.description,rolepermissions.rolePermissionId
            FROM permissions
            LEFT JOIN rolepermissions ON permissions.permissionId = rolepermissions.permissionId
            AND rolepermissions.roleId = ?
            ORDER BY permissions.description asc";
            $res->rolepermissions = $this->db->query($sql,$roleId)->result();
    
            $sql = 
            "SELECT COUNT(rolepermissions.rolePermissionId) as sizeOfPermissions
            FROM permissions
            LEFT JOIN rolepermissions ON permissions.permissionId = rolepermissions.permissionId
            AND rolepermissions.roleId = ?
            AND rolepermissions.rolePermissionId IS NOT NULL";
            $res->sizeOfPermissions = $this->db->query($sql,$roleId)->row()->sizeOfPermissions;
            
        }
        
    	return $res;
    }

    public function getPermissions()
    {
    	return $this->db->query("SELECT * FROM permissions ORDER BY description asc")->result();
    }

    public function createRole($form){

    	$this->db->insert('roles',array('name' => htmlspecialchars($form['name'])));

    	$insert_id   = $this->db->insert_id();
    	$permissions = $this->getPermissions();

    	if (isset($form['active'])){
    
    		foreach ($form['active'] as $active) {

    			$this->db->insert('rolepermissions',array('roleId' => $insert_id, 'permissionId' => $active));
    		}
    	}	
    }

    public function editRole($form){

    	$sql = "UPDATE roles
    	SET name = ?
    	WHERE roles.roleId = ?";
    	$this->db->query($sql,array('name' => htmlspecialchars($form['name']),"roleId" =>$form['roleId']));

    	$sql = "DELETE FROM rolepermissions
    	WHERE rolepermissions.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));

    	if (isset($form['active'])){

    		foreach ($form['active'] as $active) {

    			$this->db->insert('rolepermissions',array('roleId' => $form['roleId'], 'permissionId' => $active));
    		}
    	}	
    } 

    public function deleteRole($form){

    	$sql = "DELETE FROM rolepermissions
    	WHERE rolepermissions.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));

    	$sql = "DELETE FROM roles
    	WHERE roles.roleId = ?";
    	$this->db->query($sql,array("roleId" =>$form['roleId'] ));
    } 

    public function roleIdExists($roleId)
    {
        $sql = "SELECT r.roleId
        FROM roles r
        WHERE r.roleId = ?
        LIMIT 1";
        $query = $this->db->query($sql,$roleId)->row();

        return isset($query);
    }
}

?>