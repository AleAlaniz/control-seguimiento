<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Providers_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getProviders()
        {
            $sql = "SELECT providerId,name FROM providers WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getProvider($providerId)
        {
            $sql = "SELECT * FROM providers WHERE providerId = ? AND active = 1";
            $res = $this->db->query($sql,$providerId)->row();
            return $res;
        }
        
        function Create($provider)
        {
            if(isset($provider))
            {
                $this->db->insert('providers', $provider);
                return "success";
            }
            return "error";
        }

        public function Edit($provider)
        {
            if(isset($provider))
            {
                $this->db->where('providerId', $provider['providerId']);
                $this->db->update('providers',$provider);
                return "success";
            }
            return "error";
        }

        public function Delete($provider)
        {
            if(isset($provider))
            {
                $this->db->where('providerId', $provider['providerId']);
                $this->db->update('providers', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file providers_model.php */
?>