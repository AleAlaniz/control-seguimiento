<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliesrequests_model extends CI_Model {
    
    public function GetRequests($firstDate,$secondDate,$search_value)
    {
        $query = "SELECT requestId, stateFile, priority, manager, adviser, dateFile, dateCheck, sv.operatorId operator, supplies, state, nombre, apellido 
        FROM suppliesrequests sv JOIN operators o ON sv.operatorId = o.operatorId
        WHERE dateFile 
        -- BETWEEN '$firstDate' AND '$secondDate' 
        AND state IS NULL ";

        $countQuery = "SELECT COUNT(requestId) as validaciones 
        FROM suppliesrequests sv JOIN operators o ON sv.operatorId = o.operatorId
        WHERE dateFile 
        -- BETWEEN '$firstDate' AND '$secondDate' 
        AND state IS NULL ";

        if ($search_value){
            $query      = $query." AND (nombre LIKE '%".htmlspecialchars($search_value)."%' ";  
            $countQuery = $countQuery." AND (nombre LIKE '%".htmlspecialchars($search_value)."%' ";  

            $query      = $query." OR apellido LIKE '%".htmlspecialchars($search_value)."%' ) ";  
            $countQuery = $countQuery." OR apellido LIKE '%".htmlspecialchars($search_value)."%' ) ";  
        } 

        $query.= "ORDER BY dateFile DESC LIMIT ".$_POST['start'].", ".$_POST['length']." ";
        $countQuery.= "ORDER BY dateFile DESC";

        $requests = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        $data = array();

        for ($i=0; $i < count($requests); $i++) { 
            $nestedData = array();

            $nestedData[] = $requests[$i]->nombre." ".$requests[$i]->apellido;
            $nestedData[] = $requests[$i]->stateFile;
            $nestedData[] = $requests[$i]->priority;
            $nestedData[] = $requests[$i]->manager;
            $nestedData[] = $requests[$i]->adviser;
            $nestedData[] = $requests[$i]->dateFile;
            // $nestedData[] = $requests[$i]->dateCheck;
            $nestedData[] = $requests[$i]->supplies;
            $nestedData[] = $requests[$i]->requestId;
            $nestedData[] = $requests[$i]->operator;

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->validaciones,
            "recordsFiltered"   => $countData->validaciones,
            "data"              => $data
        );

        echo json_encode($json_data);
    }
    
    public function Edit($requestId,$state)
    {
        $date = date('Y-m-d H:i:s',strtotime('-3 hours'));
        $this->db->set('dateCheck', $date);
        $this->db->set('state', $state);
        $this->db->where('requestId', $requestId);
        $this->db->update('suppliesrequests');
        
        $this->session->set_flashdata('requestMessage', 'edit');
        echo "sucess";
    }

    public function GetRequestsToSend()
    {
        $query = "SELECT requestId, sv.dateFile, sv.dateCheck, sv.operatorId, supplies, CONCAT(o.nombre,' ',o.apellido,'(',o.dni,')') as name
        FROM suppliesrequests sv JOIN operators o ON sv.operatorId = o.operatorId
        WHERE state = ?";

        return $result = $this->db->query($query,getSuppliesRequestState()[0])->result();
    }

    public function SetStateToSend($requests)
    {
        foreach ($requests as $req) {
            $this->db->set('state', 'Enviada');
            $this->db->where('requestId', $req);
            $this->db->update('suppliesrequests');
        }

        echo "success";
    }

    public function UploadFile($data)
    {
        $this->db->insert_batch('suppliesrequests', $data);
    }

    public function searchRequests($requestData)
    {
        $query = "SELECT requestId, sv.operatorId, supplies
        FROM suppliesrequests sv JOIN operators o ON sv.operatorId = o.operatorId
        WHERE o.operatorId = ? 
        AND requestId != ?
        AND state = ?";

        $result = $this->db->query($query,array($requestData->operatorId,$requestData->requestId,getSuppliesRequestState()[0]))->result();
        return $result;
    }
}

/* End of file Suppliesrequests_model.php */
