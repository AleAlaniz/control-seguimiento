<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operators_model extends CI_Model {

    function GetOperators($filters)
    {
        $query = "SELECT operatorId, dni, empresa, legajo, cuenta, usuario, nombre, apellido, celular, SUBSTRING(calle,1,10) as calle, turno
        FROM operators";

        $countQuery = "SELECT COUNT(operatorId) AS operators
        FROM operators";
        
        if ($filters->search_value){
            $query      = $query." WHERE (dni LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." WHERE (dni LIKE '%".htmlspecialchars($filters->search_value)."%' ";  

            $query      = $query." OR nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  
            $countQuery = $countQuery." OR nombre LIKE '%".htmlspecialchars($filters->search_value)."%' ";  

            $query      = $query." OR apellido LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
            $countQuery = $countQuery." OR apellido LIKE '%".htmlspecialchars($filters->search_value)."%' )";  
        } 
        
        $query      .= ' ORDER BY operatorId DESC LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';
        $countQuery .= ' ORDER BY operatorId DESC  ';

        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();

        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuenta;
            $nestedData[] = $operators[$i]->usuario;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->celular;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->operatorId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );

        echo json_encode($json_data);
                
        $result = $this->db->get('operators')->result();
        return $result;
    }

    function GetOperatorById($id)
    {
        $this->db->select('*');
        $this->db->where('operatorId', $id);
        
        $result = $this->db->get('operators')->row();
        return $result;
    }

    function Create($operator)
    {
        if(isset($operator))
        {
            $this->db->insert('operators', $operator);
            $operator['operatorId'] = $this->db->insert_id();
            insert_audit_logs('operators','CREATE',$operator);

            return $this->db->insert_id();
        }
        return null;
    }

    function Edit($operator)
    {
        if(isset($operator))
        {
            $query = "SELECT * FROM operators WHERE operatorId = ?";
            $res = $this->db->query($query,$operator['operatorId'])->row();
            insert_audit_logs('operators','EDIT',$res);

            $this->db->where('operatorId', $operator['operatorId']);
            $this->db->update('operators',$operator);
            return "success";
        }
        return "error";
    }

    function Delete($id)
    {
        if(isset($id))
        {
            $query = "SELECT operatorId FROM operators WHERE operatorId = ?";
            $res = $this->db->query($query,$id)->row();
            insert_audit_logs('operators','DELETE',$res);

            $this->db->set('active', 0);
            $this->db->where('operatorId', $id);
            $this->db->update('operators');

            return "success";
        }
        return "error";
    }

    public function SearchOperators($searchString)
    {
        
        $sql = "SELECT CONCAT(o.nombre,' ',o.apellido,'(',o.dni,')') as name, o.operatorId
        FROM operators o
        WHERE o.active = 1 
        AND (LOWER(o.nombre) LIKE ? OR LOWER(o.apellido) LIKE ? OR LOWER(o.dni) LIKE ?)
        ORDER BY o.nombre
        LIMIT 10";

        return $this->db->query($sql,array('%'.$searchString.'%','%'.$searchString.'%','%'.$searchString.'%'))->result();
    }

    public function GetOperatorsWithSupplies()
    {
        $sql="SELECT CONCAT(o.nombre,' ',o.apellido,'(',o.dni,')') as name, o.operatorId
        FROM operators o
        JOIN assignedsupplies a
        ON o.operatorId = a.operatorId
        JOIN shipments s ON o.operatorId = s.operatorId
        WHERE o.active = 1
        AND s.state = ?
        AND o.operatorId NOT IN (
            SELECT operatorId from shipments 
            WHERE operatorId = 1869
            AND type = ?
            AND active = 1
            AND state = ?)
        GROUP BY o.operatorId";

        return $this->db->query($sql,array(getShipmentState()[0],getShipmentType()[1],getShipmentState()[1]))->result();
    }

    public function GetOperatorRecord($operatorId)
    {
        $sql = "SELECT date, type, numero_serie,s.nombre
        FROM suppliesrecords sr
        JOIN operators o ON sr.operatorId = o.operatorId
        JOIN supplies s ON sr.supplyId = s.supplyId
        WHERE o.operatorId = ? ORDER BY date ASC";

        return $this->db->query($sql,$operatorId)->result();
    }

    public function GetOperatorByDni($dni)
    {
        $this->db->select('operatorId');
        return $this->db->get_where('operators',array('dni' => $dni),1)->row();
    }

    public function GetOperatorBySupplyId($supplyId)
    {
        $sql = "SELECT o.dni,CONCAT(o.nombre,' ',o.apellido) as name
        FROM operators o
        JOIN assignedsupplies a
        ON o.operatorId = a.operatorId
        WHERE a.supplyId = ?
        AND a.active = 1";
        return $this->db->query($sql,$supplyId)->row();
    }

    //Aperturesite matriz
    public function getOperatorsAperturesite($filters)
    {
        $query = "SELECT t1.operatorId,
        t1.dni,
        t1.legajo,
        t1.cuil,
        t1.apellido,
        t1.nombre,
        t1.fecha_nacimiento,
        t1.empresa,
        t1.cliente,
        t1.campana,
        t1.site_original,
        t1.turno,
        t1.horario,
        t1.puesto,
        t1.supervisor,
        t1.team_leader,
        t1.calle,
        t1.altura,
        t1.piso_depto,
        t1.localidad,
        t1.provincia,
        t1.codigo_postal,
        t1.telet_estado_actual,
        t1.telet_conectividad_historica,
        t1.telet_persona_riesgo,
        t1.telet_fliares_cargo,
        t1.telet_pc_nb_propia,
        t1.telet_elige_operar_site,
        t1.telet_prioridad_campana,
        t1.perform_cuartil,
        t1.perform_presentismo,
        t1.perform_prioridad_empleado,
        t1.perform_calidad,
        sum(CASE WHEN t2.name = 'HEADSET USB' THEN 1 ELSE 0 END) AS 'HEADSET_USB',
        sum(CASE WHEN t2.name = 'CABLE UTP' THEN 1 ELSE 0 END) AS 'CABLE_UTP',
        sum(CASE WHEN t2.name = 'MANOS LIBRES' THEN 1 ELSE 0 END) AS 'MANOS_LIBRES',
        sum(CASE WHEN t2.name = 'BIFURCADOR' THEN 1 ELSE 0 END) AS 'BIFURCADOR',
        sum(CASE WHEN t2.name = 'HEADSET' THEN 1 ELSE 0 END) AS 'HEADSET',
        sum(CASE WHEN t2.name = 'MODEM 4G' THEN 1 ELSE 0 END) AS 'MODEM_4G',
        sum(CASE WHEN t2.name = 'CHIP' THEN 1 ELSE 0 END) AS 'CHIP',
        sum(CASE WHEN t2.name = 'NOTEBOOK' THEN 1 ELSE 0 END) AS 'NOTEBOOK',
        sum(CASE WHEN t2.name = 'CPU' THEN 1 ELSE 0 END) AS 'CPU',
        sum(CASE WHEN t2.name = 'NOTEBOOK SANTANDER' THEN 1 ELSE 0 END) AS 'NOTEBOOK_SANTANDER',
        sum(CASE WHEN t2.name = 'MONITOR' THEN 1 ELSE 0 END) AS 'MONITOR',
        sum(CASE WHEN t2.name = 'MOUSE' THEN 1 ELSE 0 END) AS 'MOUSE',
        sum(CASE WHEN t2.name = 'CABLE' THEN 1 ELSE 0 END) AS 'CABLE',
        sum(CASE WHEN t2.name = 'TECLADO' THEN 1 ELSE 0 END) AS 'TECLADO',
        sum(CASE WHEN t2.name = 'ADAPTADOR USB' THEN 1 ELSE 0 END) AS 'ADAPTADOR_USB',
        sum(CASE WHEN t2.name = 'CABLE FUENTE' THEN 1 ELSE 0 END) AS 'CABLE_FUENTE',
        sum(CASE WHEN t2.name = 'CARGADOR NOTEBOOK' THEN 1 ELSE 0 END) AS 'CARGADOR_NOTEBOOK',
        sum(CASE WHEN t2.name = 'NOTEBOOK CARDIF' THEN 1 ELSE 0 END) AS 'NOTEBOOK_CARDIF',
        sum(CASE WHEN t2.name = 'INSTALACIONES' THEN 1 ELSE 0 END) AS 'INSTALACIONES',
        sum(CASE WHEN t2.name = 'CELULAR' THEN 1 ELSE 0 END) AS 'CELULAR',
        sum(CASE WHEN t2.name = 'CAMARA WEB' THEN 1 ELSE 0 END) AS 'CAMARA_WEB',
        sum(CASE WHEN t2.name = 'MUEBLE' THEN 1 ELSE 0 END) AS 'MUEBLE'
        
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";


        $countQuery = "SELECT count(t1.operatorId) operators
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";
        
        if(count($filters)> 0 ){
            foreach ($filters as $key => $value) {
                if(count($value) > 0){
                    $query .= " AND $key IN (";
                    $countQuery .= " AND $key IN (";
                    foreach ($value as $valuefilter) {
                        $query .= "'$valuefilter',";
                        $countQuery .= "'$valuefilter',";
                    }
                    $query = substr($query,0,-1);
                    $query .= ") ";

                    $countQuery = substr($countQuery,0,-1);
                    $countQuery .= ") ";
                }
            }
        }

        $query      .= ' GROUP BY t1.operatorID ORDER BY dni LIMIT  ' .$_POST['start']. ',' .$_POST['length']. '   ';
        
        // print_r($query);show_404();
        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        
        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = $operators[$i]->operatorId;
            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuil;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->fecha_nacimiento;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->cliente;
            $nestedData[] = $operators[$i]->campana;
            $nestedData[] = $operators[$i]->site_original;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->horario;
            $nestedData[] = $operators[$i]->puesto;
            $nestedData[] = $operators[$i]->supervisor;
            $nestedData[] = $operators[$i]->team_leader;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->altura;
            $nestedData[] = $operators[$i]->piso_depto;
            $nestedData[] = $operators[$i]->localidad;
            $nestedData[] = $operators[$i]->provincia;
            $nestedData[] = $operators[$i]->codigo_postal;
            $nestedData[] = $operators[$i]->telet_estado_actual;
            $nestedData[] = $operators[$i]->telet_conectividad_historica;
            $nestedData[] = $operators[$i]->telet_persona_riesgo;
            $nestedData[] = $operators[$i]->telet_fliares_cargo;
            $nestedData[] = $operators[$i]->telet_pc_nb_propia;
            $nestedData[] = $operators[$i]->telet_elige_operar_site;
            $nestedData[] = $operators[$i]->telet_prioridad_campana;
            $nestedData[] = $operators[$i]->perform_cuartil;
            $nestedData[] = $operators[$i]->perform_presentismo;
            $nestedData[] = $operators[$i]->perform_prioridad_empleado;
            $nestedData[] = $operators[$i]->perform_calidad;
            $nestedData[] = $operators[$i]->HEADSET_USB;
            $nestedData[] = $operators[$i]->CABLE_UTP;
            $nestedData[] = $operators[$i]->MANOS_LIBRES;
            $nestedData[] = $operators[$i]->HEADSET;
            $nestedData[] = $operators[$i]->MODEM_4G;
            $nestedData[] = $operators[$i]->CHIP;
            $nestedData[] = $operators[$i]->NOTEBOOK;
            $nestedData[] = $operators[$i]->CPU;
            $nestedData[] = $operators[$i]->NOTEBOOK_SANTANDER;
            $nestedData[] = $operators[$i]->MONITOR;
            $nestedData[] = $operators[$i]->MOUSE;
            $nestedData[] = $operators[$i]->CABLE;
            $nestedData[] = $operators[$i]->TECLADO;
            $nestedData[] = $operators[$i]->ADAPTADOR_USB;
            $nestedData[] = $operators[$i]->CABLE_FUENTE;
            $nestedData[] = $operators[$i]->CARGADOR_NOTEBOOK;
            $nestedData[] = $operators[$i]->NOTEBOOK_CARDIF;
            $nestedData[] = $operators[$i]->INSTALACIONES;
            $nestedData[] = $operators[$i]->CELULAR;
            $nestedData[] = $operators[$i]->CAMARA_WEB;
            $nestedData[] = $operators[$i]->MUEBLE;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );
        echo json_encode($json_data);
    }

    public function UpdateByFile($dataArray)
    {
        $res = $this->db->update_batch('operators',$dataArray,'dni');
    }

    //REPORT
    public function getOperatorsApertureReport()
    {
        $query = "SELECT 
        t1.operatorId,
        t1.dni,
        t1.legajo,
        t1.cuil,
        t1.apellido,
        t1.nombre,
        t1.fecha_nacimiento,
        t1.empresa,
        t1.cliente,
        t1.campana,
        t1.site_original,
        t1.turno,
        t1.horario,
        t1.puesto,
        t1.supervisor,
        t1.team_leader,
        t1.calle,
        t1.altura,
        t1.piso_depto,
        t1.localidad,
        t1.provincia,
        t1.codigo_postal,
        t1.telet_estado_actual,
        t1.telet_conectividad_historica,
        t1.perform_cuartil,
        t1.perform_presentismo,
        t1.perform_calidad
        
        from operators_customers t1
        left join assigned_supplies_categories t2
        ON t1.operatorId = t2.operatorId";


        $countQuery = "SELECT count(t1.operatorId) operators
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId";
        
        $query      .= ' GROUP BY t1.operatorID ORDER BY dni LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';

        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        
        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = NULL;
            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuil;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->fecha_nacimiento;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->cliente;
            $nestedData[] = $operators[$i]->campana;
            $nestedData[] = $operators[$i]->site_original;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->horario;
            $nestedData[] = $operators[$i]->puesto;
            $nestedData[] = $operators[$i]->supervisor;
            $nestedData[] = $operators[$i]->team_leader;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->altura;
            $nestedData[] = $operators[$i]->piso_depto;
            $nestedData[] = $operators[$i]->localidad;
            $nestedData[] = $operators[$i]->provincia;
            $nestedData[] = $operators[$i]->codigo_postal;
            $nestedData[] = $operators[$i]->telet_estado_actual;
            $nestedData[] = $operators[$i]->telet_conectividad_historica;
            $nestedData[] = $operators[$i]->perform_cuartil;
            $nestedData[] = $operators[$i]->perform_presentismo;
            $nestedData[] = $operators[$i]->perform_calidad;
            $nestedData[] = $operators[$i]->operatorId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );
        echo json_encode($json_data);
    }

    // RRHH
    public function GetOperatorsApertureRrhh()
    {
        $query = "SELECT 
        t1.operatorId,
        t1.dni,
        t1.legajo,
        t1.cuil,
        t1.apellido,
        t1.nombre,
        t1.fecha_nacimiento,
        t1.empresa,
        t1.cliente,
        t1.campana,
        t1.site_original,
        t1.turno,
        t1.horario,
        t1.puesto,
        t1.supervisor,
        t1.team_leader,
        t1.calle,
        t1.altura,
        t1.piso_depto,
        t1.localidad,
        t1.provincia,
        t1.codigo_postal,
        t1.telet_persona_riesgo,
        t1.telet_fliares_cargo
        
        from operators_customers t1
        left join assigned_supplies_categories t2
        ON t1.operatorId = t2.operatorId";


        $countQuery = "SELECT count(t1.operatorId) operators
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId";
        
        $query      .= ' GROUP BY t1.operatorID ORDER BY dni LIMIT ' .$_POST['start']. ',' .$_POST['length']. '   ';

        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        
        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = NULL;
            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuil;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->fecha_nacimiento;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->cliente;
            $nestedData[] = $operators[$i]->campana;
            $nestedData[] = $operators[$i]->site_original;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->horario;
            $nestedData[] = $operators[$i]->puesto;
            $nestedData[] = $operators[$i]->supervisor;
            $nestedData[] = $operators[$i]->team_leader;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->altura;
            $nestedData[] = $operators[$i]->piso_depto;
            $nestedData[] = $operators[$i]->localidad;
            $nestedData[] = $operators[$i]->provincia;
            $nestedData[] = $operators[$i]->codigo_postal;
            $nestedData[] = $operators[$i]->telet_persona_riesgo;
            $nestedData[] = $operators[$i]->telet_fliares_cargo;
            $nestedData[] = $operators[$i]->operatorId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );
        echo json_encode($json_data);
    }

    // OPS
    public function GetOperatorsApertureOps()
    {
        $query = "SELECT t1.operatorId,
        t1.dni,
        t1.legajo,
        t1.cuil,
        t1.apellido,
        t1.nombre,
        t1.fecha_nacimiento,
        t1.empresa,
        t1.cliente,
        t1.campana,
        t1.site_original,
        t1.turno,
        t1.horario,
        t1.puesto,
        t1.supervisor,
        t1.team_leader,
        t1.calle,
        t1.altura,
        t1.piso_depto,
        t1.localidad,
        t1.provincia,
        t1.codigo_postal,
        t1.telet_elige_operar_site,
        t1.telet_prioridad_campana,
        t1.perform_prioridad_empleado

        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";


        $countQuery = "SELECT count(t1.operatorId) operators
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";
        
        if (isset($filters->state_search)){
            $query      = $query." AND telet_estado_actual = '$filters->state_search'";  
            $countQuery = $countQuery." AND telet_estado_actual = '$filters->state_search'";  
        }

        if(isset($filters->risk_search))
        {
            $query      = $query." AND telet_persona_riesgo = '$filters->risk_search'";  
            $countQuery = $countQuery." AND telet_persona_riesgo = '$filters->risk_search'";  
        }

        if(isset($filters->propia_search))
        {
            $query      = $query." AND telet_pc_nb_propia = '$filters->propia_search' ";  
            $countQuery = $countQuery." AND telet_pc_nb_propia = '$filters->propia_search' ";  
        } 
        
        $query      .= ' GROUP BY t1.operatorID ORDER BY dni LIMIT  ' .$_POST['start']. ',' .$_POST['length']. '   ';

        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        
        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = NULL;
            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuil;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->fecha_nacimiento;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->cliente;
            $nestedData[] = $operators[$i]->campana;
            $nestedData[] = $operators[$i]->site_original;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->horario;
            $nestedData[] = $operators[$i]->puesto;
            $nestedData[] = $operators[$i]->supervisor;
            $nestedData[] = $operators[$i]->team_leader;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->altura;
            $nestedData[] = $operators[$i]->piso_depto;
            $nestedData[] = $operators[$i]->localidad;
            $nestedData[] = $operators[$i]->provincia;
            $nestedData[] = $operators[$i]->codigo_postal;
            $nestedData[] = $operators[$i]->telet_elige_operar_site;
            $nestedData[] = $operators[$i]->telet_prioridad_campana;
            $nestedData[] = $operators[$i]->perform_prioridad_empleado;
            $nestedData[] = $operators[$i]->operatorId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );
        echo json_encode($json_data);
    }

    // LOGIS
    public function GetOperatorsApertureLogis()
    {
        $query = "SELECT t1.operatorId,
        t1.dni,
        t1.legajo,
        t1.cuil,
        t1.apellido,
        t1.nombre,
        t1.fecha_nacimiento,
        t1.empresa,
        t1.cliente,
        t1.campana,
        t1.site_original,
        t1.turno,
        t1.horario,
        t1.puesto,
        t1.supervisor,
        t1.team_leader,
        t1.calle,
        t1.altura,
        t1.piso_depto,
        t1.localidad,
        t1.provincia,
        t1.codigo_postal,
        t1.telet_pc_nb_propia

        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";


        $countQuery = "SELECT count(t1.operatorId) operators
        from operators_customers t1
        left join assigned_supplies_categories t2
        on t1.operatorId = t2.operatorId
        WHERE 1";
        
        if (isset($filters->state_search)){
            $query      = $query." AND telet_estado_actual = '$filters->state_search'";  
            $countQuery = $countQuery." AND telet_estado_actual = '$filters->state_search'";  
        }

        if(isset($filters->risk_search))
        {
            $query      = $query." AND telet_persona_riesgo = '$filters->risk_search'";  
            $countQuery = $countQuery." AND telet_persona_riesgo = '$filters->risk_search'";  
        }

        if(isset($filters->propia_search))
        {
            $query      = $query." AND telet_pc_nb_propia = '$filters->propia_search' ";  
            $countQuery = $countQuery." AND telet_pc_nb_propia = '$filters->propia_search' ";  
        } 
        
        $query      .= ' GROUP BY t1.operatorID ORDER BY dni LIMIT  ' .$_POST['start']. ',' .$_POST['length']. '   ';

        $operators = $this->db->query($query)->result();
        $countData = $this->db->query($countQuery)->row();
        
        $data = array();

        for ($i=0; $i < count($operators); $i++) { 

            $nestedData = array();

            $nestedData[] = NULL;
            $nestedData[] = $operators[$i]->dni;
            $nestedData[] = $operators[$i]->legajo;
            $nestedData[] = $operators[$i]->cuil;
            $nestedData[] = $operators[$i]->apellido;
            $nestedData[] = $operators[$i]->nombre;
            $nestedData[] = $operators[$i]->fecha_nacimiento;
            $nestedData[] = $operators[$i]->empresa;
            $nestedData[] = $operators[$i]->cliente;
            $nestedData[] = $operators[$i]->campana;
            $nestedData[] = $operators[$i]->site_original;
            $nestedData[] = $operators[$i]->turno;
            $nestedData[] = $operators[$i]->horario;
            $nestedData[] = $operators[$i]->puesto;
            $nestedData[] = $operators[$i]->supervisor;
            $nestedData[] = $operators[$i]->team_leader;
            $nestedData[] = $operators[$i]->calle;
            $nestedData[] = $operators[$i]->altura;
            $nestedData[] = $operators[$i]->piso_depto;
            $nestedData[] = $operators[$i]->localidad;
            $nestedData[] = $operators[$i]->provincia;
            $nestedData[] = $operators[$i]->codigo_postal;
            $nestedData[] = $operators[$i]->telet_pc_nb_propia;
            $nestedData[] = $operators[$i]->operatorId;
            
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"              => intval($_POST['draw']),
            "recordsTotal"      => $countData->operators,
            "recordsFiltered"   => $countData->operators,
            "data"              => $data
        );
        echo json_encode($json_data);
    }
}

/* End of file Operators_model.php */
