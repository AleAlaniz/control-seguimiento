<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Refundreasons_model extends CI_Model {

        
        public function __construct()
        {
            parent::__construct();
        }

        public function getRefundReasons()
        {
            $sql = "SELECT refundreasonId,name FROM refundreasons WHERE active = 1";
            $tls = $this->db->query($sql)->result();

            return $tls;
        }

        public function getRefundReason($refundreasonId)
        {
            $sql = "SELECT * FROM refundreasons WHERE refundreasonId = ? AND active = 1";
            $res = $this->db->query($sql,$refundreasonId)->row();
            return $res;
        }
        
        function Create($refundreason)
        {
            if(isset($refundreason))
            {
                $this->db->insert('refundreasons', $refundreason);
                return "success";
            }
            return "error";
        }

        public function Edit($refundreason)
        {
            if(isset($refundreason))
            {
                $this->db->where('refundreasonId', $refundreason['refundreasonId']);
                $this->db->update('refundreasons',$refundreason);
                return "success";
            }
            return "error";
        }

        public function Delete($refundreason)
        {
            if(isset($refundreason))
            {
                $this->db->where('refundreasonId', $refundreason['refundreasonId']);
                $this->db->update('refundreasons', array('active' => 0));
                return "success";
            }
            return "error";
        }

    }

    /* End of file refundreasons_model.php */
?>