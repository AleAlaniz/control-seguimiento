<div class="col-md-12">
  <div id="map" style="width: 100%; height: 600px;">
  
  </div>
</div>
<script>
    var geocoder;
    var map;
    var addresses ;
    let delay = 100;
    let nextAddress = 0, coordinates = [];
    
    function getAddresses() {
      return new Promise((resolve,reject)=>{
        $.ajax({
          method 	  : "POST",
          url 	    : 'maps/getAddresses',
          dataType 	: 'JSON'
        }).done(data=>{
          resolve(data);
        })
      })
    }

    async function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: -34.6131500, lng: -58.3772300}
      });
      geocoder = new google.maps.Geocoder();
      addresses = await getAddresses();

      addresses.map(address=>{
        let position = { lat: parseFloat(address.lat,10), lng: parseFloat(address.lang,10) };
        console.log(position);
        var marker = new google.maps.Marker({
          map: map,
          position: position
        });
      })
      // theNext();
        // for (let index = 0; index < data.length; index++) {
        //   const element = data[index];
        //   setTimeout(() => {
        //     codeAddress(geocoder, map,element.address,index);
        //   }, index * 8000);
        // }

    }

    function codeAddress(address,next) {
      console.log(address);
      geocoder.geocode({'address': address.address}, function(results, status) {
        if (status === 'OK') {
          // if(index == 0){
          //   map.setCenter(results[0].geometry.location);
          // }
          let p = results[0].geometry.location;

          let latlang = {
            "addressId": address.addressId,
            "lat": p.lat(),
            "lang": p.lng()
          }
          coordinates.push(latlang);

          var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
          });
        } 
        else {
          if(status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT){
            nextAddress--;
            delay++;
            console.log("entre a query limit",nextAddress,delay);
          }
          else{
            console.log('Geocode was not successful for the following reason: ' + status);
          }
        }
        next();
      });
    }
    
    function theNext() 
    {
      if (nextAddress < addresses.length) 
      {
        try {
          setTimeout(codeAddress(addresses[nextAddress],theNext), delay);
          nextAddress++;
        } catch (error) {
          console.log("error consol",error);            
        }
      } 
      else{
        new Notification("Ya terminó de mostrar marcadores");
        $.ajax({
        method 	  : 'POST',
        url 	    : 'maps/saveCoordinates',
        data 	    : {coordinates},
        dataType  : 'JSON'
        }).done(function (data) {
          console.log(data);
          new Notification("Ya se guardaron en la bbdd");
        })
      }
    }
    
</script>
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCz5ka7qXmf0X7H1ig3bk1DCw6k-rz__2s&callback=initMap"type="text/javascript"></script>