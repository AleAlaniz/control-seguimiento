<link rel="stylesheet" type="text/css" href="/<?php echo ASSETSFOLDER; ?>/libraries/seatchart.js/css/seatchart.css">
<div class="content">
   <div id="map-container"></div>
</div>
<script src="/<?php echo ASSETSFOLDER; ?>/libraries/seatchart.js/js/seatchart.js"></script>
<script>
   var options = {
        // Reserved and disabled seats are indexed
        // from left to right by starting from 0.
        // Given the seatmap as a 2D array and an index [R, C]
        // the following values can obtained as follow:
        // I = columns * R + C
        //   llenar un array [...Array(10).keys()]

       map: {
           id: 'map-container',
           rows: 9,
           columns: 26,
           // e.g. Reserved Seat [Row: 1, Col: 2] = 7 * 1 + 2 = 9
           reserved: {
               seats:[]
           },
           disabled: {
               seats: [2,28,54,80,106,132,5,31,57,83,109,135,7,8,34,60,86,112,138,9,11,37,63,89,115,141,14,40,66,92,118,144,16,17,43,69,95,121,147,18,20,46,72,98,124,150,23,49,75,101,127,153,182,208,185,211,188,214,190,215,216,192,218,195,221,197,222,223,199,225,226,202,203,228,229,206,207,231,232,233],
               rows: [6],
            //    columns: [2,5]
           }
       },
       types: [
           { type: "disponible", backgroundColor: "#00FF00", price:10,selected:[0,27,78,131,30,33,36,39,42,45,48,51,81,84,87,90,93,96,99,102,134,137,140,143,146,149,152,155,183,186,189,194,196,200,205,224,227]},
           { type: "teamleader", backgroundColor: "#00FFFF", price: 7.5},
           { type: "supervisor", backgroundColor: "#FFFF00", price: 10},
       ]
};
    let userId = 4784;
    var sc = new Seatchart(options);

    //agrego evento click a cada asiento
    document.querySelectorAll('.clicked').forEach(btn=>{
        btn.addEventListener('click',function () {
            console.log(this.id);
            var currentClassList = [];
            for (var j = 0; j < this.classList.length; j += 1) {
                currentClassList.push(this.classList[j]);
            }
    
            for (var i = 0; i < currentClassList.length; i += 1) {
                var currentClass = currentClassList[i];
                var newClass;
    
                if (currentClass !== 'sc-seat' && currentClass !== 'clicked' && currentClass == 'disponible') {
                    this.classList.remove('disponible');
                    this.classList.add('assigned');
                }
            }
        })
    });
</script>