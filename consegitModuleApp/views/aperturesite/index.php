<link rel="stylesheet" type="text/css" href="/<?php echo ASSETSFOLDER; ?>/libraries/timepicker.js/css/jquery.timepicker.min.css">
<div ng-app="controlSeguimiento" ng-controller="aperturesite" class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo  $this->lang->line('main_aperturesite') ?></h3>
    </div>
    <!-- alert de aviso -->
    <?php if(isset($_SESSION['aperturesiteMessage']))
    { ?>
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fas fa-check"></i></strong> 
            <?php if ($_SESSION['aperturesiteMessage'] == 'upload'){
                echo $this->lang->line('request_correctly_uploaded');
            }
            if ($_SESSION['aperturesiteMessage'] == 'edit'){
                echo $this->lang->line('admin_operators_editmessage');
            }
            ?>
        </div>
    <?php } ?>
    
    <!-- <div class="card-body">
        <div class="form form-inline">
            <form id="importForm" method="post">
                <div class="form-group row">
                    <input type="file" name="excelFile" id="excelFile">
                </div>
                <hr>

                <div class="form-group row">
                    <button class="btn btn-info" id="btnSubmit"><?= $this->lang->line('request_upload_excel'); ?></button>
                    <span id="loading" class="d-none"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                </div>
            </form>
        </div>   
    </div> 
    <hr> -->

    <div class="container row">
        <div class="form form-inline">
            <label class="col-md-2"><?php echo $this->lang->line('general_add_filter');?></label>
            <div class="col-md-10">
                <select class="form-control" id="filters" ng-model="filterSelected" ng-options="f.value for f in filters">
                </select>
                <button class="btn btn-success btn-sm" ng-click="addFilter(filterSelected)" title="<?= $this->lang->line('general_add_filter'); ?>"><i class="fa fa-plus"></i></button>
                <button ng-show="filtersAdded.length > 0" class="ng-hide ml-1 btn btn-outline-danger btn-sm" ng-click="clearFilters()" title="<?= $this->lang->line('general_clear_filters'); ?>"><i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
    <form method="POST" ng-submit="formSubmit()">
        <div class="container">
            <div class="col-md-12 mb-3 mt-3 ng-hide" ng-show="filtersAdded.length > 0" ng-repeat="f in filtersAdded">
                <h3>{{f.value}}
                    <button class="float-right btn btn-danger btn-sm" ng-click="removeFilter(f)" title="<?= $this->lang->line('general_delete_filter'); ?>"><i class="fa fa-times"></i></button>
                </h3>
                <hr>
                <div class="form-check" ng-repeat="object in f.data | filter :'' && object.keyDb.length > 0">
                    <input class="form-check-input" name="{{f.key}}" type="checkbox" id="{{f.value+object.keyDb}}" value="{{object.keyDb}}">
                    <label class="form-check-label" for="{{f.value+object.keyDb}}">{{object.keyDb}}</label>
                </div>
            </div>

            <div class="col-md-12 mt-3 mb-3 ng-hide" ng-show="filtersAdded.length > 0">
                <button class="btn btn-primary"><?= $this->lang->line('general_apply_filter'); ?></button>
            </div>
        </div>
    </form>


    <div class="card-body">
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('main_aperturesite_edit'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editRowForm" method="POST" novalidate>
                            <div class="form-group row">
                                <label for="telet_estado_actual" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_estado_actual');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="operatorId" id="operatorId" val="">
                                    <input class="form-control" type="text" name="telet_estado_actual" id="telet_estado_actual" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telet_conectividad_historica" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_conectividad_historica');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" id="telet_conectividad_historica" name="telet_conectividad_historica">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telet_persona_riesgo" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_persona_riesgo');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="telet_persona_riesgo" id="telet_persona_riesgo" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="telet_fliares_cargo" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_fliares_cargo');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="telet_fliares_cargo" id="telet_fliares_cargo" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="telet_pc_nb_propia"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="telet_pc_nb_propia" id="telet_pc_nb_propia" value="" />
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-md-2" for="telet_elige_operar_site"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_elige_operar_site');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="telet_elige_operar_site" id="telet_elige_operar_site" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="telet_prioridad_campana"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_prioridad_campana');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="telet_prioridad_campana" id="telet_prioridad_campana" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="perform_cuartil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_cuartil');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="perform_cuartil" id="perform_cuartil" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="perform_presentismo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_presentismo');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="perform_presentismo" id="perform_presentismo" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="perform_prioridad_empleado"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="perform_prioridad_empleado" id="perform_prioridad_empleado" value="" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="perform_calidad"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_calidad');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" type="text" name="perform_calidad" id="perform_calidad" value="" />
                                </div>
                            </div>

                            <div class="form-group row form-error">

                            </div>		

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_save');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="positionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('admin_shipments_edit'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="positionModal-form" method="POST" novalidate>
                            <input type="hidden" id="operatorId" name="operatorId" value="">
                            <div class="form-group row">
                                <label for="turn" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_turn');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <select name="turn" id="turn" class="form-control" required>

                                    </select>                                        
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="startSchedule" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_number');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control timepicker" type="text" name="startSchedule" id="startSchedule"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="endSchedule" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_number');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control timepicker" type="text" name="endSchedule" id="endSchedule"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="date"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_date');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" id="date" name="date" type="text" placeholder="<?php echo $this->lang->line('admin_shipments_date');?>" value="<?php echo set_value('date',$this->input->post('date'));?>" required>
                                    <p class="text-danger"><?php echo form_error('date'); ?></p>
                                </div>
                            </div>

                            <div class="form-group row form-error">

                            </div>		

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_edit');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-hover" id="operators">
                <thead>
                    <tr class="active">
                        <th></th>
                        <th><?php echo $this->lang->line('sales_dni');?></th>
                        <th><?php echo $this->lang->line('operator_legajo');?></th>          
                        <th><?php echo $this->lang->line('admin_users_cuil');?></th>  
                        <th><?php echo $this->lang->line('admin_users_lastName');?></th>          
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th><?php echo $this->lang->line('admin_users_birthDate');?></th>         
                        <th><?php echo $this->lang->line('admin_operators_empresa');?></th>
                        <th><?php echo $this->lang->line('admin_operators_customer');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_campaign');?></th>
                        <th><?php echo $this->lang->line('admin_operators_original_site');?></th>
                        <th><?php echo $this->lang->line('admin_operators_turn');?></th>
                        <th><?php echo $this->lang->line('admin_operators_schedule');?></th>
                        <th><?php echo $this->lang->line('admin_operators_position');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_supervisor');?></th>          
                        <th><?php echo $this->lang->line('report_team_leader');?></th>          
                        <th><?php echo $this->lang->line('admin_operators_street');?></th>          
                        <th><?php echo $this->lang->line('admin_operators_height');?></th>         
                        <th><?php echo $this->lang->line('admin_operators_floor_dept');?></th>
                        <th><?php echo $this->lang->line('admin_operators_location');?></th>
                        <th><?php echo $this->lang->line('admin_operators_province');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_postal_code');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_estado_actual');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_conectividad_historica');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_persona_riesgo');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_fliares_cargo');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_elige_operar_site');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_prioridad_campana');?></th>
                        <th><?php echo $this->lang->line('admin_operators_perform_cuartil');?></th>
                        <th><?php echo $this->lang->line('admin_operators_perform_presentismo');?></th>
                        <th><?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?></th>
                        <th><?php echo $this->lang->line('admin_operators_perform_calidad');?></th>
                        <th><?php echo $this->lang->line('admin_operators_HEADSET_USB');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CABLE_UTP');?></th>
                        <th><?php echo $this->lang->line('admin_operators_MANOS_LIBRES');?></th>
                        <th><?php echo $this->lang->line('admin_operators_HEADSET');?></th>
                        <th><?php echo $this->lang->line('admin_operators_MODEM_4G');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CHIP');?></th>
                        <th><?php echo $this->lang->line('admin_operators_NOTEBOOK');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CPU');?></th>
                        <th><?php echo $this->lang->line('admin_operators_NOTEBOOK_SANTANDER');?></th>
                        <th><?php echo $this->lang->line('admin_operators_MONITOR');?></th>
                        <th><?php echo $this->lang->line('admin_operators_MOUSE');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CABLE');?></th>
                        <th><?php echo $this->lang->line('admin_operators_TECLADO');?></th>
                        <th><?php echo $this->lang->line('admin_operators_ADAPTADOR_USB');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CABLE_FUENTE');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CARGADOR_NOTEBOOK');?></th>
                        <th><?php echo $this->lang->line('admin_operators_INSTALACIONES');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CELULAR');?></th>
                        <th><?php echo $this->lang->line('admin_operators_CAMARA_WEB');?></th>
                        <th><?php echo $this->lang->line('admin_operators_MUEBLE');?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script src="/<?php echo ASSETSFOLDER; ?>/libraries/timepicker.js/js/jquery.timepicker.min.js"></script>
<script>
    let filters = {};
    $(function () {
        $('#state_search').change(function() {
           $('#operators').DataTable().ajax.reload();
        });

        $('#risk_search').change(function() {
           $('#operators').DataTable().ajax.reload();
        });

        $('#propia_search').change(function() {
           $('#operators').DataTable().ajax.reload();
        });
        $('#operators').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            // "searching": true,
            "ajax":{
                url :"Aperturesite/getOperatorsAperturesite", // json datasource
                data: function(d){
                    d.filters = JSON.stringify(filters)
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
               "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ usuarios",
               "sZeroRecords":    "<i class='fa fa-operators'></i> No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
               "sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "sUrl":            "",
               "sInfoThousands":  ",",
               "sLoadingRecords": "Cargando...",
               "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": null,
                    "render": function (data,type,row,meta) {
                        return  `
                        <div class="btn-group editRow" role="group"  aria-label="Basic example">
                            <button class="btn btn-primary " title="<?php echo $this->lang->line('admin_shipments_edit'); ?>" data-id=${data[0]} data-toggle="modal" data-target="#positionModal"><i class="fas fa-map-marker-alt"></i></button>
                            <button class="btn btn-warning" title="<?php echo $this->lang->line('admin_shipments_edit'); ?>" data-id=${data[6]} data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></button>
                            <input type="hidden" value="${data[0]}" />
                        </div>`
                    }
                }
            ]
        });

        $('#importForm').submit(function (e) {
            e.preventDefault();
            var fileInput = document.querySelector('form input[type=file]');
            var file = fileInput.files[0];

            var formData = new FormData(); 
            formData.append('file', file, file.name);

            // $.ajax({
            // method 	: 'POST',
            // url 	: 'Aperturesite/uploadFile',
            // data 	: formData,
            // contentType: false,
            // processData: false,
            // beforeSend: function () {
            //     $('#btnSubmit').addClass("d-none");
            //     $('#loading').removeClass("d-none");
            // }
            // }).done(function(data) {
            //     $('#btnSubmit').removeClass("d-none");
            //     $('#loading').addClass("d-none");
            //     $('#excelFile').val('');
            //     if(data == "success"){
            //         window.location.reload();
            //     }
            // });
        })

        $('#operators').on('click' ,'.editRow',function (e) {
            dataEdit = $(this).closest('tr')[0].innerText.split('	');

            $('#telet_estado_actual').val(dataEdit[22]);
            $('#telet_conectividad_historica').val(dataEdit[23]);
            $('#telet_persona_riesgo').val(dataEdit[24]);
            $('#telet_fliares_cargo').val(dataEdit[25]);
            $('#telet_pc_nb_propia').val(dataEdit[26]);
            $('#telet_elige_operar_site').val(dataEdit[27]);
            $('#telet_prioridad_campana').val(dataEdit[28]);
            $('#perform_cuartil').val(dataEdit[29]);
            $('#perform_presentismo').val(dataEdit[30]);
            $('#perform_prioridad_empleado').val(dataEdit[31]);
            $('#perform_calidad').val(dataEdit[32]);
            $('#operatorId').val($(this).find('input').attr("value"));
        });

        $('#editRowForm').submit(function (e) {
            e.preventDefault();
            let checkForm = true;

            // if
            // (
            //     ($('#telet_estado_actual').val() == "" || $('#telet_estado_actual').val() == null) ||
            //     ($('#telet_conectividad_historica').val() == "" || $('#telet_conectividad_historica').val() == null) ||
            //     ($('#telet_persona_riesgo').val() == "" || $('#telet_persona_riesgo').val() == null) ||
            //     ($('#telet_fliares_cargo').val() == "" || $('#telet_fliares_cargo').val() == null) ||
            //     ($('#telet_pc_nb_propia').val() == "" || $('#telet_pc_nb_propia').val() == null) || 
            //     ($('#telet_elige_operar_site').val() == "" || $('#telet_elige_operar_site').val() == null) ||
            //     ($('#telet_prioridad_campana').val() == "" || $('#telet_prioridad_campana').val() == null) ||
            //     ($('#perform_cuartil').val() == "" || $('#perform_cuartil').val() == null) ||
            //     ($('#perform_presentismo').val() == "" || $('#perform_presentismo').val() == null) ||
            //     ($('#perform_prioridad_empleado').val() == "" || $('#perform_prioridad_empleado').val() == null) ||
            //     ($('#perform_calidad').val() == "" || $('#perform_calidad').val() == null) 
            // )
            // {
            //     $('.form-error').append('<div class="text-danger col-sm-10">Completa todos los campos obligatorios</div>');
            //     checkForm = false;
            // }
            // else{
            //     checkForm = true;
            // }

            if(checkForm){
                $.ajax({
                    type: "POST",
                    data:$("#editRowForm").serialize(),
                    url: 'aperturesite/edit',
                    dataType: "json",
                    success:function(data)
                    {
                        if (data.status =='form-error'){
                            $('.form-error').append('<div class="text-danger col-sm-10">'+data.message+'</div>');
                        }
                        else{
                            if (data.status == 'success') {
    
                                $( "#editModal" ).modal('toggle');
                                window.location.href = window.location.href;
                            }
                            else{
                                $('.form-error').append(`<div class="text-danger col-sm-10">${data.message}</div>`);
                            }
                        }
                    },
                    error:function()
                    {
                        console.log('Error');
                    }
                });
            }
        });

        //inicio los timepickers
        $('.timepicker').timepicker({
            timeFormat: 'h:mm p',
            interval: 30,
            maxTime: '21:00pm',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });
    })
    var app = angular.module("controlSeguimiento",[]);
    app.controller("aperturesite",function ($scope,$http) {
        let filtersScope  = [
            {
                key: 'telet_estado_actual',
                value: 'Estado actual'
            },
            {
                key: 'telet_conectividad_historica',
                value: 'Conectividad histórica'
            },
            {
                key: 'telet_persona_riesgo',
                value: 'Persona de riesgo'
            },
            {
                key: 'telet_fliares_cargo',
                value: 'Familiares a cargo'
            },
            {
                key: 'telet_pc_nb_propia',
                value: 'Pc/notebook propia'
            },
            {
                key: 'telet_elige_operar_site',
                value: 'Elige operar site'
            },
            {
                key: 'telet_prioridad_campana',
                value: 'Prioridad campaña'
            },
            {
                key: 'perform_cuartil',
                value: 'Performance cuartil'
            },
            {
                key: 'perform_presentismo',
                value: 'Performance presentismo'
            },
            {
                key: 'perform_prioridad_empleado',
                value: 'Performance prioridad empleado'
            },
            {
                key: 'perform_calidad',
                value: 'Performance calidad'
            }
        ];
        
        $scope.filters = filtersScope;

        $scope.filtersAdded = [];

        $scope.addFilter = function(filter){
            if(filter){
                if(!$scope.filtersAdded.includes(filter)){
                    $.ajax({
                        method 	: 'POST',
                        url 	: 'Aperturesite/getFiltersData',
                        dataType: 'JSON',
                        data 	: {'filterName': filter.key}
                    }).done(function(data) {
                            filter.data = data;
                            $scope.filtersAdded.push(filter);
                            $scope.filters = $scope.filters.filter(el => el.key != filter.key);
                            $scope.$apply();
                        });
                }
            }
        };

        $scope.removeFilter = function (filter) {
            $scope.filtersAdded = $scope.filtersAdded.filter(el => el.key != filter.key);
            $scope.filters.push(filter);
            if($scope.filtersAdded.length == 0){
                $('#operators').DataTable().ajax.reload();
            }
        }

        $scope.clearFilters = function () {
            $scope.filters = filtersScope;
            $scope.filtersAdded = [];
            $('#operators').DataTable().ajax.reload();
        }

        $scope.formSubmit = function () {
            filters = {};
            $scope.filtersAdded.forEach(filter => {
                let checked = $(`input[name="${filter.key}"]:checked`);
                let values = new Array();
                jQuery.each(checked,function (index,element) {
                    values.push(element.value);
                })
                filters [filter.key] = values;
            });
            $('#operators').DataTable().ajax.reload();
        }
        
    })
</script>