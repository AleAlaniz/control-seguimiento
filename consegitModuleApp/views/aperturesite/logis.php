<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo  $this->lang->line('main_aperturesite_logis') ?></h3>
    </div>
    <!-- alert de aviso -->
    <?php if(isset($_SESSION['aperturesiteMessage']))
    { ?>
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong><i class="fas fa-check"></i></strong> 
            <?php if ($_SESSION['aperturesiteMessage'] == 'upload'){
                echo $this->lang->line('request_correctly_uploaded');
            }
            if ($_SESSION['aperturesiteMessage'] == 'edit'){
                echo $this->lang->line('admin_operators_editmessage');
            }
            ?>
        </div>
    <?php } ?>
    
    <div class="card-body">
        <div class="form form-inline">
            <form id="importForm" method="post">
                <div class="form-group row">
                    <input type="file" name="excelFile" id="excelFile">
                </div>
                <hr>

                <div class="form-group row">
                    <button class="btn btn-info" id="btnSubmit"><?= $this->lang->line('request_upload_excel'); ?></button>
                    <span id="loading" class="d-none"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                </div>
            </form>
        </div>   
    </div>

    <div class="card-body">
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('main_aperturesite_edit'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="editRowForm" method="POST" novalidate>
                            
                            <div class="form-group row">
                                <label for="telet_pc_nb_propia" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="operatorId" id="operatorId" val="">
                                    <input class="form-control" type="text" name="telet_pc_nb_propia" id="telet_pc_nb_propia" value="" />
                                </div>
                            </div>

                            <div class="form-group row form-error">
                            </div>		

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_save');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover" id="operators">
                <thead>
                    <tr class="active">
                        <th></th>
                        <th><?php echo $this->lang->line('sales_dni');?></th>
                        <th><?php echo $this->lang->line('operator_legajo');?></th>          
                        <th><?php echo $this->lang->line('admin_users_cuil');?></th>  
                        <th><?php echo $this->lang->line('admin_users_lastName');?></th>          
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th><?php echo $this->lang->line('admin_users_birthDate');?></th>         
                        <th><?php echo $this->lang->line('admin_operators_empresa');?></th>
                        <th><?php echo $this->lang->line('admin_operators_customer');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_campaign');?></th>
                        <th><?php echo $this->lang->line('admin_operators_original_site');?></th>
                        <th><?php echo $this->lang->line('admin_users_turn');?></th>
                        <th><?php echo $this->lang->line('admin_operators_schedule');?></th>
                        <th><?php echo $this->lang->line('admin_operators_position');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_supervisor');?></th>          
                        <th><?php echo $this->lang->line('report_team_leader');?></th>          
                        <th><?php echo $this->lang->line('admin_operators_street');?></th>          
                        <th><?php echo $this->lang->line('admin_operators_height');?></th>         
                        <th><?php echo $this->lang->line('admin_operators_floor_dept');?></th>
                        <th><?php echo $this->lang->line('admin_operators_location');?></th>
                        <th><?php echo $this->lang->line('admin_operators_province');?></th>  
                        <th><?php echo $this->lang->line('admin_operators_postal_code');?></th>
                        <th><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#operators').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            // "searching": true,
            "ajax":{
                url :"getOperatorsApertureLogis", // json datasource
                data: function(d){
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
               "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ usuarios",
               "sZeroRecords":    "<i class='fa fa-operators'></i> No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
               "sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "sUrl":            "",
               "sInfoThousands":  ",",
               "sLoadingRecords": "Cargando...",
               "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            },
            "columnDefs": [
                {
                    "targets": 0,
                    "data": null,
                    "render": function (data,type,row,meta) {
                        return  `
                        <div class="btn-group editRow" role="group"  aria-label="Basic example">
                            <input type="hidden" value="${data[23]}" />
                            <button class="btn btn-warning" title="<?php echo $this->lang->line('admin_shipments_edit'); ?>" data-id=${data[6]} data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></button>
                        </div>`
                    }
                }
            ]
        });

        $('#importForm').submit(function (e) {
            e.preventDefault();
            var fileInput = document.querySelector('form input[type=file]');
            var file = fileInput.files[0];

            var formData = new FormData(); 
            formData.append('file', file, file.name);

            $.ajax({
            method 	: 'POST',
            url 	: 'uploadFileLogis',
            data 	: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btnSubmit').addClass("d-none");
                $('#loading').removeClass("d-none");
            }
            }).done(function(data) {
                $('#btnSubmit').removeClass("d-none");
                $('#loading').addClass("d-none");
                $('#excelFile').val('');
                if(data == "success"){
                    window.location.reload();
                }
            });
        })

        let modalData;
        $('#operators').on('click' ,'.editRow',function (e) {
            dataEdit = $(this).closest('tr')[0].innerText.split('	');
            
            $('#telet_pc_nb_propia').val(dataEdit[22]);
            $('#operatorId').val($(this).find('input').attr("value"));
        });

        $('#editRowForm').submit(function (e) {
            e.preventDefault();
            let checkForm = true;

            // if
            // (
            //     ($('#telet_pc_nb_propia').val() == "" || $('#telet_pc_nb_propia').val() == null)
            // )
            // {
            //     $('.form-error').append('<div class="text-danger col-sm-10">Completa todos los campos obligatorios</div>');
            //     checkForm = false;
            // }
            // else{
            //     checkForm = true;
            // }

            if(checkForm){
                $.ajax({
                    type: "POST",
                    data:$("#editRowForm").serialize(),
                    url: 'edit',
                    dataType: "json",
                    success:function(data)
                    {
                        if (data.status =='form-error'){
                            $('.form-error').append('<div class="text-danger col-sm-10">'+data.message+'</div>');
                        }
                        else{
                            if (data.status == 'success') {
    
                                $( "#editModal" ).modal('toggle');
                                window.location.href = window.location.href;
                            }
                            else{
                                $('.form-error').append(`<div class="text-danger col-sm-10">${data.message}</div>`);
                            }
                        }
                    },
                    error:function()
                    {
                        console.log('Error');
                    }
                });
            }
        })
    })
</script>