    <?php $this->load->view('_shared/_admin_nav.php') ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_boxes_areyousure_delete')); ?>
        </div>
    
    <div class="card">
        <div class="card-header">
            <h3><?php echo $this->lang->line('admin_boxes_details') ?></h3>
        </div>
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('box_name');?> : </dt>
                    <dd><?php echo $box->box_name ?></dd>
                </dl>
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('site_name');?> : </dt>
                    <dd><?php echo $box->site_name ?></dd>
                </dl>
            </div>
            <div class="row">
                <div class="mx-auto mt-3">
                    <form method="POST">
                        <input type="hidden" name="boxId" value="<?php echo $box->boxId?>">
                        <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                        <a href="<?php echo (base_url()) ?>users" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    	$(function() {
    		$('#delete').addClass('active');
    	})
    </script>