<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_boxes') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('boxes/admin')) { ?>
            <div class="card-body">
                <a href="/<?php echo FOLDERADD; ?>/boxes/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_boxes_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['boxesMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['boxesMessage'] == 'create'){
                        echo $this->lang->line('admin_boxes_successmessage');
                    }
                    elseif ($_SESSION['boxesMessage'] == 'edit'){
                        echo $this->lang->line('admin_boxes_editmessage');
                    }
                    elseif ($_SESSION['boxesMessage'] == 'delete'){
                        echo $this->lang->line('admin_boxes_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="boxes">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('box_name');?></th>
                        <th><?php echo $this->lang->line('site_name');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($boxes as $box ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $box->box_name ?></td>
                                <td><?php echo $box->site_name ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('boxes/admin')){ ?>
                                        <a href="/<?php echo FOLDERADD; ?>/boxes/edit/<?php echo $box->boxId?>" title="<?php echo $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                        <a href="/<?php echo FOLDERADD; ?>/boxes/delete/<?php echo $box->boxId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#boxes').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ boxes",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando boxes del _START_ al _END_ de un total de _TOTAL_ boxes",
					"sInfoEmpty":      "Mostrando boxes del 0 al 0 de un total de 0 boxes",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ boxes)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>