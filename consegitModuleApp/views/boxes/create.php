<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_boxes_create');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="box_name"><span class="font-weight-bold"><?php echo $this->lang->line('box_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="box_name" name="box_name" type="text" placeholder="<?php echo $this->lang->line('box_name');?>" maxlength="50">
                    <p class="text-danger"><?php echo form_error('box_name'); ?></p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2" for="site_id"><span class="font-weight-bold"><?php echo $this->lang->line('site');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select class="form-control" id="site_id" name="site_id" type="text" placeholder="<?php echo $this->lang->line('box_name');?>" maxlength="50">
                        <?php foreach ($sites as $site ) { ?>
                            <option value="<?php echo $site->siteId;?>"><?php echo $site->name;?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('site_id'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_boxes_create');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/boxes" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>
        
    </div>
</div>