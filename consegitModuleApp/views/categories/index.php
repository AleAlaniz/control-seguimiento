<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_categorias') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('categories/admin')) { ?>
            <div class="card-body">
                <a href="/<?php echo FOLDERADD; ?>/categories/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_category_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['categoriesMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['categoriesMessage'] == 'create'){
                        echo $this->lang->line('admin_category_successmessage');
                    }
                    elseif ($_SESSION['categoriesMessage'] == 'edit'){
                        echo $this->lang->line('admin_category_editmessage');
                    }
                    elseif ($_SESSION['categoriesMessage'] == 'delete'){
                        echo $this->lang->line('admin_category_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="categories">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($categories as $category ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $category->name ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('categories/admin')){ ?>
                                        <a href="/<?php echo FOLDERADD; ?>/categories/edit/<?php echo $category->categoryId?>" title="<?php echo $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                        <a href="/<?php echo FOLDERADD; ?>/categories/delete/<?php echo $category->categoryId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#categories').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ categorias",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando categorias del _START_ al _END_ de un total de _TOTAL_ categorias",
					"sInfoEmpty":      "Mostrando categorias del 0 al 0 de un total de 0 categorias",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ categorias)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>