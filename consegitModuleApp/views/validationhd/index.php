
<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo $this->lang->line('validationhd_returned_supplies') ?></h3>
    </div>
    <?php if ($this->Identity_model->Validate('validationhd/admin')) { ?>

        <div class="card-body">
            <a href="/<?php echo FOLDERADD; ?>/validationhd/report" class="btn btn-sm btn-outline-primary "><strong> <?php echo $this->lang->line('validationhd_reports');?></strong></a>
        </div>

    <?php } ?>

    <!-- <div class="card-body">
        <div class="form form-inline">
            <label><?php echo $this->lang->line('admin_shipments_select_date_range');?></label>
            <div class="col-sm-4">
                <div class="input-group input-daterange">
                    <input type="text" class="form-control" placeholder="Fecha Inicio"  readonly="readonly" id="date_init">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                    <input type="text" class="form-control" placeholder="Fecha Fin"  readonly="readonly"  id="date_end">
                </div>
            </div>
            <button class="btn btn-warning" onclick="cleanDates()">Limpiar rango</button>
        </div>
    </div>  -->

    <div class="card-body">

        <?php if(isset($_SESSION['validationMessage']))
        { ?>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-check"></i></strong> 
                <?php if ($_SESSION['validationMessage'] == 'create'){
                    echo $this->lang->line('validationhd_create');
                }
                ?>
            </div>
        <?php } ?>

        <div class="modal fade" id="checkSupply" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('validationhd_check'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="checkSupplyForm" action="shipments/edit" method="POST" novalidate>
                            <div class="form-group row">
                                <label for="completeName" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_completename');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="completeName" id="completeName" value="" readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="serialNumber" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_numero_serie');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="serialNumber" id="serialNumber" value="" readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="name" id="name" value="" readonly/>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="category" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('category');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="category" id="category" value="" readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="state" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_state');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <select name="state" id="state" class="form-control" required>

                                    </select>                                        
                                </div>
                            </div>

                            <div class="form-group row form-error">

                            </div>		

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_edit');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <table class="table table-hover" id="supplies">
            <thead>
                <tr class="active">
                    <th><?php echo $this->lang->line('admin_operators_completename');?></th>
                    <th><?php echo $this->lang->line('operator_legajo');?></th>
                    <th><?php echo $this->lang->line('admin_supplies_numero_serie');?></th>
                    <th><?php echo $this->lang->line('general_name');?></th>
                    <th><?php echo $this->lang->line('category');?></th>
                    <!-- <th><?php echo $this->lang->line('admin_shipments_state');?></th> -->
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>

    $(function () {
        $('#supplies').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"Validationhd/getSuppliesForValidation", // json datasource
                data: function(d){
                    d.state     = $('#state_search').val(),
                    d.startDate    = $('#date_init').val(),
                    d.endDate      = $('#date_end').val()
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
             "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ Envíos",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando Envíos del _START_ al _END_ de un total de _TOTAL_ Envíos",
             "sInfoEmpty":      "Mostrando Envíos del 0 al 0 de un total de 0 Envíos",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ Envíos)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar por texto:",
             "sUrl":            "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
            },
            "columnDefs": [
                {
                    "targets": 5,
                    "data": null,
                    "render": function (data,type,row,meta) {
                            return  `
                            <div class="btn-group validateSupplyButton" role="group" aria-label="Basic example">
                                <input type="hidden" id="operatorIdEdit" value="${data[5]}" />
                                <input type="hidden" id="supplyIdEdit" value="${data[6]}" />
                                <input type="hidden" id="shipmentIdEdit" value="${data[7]}" />
                                <button class="btn btn-info " title="<?php echo $this->lang->line('validationhd_check'); ?>" data-toggle="modal" data-target="#checkSupply"><i class="fas fa-check"></i></button>
                            </div>`
                    }
                }
                ,
                {
                    "targets":0,
                    "data":null,
                    "render": function (data,type,row,meta) {
                        return `
                        <a href="/<?php echo FOLDERADD; ?>/operators/view/${data[5]}" title="<?php echo  $this->lang->line('general_details') ?>">${data[0]}</a> &nbsp;`
                    }
                }
        ]
        });

        //modal edit
        var states = <?php echo json_encode(getValidationStates()); ?>;
        var supplyId, operatorId,shipmentId, dataEdit = "";

        $('#supplies').on('click' ,'.validateSupplyButton',function (e) {
            dataEdit = $(this).closest('tr')[0].innerText.split('	');
            operatorId = $(this).find('#operatorIdEdit').attr("value");
            supplyId = $(this).find('#supplyIdEdit').attr("value");
            shipmentId = $(this).find('#shipmentIdEdit').attr("value");
        })

        $( "#checkSupply" ).on('show.bs.modal', function(){
            $('#completeName').val(dataEdit[0]);
            $('#serialNumber').val(dataEdit[1]);
            $('#name').val(dataEdit[2]);
            $('#category').val(dataEdit[3]);

            $('#state').html(' ');
            for (let i = 0; i < states.length; i++) {
                const state = states[i];
                $('#state').append(`<option value="${state}">${state}</option>`)
            }
        })

        $("#checkSupplyForm").submit(function(e){
            e.preventDefault();
            $('.form-error').empty();
            let state = $('#state').val();
            let formData = {
                operatorId,
                supplyId,
                shipmentId,
                state
            }

            $.ajax({
            method 	: 'POST',
            url 	: 'validationhd/create',
            data 	: formData,
            datatype: 'json'
            }).done(function(data) {
                if (data == 'success') {
                    location.reload();
                }
            });
               
        });
        //fin modal edit
        
    })
</script>