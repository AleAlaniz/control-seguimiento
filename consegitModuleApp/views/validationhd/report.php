
<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo $this->lang->line('validationhd_main') ?></h3>
    </div>
    
    <div class="card-body">
        <div class="form form-inline">
            <label><?php echo $this->lang->line('admin_shipments_select_state');?></label>
            <div class="col-sm-3">
                <select class="form-control" id="state_search">
                    <?php foreach ($states as $state) { ?>
                    <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    <option value="0">Todos</option>
                </select>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form form-inline">
            <label><?php echo $this->lang->line('admin_shipments_select_date_range');?></label>
            <div class="col-sm-4">
                <div class="input-group input-daterange">
                    <input type="text" class="form-control" placeholder="Fecha Inicio"  readonly="readonly" id="date_init">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                    <input type="text" class="form-control" placeholder="Fecha Fin"  readonly="readonly"  id="date_end">
                </div>
            </div>
            <button class="btn btn-warning" onclick="cleanDates()">Limpiar rango</button>
        </div>
    </div> 

    <div class="card-body">
        
        <table class="table table-hover" id="validations">
            <thead>
                <tr class="active">
                    <th><?php echo $this->lang->line('admin_operators_completename');?></th>
                    <th><?php echo $this->lang->line('operator_legajo');?></th>
                    <th><?php echo $this->lang->line('admin_supplies_numero_serie');?></th>
                    <th><?php echo $this->lang->line('general_name');?></th>
                    <th><?php echo $this->lang->line('category');?></th>
                    <th><?php echo $this->lang->line('validationhd_validationDate');?></th>
                    <th><?php echo $this->lang->line('admin_supplies_state');?></th>
                    <th><?php echo $this->lang->line('login_username');?></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    function cleanDates() {
        $('#date_init').val("");
        $('#date_end').val("");
        $('#validations').DataTable().ajax.reload();
    }

    $(function () {
        $('#state_search').change(function() {
           $('#validations').DataTable().ajax.reload();
        });
        $('#state_search option[value="0"]').attr("selected",true);

        $('.input-daterange input').datepicker({ dateFormat: 'yyyy-mm-dd',language:'es'});

        $('.input-daterange').datepicker()
            .on("input change", function (e) {
            $('#validations').DataTable().ajax.reload();
        });


        $('#validations').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"getValidations", // json datasource
                data: function(d){
                    d.state        = $('#state_search').val(),
                    d.startDate    = $('#date_init').val(),
                    d.endDate      = $('#date_end').val()
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
             "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ Envíos",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando Envíos del _START_ al _END_ de un total de _TOTAL_ Envíos",
             "sInfoEmpty":      "Mostrando Envíos del 0 al 0 de un total de 0 Envíos",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ Envíos)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar por texto:",
             "sUrl":            "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    })
</script>