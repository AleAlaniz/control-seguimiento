<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_refundreasons') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('refundreasons/admin')) { ?>
            <div class="card-body">
                <a href="<?php echo (base_url()) ?>refundreasons/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_refundreasons_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['refundreasonMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['refundreasonMessage'] == 'create'){
                        echo $this->lang->line('admin_refundreasons_successmessage');
                    }
                    elseif ($_SESSION['refundreasonMessage'] == 'edit'){
                        echo $this->lang->line('admin_refundreasons_editmessage');
                    }
                    elseif ($_SESSION['refundreasonMessage'] == 'delete'){
                        echo $this->lang->line('admin_refundreasons_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="refundreasons">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($refundreasons as $refundreason ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $refundreason->name ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('refundreasons/admin')){ ?>
                                        <a href="<?php echo (base_url()) ?>refundreasons/edit/<?php echo $refundreason->refundreasonId?>" title="<?php echo $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                        <a href="<?php echo (base_url()) ?>refundreasons/delete/<?php echo $refundreason->refundreasonId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#refundreasons').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "<i class='fa fa-refundreasons'></i> No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>