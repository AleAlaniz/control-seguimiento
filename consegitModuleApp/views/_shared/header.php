<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> -->
    <title>Control y seguimiento de insumos</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/css/bootstrap.min.css">
    <!-- Font Awesome --> 
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/fontawesome-free-5.5.0/css/all.min.css">
    <!-- Datatables -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/DataTables-1.10.18/css/jquery.dataTables.min.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/css/bootstrap-datepicker3.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/css/custom-style.css">
    <link rel="icon" type="image/png" href="/<?php echo ASSETSFOLDER; ?>/images/favicon.png" />

    <!-- Jquery -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/jquery-3.3.1.min.js"></script>
    <!-- AngularJS -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/angular.min.js"></script>
    <!-- Bootstrap -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
    <!-- Datatables -->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>

    <!--Datepicker-->
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script>

    <!-- App -->
    <script>
        const FOLDERADD = "<?= base_url();?>";
    </script>
    <script src="/<?php echo ASSETSFOLDER; ?>/js/app.js"></script>
</head>
<body>
    <header class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
        <span class="navbar-brand">
            <img src="/<?php echo ASSETSFOLDER; ?>/images/logo-mini.png" alt="">
        </span>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <div class="navbar-nav-scroll">
            <ul class="navbar-nav mr-auto">
                <?php if($this->Identity_model->Validate('shipments/admin')){ ?>
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo FOLDERADD; ?>/shipments"><?php echo($this->lang->line('main_shipments')) ?></a>
                </li>
                <?php } 
                if($this->Identity_model->Validate('supplies/admin')){ ?>
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo FOLDERADD; ?>/supplies"><?php echo($this->lang->line('main_supplies')) ?></a>
                </li>
                <?php } 
                if($this->Identity_model->Validate('remits/admin')){ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/<?php echo FOLDERADD; ?>/remits"><?php echo($this->lang->line('admin_remits')) ?></a>
                    </li>
                <?php } 
                if($this->Identity_model->Validate('operators/admin')){ ?>
                    <li class="nav-item">
                        <a class="nav-link" href="/<?php echo FOLDERADD; ?>/operators"><?php echo($this->lang->line('main_operators')) ?></a>
                    </li>
                <?php } 
                if($this->Identity_model->Validate('suppliesrequests/admin')){ ?>
                <li class="nav-item">
                    <a class="nav-link" href="/<?php echo FOLDERADD; ?>/suppliesrequests"><?php echo($this->lang->line('main_supplies_requests')) ?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <ul class="navbar-nav ml-md-auto">
        
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $this->session->fullName?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <?php 
                if($this->Identity_model->Validate('users/changepass'))
                    { ?>
                        <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/users/changePass"><?php echo($this->lang->line('admin_users_changepass')) ?></a>
                        <div class="dropdown-divider"></div>
            <?php   } ?>
                    <a class="dropdown-item" href='/<?php echo FOLDERADD; ?>/login/logout'><i class="fas fa-sign-out-alt"></i> <?php echo($this->lang->line('main_log_out')) ?></a>
                </div>
            </li>
            <?php if ($this->Identity_model->Validate('users/view') || $this->Identity_model->Validate('users/admin') || $this->Identity_model->Validate('roles/view') || $this->Identity_model->Validate('roles/admin') || $this->Identity_model->Validate('teamleaders/view') || $this->Identity_model->Validate('teamleaders/admin') || $this->Identity_model->Validate('campaigns/view') || $this->Identity_model->Validate('campaigns/admin')) { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="" id="dropConfig" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-cog"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <?php if ($this->Identity_model->Validate('shipments/view') || $this->Identity_model->Validate('shipments/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/shipments"><?php echo($this->lang->line('main_shipments')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('supplies/view') || $this->Identity_model->Validate('supplies/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/supplies"><?php echo($this->lang->line('main_supplies')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('operators/view') || $this->Identity_model->Validate('operators/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/operators"><?php echo($this->lang->line('main_operators')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('categories/view') || $this->Identity_model->Validate('categories/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/categories"><?php echo($this->lang->line('main_categories')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('customers/view') || $this->Identity_model->Validate('customers/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/customers"><?php echo($this->lang->line('main_customers')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('owners/view') || $this->Identity_model->Validate('owners/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/owners"><?php echo($this->lang->line('main_owners')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('providers/view') || $this->Identity_model->Validate('providers/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/providers"><?php echo($this->lang->line('main_providers')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('sites/view') || $this->Identity_model->Validate('sites/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/sites"><?php echo($this->lang->line('main_sites')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('boxes/view') || $this->Identity_model->Validate('boxes/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/boxes"><?php echo($this->lang->line('main_boxes')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('refundreasons/view') || $this->Identity_model->Validate('refundreasons/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/refundreasons"><?php echo($this->lang->line('main_refundreasons')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('validationhd/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/validationhd"><?php echo($this->lang->line('validationhd')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('productivity/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/productivity"><?php echo($this->lang->line('main_productivity')) ?></a>
                        <?php } ?>
                        <div class="dropdown-divider"></div>
                        <?php 
                        if ($this->Identity_model->Validate('aperturesite/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/aperturesite"><?php echo($this->lang->line('main_aperturesite')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('aperturesite/report')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/aperturesite/report"><?php echo($this->lang->line('main_aperturesite_report')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('aperturesite/rrhh')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/aperturesite/rrhh"><?php echo($this->lang->line('main_aperturesite_rrhh')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('aperturesite/ops')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/aperturesite/ops"><?php echo($this->lang->line('main_aperturesite_ops')) ?></a>
                        <?php }
                        if ($this->Identity_model->Validate('aperturesite/logis')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/aperturesite/logis"><?php echo($this->lang->line('main_aperturesite_logis')) ?></a>
                        <?php }
                        ?>
                        <div class="dropdown-divider"></div>
                        <?php
                        if ($this->Identity_model->Validate('aperturesite/report') || $this->Identity_model->Validate('users/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/users"><?php echo($this->lang->line('main_users')) ?></a>
                        <?php } 
                        if ($this->Identity_model->Validate('roles/view') || $this->Identity_model->Validate('roles/admin')){ ?>
                            <a class="dropdown-item" href="/<?php echo FOLDERADD; ?>/roles"><?php echo($this->lang->line('main_roles')) ?></a>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>
        </ul>

    </div>
</header>
<div class="<?php echo((isset($max_width) && $max_width == TRUE) ? "ml-5 mr-5" : "container") ?>">
<script>

</script>
