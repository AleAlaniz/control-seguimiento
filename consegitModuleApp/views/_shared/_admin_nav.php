<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link" href="/<?php echo FOLDERADD.'/'.$this->uri->segment(1); ?>" id="goBack">
					<i class="fas fa-arrow-left"></i> &nbsp;<?php echo($this->lang->line('general_goback')) ?>
				</a>
			</li>
		<?php 	if($this->uri->segment(1) != 'categories' && $this->uri->segment(1) != 'sites' && $this->uri->segment(1) != 'customers' && $this->uri->segment(1) != 'owners'){ ?>
			<li class="nav-item">
				<a class="nav-link" href="/<?php echo FOLDERADD.'/'.$this->uri->segment(1); ?>/view/<?php echo (isset($unique)) ? $unique : $this->uri->segment(3); ?>" id="view">
					<i class="fas fa-search"></i> &nbsp;<?php echo($this->lang->line('general_details')) ?>	
				</a>
			</li> 
		<?php } ?>
			<li class="nav-item">
				<a class="nav-link" href="/<?php echo FOLDERADD.'/'.$this->uri->segment(1); ?>/edit/<?php echo (isset($unique)) ? $unique : $this->uri->segment(3); ?>" id="edit">
					<i class="fas fa-edit"></i> &nbsp;<?php echo($this->lang->line('general_edit')) ?>
				</a>
			</li>
		<?php
		if(($this->uri->segment(1) != 'users') || (($this->uri->segment(1) == 'users') && ($this->uri->segment(3) != $this->session->UserId)) ){ ?>
			<li class="nav-item">
				<a class="nav-link" href="/<?php echo FOLDERADD.'/'.$this->uri->segment(1); ?>/delete/<?php echo(isset($unique)) ? $unique : $this->uri->segment(3); ?>" id="delete">
					<i class="fas fa-trash-alt text-danger"></i> &nbsp;<span class=" text-danger"><?php echo($this->lang->line('general_delete')) ?></span>
				</a>
			</li> 
		<?php } ?>
		</ul>
	</div>
</nav>
<br>
