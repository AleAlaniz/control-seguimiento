<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_users_create');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="name" name="name" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo set_value('name',$this->input->post('name'));?>">
                    <p class="text-danger"><?php echo form_error('name'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="lastName"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_lastName');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="lastName" name="lastName" type="text" placeholder="<?php echo $this->lang->line('admin_users_lastName');?>" value="<?php echo set_value('lastName',$this->input->post('lastName'));?>">
                    <p class="text-danger"><?php echo form_error('lastName'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 pr-4" for="userName"><span class="font-weight-bold"><?php echo $this->lang->line('login_username');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="userName" name="userName" type="text" placeholder="<?php echo $this->lang->line('login_username');?>" value="<?php echo set_value('userName',$this->input->post('userName'));?>">
                    <p class="text-danger"><?php echo form_error('userName'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="password"><span class="font-weight-bold"><?php echo $this->lang->line('login_password');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="password" name="password" type="password" placeholder="<?php echo $this->lang->line('login_password');?>" value="<?php echo set_value('password',$this->input->post('password'));?>">
                    <p class="text-danger"><?php echo form_error('password'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2 pr-2" for="confirmPassword"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_confirm_password');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="confirmPassword" name="confirmPassword" type="password" placeholder="<?php echo $this->lang->line('admin_users_confirm_password');?>" value="<?php echo set_value('confirmPassword',$this->input->post('confirmPassword'));?>">
                    <p class="text-danger"><?php echo form_error('confirmPassword'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="role"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_role');?>:</span></label>
                <div class="col-md-10">
                    <select name="role" id="role" class="form-control" >
                        <option value="EMPTY"></option>
                        <?php 
                        foreach ($roles as $role) { ?>
                            <option value="<?php echo $role->roleId ?>" <?php echo set_select('role',$role->roleId,($role->roleId == $this->input->post('role')))?> >
                                <?php echo $role->name ?>
                            </option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('role'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_users_create');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        var timePress;

        $("#search_result").on('click', '[name="searchOption"]',addCampaign);
        function addCampaign(){

            var campaignId = $(this).attr('id');

            $('#campaigns_added_show').prepend('<span class="badge badge-info" style="margin-right:5px">'+$('#'+campaignId).text()+'<span name="userSelected" class="badge badge-pill badge-danger btn" id="btn_'+$('#'+campaignId).attr('id')+'"><i class="fas fa-times"></i></span></span>');
            $('#campaigns_added_send').prepend('<option value="'+$('#'+campaignId).attr('id')+'" selected></option>')
            $('#'+campaignId).remove();
        }

        $("#campaigns_added_show").on('click', '[name="userSelected"]',deleteCampaign);
        function deleteCampaign() {

            var selectedUser = $(this);
            selectedUser.parent().remove();
            console.log(selectedUser.attr('id').split('_'));
            $('#campaigns_added_send').children('[value="'+selectedUser.attr('id').split('_')[1]+'"]').remove();
        }

        $('#input_search').on('keyup paste', function()
        {
            $('#loadingSpinner').css('display', 'block');

            if($('#input_search').val().length > 0)
            {
                clearTimeout(timePress);
                timePress = setTimeout(searchUser, 300);
            }
            else
                searchUser();
        });

        function searchUser() {

            $("#search_result").empty();
            var searchText = $('#input_search').val().toLowerCase().trim();

            if (searchText.length > 0){

                $.ajax({   
                    type:     'POST',
                    url:      '/<?php echo FOLDERADD; ?>/campaigns/search_campaigns',
                    data:     {'search':searchText},
                    dataType: 'JSON',                
                    success:  ajaxResponse
                })
            }
            else{

                $('#loadingSpinner').css('display', 'none');
            }

            function ajaxResponse(data)
            {
                var campaingsAdded = $("#campaigns_added_send > option");

                for (var i = 0; i < data.length; i++)
                {
                    var campaignFound = false;

                    for (var j = 0; j < campaingsAdded.length; j++) 
                    {   
                        if (campaingsAdded[j].value == data[i].campaignId)
                        {
                            campaignFound = true;
                        }
                    }

                    if(!campaignFound)
                    {
                        $('#search_result').append('<li class="list-group-item cat-pointer" name="searchOption" id="'+data[i].campaignId+'">'+data[i].name+'</li>');
                    }
                }

                $('#loadingSpinner').css('display', 'none');
            }

        }
    }); 
</script>

