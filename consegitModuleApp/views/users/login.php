<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap-->
  <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/css/bootstrap.min.css">
  <!-- Font Awesome --> 
  <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/fontawesome-free-5.5.0/css/all.min.css">
  <!-- Custom styles -->
  <link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/css/custom-style.css">

  <!-- Jquery -->
  <script src="/<?php echo ASSETSFOLDER; ?>/libraries/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap -->
  <script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
  <!-- Datatables -->
</head>

<body class="container">
  <div style="margin-top:10%">
    <div class="row">
      <div class="col-md-12">
        <div class="text-center">
          <img class="img-fluid" src="/<?php echo ASSETSFOLDER; ?>/images/logo-cat.jpg">
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 20px">
      <div class="col-md-4 offset-md-4">
        <?php if (isset($_SESSION['auth_status'])){?>
          <div class="alert alert-danger loginmessage" role="alert" id="ErrorMessage">
            <?php echo $_SESSION['auth_status']; ?>
          </div>
        <?php } ?>
        <form action="/<?=FOLDERADD?>/login/autenticate" method="POST" id="FormLogin" autocomplete="false | unknown-autocomplete-value">

          <div class="form-group row">
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text"><i class="fas fa-user"></i></div>
              </div>
              <input type="text" class="form-control" placeholder="<?php echo $this->lang->line('login_username')?>" id="userName" name="userName" required>
            </div>
          </div>

          <div class="form-group row">
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text"><i class="fas fa-lock"></i></div>
              </div>
              <input type="password" class="form-control" placeholder="<?php echo $this->lang->line('login_password')?>" id="password" name="password" required>
            </div>
          </div>

          <div class="text-center">
            <input type="submit" id="enter" class="btn btn-outline-primary" value="<?php echo $this->lang->line('login_signin')?>">
            <img id="ajax-load-login" src="<?php echo (asset_url()) ?>/images/ajax-loader.gif" style="display:none;" class="mx-auto">
          </div>

        </form>
      </div>
    </div>
  </div>


  <script>
    $(function () {
      // $('#FormLogin').submit(function (e) {
      //   e.preventDefault();
      //   var datosForm = {
      //     userName : $(this).find('#userName').val(),
      //     password : $(this).find('#password').val()
      //   };
      //   $('#enter').hide();
      //   $("#ajax-load-login").show();
        
      //   $.ajax({
      //     method 	: "POST",
      //     url 	: 'login/autenticate',
      //     data 	: datosForm
      //   }).done(function(data) {
      //     if(data == 'success'){
      //       location.reload();
      //     }
      //     else{
      //       $("#errorMessage").slideDown();
      //       $("#ajax-load-login").hide();
      //       $("#errorMessage").html("<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> " + data);
      //       $('#enter').show();
      //     }
      //   });
      // })
    });
  </script>
</body>
</html>
