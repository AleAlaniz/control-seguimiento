    <?php $this->load->view('_shared/_admin_nav.php') ?>
    <?php if($type == "delete") { ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_areyousure_delete_user')); ?>
        </div>
    <?php } ?>    
    
    <div class="card">
        <div class="card-header">
            <h2><?php echo $this->lang->line('admin_users_details') ?></h2>
        </div>
        
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('general_name');?> : </dt>
                    <dd><?php echo $user->name ?></dd>
                    <dt><?php echo $this->lang->line('admin_users_lastName');?> : </dt>
                    <dd><?php echo $user->lastName ?></dd>
                    <dt><?php echo $this->lang->line('login_username');?> : </dt>
                    <dd><?php echo $user->userName ?></dd>
                </dl>
                
            </div>
            <?php if($type == "delete") { ?>
                <div class="row">
                    <div class="mx-auto mt-3">
                        <form method="POST">
                            <input type="hidden" name="userId" value="<?php echo $user->userId?>">
                            <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                            <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php if($type == "delete") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#delete").addClass("active");
        	})
        </script>
    <?php } ?>
    <?php if($type == "view") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#view").addClass("active");
        	})
        </script>
        <?php } ?>