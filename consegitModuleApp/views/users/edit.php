<?php $this->load->view('_shared/_admin_nav.php') ?>
<div class="card">
    <div class="card-header ">
        <h3><?php echo $this->lang->line('admin_users_edit');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">

            <div class="form-group row">
                <label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="name" name="name" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo $user->name ?>">
                    <p class="text-danger"><?php echo form_error('name'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="lastName"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_lastName');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="lastName" name="lastName" type="text" placeholder="<?php echo $this->lang->line('admin_users_lastName');?>" value="<?php echo $user->lastName ?>">
                    <p class="text-danger"><?php echo form_error('lastName'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2 pr-4" for="userName"><span class="font-weight-bold"><?php echo $this->lang->line('login_username');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="userName" name="userName" type="text" placeholder="<?php echo $this->lang->line('login_username');?>" value="<?php echo $user->userName ?>">
                    <p class="text-danger"><?php echo form_error('userName'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="role"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_role');?>:</span></label>
                <div class="col-md-10">
                    <select name="role" id="role" class="form-control" >
                        <option value="EMPTY"></option>
                        <?php 
                        foreach ($roles as $role) { ?>
                            <option value="<?php echo $role->roleId?>" <?php echo  set_select('role', $role->roleId, ($user->roleId == $role->roleId)); ?> ><?php echo $role->name?></option>
                        <?php } ?>
                        ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('role'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_users_edit');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/users" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
    </form>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function()
    {
        $('#edit').addClass('active');
    }); 
</script>