<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo  $this->lang->line('main_users') ?></h3>
    </div>
    <?php if ($this->Identity_model->Validate('users/admin')) { ?>
        <div class="card-body">
            <a href="/<?php echo FOLDERADD; ?>/users/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_users_create');?></strong></a>
        </div>
    <?php } ?>
    <div class="card-body">

        <?php if(isset($_SESSION['userMessage']))
        { ?>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-check"></i></strong> 
                <?php if ($_SESSION['userMessage'] == 'create'){
                    echo $this->lang->line('admin_users_successmessage');
                }
                elseif ($_SESSION['userMessage'] == 'edit'){
                    echo $this->lang->line('admin_users_editmessage');
                }
                elseif ($_SESSION['userMessage'] == 'delete'){
                    echo $this->lang->line('admin_users_deletemessage');
                }

                ?>
            </div>
        <?php } ?>

        <?php if(isset($_SESSION['deleteMessage']))
        { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-skull"></i></strong> 
                <?php if ($_SESSION['deleteMessage'] == 'selfDelete'){
                    echo $this->lang->line('admin_users_selfDelete');
                }
                ?>
            </div>
        <?php } ?>
        <table class="table table-hover" id="users">
            <thead>
                <tr class="active">
                    <th><?php echo $this->lang->line('general_name');?></th>
                    <th><?php echo $this->lang->line('admin_users_lastName');?></th>
                    <th><?php echo $this->lang->line('login_username');?></th>
                    <th><?php echo $this->lang->line('admin_users_role');?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach ($users as $user ) { ?>
                        <tr class="optionsUser">
                            <td><?php echo $user->name ?></td>
                            <td><?php echo $user->lastName ?></td>
                            <td><?php echo $user->userName ?></td>
                            <td><?php echo($user->role ? $user->role : $this->lang->line('general_undefined')) ?></td>
                            <td>
                                <a href="/<?php echo FOLDERADD; ?>/users/view/<?php echo $user->userId?>" title="<?php echo  $this->lang->line('general_details') ?>"><i class="fas fa-search"></i></a>&nbsp; 
                                <?php if($this->Identity_model->Validate('users/admin')){ ?>
                                    <a href="/<?php echo FOLDERADD; ?>/users/edit/<?php echo $user->userId?>" title="<?php echo  $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                    <?php if($this->session->UserId != $user->userId){ ?>
                                    <a href="/<?php echo FOLDERADD; ?>/users/delete/<?php echo $user->userId?>" title="<?php echo  $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php } ?>
                                <?php }?>
                            </td>
                        </tr>
            <?php   } ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    $(function () {
        $('#users').DataTable({
            language: {
               "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ usuarios",
               "sZeroRecords":    "<i class='fa fa-users'></i> No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
               "sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "sUrl":            "",
               "sInfoThousands":  ",",
               "sLoadingRecords": "Cargando...",
               "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
          }
      });
    })
</script>