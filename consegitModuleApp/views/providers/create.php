<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_providers_create');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="name" name="name" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" maxlength="50">
                    <p class="text-danger"><?php echo form_error('name'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_providers_create');?></button>
                    <a href="<?php echo (base_url()) ?>providers" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>
        
    </div>
</div>