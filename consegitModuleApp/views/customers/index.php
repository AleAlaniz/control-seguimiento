<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_customers') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('customers/admin')) { ?>
            <div class="card-body">
                <a href="<?php echo (base_url()) ?>customers/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_customers_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['customerMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['customerMessage'] == 'create'){
                        echo $this->lang->line('admin_customers_successmessage');
                    }
                    elseif ($_SESSION['customerMessage'] == 'edit'){
                        echo $this->lang->line('admin_customers_editmessage');
                    }
                    elseif ($_SESSION['customerMessage'] == 'delete'){
                        echo $this->lang->line('admin_customers_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="customers">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($customers as $customer ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $customer->name ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('customers/admin')){ ?>
                                        <a href="<?php echo (base_url()) ?>customers/edit/<?php echo $customer->customerId?>" title="<?php echo $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a>&nbsp; 
                                        <a href="<?php echo (base_url()) ?>customers/delete/<?php echo $customer->customerId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a>
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#customers').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "<i class='fa fa-customers'></i> No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>