<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_supplies_requests') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('supplies/admin')) { ?>

            <!-- <div class="card-body">
                <button class="btn btn-sm btn-outline-primary" type="button" id="showToggleFilters"><?php echo $this->lang->line('admin_filters_show') ?></button> 
                <div class="form form-inline">
                    <form id="importForm" method="post">
                        <div class="form-group row">
                            <input type="file" name="excelFile" id="excelFile">
                        </div>
                        <hr>

                        <div class="form-group row">
                            <button class="btn btn-info" id="btnSubmit"><?= $this->lang->line('request_upload_excel'); ?></button>
                            <span id="loading" class="d-none"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                        </div>
                    </form>
                </div>   
            </div> -->
            
        <?php } ?>
        <div id="filters" style="display:none;">
            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_site');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="site_search">
                        <?php foreach ($sites as $site) { ?>
                            <option value="<?php echo $site->siteId;?>"><?php echo $site->name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_box');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="box_search">
                        <?php foreach ($boxes as $box) { ?>
                            <option value="<?php echo $box->boxId;?>"><?php echo $box->box_name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_category');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="category_search">
                        <?php foreach ($categories as $category) { ?>
                            <option value="<?php echo $category->categoryId;?>"><?php echo $category->name;?></option>
                        <?php } ?>
                        <option value="0">Todas</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_owner');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="owner_search">
                        <?php foreach ($owners as $owner) { ?>
                            <option value="<?php echo $owner->ownerId;?>"><?php echo $owner->name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_state');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="state_search">
                        <?php foreach ($states as $state) { ?>
                            <option value="<?php echo $state;?>"><?php echo $state;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            
            <!-- alert de edicion -->
            <?php if(isset($_SESSION['requestMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['requestMessage'] == 'edit'){
                        echo $this->lang->line('request_correctly_edited');
                    }
                    if ($_SESSION['requestMessage'] == 'upload'){
                        echo $this->lang->line('request_correctly_uploaded');
                    }
                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="requests">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('admin_operator');?></th>
                        <th><?php echo $this->lang->line('requests_state');?></th>
                        <th><?php echo $this->lang->line('requests_priority');?></th>
                        <th><?php echo $this->lang->line('requests_manager');?></th>
                        <th><?php echo $this->lang->line('requests_adviser');?></th>
                        <th><?php echo $this->lang->line('requests_date_file');?></th>
                        <!-- <th><?php echo $this->lang->line('requests_date_check');?></th> -->
                        <th><?php echo $this->lang->line('main_supplies');?></th>
                        <th></th>
                    </tr>
                </thead>
            </table>

            <!-- modal de confirmacion de eliminar -->
            <div class="modal fade" id="editRequest" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4><?= $this->lang->line('request_what_do_you_want') ?></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <span class="request btn btn-success" id="acceptRequest"><?php echo $this->lang->line('request_accept'); ?></span>
                                    <span id="loading" class="d-none"><i class="fas fa-spinner fa-spin fa-2x"></i></span>
                                    <span class="request btn btn-danger" id="rejectRequest" ><?php echo $this->lang->line('request_reject'); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<script>
    $(function () {
        $('#requests').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"Suppliesrequests/getRequests", // json datasource
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
             "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ Envíos",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando Envíos del _START_ al _END_ de un total de _TOTAL_ Envíos",
             "sInfoEmpty":      "Mostrando Envíos del 0 al 0 de un total de 0 Envíos",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ Envíos)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar por nombre y apellido:",
             "sUrl":            "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "columnDefs": [
                {
                    "targets": 7,
                    "data": null,
                    "render": function (data,type,row,meta) {
                            return  `
                            <div class="btn-group edit_request" role="group" aria-label="Basic example">
                                <input type="hidden" value="${data[7]}" />
                                <button class="btn btn-info" title="<?php echo $this->lang->line('request_check'); ?>" data-toggle="modal" data-target="#editRequest"><i class="fas fa-check-double"></i></button>
                            </div>`
                    }
                },
                {
                    "targets":0,
                    "data":null,
                    "render": function (data,type,row,meta) {
                        return `<a href="/<?php echo FOLDERADD; ?>/operators/edit/${data[8]}" title="<?php echo  $this->lang->line('general_edit') ?>">${data[0]}</a> &nbsp;`
                    }
                }
            ]
        });
        let requestId = "";
        $('#requests').on('click' ,'.edit_request',function (e) {
            requestId = $(this).find('input').attr("value");
        });

        $('.request').click(function () {
            let formData = {
                "requestId" : requestId,
                "state"     : this.id
            }
            requestId = "";
            $.ajax({
            method 	: "POST",
            url 	: FOLDERADD+'/suppliesrequests/edit',
            data 	: formData
            }).done(function(data) {
                $('#editRequest').modal('hide');
                location.reload();
            });
        });

        $('#importForm').submit(function (e) {
            e.preventDefault();
            var fileInput = document.querySelector('form input[type=file]');
            var file = fileInput.files[0];

            var formData = new FormData(); 
            formData.append('file', file, file.name);

            $.ajax({
            method 	: 'POST',
            url 	: 'Suppliesrequests/uploadFile',
            data 	: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btnSubmit').addClass("d-none");
                $('#loading').removeClass("d-none");
            }
            }).done(function(data) {
                $('#btnSubmit').removeClass("d-none");
                $('#loading').addClass("d-none");
                $('#excelFile').val('');
                if(data == "success"){
                    window.location.reload();
                }
            });
        })
    })
</script>