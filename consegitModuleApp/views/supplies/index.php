<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('main_supplies') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('supplies/admin')) { ?>

            <div class="card-body">
                <a href="/<?php echo FOLDERADD; ?>/supplies/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_supplies_create');?></strong></a>
                <button class="btn btn-sm btn-outline-primary" type="button" id="showToggleFilters"><?php echo $this->lang->line('admin_filters_show') ?></button>

                <a href="/<?php echo FOLDERADD; ?>/supplies/summarybsas" class="btn btn-sm btn-outline-info "><strong> <?php echo $this->lang->line('admin_supplies_summarybsas');?></strong></a>
                <a href="/<?php echo FOLDERADD; ?>/supplies/summarylp" class="btn btn-sm btn-outline-info "><strong> <?php echo $this->lang->line('admin_supplies_summarylp');?></strong></a>

                <p class=" mt-2 text-info font-weight-bold"><?php print_r($this->lang->line('admin_supplies_assigned_counter').': ');?><span id="assigned"><?= $colorCounters->assigned;?></span></p>
                <p class="text-success font-weight-bold"><?php print_r($this->lang->line('admin_supplies_available_counter').': ');?><span id="available"><?= $colorCounters->available;?></span></p>
                <p class="text-warning font-weight-bold"><?php print_r($this->lang->line('admin_supplies_repair_counter').': ');?><span id="repair"><?= $colorCounters->repair;?></span></p>
                <p class="text-danger font-weight-bold"><?php print_r($this->lang->line('admin_supplies_broken_counter').': ');?><span id="broken"><?= $colorCounters->broken;?></span></p>
            </div>
            
        <?php } ?>
        <div id="filters" style="display:none;">
            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_site');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="site_search">
                        <?php foreach ($sites as $site) { ?>
                            <option value="<?php echo $site->siteId;?>"><?php echo $site->name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_box');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="box_search">
                        <?php foreach ($boxes as $box) { ?>
                            <option value="<?php echo $box->boxId;?>"><?php echo $box->box_name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_category');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="category_search">
                        <?php foreach ($categories as $category) { ?>
                            <option value="<?php echo $category->categoryId;?>"><?php echo $category->name;?></option>
                        <?php } ?>
                        <option value="0">Todas</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_owner');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="owner_search">
                        <?php foreach ($owners as $owner) { ?>
                            <option value="<?php echo $owner->ownerId;?>"><?php echo $owner->name;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form form-inline">
                    <label><?php echo $this->lang->line('admin_supplies_select_state');?></label>
                    <div class="col-sm-3">
                        <select class="form-control" id="state_search">
                        <?php foreach ($states as $state) { ?>
                            <option value="<?php echo $state;?>"><?php echo $state;?></option>
                        <?php } ?>
                        <option value="0">Todos</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            
            <?php if(isset($_SESSION['suppliesMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['suppliesMessage'] == 'create'){
                        echo $this->lang->line('admin_supplies_successmessage');
                    }
                    elseif ($_SESSION['suppliesMessage'] == 'edit'){
                        echo $this->lang->line('admin_supplies_editmessage');
                    }
                    elseif ($_SESSION['suppliesMessage'] == 'delete'){
                        echo $this->lang->line('admin_supplies_deletemessage');
                    }

                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="supplies">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('site');?></th>
                        <th><?php echo $this->lang->line('admin_supplies_box');?></th>
                        <th><?php echo $this->lang->line('category');?></th>
                        <th><?php echo $this->lang->line('admin_supplies_marca');?></th>
                        <th><?php echo $this->lang->line('admin_supplies_numero_serie');?></th>
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th><?php echo $this->lang->line('admin_supplies_state');?></th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
</div>
<script>
    $(function () {


        $('#category_search').change(function() {
           let categoryId = $('#category_search').val();

           var url = '<?php echo base_url('supplies'),'/getColorCounters' ?>' ;
            $.ajax({
            method 	: "POST",
            url 	: url,
            data 	: {'categoryId' : categoryId}
            }).done(function(data) {
                let res = JSON.parse(data);
                $('#assigned').html(res.assigned);
                $('#available').html(res.available);
                $('#repair').html(res.repair);
                $('#broken').html(res.broken);
            });

           $('#supplies').DataTable().ajax.reload();
        });

        $('#site_search').change(function() {
           $('#supplies').DataTable().ajax.reload();
        });
        
        $('#box_search').change(function() {
           $('#supplies').DataTable().ajax.reload();
        });

        $('#site_search option[value="0"]').attr("selected",true);
        $('#box_search option[value="0"]').attr("selected",true);
        $('#owner_search option[value="0"]').attr("selected",true);
        $('#state_search option[value="0"]').attr("selected",true);
        $('#category_search option[value="0"]').attr("selected",true);

        $('#owner_search').change(function() {
           $('#supplies').DataTable().ajax.reload();
        });

        $('#state_search').change(function() {
           $('#supplies').DataTable().ajax.reload();
        });


        $('#supplies').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"Supplies/getSupplies", // json datasource
                data: function(d){
                    d.category  = $('#category_search').val(),
                    d.site      = $('#site_search').val(),
                    d.box       = $('#box_search').val(),
                    d.state     = $('#state_search').val(),
                    d.owner     = $('#owner_search').val()
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ Insumos",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando Insumos del _START_ al _END_ de un total de _TOTAL_ Insumos",
					"sInfoEmpty":      "Mostrando Insumos del 0 al 0 de un total de 0 Insumos",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ Insumos)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar por número de serie:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},
                "columnDefs": [ 
                    
                    {
                        "targets": 7,
                        "data": null,
                        "render": function (data,type,row,meta) {
                            return  `
                                    <a href="/<?php echo FOLDERADD; ?>/supplies/view/${data[7]}" title="<?php echo  $this->lang->line('general_details') ?>"><i class="fas fa-search"></i></a> &nbsp;
                                    <a href="/<?php echo FOLDERADD; ?>/supplies/edit/${data[7]}" title="<?php echo  $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a> &nbsp;
                                    <a href="/<?php echo FOLDERADD; ?>/supplies/delete/${data[7]}" title="<?php echo  $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a> &nbsp;`
                        }
                        // defaultContent: 
                    }
                ]
        });

        $('#showToggleFilters').click(function () {
            $('#filters').toggle('fast');
            let text = $(this).html();
            $(this).html($(this).html() == '<?php echo $this->lang->line('admin_filters_hide') ?>' ? 
            '<?php echo $this->lang->line('admin_filters_show') ?>' : '<?php echo $this->lang->line('admin_filters_hide') ?>')
        })

    })
</script>