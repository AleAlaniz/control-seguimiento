<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo $this->lang->line('admin_supplies_summarylp') ?></h3>
    </div>
    <div class="card-body">

        <div class="table-responsive">
            <h3><?php echo $this->lang->line('admin_supplies_lpin') ?></h3>
            <table class="table col-md-8" id="basin">
                <thead>
                    <tr>
                        <?php 
                        $counter = $counter = $generalCounters->In; 
                        $counterArray = get_object_vars($counter);
                        foreach ($counterArray as $key => $value) {
                            print_r("<th>$key</th>");
                        }          
                        ?> 
                    <tr>
                </thead>
                <tbody>
                    <tr>
                    <?php 
                    
                        foreach ($counterArray as $key => $value) { ?>
                            <td class='text-center'><?=isset($value) ? $value : 0?></td>
                        <?php } ?>
                    <tr>
                </tbody>
            </table>
        </div>

        <div class="table-responsive">
            <h3><?php echo $this->lang->line('admin_supplies_lpout') ?></h3>
            <table class="table col-md-8" id="basout">
                <thead>
                    <tr>
                        <?php 
                        $counter = $counter = $generalCounters->Out; 
                        $counterArray = get_object_vars($counter);
                        foreach ($counterArray as $key => $value) {
                            print_r("<th>$key</th>");
                        }          
                        ?> 
                    <tr>
                </thead>
                <tbody>
                    <tr>
                    <?php 
                    
                        foreach ($counterArray as $key => $value) { 
                            print_r("<td class='text-center'>$value</td>");
                        } ?>
                    <tr>
                </tbody>
            </table>
        </div>

        <br>
        <div class="table-responsive">
            <h3><?php echo $this->lang->line('admin_customers_counters') ?></h3>
            <table class="table col-md-8" id="categories">
                <thead>
                    <tr>
                <?php 
                    print_r("<th class='text-center'>".$this->lang->line('campaign_campaings')."</th>");
                    for ($i=0; $i < count($mainCategories); $i++) { ?>
                        <th><?=$mainCategories[$i]->name;?></th>
                <?php   } ?>
                    <tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($generalCounters->customerCounters as $counter ) { ?>
                            <tr>
                                <td class="text-center"><?= $counter->name?></td>
                                <?php foreach ($mainCategories as $category) { 
                                    $counterArr = get_object_vars($counter);
                                    ?>
                                    <td class="text-center"><?= isset($counterArr[$category->name]) ? $counterArr[$category->name] : 0 ?> </td>
                                <?php } ?>
                            <tr>
                    <?php   } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $('#basout').DataTable({
        "scrollX": true
    })
</script>