<?php $this->load->view('_shared/_admin_nav.php') ?>
    <?php if($type == "delete") { ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_category_areyousure_delete')); ?>
        </div>
    <?php } ?>
<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_supply_details') ?></h3>
    </div>
    <div class="card-body">
        <div class="row col-md-12">
            <dl class="col-md-6">
                <dt><?php echo $this->lang->line('admin_supplies_numero_serie');?> : </dt>
                <dd><?php echo $supply->numero_serie ?></dd>
                <dt><?php echo $this->lang->line('general_name');?> : </dt>
                <dd><?php echo $supply->nombre ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_state');?> : </dt>
                <dd><?php echo $supply->state ?></dd>
                <dt><?php echo $this->lang->line('category');?> : </dt>
                <dd><?php echo $supply->category ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_sophos_fecha');?> : </dt>
                <dd><?php echo $supply->sophos_fecha ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_id_teamviewer');?> : </dt>
                <dd><?php echo $supply->id_teamviewer ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_marca');?> : </dt>
                <dd><?php echo $supply->marca ?></dd>
                
            </dl>
            <dl class="col-md-6">
                <?php if($supply->categoryId == 2 || $supply->categoryId == 3) {?>
                    <dt><?php echo $this->lang->line('admin_supplies_disco');?> : </dt>
                    <dd><?php echo $supply->disco ?></dd>
                    <dt><?php echo $this->lang->line('admin_supplies_memoria');?> : </dt>
                    <dd><?php echo $supply->memoria ?></dd>
                    <dt><?php echo $this->lang->line('admin_supplies_procesador');?> : </dt>
                    <dd><?php echo $supply->procesador ?></dd>
                <?php }?>
                <?php if($supply->phone != "-"){ ?>
                    <dt><?php echo $this->lang->line('main_supplies_phone');?> : </dt>
                    <dd><?php echo $supply->phone ?></dd>
                <?php } ?>
                <dt><?php echo $this->lang->line('site');?> : </dt>
                <dd><?php echo $supply->site ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_box');?> : </dt>
                <dd><?php echo $supply->box ?></dd>
                <dt><?php echo $this->lang->line('main_owner');?> : </dt>
                <dd><?php echo $supply->owner ?></dd>
                <dt><?php echo $this->lang->line('main_provider');?> : </dt>
                <dd><?php echo $supply->provider ?></dd>
                <dt><?php echo $this->lang->line('admin_supplies_stock');?> : </dt>
                <dd><?php echo $supply->stock ?></dd>
            </dl>

            <?php 
                $imgArray = explode(";",$supply->imgs);
                if($imgArray[0] != ""){
                    for ($i=0; $i < count($imgArray)-1; $i++) {
                        echo "<div class='col-md-6'>";
                        echo '<img class="img-fluid" src="/'.FOLDERADD.'/_uploads/'.$imgArray[$i].'" alt="Imagen">';
                        echo "</div>";
                    }
                }
            ?>
        </div>

        <?php if(isset($operator)>0){ ?>
            <div class="col-md-12">
                <h1><?php echo $this->lang->line('admin_operators_currently') ?></h1>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sales_dni') ?></th>
                            <th><?php echo $this->lang->line('admin_operators_completename') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="optionsUser">
                            <td><?php echo $operator->dni ?></td>
                            <td><?php echo $operator->name ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <?php if(count($records)>0){ ?>
        <div class="col-md-12">
            <h1><?php echo $this->lang->line('admin_supplies_record') ?></h1>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th><?php echo $this->lang->line('admin_shipments_date') ?></th>
                        <th><?php echo $this->lang->line('report_agent') ?></th>
                        <th><?php echo $this->lang->line('admin_shipments_type') ?></th>
                        <th><?php echo $this->lang->line('box_name') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        for ($i=0; $i < count($records); $i++) { 
                            $record = $records[$i] ?>
                            <tr class="optionsUser">
                                <td><?php echo $record->date ?></td>
                                <td><?php echo $record->completeName ?></td>
                                <td><?php echo $record->type ?></td>
                                <td><?php echo $record->name ?></td>
                            </tr>
                    <?php   } ?>
                </tbody>
            </table>
        </div>
        <?php } ?>
        <?php
        if($type == "delete") { ?>
            <div class="row">
                <div class="mx-auto mt-3">
                    <form method="POST">
                        <input type="hidden" name="supplyId" value="<?php echo $supply->supplyId?>">
                        <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                        <a href="/<?php echo FOLDERADD; ?>/supplies" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php if($type == "delete") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#delete").addClass("active");
        	})
        </script>
    <?php } ?>
    <?php if($type == "view") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#view").addClass("active");
        	})
        </script>
<?php } ?>