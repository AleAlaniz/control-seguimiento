<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_supplies_edit');?></h3>
    </div>
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-md-2" for="numero_serie"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_numero_serie');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="numero_serie" name="numero_serie" type="text" placeholder="<?php echo $this->lang->line('admin_supplies_numero_serie');?>" value="<?php echo $supply->numero_serie;?>">
                    <p class="text-danger"><?php echo form_error('numero_serie'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="categoryId"><span class="font-weight-bold"><?php echo $this->lang->line('category');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="categoryId" id="categoryId" class="form-control">
                        <?php 
                        foreach ($categories as $category) { ?>
                            <option value="<?php echo $category->categoryId ?>"  <?php echo set_select('category',$category->categoryId,($category->categoryId == $supply->categoryId))?> ><?php echo  $category->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('categoryId'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="nombre"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="nombre" name="nombre" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo $supply->nombre;?>">
                    <p class="text-danger"><?php echo form_error('nombre'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="purchaseDate"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_purchase_date');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="purchaseDate" name="purchaseDate" type="text" placeholder="<?php echo $this->lang->line('admin_supplies_purchase_date');?>" value="<?php echo $supply->purchaseDate;?>">
                    <p class="text-danger"><?php echo form_error('purchaseDate'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="state"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_state');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="state" id="state" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($states as $state) { ?>
                            <option value="<?php echo $state ?>"  <?php echo set_select('state', $state, ($supply->state == $state));?> ><?php echo  $state ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('state'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="userFile[]"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_img');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" type="file" name="userFile[]" id="userFile[]" multiple>
                    <p class="text-danger"><?php echo form_error('userFile[]'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="marca"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_marca');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="marca" id="marca" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($brands as $brand) { ?>
                            <option value="<?php echo $brand->name ?>"  <?php echo set_select('brand',$brand->name,($supply->marca == $brand->name))?> ><?php echo  $brand->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('marca'); ?></p>
                </div>
            </div>

            <div class="form-group row" id="bodyDisc">
                <label class="col-md-2" for="disco"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_disco');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="disco" id="disco" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($discs as $disc) { ?>
                            <option value="<?php echo $disc ?>"  <?php echo set_select('disc', $disc, ($supply->disco == $disc));?> ><?php echo  $disc ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('disc'); ?></p>
                </div>
            </div>

            <div class="form-group row" id="bodyMemory">
                <label class="col-md-2" for="memoria"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_memoria');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="memoria" id="memoria" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($memories as $memory) { ?>
                            <option value="<?php echo $memory ?>"  <?php echo set_select('memory', $memory, ($supply->memoria == $memory));?> ><?php echo  $memory ?></option>
                        <?php   } ?>
                    </select>
                </div>
            </div>

            <div class="form-group row" id="bodyProce">
                <label class="col-md-2" for="procesador"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_procesador');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="procesador" id="procesador" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($procesors as $procesor) { ?>
                            <option value="<?php echo $procesor ?>"  <?php echo set_select('procesor', $procesor, ($supply->procesador == $procesor));?> ><?php echo  $procesor ?></option>
                        <?php   } ?>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="sophos_fecha"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_sophos_fecha');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="sophos_fecha" name="sophos_fecha" type="text" placeholder="<?php echo $this->lang->line('admin_supplies_sophos_fecha');?>" value="<?php echo $supply->sophos_fecha;?>">
                    <p class="text-danger"><?php echo form_error('sophos_fecha'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="id_teamviewer"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_id_teamviewer');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="id_teamviewer" name="id_teamviewer" type="text" placeholder="<?php echo $this->lang->line('admin_supplies_id_teamviewer');?>" value="<?php echo $supply->id_teamviewer;?>">
                    <p class="text-danger"><?php echo form_error('id_teamviewer'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="ownerId"><span class="font-weight-bold"><?php echo $this->lang->line('main_owner');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="ownerId" id="ownerId" class="form-control">
                        <?php 
                        foreach ($owners as $owner) { ?>
                            <option value="<?php echo $owner->ownerId ?>"  <?php echo set_select('ownerId', $owner->ownerId, ($supply->ownerId == $owner->ownerId));?> ><?php echo  $owner->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('ownerId'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="providerId"><span class="font-weight-bold"><?php echo $this->lang->line('main_provider');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="providerId" id="providerId" class="form-control">
                        <?php 
                        foreach ($providers as $provider) { ?>
                            <option value="<?php echo $provider->providerId ?>"  <?php echo set_select('providerId', $provider->providerId, ($supply->providerId == $provider->providerId));?> ><?php echo  $provider->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('providerId'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="siteId"><span class="font-weight-bold"><?php echo $this->lang->line('site');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="siteId" id="siteId" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($sites as $site) { ?>
                            <option value="<?php echo $site->siteId ?>"  <?php echo set_select('siteId',$site->siteId,($site->siteId == $supply->siteId))?> ><?php echo  $site->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('siteId'); ?></p>
                </div>
            </div>

            <div class="form-group row" id="boxBody">
                <label class="col-md-2" for="boxId"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_box');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="boxId" id="boxId" class="form-control">
                        
                    </select>
                    <p class="text-danger"><?php echo form_error('boxId'); ?></p>
                </div>
            </div>
            

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_supplies_edit');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/supplies" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
    let category = $('#categoryId').val();
    if(category != 9 && category != 16){

        $('#bodyDisc').hide("fast");
        $('#bodyMemory').hide("fast");
        $('#bodyProce').hide("fast");
    }

    let site = $('#siteId').val();
    if(site != " "){
        getBoxes(site);
    }
    
    $('#sophos_fecha').datepicker({
        language 		: 'es',
        autoclose 		: true,
        todayHighlight 	: true,
        todayBtn 		: "linked"
    });

    $('#purchaseDate').datepicker({
        language 		: 'es',
        autoclose 		: true,
        todayHighlight 	: true,
        todayBtn 		: "linked"
    });

    $('#categoryId').change(function () {
        if(this.value == 9 || this.value == 16){
            $('#bodyDisc').show("fast");
            $('#bodyMemory').show("fast");
            $('#bodyProce').show("fast");
        }
        else{
            $('#bodyDisc').hide("fast");
            $('#bodyMemory').hide("fast");
            $('#bodyProce').hide("fast");
        }

    })

    $('#siteId').change(function () {
        getBoxes(this.value)
    })
    function getBoxes(siteId) {
        if(siteId != " "){
            var url = '<?php echo base_url('boxes'),'/getboxesbysite' ?>' ;
            $.ajax({
            method 	: "POST",
            url 	: url,
            data 	: {'siteId' : siteId}
            }).done(function(data) {
                $('#boxId').html('');
                let res = JSON.parse(data);
                for (let i = 0; i < res.length; i++) {
                    const e = res[i];
                    $('#boxId').append(`<option value="${e.boxId}">${e.box_name}</option>`)
                }
                $('#boxBody').show("fast");
            });
        }
        else{
            $('#boxId').html('');
        }
    }

})
</script>