<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_operators_create');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="id_lote"><span class="font-weight-bold"><?php echo $this->lang->line('operator_id_lote');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="id_lote" name="id_lote" type="text" placeholder="<?php echo $this->lang->line('operator_id_lote');?>" value="<?php echo set_value('id_lote',$this->input->post('id_lote'));?>">
                    <p class="text-danger"><?php echo form_error('id_lote'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="id_interno"><span class="font-weight-bold"><?php echo $this->lang->line('operator_id_interno');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="id_interno" name="id_interno" type="text" placeholder="<?php echo $this->lang->line('operator_id_interno');?>" value="<?php echo set_value('id_interno',$this->input->post('id_interno'));?>">
                    <p class="text-danger"><?php echo form_error('id_interno'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="empresa"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_empresa');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="empresa" name="empresa" type="text" placeholder="<?php echo $this->lang->line('admin_operators_empresa');?>" value="<?php echo set_value('empresa',$this->input->post('empresa'));?>">
                    <p class="text-danger"><?php echo form_error('empresa'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="legajo"><span class="font-weight-bold"><?php echo $this->lang->line('operator_legajo');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="legajo" name="legajo" type="text" placeholder="<?php echo $this->lang->line('operator_legajo');?>" value="<?php echo set_value('legajo',$this->input->post('legajo'));?>">
                    <p class="text-danger"><?php echo form_error('legajo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="dni"><span class="font-weight-bold"><?php echo $this->lang->line('sales_dni');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="dni" name="dni" type="text" placeholder="<?php echo $this->lang->line('sales_dni');?>" value="<?php echo set_value('dni',$this->input->post('dni'));?>">
                    <p class="text-danger"><?php echo form_error('dni'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="cuenta"><span class="font-weight-bold"><?php echo $this->lang->line('operator_cuenta');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="cuenta" name="cuenta" type="text" placeholder="<?php echo $this->lang->line('operator_cuenta');?>" value="<?php echo set_value('cuenta',$this->input->post('cuenta'));?>">
                    <p class="text-danger"><?php echo form_error('cuenta'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="usuario"><span class="font-weight-bold"><?php echo $this->lang->line('operator_usuario');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="usuario" name="usuario" type="text" placeholder="<?php echo $this->lang->line('operator_usuario');?>" value="<?php echo set_value('usuario',$this->input->post('usuario'));?>">
                    <p class="text-danger"><?php echo form_error('usuario'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="nombre"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="nombre" name="nombre" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo set_value('nombre',$this->input->post('nombre'));?>">
                    <p class="text-danger"><?php echo form_error('nombre'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="apellido"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_lastName');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="apellido" name="apellido" type="text" placeholder="<?php echo $this->lang->line('admin_users_lastName');?>" value="<?php echo set_value('apellido',$this->input->post('apellido'));?>">
                    <p class="text-danger"><?php echo form_error('apellido'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="celular"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_celular');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="celular" name="celular" type="text" placeholder="<?php echo $this->lang->line('admin_operators_celular');?>" value="<?php echo set_value('celular',$this->input->post('celular'));?>">
                    <p class="text-danger"><?php echo form_error('celular'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="email"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_email');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="email" name="email" type="text" placeholder="<?php echo $this->lang->line('admin_operators_email');?>" value="<?php echo set_value('email',$this->input->post('email'));?>">
                    <p class="text-danger"><?php echo form_error('email'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="province"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_province');?>:</span><span class="text-danger"><strong>* </strong></span></label>
                <div class="col-md-10">
                    <select name="province" id="province" class="form-control">
                        <?php 
                        foreach ($provinces as $province) { ?>
                            <option value="<?php echo $province ?>"  <?php echo set_select('province',$province,($province == $this->input->post('province')))?> ><?php echo  $province ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('province'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="location"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_location');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="location" name="location" type="text" placeholder="<?php echo $this->lang->line('admin_operators_location');?>" value="<?php echo set_value('location',$this->input->post('location'));?>">
                    <p class="text-danger"><?php echo form_error('location'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="street"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_street');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="street" name="street" type="text" placeholder="<?php echo $this->lang->line('admin_operators_street');?>" value="<?php echo set_value('street',$this->input->post('street'));?>">
                    <p class="text-danger"><?php echo form_error('street'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="height"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_height');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="height" name="height" type="text" placeholder="<?php echo $this->lang->line('admin_operators_height');?>" value="<?php echo set_value('height',$this->input->post('height'));?>">
                    <p class="text-danger"><?php echo form_error('height'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="postal_code"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_postal_code');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="postal_code" name="postal_code" type="text" placeholder="<?php echo $this->lang->line('admin_operators_postal_code');?>" value="<?php echo set_value('postal_code',$this->input->post('postal_code'));?>">
                    <p class="text-danger"><?php echo form_error('postal_code'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="floor_dept"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_floor_dept');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="floor_dept" name="floor_dept" type="text" placeholder="<?php echo $this->lang->line('admin_operators_floor_dept');?>" value="<?php echo set_value('floor_dept',$this->input->post('floor_dept'));?>">
                    <p class="text-danger"><?php echo form_error('floor_dept'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="turno"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_turn');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="turno" id="turno" class="form-control">
                        <?php 
                        foreach ($turns as $turn) { ?>
                            <option value="<?php echo $turn ?>"  <?php echo set_select('turn',$turn,($turn == $this->input->post('turn')))?> ><?php echo  $turn ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('admin_users_turn'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="contacto"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_contacto');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="contacto" name="contacto" type="text" placeholder="<?php echo $this->lang->line('admin_operators_contacto');?>" value="<?php echo set_value('contacto',$this->input->post('contacto'));?>">
                    <p class="text-danger"><?php echo form_error('contacto'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="prioridad"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_prioridad');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="prioridad" name="prioridad" type="text" placeholder="<?php echo $this->lang->line('admin_operators_prioridad');?>" value="<?php echo set_value('prioridad',$this->input->post('prioridad'));?>">
                    <p class="text-danger"><?php echo form_error('prioridad'); ?></p>
                </div>
            </div>
        
            <div class="form-group row">
                <label class="col-md-2" for="pc_de_cat"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_pc_de_cat');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="pc_de_cat" id="pc_de_cat" class="form-control">
                        <option value="Si">Si</option>
                        <option value="No">No</option>
                    </select>
                    <p class="text-danger"><?php echo form_error('pc_de_cat'); ?></p>
                </div>
            </div>

            <hr /> 

            <div class="form-group row">
                <label class="col-md-2" for="cuil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_cuil');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="cuil" name="cuil" type="text" placeholder="<?php echo $this->lang->line('admin_users_cuil');?>" value="<?php echo set_value('cuil',$this->input->post('cuil'));?>">
                    <p class="text-danger"><?php echo form_error('cuil'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="fecha_nacimiento"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_birthDate');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" type="text" placeholder="<?php echo $this->lang->line('admin_users_birthDate');?>" value="<?php echo set_value('fecha_nacimiento',$this->input->post('fecha_nacimiento'));?>">
                    <p class="text-danger"><?php echo form_error('fecha_nacimiento'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="site_original"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_original_site');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="site_original" name="site_original" type="text" placeholder="<?php echo $this->lang->line('admin_users_original_site');?>" value="<?php echo set_value('site_original',$this->input->post('site_original'));?>">
                    <p class="text-danger"><?php echo form_error('site_original'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="horario"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_schedule');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="horario" name="horario" type="text" placeholder="<?php echo $this->lang->line('admin_users_schedule');?>" value="<?php echo set_value('horario',$this->input->post('horario'));?>">
                    <p class="text-danger"><?php echo form_error('horario'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="puesto"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_position');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="puesto" name="puesto" type="text" placeholder="<?php echo $this->lang->line('admin_users_position');?>" value="<?php echo set_value('puesto',$this->input->post('puesto'));?>">
                    <p class="text-danger"><?php echo form_error('puesto'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="supervisor"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_supervisor');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="supervisor" name="supervisor" type="text" placeholder="<?php echo $this->lang->line('admin_users_supervisor');?>" value="<?php echo set_value('supervisor',$this->input->post('supervisor'));?>">
                    <p class="text-danger"><?php echo form_error('supervisor'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="team_leader"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_teamleader');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="team_leader" name="team_leader" type="text" placeholder="<?php echo $this->lang->line('admin_users_teamleader');?>" value="<?php echo set_value('team_leader',$this->input->post('team_leader'));?>">
                    <p class="text-danger"><?php echo form_error('team_leader'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_estado_actual"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_estado_actual');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_estado_actual" id="telet_estado_actual" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_estado_actual'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_persona_riesgo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_persona_riesgo');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_persona_riesgo" id="telet_persona_riesgo" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_persona_riesgo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_fliares_cargo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_fliares_cargo');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_fliares_cargo" id="telet_fliares_cargo" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_fliares_cargo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_pc_nb_propia"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_pc_nb_propia" id="telet_pc_nb_propia" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_pc_nb_propia'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_elige_operar_site"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_elige_operar_site');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_elige_operar_site" id="telet_elige_operar_site" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_elige_operar_site'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_cuartil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_cuartil');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_cuartil" name="perform_cuartil" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_cuartil');?>" value="<?php echo set_value('perform_cuartil',$this->input->post('perform_cuartil'));?>">
                    <p class="text-danger"><?php echo form_error('perform_cuartil'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_presentismo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_presentismo');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_presentismo" name="perform_presentismo" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_presentismo');?>" value="<?php echo set_value('perform_presentismo',$this->input->post('perform_presentismo'));?>">
                    <p class="text-danger"><?php echo form_error('perform_presentismo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_prioridad_empleado"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_prioridad_empleado" name="perform_prioridad_empleado" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?>" value="<?php echo set_value('perform_prioridad_empleado',$this->input->post('perform_prioridad_empleado'));?>">
                    <p class="text-danger"><?php echo form_error('perform_prioridad_empleado'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_calidad"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_calidad');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_calidad" name="perform_calidad" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_calidad');?>" value="<?php echo set_value('perform_calidad',$this->input->post('perform_calidad'));?>">
                    <p class="text-danger"><?php echo form_error('perform_calidad'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_operators_create');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/operators" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#fecha_nacimiento').datepicker({
        language 		: 'es',
        autoclose 		: true,
        todayHighlight 	: true,
        todayBtn 		: "linked"
    });
    })
</script>