<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo  $this->lang->line('main_operators') ?></h3>
    </div>
    <?php if ($this->Identity_model->Validate('operators/admin')) { ?>
        <div class="card-body">
            <a href="/<?php echo FOLDERADD; ?>/operators/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_operators_create');?></strong></a>
        </div>
    <?php } ?>
    <div class="card-body">

        <?php if(isset($_SESSION['operatorMessage']))
        { ?>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-check"></i></strong> 
                <?php if ($_SESSION['operatorMessage'] == 'create'){
                    echo $this->lang->line('admin_operators_successmessage');
                }
                elseif ($_SESSION['operatorMessage'] == 'edit'){
                    echo $this->lang->line('admin_operators_editmessage');
                }
                elseif ($_SESSION['operatorMessage'] == 'delete'){
                    echo $this->lang->line('admin_operators_deletemessage');
                }

                ?>
            </div>
        <?php } ?>

        <?php if(isset($_SESSION['deleteMessage']))
        { ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-skull"></i></strong> 
                <?php if ($_SESSION['deleteMessage'] == 'selfDelete'){
                    echo $this->lang->line('admin_operators_selfDelete');
                }
                ?>
            </div>
        <?php } ?>
        <table class="table table-hover" id="operators">
            <thead>
                <tr class="active">

                    <th><?php echo $this->lang->line('sales_dni');?></th>
                    <!-- <th><?php echo $this->lang->line('operator_id_lote');?></th>          -->
                    <!-- <th><?php echo $this->lang->line('operator_id_interno');?></th>       -->
                    <th><?php echo $this->lang->line('admin_operators_empresa');?></th>  
                    <th><?php echo $this->lang->line('operator_legajo');?></th>          
                    <th><?php echo $this->lang->line('operator_cuenta');?></th>          
                    <th><?php echo $this->lang->line('operator_usuario');?></th>         
                    <th><?php echo $this->lang->line('general_name');?></th>
                    <th><?php echo $this->lang->line('admin_users_lastName');?></th>
                    <th><?php echo $this->lang->line('admin_operators_celular');?></th>  
                    <th><?php echo $this->lang->line('admin_operators_domicilio');?></th>
                    <th><?php echo $this->lang->line('admin_users_turn');?></th>
                    <!-- <th><?php echo $this->lang->line('admin_operators_contacto');?></th> 
                    <th><?php echo $this->lang->line('admin_operators_prioridad');?></th>
                    <th><?php echo $this->lang->line('admin_operators_pc_de_cat');?></th> -->
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script>
    $(function () {
        $('#operators').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"Operators/getOperators", // json datasource
                data: function(d){
                    
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
               "sProcessing":     "Procesando...",
               "sLengthMenu":     "Mostrar _MENU_ usuarios",
               "sZeroRecords":    "<i class='fa fa-operators'></i> No se encontraron resultados",
               "sEmptyTable":     "Ningún dato disponible en esta tabla",
               "sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
               "sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
               "sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
               "sInfoPostFix":    "",
               "sSearch":         "Buscar:",
               "sUrl":            "",
               "sInfoThousands":  ",",
               "sLoadingRecords": "Cargando...",
               "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "oAria": {
                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
              }
            },
            "columnDefs": [ 
                {
                    "targets": 10,
                    "data": null,
                    "render": function (data,type,row,meta) {
                        return  `
                                <a href="/<?php echo FOLDERADD; ?>/operators/view/${data[10]}" title="<?php echo  $this->lang->line('general_details') ?>"><i class="fas fa-search"></i></a> &nbsp;
                                <a href="/<?php echo FOLDERADD; ?>/operators/edit/${data[10]}" title="<?php echo  $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a> &nbsp;
                                <a href="/<?php echo FOLDERADD; ?>/operators/delete/${data[10]}" title="<?php echo  $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a> &nbsp;`
                    }
                    // defaultContent: 
                }
            ]
      });
    })
</script>