    <?php $this->load->view('_shared/_admin_nav.php') ?>
    <?php if($type == "delete") { ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_areyousure_delete_user')); ?>
        </div>
    <?php } ?>    
    
    <div class="card">
        <div class="card-header">
            <h2><?php echo $this->lang->line('admin_users_details') ?></h2>
        </div>
        
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('sales_dni');?></dt>
                    <dd><?php echo $operator->dni ?></<dd>
                    <dt><?php echo $this->lang->line('general_name');?></dt>
                    <dd><?php echo $operator->nombre ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_lastName');?></dt>
                    <dd><?php echo $operator->apellido ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_celular');?></dt>  
                    <dd><?php echo $operator->celular ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_email');?></dt>  
                    <dd><?php echo $operator->email ?></<dd>
                    <dt><?php echo $this->lang->line('operator_id_lote');?></dt>
                    <dd><?php echo $operator->idlote ?></<dd>
                    <dt><?php echo $this->lang->line('operator_id_interno');?></dt>
                    <dd><?php echo $operator->idinterno ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_empresa');?></dt>  
                    <dd><?php echo $operator->empresa ?></<dd>
                    <dt><?php echo $this->lang->line('operator_legajo');?></dt>          
                    <dd><?php echo $operator->legajo ?></<dd>
                    <dt><?php echo $this->lang->line('operator_cuenta');?></dt>          
                    <dd><?php echo $operator->cuenta ?></<dd>
                    <dt><?php echo $this->lang->line('operator_usuario');?></dt>         
                    <dd><?php echo $operator->usuario ?></<dd>
                </dl>
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('admin_operators_province');?></dt>
                    <dd><?php echo $operator->provincia ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_location');?></dt>
                    <dd><?php echo $operator->localidad ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_street');?></dt>
                    <dd><?php echo $operator->calle ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_postal_code');?></dt>
                    <dd><?php echo $operator->codigo_postal ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_floor_dept');?></dt>
                    <dd><?php echo $operator->piso_depto ?></<dd>
                    <dt><?php echo $this->lang->line('admin_users_turn');?></dt>
                    <dd><?php echo $operator->turno ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_contacto');?></dt> 
                    <dd><?php echo $operator->contacto ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_prioridad');?></dt>
                    <dd><?php echo $operator->prioridad ?></<dd>
                    <dt><?php echo $this->lang->line('admin_operators_pc_de_cat');?></dt>
                    <dd><?php echo $operator->pc_de_cat ?></<dd>
                </dl>
            </div>

            <?php if(count($supplies)>0){ ?>
            <div class="col-md-12">
                <h1><?php echo $this->lang->line('admin_shipments_supplies_assigned_currently') ?></h1>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('admin_supplies_numero_serie') ?></th>
                            <th><?php echo $this->lang->line('general_name') ?></th>
                            <th><?php echo $this->lang->line('category') ?></th>
                            <th><?php echo $this->lang->line('site') ?></th>
                            <th><?php echo $this->lang->line('admin_supplies_box') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for ($i=0; $i < count($supplies); $i++) { 
                                $supply = $supplies[$i]; ?>
                                <tr class="optionsUser">
                                    <td><?= "<a href=/".FOLDERADD."/supplies/view/$supply->supplyId />"?><?php echo $supply->numero_serie ?></td>
                                    <td><?php echo $supply->nombre ?></td>
                                    <td><?php echo $supply->category ?></td>
                                    <td><?php echo $supply->site ?></td>
                                    <td><?php echo $supply->box ?></td>
                                </tr>
                        <?php   } ?>
                    </tbody>
                </table>
            </div>
            <?php } ?>

            <?php if(count($records)>0){ ?>
            <div class="col-md-12">
                <h1><?php echo $this->lang->line('admin_supplies_record') ?></h1>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('admin_shipments_date') ?></th>
                            <th><?php echo $this->lang->line('admin_shipments_type') ?></th>
                            <th><?php echo $this->lang->line('admin_supplies_numero_serie') ?></th>
                            <th><?php echo $this->lang->line('general_name') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            for ($i=0; $i < count($records); $i++) { 
                                $record = $records[$i] ?>
                                <tr class="optionsUser">
                                    <td><?php echo $record->date ?></td>
                                    <td><?php echo $record->type ?></td>
                                    <td><?php echo $record->numero_serie ?></td>
                                    <td><?php echo $record->nombre ?></td>
                                </tr>
                        <?php   } ?>
                    </tbody>
                </table>
            </div>
            <?php } ?>
            
            <?php if($type == "delete") { ?>
                <div class="row">
                    <div class="mx-auto mt-3">
                        <form method="POST">
                            <input type="hidden" name="operatorId" value="<?php echo $operator->operatorId?>">
                            <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                            <a href="/<?php echo FOLDERADD; ?>/operators" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php if($type == "delete") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#delete").addClass("active");
        	})
        </script>
    <?php } ?>
    <?php if($type == "view") { ?>
        <script type="text/javascript">
        	$(function() {
        		$("#view").addClass("active");
        	})
        </script>
        <?php } ?>