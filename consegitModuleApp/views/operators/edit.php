<?php $this->load->view('_shared/_admin_nav.php') ?>
<div class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_operators_edit');?></h3>
    </div>
    <div class="card-body">
        <form method="POST">
            <div class="form-group row">
                <label class="col-md-2" for="id_lote"><span class="font-weight-bold"><?php echo $this->lang->line('operator_id_lote');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="id_lote" name="id_lote" type="text" placeholder="<?php echo $this->lang->line('operator_id_lote');?>" value="<?php echo $operator->idlote;?>">
                    <p class="text-danger"><?php echo form_error('id_lote'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="id_interno"><span class="font-weight-bold"><?php echo $this->lang->line('operator_id_interno');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="id_interno" name="id_interno" type="text" placeholder="<?php echo $this->lang->line('operator_id_interno');?>" value="<?php echo $operator->idinterno;?>">
                    <p class="text-danger"><?php echo form_error('id_interno'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="empresa"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_empresa');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="empresa" name="empresa" type="text" placeholder="<?php echo $this->lang->line('admin_operators_empresa');?>" value="<?php echo $operator->empresa;?>">
                    <p class="text-danger"><?php echo form_error('empresa'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="legajo"><span class="font-weight-bold"><?php echo $this->lang->line('operator_legajo');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="legajo" name="legajo" type="text" placeholder="<?php echo $this->lang->line('operator_legajo');?>" value="<?php echo $operator->legajo;?>">
                    <p class="text-danger"><?php echo form_error('legajo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="dni"><span class="font-weight-bold"><?php echo $this->lang->line('sales_dni');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="dni" name="dni" type="text" placeholder="<?php echo $this->lang->line('sales_dni');?>" value="<?php echo $operator->dni;?>">
                    <p class="text-danger"><?php echo form_error('dni'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="cuenta"><span class="font-weight-bold"><?php echo $this->lang->line('operator_cuenta');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="cuenta" name="cuenta" type="text" placeholder="<?php echo $this->lang->line('operator_cuenta');?>" value="<?php echo $operator->cuenta;?>">
                    <p class="text-danger"><?php echo form_error('cuenta'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="usuario"><span class="font-weight-bold"><?php echo $this->lang->line('operator_usuario');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="usuario" name="usuario" type="text" placeholder="<?php echo $this->lang->line('operator_usuario');?>" value="<?php echo $operator->usuario;?>">
                    <p class="text-danger"><?php echo form_error('usuario'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="nombre"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="nombre" name="nombre" type="text" placeholder="<?php echo $this->lang->line('general_name');?>" value="<?php echo $operator->nombre;?>">
                    <p class="text-danger"><?php echo form_error('nombre'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="apellido"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_lastName');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="apellido" name="apellido" type="text" placeholder="<?php echo $this->lang->line('admin_users_lastName');?>" value="<?php echo $operator->apellido;?>">
                    <p class="text-danger"><?php echo form_error('apellido'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="celular"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_celular');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="celular" name="celular" type="text" placeholder="<?php echo $this->lang->line('admin_operators_celular');?>" value="<?php echo $operator->celular;?>">
                    <p class="text-danger"><?php echo form_error('celular'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="email"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_email');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="email" name="email" type="text" placeholder="<?php echo $this->lang->line('admin_operators_email');?>" value="<?php echo $operator->email;?>">
                    <p class="text-danger"><?php echo form_error('email'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="province"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_province');?>:</span><span class="text-danger"><strong>* </strong></span></label>
                <div class="col-md-10">
                    <select name="province" id="province" class="form-control">
                        <?php 
                        foreach ($provinces as $province) { ?>
                            <option value="<?php echo $province ?>"  <?php echo set_select('province', $province, ($operator->provincia == $province));?> ><?php echo  $province ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('province'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="location"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_location');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="location" name="location" type="text" placeholder="<?php echo $this->lang->line('admin_operators_location');?>" value="<?php echo $operator->localidad;?>">
                    <p class="text-danger"><?php echo form_error('location'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="street"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_street');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="street" name="street" type="text" placeholder="<?php echo $this->lang->line('admin_operators_street');?>" value="<?php echo $operator->calle;?>">
                    <p class="text-danger"><?php echo form_error('street'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="height"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_height');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="height" name="height" type="text" placeholder="<?php echo $this->lang->line('admin_operators_height');?>" value="<?php echo $operator->altura;?>">
                    <p class="text-danger"><?php echo form_error('height'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="postal_code"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_postal_code');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="postal_code" name="postal_code" type="text" placeholder="<?php echo $this->lang->line('admin_operators_postal_code');?>" value="<?php echo $operator->codigo_postal;?>">
                    <p class="text-danger"><?php echo form_error('postal_code'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="floor_dept"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_floor_dept');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="floor_dept" name="floor_dept" type="text" placeholder="<?php echo $this->lang->line('admin_operators_floor_dept');?>" value="<?php echo $operator->piso_depto;?>"/>
                    <p class="text-danger"><?php echo form_error('floor_dept'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="turno"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_turn');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="turno" id="turno" class="form-control">
                        <?php 
                        foreach ($turns as $turn) { ?>
                            <option value="<?php echo $turn ?>"  <?php echo set_select('turn', $turn, ($operator->turno == $turn));?> ><?php echo  $turn ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('admin_users_turn'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="contacto"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_contacto');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="contacto" name="contacto" type="text" placeholder="<?php echo $this->lang->line('admin_operators_contacto');?>" value="<?php echo $operator->contacto;?>">
                    <p class="text-danger"><?php echo form_error('contacto'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="prioridad"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_prioridad');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="prioridad" name="prioridad" type="text" placeholder="<?php echo $this->lang->line('admin_operators_prioridad');?>" value="<?php echo $operator->prioridad;?>">
                    <p class="text-danger"><?php echo form_error('prioridad'); ?></p>
                </div>
            </div>
        
            <div class="form-group row">
                <label class="col-md-2" for="pc_de_cat"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_pc_de_cat');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="pc_de_cat" id="pc_de_cat" class="form-control">
                        <option value="Si">Si</option>
                        <option value="No">No</option>
                    </select>
                    <!-- <input class="form-control" id="pc_de_cat" name="pc_de_cat" type="text" placeholder="<?php echo $this->lang->line('admin_operators_pc_de_cat');?>" value="<?php echo set_value('pc_de_cat',$this->input->post('pc_de_cat'));?>"> -->
                    <p class="text-danger"><?php echo form_error('pc_de_cat'); ?></p>
                </div>
            </div>

            <hr /> 

            <div class="form-group row">
                <label class="col-md-2" for="cuil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_cuil');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="cuil" name="cuil" type="text" placeholder="<?php echo $this->lang->line('admin_users_cuil');?>" value="<?= $operator->cuil?>">
                    <p class="text-danger"><?php echo form_error('cuil'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="fecha_nacimiento"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_birthDate');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" type="text" placeholder="<?php echo $this->lang->line('admin_users_birthDate');?>" value="<?php echo $operator->fecha_nacimiento?>">
                    <p class="text-danger"><?php echo form_error('fecha_nacimiento'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="site_original"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_original_site');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="site_original" name="site_original" type="text" placeholder="<?php echo $this->lang->line('admin_users_original_site');?>" value="<?php echo $operator->site_original?>">
                    <p class="text-danger"><?php echo form_error('site_original'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="horario"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_schedule');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="horario" name="horario" type="text" placeholder="<?php echo $this->lang->line('admin_users_schedule');?>" value="<?php echo $operator->horario?>">
                    <p class="text-danger"><?php echo form_error('horario'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="puesto"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_position');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="puesto" name="puesto" type="text" placeholder="<?php echo $this->lang->line('admin_users_position');?>" value="<?php echo $operator->puesto?>">
                    <p class="text-danger"><?php echo form_error('puesto'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="supervisor"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_supervisor');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="supervisor" name="supervisor" type="text" placeholder="<?php echo $this->lang->line('admin_users_supervisor');?>" value="<?php echo $operator->supervisor?>">
                    <p class="text-danger"><?php echo form_error('supervisor'); ?></p>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2" for="team_leader"><span class="font-weight-bold"><?php echo $this->lang->line('admin_users_teamleader');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="team_leader" name="team_leader" type="text" placeholder="<?php echo $this->lang->line('admin_users_teamleader');?>" value="<?php echo $operator->team_leader?>">
                    <p class="text-danger"><?php echo form_error('team_leader'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_estado_actual"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_estado_actual');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_estado_actual" id="telet_estado_actual" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state ?>"  <?php echo set_select('telet_estado_actual', $state, ($operator->telet_estado_actual == $state));?> ><?php echo  $state ?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_estado_actual'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_persona_riesgo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_estado_actual');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_persona_riesgo" id="telet_persona_riesgo" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state ?>"  <?php echo set_select('telet_persona_riesgo', $state, ($operator->telet_persona_riesgo == $state));?> ><?php echo  $state ?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_persona_riesgo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_fliares_cargo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_fliares_cargo');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_fliares_cargo" id="telet_fliares_cargo" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state ?>"  <?php echo set_select('telet_fliares_cargo', $state, ($operator->telet_fliares_cargo == $state));?> ><?php echo  $state ?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_fliares_cargo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_pc_nb_propia"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_pc_nb_propia');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_pc_nb_propia" id="telet_pc_nb_propia" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state ?>"  <?php echo set_select('telet_pc_nb_propia', $state, ($operator->telet_pc_nb_propia == $state));?> ><?php echo  $state ?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_pc_nb_propia'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="telet_elige_operar_site"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_telet_elige_operar_site');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="telet_elige_operar_site" id="telet_elige_operar_site" class="form-control">
                        <option value=""></option>
                    <?php foreach ($states as $state) { ?>
                        <option value="<?php echo $state ?>"  <?php echo set_select('telet_elige_operar_site', $state, ($operator->telet_elige_operar_site == $state));?> ><?php echo  $state ?></option>
                    <?php } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('telet_elige_operar_site'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_cuartil"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_cuartil');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_cuartil" name="perform_cuartil" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_cuartil');?>" value="<?php echo $operator->perform_cuartil?>">
                    <p class="text-danger"><?php echo form_error('perform_cuartil'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_presentismo"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_presentismo');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_presentismo" name="perform_presentismo" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_presentismo');?>" value="<?php echo $operator->perform_presentismo?>">
                    <p class="text-danger"><?php echo form_error('perform_presentismo'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_prioridad_empleado"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_prioridad_empleado" name="perform_prioridad_empleado" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_prioridad_empleado');?>" value="<?php echo $operator->perform_prioridad_empleado?>">
                    <p class="text-danger"><?php echo form_error('perform_prioridad_empleado'); ?></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-2" for="perform_calidad"><span class="font-weight-bold"><?php echo $this->lang->line('admin_operators_perform_calidad');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" id="perform_calidad" name="perform_calidad" type="text" placeholder="<?php echo $this->lang->line('admin_operators_perform_calidad');?>" value="<?php echo $operator->perform_calidad?>">
                    <p class="text-danger"><?php echo form_error('perform_calidad'); ?></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_operators_edit');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/operators" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#edit').addClass('active');
    }); 
</script>

