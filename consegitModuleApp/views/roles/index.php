		<div class="card">
			<div class="card-header">
				<h3><?php echo $this->lang->line('main_roles') ?></h3>
			</div>
			<?php if ($this->Identity_model->Validate('roles/admin')){ ?>
				<div class="card-body">
					<a class="btn btn-sm btn-outline-success" href="/<?php echo FOLDERADD; ?>/roles/create">
						<i class="fas fa-plus"></i>
						<strong><?php echo $this->lang->line('role_create') ?></strong>
					</a>
				</div>
			<?php } ?>
			<div class="card-body">
				<?php if(isset($_SESSION['role'])) { ?>
					<div class="alert alert-info alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong><i class="fas fa-check"></i></strong> 
					 	<?php if($_SESSION['role'] == 'create') { ?>
					 		<span>
					 			<?php echo  $this->lang->line('role_create_success') ?>
					 		</span>
					 	<?php }
					 	elseif($_SESSION['role'] == 'edit') {?>
					 		<span>
					 			<?php echo  $this->lang->line('general_edit_success') ?>
					 		</span>
					 	<?php } 
					 	elseif($_SESSION['role']=='delete') { ?>
					 		<span>
					 			<?php echo  $this->lang->line('general_delete_success') ?>
					 		</span>
					 	<?php } ?>
					</div>
				<?php } ?>
				<table id="role_table" class="table table-hover">
					<thead>
						<tr>
							<th><?php echo $this->lang->line('general_name') ?></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($roles as $role) {?>
							<tr class="optionsUser">
								<td>
									<?php echo $role->name ?>
								</td>
								<td class="text-right">
									<?php if ($this->Identity_model->Validate('roles/admin')){ ?>
										<a href="/<?php echo FOLDERADD; ?>/roles/view/<?php echo $role->roleId ?>" title="<?php echo  $this->lang->line('general_details') ?>"><i class="fas fa-search"></i></a> &nbsp;
										<a href="/<?php echo FOLDERADD; ?>/roles/edit/<?php echo $role->roleId ?>" title="<?php echo  $this->lang->line('general_edit') ?>"><i class="fas fa-edit text-warning"></i></a> &nbsp;
										<a href="/<?php echo FOLDERADD; ?>/roles/view/<?php echo $role->roleId ?>" title="<?php echo  $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a> &nbsp;
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(function(){
			$('#role_table').DataTable({
				"language": {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ Perfiles",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando Perfiles del _START_ al _END_ de un total de _TOTAL_ Perfiles",
					"sInfoEmpty":      "Mostrando Perfiles del 0 al 0 de un total de 0 Perfiles",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ Perfiles)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			});
		});
	</script>