	<?php $this->load->view('_shared/_admin_nav.php') ?>
	<div class="alert alert-danger">
		<strong><i class="fas fa-skull"></i></strong>
		<?php echo($this->lang->line('role_delete_confirm')); ?>
	</div>
	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo $this->lang->line('role_view_title')?>
			</h3>
		</div>
		<div class="card-body">
			<form id="createForm" method="POST" novalidate action="/<?php echo FOLDERADD; ?>/roles/deleteRole/<?php echo $role->roleId; ?>">
				<div class="form-group row justify-content-center">
					<label class="col-sm-7 col-form-label">
						<span class="font-weight-bold">
							<?php echo $this->lang->line('general_name')?>:&nbsp; &nbsp; 
						</span>
						<span>
							<?php echo $role->name ?>
						</span>
					</label>
				</div>
				
				<div class="form-group row  d-flex justify-content-center">

				<label class="col-form-label"><span class="font-weight-bold" style="padding-left:16px;"><?php echo $this->lang->line('role_permissions')?>:&nbsp;</span></label>

				<div class="col-sm-6">
					<input type="text" id="searchInput" placeholder="<?php echo $this->lang->line('general_search');?>" class="form-control" maxlength="100">
					<?php 
					if ( isset($role->sizeOfPermissions) && $role->sizeOfPermissions == '0') { ?>
						<span class="text-left"><?php echo $this->lang->line('no_permissions') ?></span><br>
					<?php }
					else {
						
						foreach ($role->rolepermissions as $key=>$rolePermission){

							if (isset($rolePermission->rolePermissionId)) {?>

								<span id="permission_<?php echo $key; ?>" class="text-center" style="display: flex;">
									<?php echo $rolePermission->description; ?>
								</span>

								<?php 
							} 
						}
					}?>
				</div>

			</div>
				
				<div class="form-group row d-none">
					<input type="number" name="roleId" value="<?php echo $role->roleId ?>">
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_delete') ?></button>
					<a class="btn btn-danger" href="/<?php echo FOLDERADD; ?>/roles"><?php echo $this->lang->line('general_cancel') ?></a>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#delete').addClass('active');
		$('#searchInput').on('keyup paste',function () {

			var searchArray = <?php echo json_encode($role->rolepermissions); ?>;
	
			for(var i = 0 ; i < searchArray.length ; i++){
	
				$('#permission_'+i).css('display','none');
	
				if (searchArray[i].description.toLowerCase().indexOf($('#searchInput').val().toLowerCase()) != -1){
	
					$('#permission_'+i).css('display','flex');
				}
			}
		})
	})
</script>

