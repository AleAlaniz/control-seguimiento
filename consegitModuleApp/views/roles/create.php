	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo $this->lang->line('role_create') ?>
			</h3>
		</div>
		<div class="card-body">
			<form id="createForm" method="POST" novalidate action="createRole">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label"><span class="font-weight-bold"><?php echo $this->lang->line('general_name')?>:&nbsp;<i class="text-danger">*</i></span></label>
					<div class="col-sm-10">
						<input type="text" name="name" placeholder="<?php echo $this->lang->line('general_name')?>" class="form-control" required maxlength="100">
						<?php echo form_error('name'); ?>
					</div>
				</div>

				<div class="form-group row">
					<label class="col-sm-2 col-form-label"><span class="font-weight-bold"><?php echo $this->lang->line('role_permissions')?>:&nbsp;</span></label>

					<div class="col-sm-10">
						<input type="text" id="searchInput" placeholder="<?php echo $this->lang->line('general_search')?>" class="form-control" maxlength="100">
						<?php $index = 0;
						foreach ($permissions as $permission) { ?>
							<div class="form-check" id="check<?php echo $index;?>">
								<input class="form-check-input" type="checkbox" name="active[]" value="<?php echo $permission->permissionId ?>" id="check.<?php echo $index;?>"> 
								<label class="form-check-label" for="check.<?php echo $index;?>">
									<?php echo $permission->description ?>
								</label>
								<br>
							</div>
							<?php $index = $index + 1; 
						} ?>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_save') ?></button>
					<a class="btn btn-danger" href="/<?php echo FOLDERADD; ?>/roles"><?php echo $this->lang->line('general_cancel') ?></a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#searchInput').on('keyup paste',function () {

		var searchArray = <?php echo json_encode($permissions); ?>;

		for(var i = 0 ; i < searchArray.length ; i++){

			$('#check'+i).css('display','none');

			if (searchArray[i].description.toLowerCase().indexOf($('#searchInput').val().toLowerCase()) != -1){

				$('#check'+i).css('display','block');
			}
		}
	})
</script>