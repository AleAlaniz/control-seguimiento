	<?php $this->load->view('_shared/_admin_nav.php') ?>
	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo $this->lang->line('general_edit_title') ?>
			</h3>
		</div>
		<div class="card-body">
			<div class="container">
				<form id="createForm" method="POST" novalidate action="/<?php echo FOLDERADD; ?>/roles/editRole/<?php echo $role->roleId; ?>">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><span class="font-weight-bold"><?php echo $this->lang->line('general_name')?><i class="text-danger">*</i>&nbsp;:</span></label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" required maxlength="100" value ="<?php echo $role->name; ?>"> 
							<?php echo form_error('name'); ?>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label"><span class="font-weight-bold"><?php echo $this->lang->line('role_permissions')?>&nbsp;:</span></label>
						<div class="col-sm-10">
							<input type="text" id="searchInput" placeholder="<?php echo $this->lang->line('general_search')?>" class="form-control" maxlength="100">
							<?php $index = 0; 
							foreach ($role->rolepermissions as $rolePermission){ 
								if (isset($rolePermission->rolePermissionId)) {?>
									<div class="form-check" id="check<?php echo $index;?>">
										<input class="form-check-input" type="checkbox" name="active[]" value="<?php echo $rolePermission->permissionId ?>" id="check.<?php echo $index;?>" checked> 
										<label class="form-check-label" for="check.<?php echo $index;?>">
											<?php echo $rolePermission->description ?>
										</label>
										<br>
									</div>
								<?php }
								else { ?>
									<div class="form-check" id="check<?php echo $index;?>">
										<input class="form-check-input" type="checkbox" name="active[]" value="<?php echo $rolePermission->permissionId ?>" id="check.<?php echo $index;?>">
										<label class="form-check-label" for="check.<?php echo $index;?>">
											<?php echo $rolePermission->description ?>
										</label>
										<br>
									</div>
									<?php  
								}

								$index = $index + 1; 
							}?> 
						</div>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_save') ?></button>
						<a class="btn btn-danger" href="/<?php echo FOLDERADD; ?>/roles"><?php echo $this->lang->line('general_cancel') ?></a>
					</div>
					<div class="form-group row d-none">
						<input type="number" name="roleId" value="<?php echo $role->roleId ?>">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("#edit").addClass("active");
	})
</script>
<script type="text/javascript">
	$('#searchInput').on('keyup paste',function () {

		var searchArray = <?php echo json_encode($role->rolepermissions); ?>;

		for(var i = 0 ; i < searchArray.length ; i++){

			$('#check'+i).css('display','none');

			if (searchArray[i].description.toLowerCase().indexOf($('#searchInput').val().toLowerCase()) != -1){

				$('#check'+i).css('display','block');
			}
		}
	})
</script>