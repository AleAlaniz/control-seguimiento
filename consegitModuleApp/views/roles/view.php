	<?php $this->load->view('_shared/_admin_nav.php') ?>
	<div class="card">
		<div class="card-header">
			<h3>
				<?php echo $this->lang->line('role_view_title')?>
				<button class="btn btn-danger float-right" data-toggle="modal" data-target="#delete-modal">
                    <?php echo($this->lang->line('general_delete')); ?>
                </button>
			</h3>
		</div>
		<div class="card-body">
			<div class="form-group row justify-content-center">
				<label class="col-sm-7 col-form-label">
					<span class="font-weight-bold">
						<?php echo $this->lang->line('general_name')?>:&nbsp; &nbsp; 
					</span>
					<span>
						<?php echo $role->name ?>
					</span>
				</label>
			</div>

			<div class="form-group row  d-flex justify-content-center">

				<label class="col-form-label"><span class="font-weight-bold" style="padding-left:16px;"><?php echo $this->lang->line('role_permissions')?>:&nbsp;</span></label>

				<div class="col-sm-6">
					<input type="text" id="searchInput" placeholder="<?php echo $this->lang->line('general_search');?>" class="form-control" maxlength="100">
					<?php 
					if ( isset($role->sizeOfPermissions) && $role->sizeOfPermissions == '0') { ?>
						<span class="text-left"><?php echo $this->lang->line('no_permissions') ?></span><br>
					<?php }
					else {
						
						foreach ($role->rolepermissions as $key=>$rolePermission){

							if (isset($rolePermission->rolePermissionId)) {?>

								<span id="permission_<?php echo $key; ?>" class="text-center" style="display: flex;">
									<?php echo $rolePermission->description; ?>
								</span>

								<?php 
							} 
						}
					}?>
				</div>

			</div>
		</div>	
	</div>
</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="delete-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo($this->lang->line('general_delete_title')); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('role_delete_confirm')); ?>
        </div>
      <div class="modal-footer">
        <form method="post" action="/<?php echo FOLDERADD; ?>/Roles/deleteRole/<?php echo $role->roleId ?>">
            <input type="hidden" name="roleId" value="<?php echo $role->roleId ?>">
            <div class="text-center">
                <button type="submit" class="btn btn-success"><?php echo($this->lang->line('general_delete')) ?></button>
                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close"><?php echo($this->lang->line('general_cancel')) ?></button>
                <?php echo(form_error('campaign_id'))?>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#view').addClass('active');

		$('#searchInput').on('keyup paste',function () {

		var searchArray = <?php echo json_encode($role->rolepermissions); ?>;

		for(var i = 0 ; i < searchArray.length ; i++){

			$('#permission_'+i).css('display','none');

			if (searchArray[i].description.toLowerCase().indexOf($('#searchInput').val().toLowerCase()) != -1){

				$('#permission_'+i).css('display','flex');
			}
		}
	})
	})
</script>
