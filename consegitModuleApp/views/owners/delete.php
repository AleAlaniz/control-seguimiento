    <?php $this->load->view('_shared/_admin_nav.php') ?>
        <div class="alert alert-danger">
            <strong><i class="fas fa-skull"></i></strong>
            <?php echo($this->lang->line('admin_owners_areyousure_delete')); ?>
        </div>
    
    <div class="card">
        <div class="card-header">
            <h3><?php echo $this->lang->line('admin_owners_details') ?></h3>
        </div>
        <div class="card-body">
            <div class="row col-md-12">
                <dl class="col-md-6">
                    <dt><?php echo $this->lang->line('general_name');?> : </dt>
                    <dd><?php echo $owner->name ?></dd>
                </dl>
            </div>
            <div class="row">
                <div class="mx-auto mt-3">
                    <form method="POST">
                        <input type="hidden" name="ownerId" value="<?php echo $owner->ownerId?>">
                        <button class="btn btn-success"><?php echo  $this->lang->line('general_delete') ?></button>
                        <a href="<?php echo (base_url()) ?>users" class="btn btn-danger"><?php echo  $this->lang->line('general_cancel') ?></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    	$(function() {
    		$('#delete').addClass('active');
    	})
    </script>