<div class="card">
    <div class="card-header">
        <h2><?php echo $this->lang->line('admin_shipments_details') ?></h2>
    </div>
    
    <div class="card-body">
        <div class="row col-md-12">
            <dl class="col-md-6">
                <dt><?php echo $this->lang->line('admin_shipments_number');?></dt>
                <dd><?php echo $shipment->shipmentId ?></<dd>
                <dt><?php echo $this->lang->line('admin_shipments_shipmentDate');?></dt>
                <dd><?php echo $shipment->shipmentDate ?></<dd>
                <dt><?php echo $this->lang->line('admin_shipments_deliveryDate');?></dt>
                <dd><?php echo $shipment->deliveryDate ?></<dd>
                <dt><?php echo $this->lang->line('admin_shipments_type');?></dt>  
                <dd><?php echo $shipment->type ?></<dd>
                <dt><?php echo $this->lang->line('admin_shipments_state');?></dt>          
                <dd><?php echo $shipment->state ?></<dd>
                <dt><?php echo $this->lang->line('admin_operators_completename');?></dt>          
                <dd><?php echo $shipment->completeName ?></<dd>
            </dl>
            <!-- <dl class="col-md-6">
                <h3><?php echo $this->lang->line('main_supplies');?></h3>
                <?php 
                    for ($i=0; $i < count($shipment->supplies); $i++) {  ?>
                        <dt><?php echo $this->lang->line('admin_supplies_numero_serie');?></dt>          
                        <dd><?php echo $shipment->supplies[$i]->numero_serie ?></<dd>
                        <dt><?php echo $this->lang->line('general_name');?></dt>          
                        <dd><?php echo $shipment->supplies[$i]->nombre ?></<dd>
                <?php   }  ?>
            </dl> -->
        </div>
        <?php if(count($shipment->supplies)>0){ ?>
            <div class="col-md-12">
                <h1><?php echo $this->lang->line('admin_shipments_supplies_assigned_currently') ?></h1>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('admin_supplies_numero_serie') ?></th>
                            <th><?php echo $this->lang->line('general_name') ?></th>
                            <th><?php echo $this->lang->line('category') ?></th>
                            <th><?php echo $this->lang->line('site') ?></th>
                            <th><?php echo $this->lang->line('admin_supplies_box') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            for ($i=0; $i < count($shipment->supplies); $i++) { 
                                $supply = $shipment->supplies[$i]; ?>
                                <tr class="optionsUser">
                                    <td><?= "<a href=/".FOLDERADD."/supplies/view/$supply->supplyId />"?><?php echo $supply->numero_serie ?></td>
                                    <td><?php echo $supply->nombre ?></td>
                                    <td><?php echo $supply->category ?></td>
                                    <td><?php echo $supply->site ?></td>
                                    <td><?php echo $supply->box ?></td>
                                </tr>
                        <?php   } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
</div>