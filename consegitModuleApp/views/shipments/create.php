<div ng-app="controlSeguimiento" ng-controller="Cshipments" class="card">
    <div class="card-header">
        <h3><?php echo $this->lang->line('admin_shipments_create');?></h3>
    </div>
    <div class="card-body">
        <form method="POST" ng-submit="submitShipment()">
            <input type="hidden" ng-init="delivery = '<?= getShipmentType()[0] ?>'" ng-model="delivery">
            <input type="hidden" ng-init="refund = '<?= getShipmentType()[1] ?>'" ng-model="refund">
            
            <div class="form-group row">
                <label class="col-md-2" for="type"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_type');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="type" id="type" class="form-control" ng-model="shipmentType" ng-change="selectShipmentType()">
                        <?php 
                        foreach ($types as $type) { ?>
                            <option value="<?php echo $type ?>"  <?php echo set_select('type',$type,($type == $this->input->post('type')))?> ><?php echo  $type ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('type'); ?></p>
                </div>
            </div>

            <!-- busqueda de usuarios -->
            <div class="form-group row" ng-show="shipmentType == delivery" id="searchBody" >
                <label class="col-md-2" for="input_search">
                    <span class="font-weight-bold">
                        <?php echo $this->lang->line('admin_shipments_operators');?>:
                    </span>
                </label>
                <div class="col-md-10">
                    <input type="text" name="input_search" class="form-control" placeholder="<?php echo($this->lang->line('general_search')) ?>" id="input_search" autocomplete="off"></input>
                    <?php echo(form_error('user_operators[]'))?>
                    <span class="fas fa-sync fa-spin cat-inside-input text-primary" style="display: none" id="loadingSpinner"></span>
                    <ul class="list-group" id="search_result">
                        <li ng-repeat="operator in filtered" class="list-group-item cat-pointer" name="searchOption" ng-click="addOperator(operator)">{{operator.name}}</li>
                    </ul>  
                </div>
            </div>
            <div class="form-group row" ng-show="shipmentType == delivery" id="assignedUsersBody">
                <div class="col-sm-2">
                    <label class="font-weight-bold"><?php echo($this->lang->line('admin_shipments_operators_assigned')) ?></label>
                </div>
                <div class="col-sm-10" id="operators_added_show">
                    <span ng-repeat="operator in operatorsAdded" class="badge badge-info" style="margin-right:5px">{{operator.name}}
                        <span name="userSelected" class="badge badge-pill badge-danger btn" ng-click="deleteOperator(operator)">
                            <i class="fas fa-times"></i>
                        </span>
                    </span>
                    <p class="text-danger" ng-show="operatorsAdded.length == 0 && sended"><?= $this->lang->line('admin_shipments_operators_required')?></p>
                </div>
            </div>
            <!-- busqueda de usuarios -->

            <div class="form-group row" ng-show="shipmentType == delivery" >
                <label class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('main_supplies_requests');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <div class="card card-list">
                        <ul class="list-group-custom" id="requests" >
                            <li ng-repeat="request in requestsFinded" ng-click="addRequest(request)" class="addedOperators cat-pointer">{{request.name}}</li>
                            <span ng-show="loadingRequests"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                            <li ng-show="requestsFinded.length == 0 && !loadingRequests"><?= $this->lang->line('request_empty_requests')?></li>
                        </ul>
                    </div>
                    <button class="btn btn-success btn-sm mt-1" id="searchRequest" ng-show="requestsAdded.length > 0" type="button"  ng-click="searchRequests()"><i class="fa fa-plus"></i> <?php echo $this->lang->line('request_request_addextra');?></button>
                    <ul class="list-group-custom">
                        <li ng-repeat="request in requestsSearched">
                            <div class="row">
                                <div class="col-md-4">{{request.supplies}}</div>
                                <div class="col-md-4"><button type="button" class="btn btn-success btn-sm" ng-click="addRequestSearched(request)"><i class="fa fa-plus"></i></button></div>
                            </div>
                        </li>
                        <li ng-show="requestsSearched.length == 0 && !loadingRequests"><?= $this->lang->line('request_empty_requests')?></li>
                    </ul>
                </div>
            </div>
            <div class="form-group row" ng-show="shipmentType == delivery && operatorsAdded.length > 0">
                <label class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('request_supplies_requested');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <div class="card card-list">
                        <div class="form-group row ml-2" ng-repeat="request in requestsAdded">
                            <p class="ml-2">{{request.supplies}}</p>
                            <button type="button" class="btn btn-outline-danger btn-sm mb-2 ml-1" ng-click="deleteRequestSearched(request)"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row" ng-show="shipmentType == delivery" id="categoryBody" >
                <label class="col-md-2" for="categoryId"><span class="font-weight-bold"><?php echo $this->lang->line('category');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <select name="categoryId" id="categoryId" class="form-control" ng-model="category" ng-change="changedCategory()">
                        <?php 
                        foreach ($categories as $category) { ?>
                            <option value="<?php echo $category->categoryId ?>"  <?php echo set_select('category',$category->categoryId,($category->categoryId == $this->input->post('categoryId')))?> ><?php echo  $category->name ?></option>
                        <?php   } ?>
                    </select>
                    <p class="text-danger"><?php echo form_error('categoryId'); ?></p>
                </div>
            </div>

            <div class="form-group row" ng-show="shipmentType == delivery" id="suppliesSearch" >
                <label class="col-md-2" for="supplyId"><span class="font-weight-bold"><?php echo $this->lang->line('main_supplies');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <div class="card card-list refundInput" id="supplyList">
                        <ul class="list-group-custom" id="supplies">
                            <li ng-repeat="supply in suppliesFinded" value="supply.supplyId">
                                <div class="row">
                                    <div class="col-md-4">{{supply.nombre}} ({{supply.numero_serie}})-{{supply.category}}</div>
                                    <div class="col-md-4"><button type="button" class="btn btn-success btn-sm" ng-click="addSupply(supply)"><i class="fa fa-plus"></i></button></div>
                                </div>
                            </li>
                            <li ng-show="suppliesFinded.length == 0 && !loadingSupplies"><?php echo $this->lang->line('admin_empty_supplies');?></li>
                            <span ng-show="loadingSupplies"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                        </ul>
                    </div>
                    <p class="text-danger" ng-show="suppliesAdded.length == 0 && sended"><?= $this->lang->line('admin_shipments_operators_required')?></p>
                </div>
            </div>

            <div class="form-group row" ng-show="shipmentType == delivery" id="suppliesShow" >
                <div class="card offset-md-2 col-md-8">
                    <h6 class="card-header">
                        <?php echo $this->lang->line('admin_shipments_supplies_added')?>
                    </h6>
                    <div class="card-body text-left deliveryInput" id="suppliesAddedContainer">
                        <div class="form-group row added" ng-repeat ="supply in suppliesAdded">
                            <label class="col-md-4"><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?></span></label>
                            <p>{{supply.nombre}}</p>
                            <label class="col-md-4"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_numero_serie');?></span></label>
                            <p class="ml-1">{{supply.numero_serie}}</p>
                            <input type="hidden" name="suppliesAdded[]" value="${supply[0]}"/>
                            <button type="button" class="btn btn-danger btn-sm" ng-click="deleteSupply(supply)"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <p class="text-danger" ng-show="suppliesAdded.length == 0 && sended"><?= $this->lang->line('admin_shipments_operators_required')?></p>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" id="siteBody" >
                <label class="col-md-2" for="siteId"><span class="font-weight-bold"><?php echo $this->lang->line('site');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <select name="siteId" id="siteId" class="form-control">
                        <option value=" "></option>
                        <?php 
                        foreach ($sites as $site) { ?>
                            <option value="<?php echo $site->siteId ?>"  <?php echo set_select('siteId',$site->siteId,($site->siteId == $this->input->post('siteId')))?> ><?php echo  $site->name ?></option>
                        <?php   } ?>
                    </select>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund && boxes.length > 0">
                <label class="col-md-2" for="boxId"><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_box');?>:</span><span class="text-danger"><strong></strong></span></label>
                <div class="col-md-10">
                    <select ng-show="!loadingBoxes" name="boxId" id="boxId" class="form-control refundInput" ng-model="boxId">
                        <option ng-repeat="box in boxes" value="{{box.boxId}}">{{box.box_name}}</option>
                    </select>
                    <p class="text-danger"><?php echo form_error('boxId'); ?></p>
                </div>
            </div>
            <div class="form-group row" ng-show="loadingBoxes">
                <div class="col-md-10 offset-md-2">
                    <span><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" >
                <label class="col-md-2" for="operatorsList"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipment_operators_with_supplies');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                <div class="col-md-10">
                    <input type="hidden" name="user_operators[]" id="user_operators" value="">
                    <div class="card card-list refundInput" id="operatorsList">
                        <ul class="list-group-custom" id="operators" >
                            <li ng-repeat="operator in operatorsSearched" ng-click="showSupplies(operator)" class="addedOperators cat-pointer" id="operator.operatorId" value="operator.operatorId">{{operator.name}}
                                <input hidden value="operator.operatorId"/>
                            </li>
                            <span ng-show="loadingOperators"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" id="suppliesAssignedShow" >
                <div class="card offset-md-2 col-md-8">
                    <h6 class="card-header">
                        <?php echo $this->lang->line('admin_shipments_supplies_assigned')?>
                    </h6>
                    <div class="card-body text-left refundInput" id="suppliesAssignedContainer">
                        <div class="form-group row" ng-repeat="supply in suppliesFinded">
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('category');?></span></label>
                                <p>{{supply.category}}</p>
                            </div>
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_numero_serie');?></span></label>
                                <p>{{supply.numero_serie}}</p>
                            </div>
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?></span></label>
                                <p>{{supply.nombre}}</p>
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-success btn-sm" ng-click="addSupply(supply)"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <span ng-show="loadingAssignedSupplies"><i class="fas fa-circle-notch fa-spin fa-2x"></i></span>
                    </div>
                </div>
            </div>
            
            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" id="suppliesToRefundShow" >
                <div class="card offset-md-2 col-md-8">
                    <h6 class="card-header">
                        <?php echo $this->lang->line('admin_shipments_supplies_refund')?>
                    </h6>
                    <div class="card-body text-left refundInput" id="suppliesToRefundContainer">
                        <div class="form-group row" ng-repeat="supply in suppliesAdded">
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('category');?></span></label>
                                <p>{{supply.category}}</p>
                            </div>
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('admin_supplies_numero_serie');?></span></label>
                                <p>{{supply.numero_serie}}</p>
                            </div>
                            <div class="col-md-3">
                                <label><span class="font-weight-bold"><?php echo $this->lang->line('general_name');?></span></label>
                                <p>{{supply.nombre}}</p>
                            </div>
                            <div class="col-md-1">
                                <button type="button" class="btn btn-danger btn-sm" ng-click="deleteSupply(supply)"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger"><?php echo form_error('suppliesAdded[]'); ?></p>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" id="refundreasonBody" >
                <label class="col-md-2" for="refundreasonId"><span class="font-weight-bold"><?php echo $this->lang->line('main_refundreason');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <select name="refundreasonId" id="refundreasonId" class="form-control" ng-model="refundreasonId">
                        <?php 
                        foreach ($refundreasons as $refundreason) { ?>
                            <option value="<?php echo $refundreason->refundreasonId ?>"  <?php echo set_select('refundreasonId',$refundreason->refundreasonId,($refundreason->refundreasonId == $this->input->post('refundreasonId')))?> ><?php echo  $refundreason->name ?></option>
                        <?php   } ?>
                    </select>
                </div>
            </div>

            <!-- retiro de insumos -->
            <div class="form-group row" ng-show="shipmentType == refund" id="operatorsSearch" >
                <label class="col-md-2" for="operatorsList"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_observation');?>:</span><span class="text-danger"><strong> </strong></span></label>
                <div class="col-md-10">
                    <input class="form-control" ng-model="observation" id="observation" name="observation" type="text" placeholder="<?php echo $this->lang->line('admin_shipments_observation');?>" value="<?php echo set_value('nombre',$this->input->post('nombre'));?>">
                </div>
            </div>

            <div class="row" ng-show="shipmentType != '' ">
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-success"><?php echo $this->lang->line('admin_shipments_create');?></button>
                    <a href="/<?php echo FOLDERADD; ?>/shipments" class="btn btn-danger"><?php echo $this->lang->line('general_cancel');?></a>
                </div>
            </div>
        </form>

    </div>
</div>

<script src="/<?php echo ASSETSFOLDER; ?>/js/controllers/createShipments.js?v=1"></script>