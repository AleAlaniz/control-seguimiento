
<link rel="stylesheet" href="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
<script src="/<?php echo ASSETSFOLDER; ?>/libraries/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>
<div class="card">
    <div class="card-header">
        <h3 class="d-inline"><?php echo $this->lang->line('main_shipments') ?></h3>
    </div>
    <?php if ($this->Identity_model->Validate('shipments/admin')) { ?>

        <div class="card-body">
        <?php if(isset($_SESSION['shipmentsMessage']))
        { ?>
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fas fa-check"></i></strong> 
                <?php if ($_SESSION['shipmentsMessage'] == 'create'){
                    echo $this->lang->line('admin_shipments_successmessage');
                    $_SESSION['shipmentsMessage'] = NULL;
                }
                elseif ($_SESSION['shipmentsMessage'] == 'edit'){
                    echo $this->lang->line('admin_shipments_edit_success');
                }
                elseif ($_SESSION['shipmentsMessage'] == 'delete'){
                    echo $this->lang->line('admin_shipments_delete_message');
                }

                ?>
            </div>
        <?php } ?>
            <a href="/<?php echo FOLDERADD; ?>/shipments/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_shipments_create');?></strong></a>
        </div>

    <?php } ?>

    <div class="card-body">
        <div class="form form-inline">
            <label><?php echo $this->lang->line('admin_shipments_select_state');?></label>
            <div class="col-sm-3">
                <select class="form-control" id="state_search">
                    <?php foreach ($states as $state) { ?>
                    <option value="<?php echo $state;?>"><?php echo $state;?></option>
                    <?php } ?>
                    <option value="0">Todos</option>
                </select>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="form form-inline">
            <label><?php echo $this->lang->line('admin_shipments_select_date_range');?></label>
            <div class="col-sm-4">
                <div class="input-group input-daterange">
                    <input type="text" class="form-control" placeholder="Fecha Inicio"  readonly="readonly" id="date_init">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                    <input type="text" class="form-control" placeholder="Fecha Fin"  readonly="readonly"  id="date_end">
                </div>
            </div>
            <button class="btn btn-warning" onclick="cleanDates()">Limpiar rango</button>
        </div>
    </div> 

    <div class="card-body">

        <!-- modal de confirmacion de eliminar -->
        <div class="modal fade" id="popDelete" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <p><?php echo $this->lang->line('admin_shipment_areyousure_delete')?>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <span class="btn btn-success" id="acceptDelete"><?php echo $this->lang->line('general_accept'); ?></span>
                                <span id="loading" class="d-none"><i class="fas fa-spinner fa-spin fa-2x"></i></span>
                                <span id="cancel" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><?php echo $this->lang->line('general_cancel'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- modal de edicion -->
        <div class="modal fade" id="edit-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('admin_shipments_edit'); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="edit-shipment-form" action="shipments/edit" method="POST" novalidate>
                            <input type="hidden" id="operatorId" name="operatorId" value="">
                            <div class="form-group row">
                                <label for="contractNumber" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_number');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="shipmentId" id="shipmentId" value="" readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="contractNumber" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_type');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="type" id="type" value="" readonly/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="state" class="col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_state');?></span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-sm-10">
                                    <input  id="oldState" name="oldState" type="hidden">
                                    <select name="state" id="state" class="form-control" required>

                                    </select>                                        
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2" for="date"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_date');?>:</span><span class="text-danger"><strong> *</strong></span></label>
                                <div class="col-md-10">
                                    <input class="form-control" id="date" name="date" type="text" placeholder="<?php echo $this->lang->line('admin_shipments_date');?>" value="<?php echo set_value('date',$this->input->post('date'));?>" required>
                                    <p class="text-danger"><?php echo form_error('date'); ?></p>
                                </div>
                            </div>

                            <div class="form-group row form-error">

                            </div>		

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success"><?php echo $this->lang->line('general_edit');?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-hover" id="shipments">
            <thead>
                <tr class="active">
                    <th><?php echo $this->lang->line('admin_operators_completename');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_number');?></th>
                    <th><?php echo $this->lang->line('requests_date_file');?></th>
                    <th><?php echo $this->lang->line('requests_date_check');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_shipmentDate');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_deliveryDate');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_actualDate');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_type');?></th>
                    <th><?php echo $this->lang->line('admin_shipments_state');?></th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    function cleanDates() {
        $('#date_init').val("");
        $('#date_end').val("");
        $('#shipments').DataTable().ajax.reload();
    }

    $(function () {
        let actualDate = new Date().toLocaleString();
        actualDate = actualDate.toString();
        actualDate = actualDate.slice(0,-3);

        $('#date').datetimepicker({
            format      : 'dd/mm/yyyy hh:ii',   
            language    : 'es',
            startDate   : actualDate,
        })

        $('#state_search').change(function() {
           $('#shipments').DataTable().ajax.reload();
        });
        $('#state_search option[value="0"]').attr("selected",true);

        $('.input-daterange input').datepicker({ dateFormat: 'yyyy-mm-dd',language:'es'});

        $('.input-daterange').datepicker()
            .on("input change", function (e) {
            $('#shipments').DataTable().ajax.reload();
        });

        $('#shipments').DataTable({
            "processing": true,
            "serverSide": true,
            "bFilter": false,
            "ordering": false,
            "lengthMenu": [ 10, 25, 50, 100, 250, 500 ],
            "searching": true,
            "ajax":{
                url :"Shipments/searchShipments", // json datasource
                data: function(d){
                    d.state     = $('#state_search').val(),
                    d.startDate    = $('#date_init').val(),
                    d.endDate      = $('#date_end').val()
                },
                type: "post",  // method  , by default get
                error: function(){  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display","none");
                }
            },
            language: {
             "sProcessing":     "Procesando...",
             "sLengthMenu":     "Mostrar _MENU_ Envíos",
             "sZeroRecords":    "No se encontraron resultados",
             "sEmptyTable":     "Ningún dato disponible en esta tabla",
             "sInfo":           "Mostrando Envíos del _START_ al _END_ de un total de _TOTAL_ Envíos",
             "sInfoEmpty":      "Mostrando Envíos del 0 al 0 de un total de 0 Envíos",
             "sInfoFiltered":   "(filtrado de un total de _MAX_ Envíos)",
             "sInfoPostFix":    "",
             "sSearch":         "Buscar por nombre y apellido:",
             "sUrl":            "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "columnDefs": [
                {
                    "targets": 9,
                    "data": null,
                    "render": function (data,type,row,meta) {
                        if(data[8] == '<?php echo getShipmentState()[1] ?>'){
                            return  `
                            <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="btn btn-primary viewShipment" title="<?php echo $this->lang->line('admin_shipments_details'); ?>"><i class="fas fa-search"></i></button>
                            <button class="btn btn-warning shipment_edit_button" title="<?php echo $this->lang->line('admin_shipments_edit'); ?>" data-id=${data[6]} data-toggle="modal" data-target="#edit-shipment"><i class="fas fa-edit"></i></button>
                            <button class="btn btn-danger deleteShipment" title="<?php echo $this->lang->line('admin_shipments_delete'); ?>"><i class="fas fa-trash"></i></button>
                            </div>`
                        }
                        else{
                            return  `
                            <div class="btn-group" role="group" aria-label="Basic example">
                            <button class="btn btn-primary viewShipment" title="<?php echo $this->lang->line('admin_shipments_details'); ?>"><i class="fas fa-search"></i></button>
                            <button class="btn btn-danger deleteShipment" title="<?php echo $this->lang->line('admin_shipments_delete'); ?>"><i class="fas fa-trash"></i></button>
                            </div>`
                        }
                    }
                },
                {
                    "targets":0,
                    "data":null,
                    "render": function (data,type,row,meta) {
                        return `<a href="/<?php echo FOLDERADD; ?>/operators/view/${data[9]}" title="<?php echo  $this->lang->line('general_details') ?>">${data[0]}</a> &nbsp;`
                    }
                }
        ]
        });

        var states = <?php echo json_encode(getShipmentState()); ?>;
        var shipmentId;
        $('#shipments').on('click' ,'.viewShipment',function () {
            shipmentId = $(this).closest('tr')[0].cells[1].innerText;
            window.location.href = '/<?php  echo FOLDERADD; ?>/shipments/view/'+shipmentId;
        });
        
        //modal eliminar
        $('#shipments').on('click' ,'.deleteShipment',function (e) {
            shipmentId = $(this).closest('tr')[0].cells[1].innerText;
            $('#popDelete').modal('show');
        });

        $('#acceptDelete').on('click',function(){
            $('#acceptDelete').addClass('d-none');
            $('#cancel').addClass('d-none');
            $('#loading').removeClass('d-none');
            var url = '/<?php  echo FOLDERADD; ?>/shipments/delete' ;
            $.ajax({
                method 	: 'POST',
                url 	: url,
                data 	: {'shipmentId' : shipmentId}
            }).done(function(data) {

                if(data == "success"){
                    location.reload();
                }
            });
        })
        //fin modal eliminar

        // //modal edit
        var shipmentId, dataEdit = "",operatorId;

        $('#shipments').on('click' ,'.shipment_edit_button',function (e) {
            operatorId = $(this).attr('data-id');
            dataEdit = $(this).closest('tr')[0].innerText.split('	');
            let shipmentState = '<?php echo getShipmentState()[1] ?>';
            $('#oldState').val(shipmentState);
            shipmentId = $(this).closest('tr')[0].cells[1].innerText;
        })

        $( "#edit-shipment" ).on('show.bs.modal', function(){
            $('#operatorId').val(operatorId);
            $('#shipmentId').val(shipmentId);
            $('#type').val(dataEdit[7]);
            $('#state').html(' ');
            for (let i = 0; i < states.length; i++) {
                const state = states[i];
                $('#state').append(`<option value="${state}">${state}</option>`)
            }
        })

        $("#edit-shipment-form").submit(function(e){
            e.preventDefault();
            let checkForm = false;
            $('.form-error').empty();

            if(($('#state').val() == "" || $('#state').val() == null) ||
            ($('#date').val() == "" || $('#date').val() == null))
            {
                $('.form-error').append('<div class="text-danger col-sm-10">Completa todos los campos obligatorios</div>');
                checkForm = false;
            }
            else{
                checkForm = true;
            }
            if(checkForm){
                $.ajax({
                    type: "POST",
                    data:$("#edit-shipment-form").serialize(),
                    url: 'shipments/edit',
                    dataType: "json",
                    success:function(data)
                    {
                        if (data.status =='form-error'){
                            $('.form-error').append('<div class="text-danger col-sm-10">'+data.message+'</div>');
                        }
                        else{
                            if (data.status == 'success') {
    
                                $( "#edit-shipment" ).modal('toggle');
                                window.location.href = window.location.href;
                            }
                            else{
                                $('.form-error').append(`<div class="text-danger col-sm-10">${data.message}</div>`);
                            }
                        }
                    },
                    error:function()
                    {
                        console.log('Error');
                    }
                });
            }
        });
        //fin modal edit

        
    })
</script>