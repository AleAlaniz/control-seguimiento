<div class="card">
        <div class="card-header">
            <h3 class="d-inline"><?php echo $this->lang->line('admin_remits') ?></h3>
        </div>
        <?php if ($this->Identity_model->Validate('remits/admin')) { ?>
            <div class="card-body">
                <a href="<?php echo (base_url()) ?>remits/create" class="btn btn-sm btn-outline-success "><i class="fa fa-plus"></i><strong> <?php echo $this->lang->line('admin_remits_create');?></strong></a>
            </div>
        <?php } ?>
        <div class="card-body">
            
            <?php if(isset($_SESSION['remitMessage']))
            { ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><i class="fas fa-check"></i></strong> 
                    <?php if ($_SESSION['remitMessage'] == 'create'){
                        echo $this->lang->line('admin_remits_successmessage');
                    }
                    ?>
                </div>
            <?php } ?>
            <table class="table table-hover" id="remits">
                <thead>
                    <tr class="active">
                        <th><?php echo $this->lang->line('admin_shipments_shipmentDate');?></th>
                        <th><?php echo $this->lang->line('general_name');?></th>
                        <th><?php echo $this->lang->line('admin_remits_delivery');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        foreach ($remits as $remit ) { ?>
                            <tr class="optionsUser">
                                <td><?php echo $remit->deliveryDate ?></td>
                                <td><?php echo $remit->name ?></td>
                                <td><?php echo $remit->delivery ?></td>
                                <td class="text-right">
                                    <?php if($this->Identity_model->Validate('remits/admin')){ ?>
                                        <a href="<?php echo (base_url()) ?>remits/generatePdf/<?php echo $remit->remitId?>" title="<?php echo $this->lang->line('admin_remits_print') ?>"><i class="fas fa-print text-info"></i></a>&nbsp; 
                                        <!-- <a href="<?php echo (base_url()) ?>remits/delete/<?php echo $remit->remitId?>" title="<?php echo $this->lang->line('general_delete') ?>"><i class="fas fa-trash-alt text-danger"></i></a> -->
                                    <?php }?>
                                </td>
                            </tr>
                <?php   } ?>
                </tbody>
            </table>
        </div>
</div>
<script>
    $(function () {
        $('#remits').DataTable({
            language: {
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ usuarios",
					"sZeroRecords":    "<i class='fa fa-remits'></i> No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando usuarios del _START_ al _END_ de un total de _TOTAL_ usuarios",
					"sInfoEmpty":      "Mostrando usuarios del 0 al 0 de un total de 0 usuarios",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ usuarios)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
        });
    })
</script>