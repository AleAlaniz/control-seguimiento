<div ng-app="controlSeguimiento" ng-controller="remits">
  <div class="card">
    <div class="card-header">
      <h3><?php echo $this->lang->line('admin_remits_create');?></h3>
    </div>
    <div class="card-body">
      <form method="POST" ng-submit="submitRemit()" id="form-remit">
        <div class="form-group row">
          <label class="col-md-2" for="shipmentList"><span class="font-weight-bold"><?php echo $this->lang->line('admin_remits_selecShipment');?>:</span><span class="text-danger"><strong> * </strong></span></label>
          <div class="col-md-10" style="overflow-y: scroll;">
              <h6 class="card-header">
                  <?php echo $this->lang->line('main_shipments')?>
              </h6>
              <div class="card-body text-left" id="shipmentsContainer" style="max-height:25rem;">
                <ul class="list-group-custom" id="shipments">
                  <li class="added" ng-repeat="shipment in shipments">
                    <div class="row">
                        <div class="col-md-10">
                          <p class="mb-0">{{shipment.completeName}}</p>
                          <p class="mb-0">{{shipment.address}}-{{shipment.codigo_postal}}</p>
                          <p class="mb-0">{{shipment.shipmentDate}}-{{shipment.type}}</p>
                        </div>
                        <div class="col-md-2" ng-click="addShipment(shipment)"><button type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></button></div>
                    </div>

                  </li>
                  <li ng-if="shipments.length == 0"><?php echo $this->lang->line('admin_remits_shipments_unavailable');?></li>
                </ul>
              </div>
              <p class="text-danger"><?php echo form_error('shipments[]'); ?></p>
          </div>
        </div>

        <div class="form-group row" ng-show="shipmentsSelected.length > 0">
          <label class="offset-md-1 col-md-2"><span class="font-weight-bold"><?php echo $this->lang->line('admin_remits_shipments_selected');?>:</span><span class="text-danger"><strong></strong></span></label>
          <div class="card col-md-8">
              <h6 class="card-header">
                  <?php echo $this->lang->line('main_shipments')?>
              </h6>
              <div class="card-body text-left" id="shipmentsSelectedContainer">
                <ul class="list-group-custom" id="shipmentsSelected">
                  <li class="added" ng-repeat="shipment in shipmentsSelected">
                    <div class="row">
                        <div class="col-md-9">{{shipment.completeName}}-{{shipment.shipmentDate}}-{{shipment.type}}</div>
                        <div class="col-md-1" ng-click="removeShipment(shipment)"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button></div>
                        <div class="col-md-1" ng-click="displayModal(shipment)"><button type="button" title="<?= $this->lang->line('admin_remits_add_observation') ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></button></div>
                    </div>

                  </li>
                  <li ng-if="shipmentsSelected.length == 0"><?php echo $this->lang->line('admin_remits_shipments_unavailable');?></li>
                </ul>
              </div>
              <p class="text-danger"><?php echo form_error('shipments[]'); ?></p>
          </div>
        </div>

        <div class="form-group row">
            <label class="col-md-2" for="name"><span class="font-weight-bold"><?php echo $this->lang->line('admin_remits_delivery_name');?>:</span><span class="text-danger"><strong> *</strong></span></label>
            <div class="col-md-10">
                <input class="form-control" id="name" name="name" ng-model="name" type="text" placeholder="<?php echo $this->lang->line('admin_remits_delivery_name');?>" value="<?php echo set_value('name',$this->input->post('name'));?>">
                <p class="text-danger"><?php echo form_error('name'); ?></p>
            </div>
        </div>

        <div class="form-group row">
          <label class="col-md-2" for="delivery"><span class="font-weight-bold"><?php echo $this->lang->line('admin_remits_delivery');?>:</span><span class="text-danger"><strong>* </strong></span></label>
          <div class="col-md-10">
              <select name="delivery" id="delivery" ng-model="delivery" class="form-control">
                  <?php 
                  foreach ($deliveries as $delivery) { ?>
                      <option value="<?php echo $delivery->deliveryId ?>"  <?php echo set_select('delivery',$delivery->deliveryId,($delivery->deliveryId == $this->input->post('delivery')))?> ><?php echo  $delivery->name ?></option>
                  <?php   } ?>
              </select>
              <p class="text-danger"><?php echo form_error('delivery'); ?></p>
          </div>
        </div>
            
        <!-- <div class="form-group row">
          <label class="col-md-2" for="observation"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_observation');?>:</span><span class="text-danger"><strong> *</strong></span></label>
          <div class="col-md-10">
              <input class="form-control" id="observation" name="observation" ng-model="observation" type="text" placeholder="<?php echo $this->lang->line('admin_shipments_observation');?>" value="<?php echo set_value('observation',$this->input->post('observation'));?>">
              <p class="text-danger"><?php echo form_error('observation'); ?></p>
          </div>
        </div> -->
        
        <div class="form-group row">
          <label class="col-md-2" for="extensionDate"><span class="font-weight-bold"><?php echo $this->lang->line('admin_remits_extensionDate');?>:</span><span class="text-danger"><strong>* </strong></span></label>
          <div class="col-md-10">
              <input class="form-control" id="extensionDate" name="extensionDate" ng-model="extensionDate" type="text" placeholder="<?php echo $this->lang->line('admin_remits_extensionDate');?>" value="<?php echo set_value('extensionDate',$this->input->post('extensionDate'));?>">
              <p class="text-danger"><?php echo form_error('extensionDate'); ?></p>
          </div>
        </div>

        <div class="form-group text-center">
          <button type="submit" class="btn btn-success btn-lg" ng-hide="sending" ><?php echo $this->lang->line('general_create'); ?></button>
          <button class="btn btn-success disabled" ng-show="sending"><i class="fas fa-spinner fa-spin"></i></button>
        </div>
      </form>
    </div>
    
    <div class="modal fade" id="popObservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel"><?php echo $this->lang->line('admin_shipments_edit'); ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                          <div class="form-group row">
                              <label for="data" class="col-md-10"><span class="font-weight-bold"><?php echo $this->lang->line('main_shipment');?></span><span class="text-danger"><strong> *</strong></span></label>
                              <div class="col-sm-10">
                                  <input type="text" name="data" id="data" value="" ng-value="data" class="w-100" readonly/>
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="observation" class="col-md-10"><span class="font-weight-bold"><?php echo $this->lang->line('admin_shipments_observation');?></span><span class="text-danger"><strong> *</strong></span></label>
                              <div class="col-sm-10">
                                  <input type="text" name="observation" id="observation" ng-model="observation" class="w-100" />
                              </div>
                          </div>

                          <div class="modal-footer">
                              <button type="button" class="btn btn-success" ng-click="addObservation()"><?php echo $this->lang->line('admin_remits_add_observation');?></button>
                              <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('general_cancel');?></button>
                          </div>

                      </form>
                  </div>
              </div>
          </div>
      </div>
    
  </div>
</div>

<script>
    var app = angular.module("controlSeguimiento",[]);
    app.controller("remits",function ($scope,$http) {
      $scope.shipments = [];
      $scope.shipmentsSelected = [];
      $scope.sending = false;
      $scope.data = "";
      $scope.observations = [];
      $scope.oShipmentId = "";

      $('#extensionDate').datepicker({
        language 		: 'es',
        autoclose 		: true,
        todayHighlight 	: true,
        todayBtn 		: "linked"
      });

      $http({
      method 	: 'POST',
      url 	: 'getRemitData'
      }).success(function(data) {
        $scope.shipments = data.shipments;
      });

      $scope.addShipment = function (shipment) {
        $scope.shipmentsSelected.push(shipment);
        let index = $scope.shipments.indexOf(shipment);
        if(index != -1){
          $scope.shipments.splice(index,1);
        }
      }

      $scope.removeShipment = function (shipment) {
        $scope.shipments.push(shipment);
        let index = $scope.shipmentsSelected.indexOf(shipment);
        if(index != -1){
          $scope.shipmentsSelected.splice(index,1);
        }
      }

      $scope.displayModal = function (shipment) {
        $('#popObservation').modal('show');
        console.log(shipment);
        $scope.oShipmentId = shipment.shipmentId;
        $scope.data = `${shipment.completeName} ${shipment.shipmentDate}`;
        $scope.observation = shipment.observation;
      }

      $scope.addObservation = function () {

        let i = $scope.shipmentsSelected.map(function (e) {
          return e.shipmentId;
        }).indexOf($scope.oShipmentId);
        $scope.shipmentsSelected[i].observation = $scope.observation;

        $scope.oShipmentId = "";
        $scope.observation = "";
        $scope.data = "";

        $('#popObservation').modal('hide');
      }

      $scope.submitRemit = function(){
        $scope.sending = true;

        var formData = {
          shipments       : $scope.shipmentsSelected,
          name            : $scope.name,
          deliveryId      : $scope.delivery,
          observation     : $scope.observation,
          extensionDate   : $scope.extensionDate
        }

        $.ajax({
        method 	: 'POST',
        url 	: 'create',
        data 	: formData,
        dataType 	: 'json'})
        .always(function(data) {
            if(data == "success"){
              $scope.sending = false;
              let url = '/<?php echo FOLDERADD ?>/remits';
              window.location.href = url;
            }
          });
      }
    })
</script>