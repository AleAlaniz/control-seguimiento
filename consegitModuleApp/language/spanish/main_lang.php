<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_username']                              = 'Nombre de Usuario';
$lang['login_password']                              = 'Contrase&ntilde;a';
$lang['login_error_message']                         = 'El nombre de usuario y la contrase&ntilde;a no coinciden';
$lang['login_signin'] 		                         = 'Entrar';
$lang['user_search_field'] 						     = 'Búsqueda';
$lang['log_out']                                     = 'Salir';

//general
$lang['main_log_out']                                = 'Salir';
$lang['main_my_profile']                             = 'Mi perfil';
$lang['main_users']                                  = 'Usuarios';
$lang['main_categories']                             = 'Categorias';
$lang['main_product']                                = 'Producto';
$lang['general_create']                              = 'Crear';
$lang['general_delete']                              = 'Eliminar';
$lang['general_edit']                                = 'Editar';
$lang['general_accept']                              = 'Aceptar';
$lang['general_cancel']                              = 'Cancelar';
$lang['general_save']                                = 'Guardar';
$lang['general_goback']                              = 'Atrás';
$lang['general_details']                             = 'Detalles';
$lang['general_sales']                               = 'Ventas';
$lang['general_reports']                             = 'Reportes';
$lang['general_campaign']  		                     = 'Campaña';
$lang['general_search']  		                     = 'Buscar';
$lang['general_name']                      		     = 'Nombre';
$lang['general_undefined']                           = 'No definido';
$lang['general_completeall']                         = 'Complete todos los campos marcados con';
$lang['general_empty_rows']                    	     = 'No hay datos disponibles.';
$lang['general_yes']                                 = 'Si';
$lang['general_no']                                  = 'No';
$lang['general_add_filter']                          = 'Agregar filtro';
$lang['general_apply_filter']                        = 'Aplicar filtros';
$lang['general_delete_filter']                       = 'Borrar filtro';
$lang['general_clear_filters']                       = 'Limpiar filtros';

//roles
$lang['main_roles'] 		                         = 'Perfiles';
$lang['role_create']  		                         = 'Crear Perfil';
$lang['role_permissions']  	                         = 'Permisos';
$lang['general_edit_title'] 	                     = 'Editar perfil';
$lang['general_delete_title'] 	                     = 'Eliminar perfil';
$lang['role_view_title'] 	                         = 'Detalles del perfil';
$lang['role_delete_confirm']                 	     = '¿Está seguro que desea eliminar este perfil?';
$lang['role_create_success']                         = 'El perfil ha sido creado correctamente.';
$lang['general_edit_success'] 	                     = 'El perfil ha sido editado correctamente.';
$lang['general_delete_success']                      = 'El perfil ha sido eliminado correctamente.';
$lang['no_permissions']                 		     = 'El perfil aún no tiene permisos asignados.';

//users
$lang['admin_users_lastName']                        = 'Apellido';
$lang['admin_users_turn']                            = 'Turno';
$lang['admin_users_campaign'] 						 = 'Campañas asignadas';
$lang['admin_users_teamleader']                      = 'Team Leader';
$lang['admin_users_cuil']                            = 'Cuil';
$lang['admin_users_role']                            = 'Perfil';
$lang['admin_users_changepass']                      = 'Cambiar contraseña';
$lang['admin_users_resetpass']                       = 'Resetear contraseña';
$lang['admin_users_user_name']                       = 'Nombre del usuario';
$lang['admin_users_new_password']                    = 'Nueva contraseña';
$lang['admin_users_confirm_password']                = 'Confirmar contraseña';
$lang['admin_users_create']                          = 'Crear usuario';
$lang['admin_users_edit']                            = 'Editar usuario';
$lang['admin_users_delete']                          = 'Eliminar usuario';
$lang['admin_users_passwordmatch']                   = 'Las contraseñas no coinciden';
$lang['admin_users_successmessage']                  = 'Usuario creado correctamente';
$lang['admin_users_editmessage']                     = 'Usuario editado correctamente';
$lang['admin_users_deletemessage']                   = 'Usuario eliminado correctamente';
$lang['admin_users_details']                         = 'Detalles del usuario';
$lang['admin_users_config']                          = 'Configuración';
$lang['admin_users_resetPass']                       = 'Resetear contraseña';
$lang['admin_areyousure_delete_user']                = '¿Está seguro que desea eliminar a este usuario?';
$lang['admin_users_invalidPassword']                 = 'La contraseña ingresada debe ser la misma a la anterior';
$lang['admin_users_userNotExists']            	     = 'El usuario ingresado no existe';
$lang['admin_users_selfDelete']                      = 'No puedes eliminarte a ti mismo';
$lang['users_empty']            				     = 'No hay usuarios';
$lang['users_unexisted_turn']                        = 'No existe el turno seleccionado';
$lang['admin_users_userNotRightToEdit']        	     = 'No tiene permiso para editar la contraseña del usuario seleccionado';
$lang['admin_users_birthDate']                       = 'Fecha de nacimiento';
$lang['admin_users_customer']                        = 'Cliente';
$lang['admin_users_campaign']                        = 'Campaña';
$lang['admin_users_original_site']                   = 'Site original';
$lang['admin_users_schedule']                        = 'Horario';
$lang['admin_users_position']                        = 'Puesto';
$lang['admin_users_supervisor']                      = 'Supervisor';

//admin teamleaders
$lang['admin_category_create']                    = 'Crear categoria';
$lang['admin_category_edit']                      = 'Editar categoria';
$lang['admin_category_delete']                    = 'Eliminar categoria';
$lang['admin_category_successmessage']            = 'Categoria creada correctamente';
$lang['admin_category_editmessage']               = 'Categoria editada correctamente';
$lang['admin_category_deletemessage']             = 'Categoria eliminada correctamente';
$lang['admin_category_areyousure_delete']         = '¿Está seguro que desea eliminar a esta categoria?';
$lang['admin_category_details']                   = 'Detalles de la categoria';
$lang['category']                                 = 'Categoría';


//Campañas

$lang['campaign_campaings'] 						 = 'Campañas';
$lang['campaign_name'] 								 = 'Nombre';
$lang['campaign_description'] 						 = 'Descripción';
$lang['campaign_users'] 							 = 'Usuarios de la campaña';
$lang['campaign_users_added'] 						 = 'Usuarios añadidos';
$lang['campaign_id'] 								 = 'Identificador de campaña';
$lang['campaign_users_count'] 						 = 'Cantidad de usuarios';
$lang['campaign_products_count'] 					 = 'Cantidad de productos';

$lang['campaign_create'] 							 = 'Crear campaña';
$lang['campaign_edit'] 								 = 'Editar campaña';
$lang['campaign_details'] 							 = 'Detalles de campaña';
$lang['campaign_delete'] 							 = 'Eliminar de campaña';

$lang['campaign_del'] 							     = 'Eliminar campaña';


$lang['campaign_create_success'] 					 = 'Campaña creada correctamente.';
$lang['campaign_edit_success']	 					 = 'Campaña editada correctamente.';
$lang['campaign_delete_success'] 					 = 'Campaña eliminda correctamente.';

$lang['campaign_delete_alert'] 						 = '¿Estás seguro que deseas eliminar esta campaña?';
		 
$lang['campaign_error_user_not_exist'] 				 = 'Uno de los usuarios seleccionados no existe.';
$lang['campaign_error_campaign_not_exist'] 			 = 'La campaña a la que hace referencia no está disponible.';
$lang['product_error_product_not_exist'] 			 = 'El producto al que hace referencia no está disponible.';
$lang['campaign_error_user_exist_in_list'] 			 = 'No puede haber usuarios repetidos en la campaña.';
$lang['campaign_error_campaign_not_exist'] 			 = 'Una de las campañas seleccionadas no existe.';
$lang['campaign_error_campaign_in_list'] 		 	 = 'Un usuario no puede estar asignado mas de una vez a la misma campaña.';
$lang['role_error_role_not_exists']                  = 'El rol al que hace referencia no existe';

$lang['general_database_error'] 					 = 'Ocurrió un error inesperado.';

$lang['user_complete_name'] 						 = 'Nombre completo';
$lang['campaign_products'] 							 = 'Ver productos de la campaña';

$lang['campaign_products'] 		               		 = 'Productos de la campaña';
$lang['product_create_title']  		           		 = 'Crear producto';
$lang['product_edit_title'] 	               		 = 'Editar producto';
$lang['product_delete_title'] 	               		 = 'Eliminar producto';
$lang['product_name'] 	            				 = 'Nombre de producto';
$lang['product_detail'] 	               			 = 'Detalle de producto';
$lang['product_no_name'] 	               			 = 'Debe ingresar el nombre del producto';
$lang['campaignProducts'] 	               			 = 'Productos de campaña';
$lang['product_name_too_long'] 	               		 = 'El nombre del producto es muy largo';
$lang['product_no_detail'] 	               			 = 'Debe ingresar el nombre del producto';
$lang['product_detail_too_long'] 	           		 = 'El nombre del producto es muy largo';
$lang['campaign_error_product_name_unique_in_list']  = 'El nombre de cada producto debe ser único';
$lang['campaign_error_product_required']      		 = 'Debe ingresar los datos requeridos';
$lang['campaign_added_products']      		 		 = 'Productos añadidos';
$lang['products_empty']      		 		 		 = 'No hay productos';
$lang['product_input_only_characters']      		 = 'Solamente se aceptan letras y espacios como caracteres validos';
$lang['campaing_extra_data']                         = 'Datos extra';
$lang['campaign_extra_data_required']                = 'Datos extra obligatorios';

//Ventas
$lang['sales_salesDate']                             = 'Fecha de venta';
$lang['sales_clientNumber']                          = 'Número de cliente';
$lang['sales_contractNumber']                        = 'Número de contrato';
$lang['sales_comment']                               = 'Comentario';
$lang['sales_phone']                                 = 'Número de teléfono';
$lang['sales_dni']                                   = 'Dni';
$lang['sales_create']                                = 'Crear venta';
$lang['sales_campaign']                              = 'Campaña';
$lang['sales_census']                                = 'Id de empadronamiento';
$lang['sales_commercial_offer']                      = 'Oferta comercial';
$lang['report_team_leader']                          = 'Team leader';
$lang['report_comment']                          	 = 'Comentario';
$lang['report_phone']                          		 = 'Teléfono';
$lang['report_dni']                          		 = 'Dni';
$lang['census_id']                                   = 'Id de empadronamiento';
$lang['commercial_offer']                            = 'Oferta comercial';
$lang['sale_edit']                          		 = 'Editar venta';
$lang['sale_delete']                          		 = 'Eliminar venta';
$lang['sale_id']                                     = 'Identificador de ventas';

$lang['sales_createmessage']                         = 'Venta correctamente cargada';
$lang['sales_want_to_resale']                        = '¿Desea cargar otra venta para el mismo cliente?';
$lang['sales_not_permission']                        = 'No tienes permiso para acceder a esta vista.';

//Reportes
$lang['report_sale_reports'] 	                     = 'Reporte de ventas';
$lang['report_contract_number']                      = 'Nro de contrato';
$lang['report_username']							 = 'Nombre de usuario';
$lang['report_agent']			                     = 'Empleado';
$lang['report_campaign'] 		                     = 'Campaña';
$lang['report_product'] 		                     = 'Producto';
$lang['report_sale_date'] 		                     = 'Fecha';
$lang['report_sale_hour'] 		                 	 = 'Hora';
$lang['report_sale_time_date'] 		                 = 'Fecha y hora';

$lang['report_sales'] 			                     = 'Ventas';
$lang['report_start_date'] 		                     = 'Fecha de inicio';
$lang['report_end_date'] 		                     = 'Fecha de fin';
$lang['report_export'] 			                     = 'Exportar';

$lang['report_date_greater_error'] 	                 = 'La fecha de inicio no puede ser mayor a la fecha final.';
$lang['date_invalid'] 				                 = 'Una de las fechas indicadas no es correcta.';
$lang['date_format_error'] 				             = 'El formato del campo fecha es incorrecto.';
$lang['hour_format_error'] 				             = 'El formato del campo hora es incorrecto.';
$lang['sale_invalid'] 				             	 = 'La venta que intenta modificar no existe.';
$lang['sale'] 				             	 		 = 'Venta';

$lang['max_length_text'] 						     = '*Maximo 500 caracteres';

$lang['report_sale_areyousure_delete']				 = '¿Estas seguro que desea eliminar esta venta?';
$lang['report_sales_deletemessage']                  = 'Venta correctamente eliminada';
$lang['report_sales_editmessage']                    = 'La venta fue editada de manera exitosa';

$lang['report_sales_editmessage']                    = 'La venta fue editada de manera exitosa';

//categorias
$lang['main_categorias']                             = 'Categorías';
$lang['admin_categorias_editmessage']                = 'Categoría editada correctamente';
$lang['admin_categorias_deletemessage']              = 'Categoría eliminada';
$lang['admin_categorias_create']                     = 'Crear categoría';
$lang['admin_categorias_edit']                       = 'Editar categoría';
$lang['admin_categorias_areyousure_delete']          = '¿Estas seguro de que quiere eliminar esta categoría?';

//Sites
$lang['main_sites']                                  = 'Sites';
$lang['admin_sites_create']                          = 'Crear un site';
$lang['admin_sites_edit']                            = 'Editar site';
$lang['admin_sites_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este site?';
$lang['admin_sites_successmessage']                  = 'Site creado correctamente';
$lang['admin_sites_editmessage']                     = 'Site editado correctamente';
$lang['admin_sites_deletemessage']                   = 'Site eliminado';
$lang['site_name']                   		   		 = 'Nombre del site';
$lang['site']                   		   		     = 'Site';

//Customers
$lang['main_customers']                                  = 'Clientes';
$lang['admin_customers_create']                          = 'Crear un Cliente';
$lang['admin_customers_edit']                            = 'Editar Cliente';
$lang['admin_customers_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Cliente?';
$lang['admin_customers_successmessage']                  = 'Cliente creado correctamente';
$lang['admin_customers_editmessage']                     = 'Cliente editado correctamente';
$lang['admin_customers_deletemessage']                   = 'Cliente eliminado';

//Owners
$lang['main_owners']                                  = 'Propietarios';
$lang['main_owner']                                   = 'Propietario';
$lang['admin_owners_create']                          = 'Crear un Propietario';
$lang['admin_owners_edit']                            = 'Editar Propietario';
$lang['admin_owners_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Propietario?';
$lang['admin_owners_successmessage']                  = 'Propietario creado correctamente';
$lang['admin_owners_editmessage']                     = 'Propietario editado correctamente';
$lang['admin_owners_deletemessage']                   = 'Propietario eliminado';


//boxes
$lang['main_boxes']                                  = 'Boxes';
$lang['admin_boxes_create']                          = 'Crear box';
$lang['box_name']                                    = 'Nombre del box';
$lang['admin_boxes_edit']                            = 'Editar Box';
$lang['admin_boxes_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Box?';
$lang['admin_boxes_successmessage']                  = 'Box creado correctamente';
$lang['admin_boxes_editmessage']                     = 'Box editado correctamente';
$lang['admin_boxes_deletemessage']                   = 'Box eliminado';
$lang['site_error_site_not_exists']                  = 'El site seleccionado no existe';
$lang['admin_supplies_box']                          = 'Box';
$lang['admin_supplies_select_box']                   = 'Seleccione un box';

//Operators
$lang['operator_name']                     	   		     = 'Nombre del Empleado';
$lang['main_operators']                                  = 'Empleados';
$lang['admin_operator']                                  = 'Empleado';
$lang['admin_operators_create']                          = 'Crear Empleado';
$lang['admin_operators_edit']                            = 'Editar Empleado';
$lang['admin_operators_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Empleado?';
$lang['admin_operators_successmessage']                  = 'Empleado creado correctamente';
$lang['admin_operators_editmessage']                     = 'Empleado editado correctamente';
$lang['admin_operators_deletemessage']                   = 'Empleado eliminado';
$lang['operator_id_lote']                                = 'Id Lote';
$lang['operator_id_interno']                             = 'Id Interno';
$lang['admin_operators_empresa']                         = 'Empresa';
$lang['operator_legajo']                                 = 'Legajo';
$lang['operator_cuenta']                                 = 'Cuenta';
$lang['operator_usuario']                                = 'Usuario';
$lang['admin_operators_celular']                         = 'Celular';
$lang['admin_operators_email']                           = 'Email';
$lang['admin_operators_domicilio']                       = 'Domicilio';
$lang['admin_operators_contacto']                        = 'Contacto ultimos 2 meses';
$lang['admin_operators_prioridad']                       = 'Prioridad';
$lang['admin_operators_pc_de_cat']                       = 'Pc de Cat';
$lang['admin_operators_completename']                    = 'Nombre y apellido';
$lang['admin_operators_currently']                       = 'Operador que tiene el insumo actualmente';
$lang['admin_operators_province']                        = 'Provincia';
$lang['admin_operators_location']                        = 'Localidad';
$lang['admin_operators_street']                          = 'Calle';
$lang['admin_operators_postal_code']                     = 'Código Postal';
$lang['admin_operators_floor_dept']                      = 'Piso/Depto';
$lang['admin_operators_height']                          = 'Altura de calle';
$lang['admin_users_birthDate']                       = 'Fecha de nacimiento';
$lang['admin_operators_customer']                        = 'Cliente';
$lang['admin_operators_campaign']                        = 'Campaña';
$lang['admin_operators_original_site']                   = 'Site original';
$lang['admin_operators_schedule']                        = 'Horario';
$lang['admin_operators_position']                        = 'Puesto';
$lang['admin_operators_supervisor']                      = 'Supervisor';

$lang['admin_operators_HEADSET_USB']                     = 'HEADSET_USB';
$lang['admin_operators_CABLE_UTP']                       = 'CABLE_UTP';
$lang['admin_operators_MANOS_LIBRES']                    = 'MANOS LIBRES';
$lang['admin_operators_HEADSET']                         = 'HEADSET';
$lang['admin_operators_MODEM_4G']                        = 'MODEM 4G';
$lang['admin_operators_CHIP']                            = 'CHIP';
$lang['admin_operators_NOTEBOOK']                        = 'NOTEBOOK';
$lang['admin_operators_CPU']                             = 'CPU';
$lang['admin_operators_NOTEBOOK_SANTANDER']              = 'NOTEBOOK SANTANDER';
$lang['admin_operators_MONITOR']                         = 'MONITOR';
$lang['admin_operators_MOUSE']                           = 'MOUSE';
$lang['admin_operators_CABLE']                           = 'CABLE';
$lang['admin_operators_TECLADO']                         = 'TECLADO';
$lang['admin_operators_ADAPTADOR_USB']                   = 'ADAPTADOR USB';
$lang['admin_operators_CABLE_FUENTE']                    = 'CABLE FUENTE';
$lang['admin_operators_CARGADOR_NOTEBOOK']               = 'CARGADOR NOTEBOOK';
$lang['admin_operators_INSTALACIONES']                   = 'INSTALACIONES';
$lang['admin_operators_CELULAR']                         = 'CELULAR';
$lang['admin_operators_CAMARA_WEB']                      = 'CAMARA WEB';
$lang['admin_operators_MUEBLE']                          = 'MUEBLE';
$lang['admin_operators_filter_state']                    = 'Seleccione el estado actual';
$lang['admin_operators_filter_persona_riesgo']           = 'Seleccione si es persona de riesgo';
$lang['admin_operators_filter_pc_propia']                = 'Seleccione si tiene pc propia';


//supplies
$lang['main_supplies']                                  = 'Insumos';
$lang['main_supply']                                    = 'Insumo';
$lang['admin_supplies_create']                          = 'Agregar Insumo';
$lang['admin_supplies_name']                            = 'Nombre del insumos';
$lang['admin_supplies_edit']                            = 'Editar insumo';
$lang['admin_supplies_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este insumo?';
$lang['admin_supplies_successmessage']                  = 'Insumo agregado correctamente';
$lang['admin_supplies_editmessage']                     = 'Insumo editado correctamente';
$lang['admin_supplies_deletemessage']                   = 'Insumo eliminado';
$lang['admin_supplies_select_category']                 = 'Seleccione una categoría';
$lang['admin_supplies_select_site']                 	= 'Seleccione un site';
$lang['admin_supplies_select_state']                 	= 'Seleccione el estado del insumo';
$lang['admin_supplies_select_owner']                 	= 'Seleccione el propietario del insumo';
$lang['admin_supplies_numero_serie']                    = 'Numero de serie';
$lang['admin_supplies_tipo_de_pc']                      = 'Tipo de pc';
$lang['admin_supplies_marca']                           = 'Marca';
$lang['admin_supplies_sophos_fecha']                    = 'Sophos / Fecha';
$lang['admin_supplies_disco']                           = 'Disco';
$lang['admin_supplies_memoria']                         = 'Memoria';
$lang['admin_supplies_procesador']                      = 'Procesador';
$lang['admin_supplies_id_teamviewer']                   = 'Id de teamviewer';
$lang['admin_supplies_img']                             = 'Imagenes ';
$lang['admin_supply_details']                           = 'Detalles del insumo';
$lang['admin_supplies_state']                           = 'Estado';
$lang['admin_supplies_record']                          = 'Histórico del insumo';
$lang['admin_supplies_stock']                           = 'Stock del insumo';
$lang['admin_supplies_purchase_date']                   = 'Fecha de compra';
$lang['admin_supplies_assigned_counter']                = 'Cantidad de insumos asignados';
$lang['admin_supplies_available_counter']               = 'Cantidad de insumos disponibles';
$lang['admin_supplies_broken_counter']                  = 'Cantidad de insumos rotos';
$lang['admin_supplies_repair_counter']                  = 'Cantidad de Insumos en reparación';
$lang['admin_supplies_summarybsas']                     = 'Resumen Buenos Aires';
$lang['admin_supplies_bsasin']                          = 'Total de insumos en Buenos Aires';
$lang['admin_supplies_bsasout']                         = 'Total de insumos fuera de Buenos Aires';
$lang['admin_supplies_summarylp']                       = 'Resumen San Luis';
$lang['admin_supplies_lpin']                            = 'Total de insumos en San Luis';
$lang['admin_supplies_lpout']                           = 'Total de insumos fuera de San Luis';
$lang['admin_empty_supplies']                           = 'No hay insumos disponibles';
$lang['admin_customers_counters']                       = 'Insumos por campaña';

//shipments
$lang['main_shipments']                                 = 'Envíos';
$lang['main_shipment']                                  = 'Envío';
$lang['admin_shipments_create']                         = 'Hacer un envío';
$lang['admin_shipments_type']                           = 'Tipo de envío';
$lang['admin_shipments_operators']                      = 'Buscar Empleados';
$lang['admin_shipments_operators_assigned']             = 'Empleados asignados';
$lang['admin_shipments_supplies_added']                 = 'Insumos añadidos';
$lang['admin_shipment_operators_with_supplies']         = 'Empleados con insumos';
$lang['admin_shipments_supplies_assigned']              = 'Insumos asignados';
$lang['admin_shipments_select_date_range']              = 'Búsqueda por rango de fechas:';
$lang['admin_shipments_supplies_refund']                = 'Insumos a devolver';
$lang['admin_shipments_number']                         = 'Número de envío';
$lang['admin_shipments_shipmentDate']                   = 'Fecha de envío';
$lang['admin_shipments_deliveryDate']                   = 'Fecha de entrega';
$lang['admin_shipments_state']                          = 'Estado del envío';
$lang['admin_shipment_areyousure_delete']               = '¿Estas seguro de querer cancelar este envío?';
$lang['admin_shipments_successmessage']                 = 'Envio realizado correctamente';
$lang['admin_shipments_edit_success']                   = 'Estado del envío editado correctamente';
$lang['admin_shipments_delete_message']                 = 'Envío cancelado correctamente';
$lang['admin_shipments_details']                        = 'Detalles del envío';
$lang['admin_shipments_delete']                         = 'Eliminar envío';
$lang['admin_shipments_edit']                           = 'Editar envío';
$lang['admin_shipments_date']                           = 'Fecha';
$lang['admin_shipments_select_state']                   = 'Seleccione el estado del envío';
$lang['admin_shipments_supplies_assigned_currently']    = 'Insumos asignados actualmente';
$lang['admin_shipments_observation']                    = 'Observación';
$lang['admin_shipments_operators_required']             = 'El campo Empleados asignados es requerido';
$lang['admin_shipments_supplies_required']              = 'El campo Insumos es requerido';
$lang['admin_shipments_actualDate']                     = 'Tiempo transcurrido';

//filters
$lang['admin_filters_show']                             = 'Mostrar filtros';
$lang['admin_filters_hide']                             = 'Ocultar filtros';
$lang['admin_filters_clear']                            = 'Limpiar filtros';

//Providers
$lang['main_providers']                                  = 'Proveedores';
$lang['main_provider']                                   = 'Proveedor';
$lang['admin_providers_create']                          = 'Crear un Proveedor';
$lang['admin_providers_edit']                            = 'Editar Proveedor';
$lang['admin_providers_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Proveedor?';
$lang['admin_providers_successmessage']                  = 'Proveedor creado correctamente';
$lang['admin_providers_editmessage']                     = 'Proveedor editado correctamente';
$lang['admin_providers_deletemessage']                   = 'Proveedor eliminado';

//Refund reasons
$lang['main_refundreasons']                                  = 'Motivos  de retiro';
$lang['main_refundreason']                                   = 'Motivo de retiro';
$lang['admin_refundreasons_create']                          = 'Crear un Motivo de retiro';
$lang['admin_refundreasons_edit']                            = 'Editar Motivo de retiro';
$lang['admin_refundreasons_areyousure_delete']               = '¿Estas seguro de que quiere eliminar este Motivo de retiro?';
$lang['admin_refundreasons_successmessage']                  = 'Motivo de retiro creado correctamente';
$lang['admin_refundreasons_editmessage']                     = 'Motivo de retiro editado correctamente';
$lang['admin_refundreasons_deletemessage']                   = 'Motivo de retiro eliminado';

//remits
$lang['admin_remits']                                        = 'Remitos';
$lang['admin_remits_create']                                 = 'Hacer un remito';
$lang['admin_remits_delivery']                               = 'Transporte';
$lang['admin_remits_extensionDate']                          = 'Fecha de extensión de la cuarentena';
$lang['admin_remits_selecShipment']                          = 'Seleccione los envíos';
$lang['admin_remits_shipments_unavailable']                  = 'No hay envíos disponibles';
$lang['admin_remits_shipments_selected']                     = 'Envíos seleccionados';
$lang['admin_remits_delivery_name']                          = 'Nombre de quien envía';
$lang['admin_remits_observations']                           = 'Observaciones';
$lang['admin_nserie_nombre']                                 = 'Numero de serie / Nombre';
$lang['admin_remits_print']                                  = 'Imprimir remito';
$lang['admin_remits_successmessage']                         = 'Remito creado correctamente';
$lang['admin_remits_add_observation']                        = 'Agregar observación';

//suppliesRequests
$lang['main_supplies_requests']                              = 'Solicitudes de insumos';
$lang['requests_state']                                      = 'Estado';
$lang['requests_priority']                                   = 'Prioridad';
$lang['requests_manager']                                    = 'Gestor/a';
$lang['requests_adviser']                                    = 'Asesor/a';
$lang['requests_date_file']                                  = 'Fecha req hd';
$lang['requests_date_check']                                 = 'Fecha de aceptación';
$lang['request_check']                                       = 'Modificar solicitud';
$lang['request_accept']                                      = 'Aceptar solicitud';
$lang['request_reject']                                      = 'Denegar solicitud';
$lang['request_what_do_you_want']                            = '¿Qué deseas hacer?';
$lang['request_accepted']                                    = 'Aceptada';
$lang['request_rejected']                                    = 'Denegada';
$lang['request_error_id_required']                           = 'Error, el campo estado es requerido';
$lang['request_correctly_edited']                            = 'Solicitud correctamente editada';
$lang['request_empty_requests']                              = 'No hay solicitudes pendientes';
$lang['request_supplies_requested']                          = 'Insumos solicitados';
$lang['request_upload_excel']                                = 'Subir archivo';
$lang['request_correctly_uploaded']                          = 'Archivo cargado correctamente';
$lang['request_request_addextra']                            = 'Agregar más solicitudes';
$lang['main_supplies_phone']                                 = 'Número de teléfono';
$lang['main_supplies_areacode']                              = 'Código de área';
$lang['main_supplies_number']                                = 'Número';

//validacion hd
$lang['validationhd_returned_supplies']                      = 'Insumos devueltos a revisar';
$lang['validationhd_main']                                   = 'Validaciones revisadas';
$lang['validationhd_reports']                                = 'Reportes';
$lang['validationhd_check']                                  = 'Revisar insumo';
$lang['validationhd_create']                                 = 'Validación agegada correctamente';
$lang['validationhd_validationDate']                         = 'Fecha de validación';
$lang['validationhd']                                        = 'Validaciones help desk';


//productivity
$lang['admin_operators_telet_estado_actual']                        = 'Teletrabajo estado actual';
$lang['admin_operators_telet_conectividad_historica']               = 'Teletrabajo conectividad histórica';
$lang['admin_operators_telet_persona_riesgo']                       = 'Teletrabajo persona de riesgo';
$lang['admin_operators_telet_fliares_cargo']                        = 'Teletrabajo familiares a cargo';
$lang['admin_operators_telet_pc_nb_propia']                         = 'Teletrabajo pc nb propia';
$lang['admin_operators_telet_elige_operar_site']                    = 'Teletrabajo elige operar en site';
$lang['admin_operators_telet_prioridad_campana']                    = 'Teletrabajo prioridad campaña';
$lang['admin_operators_perform_cuartil']                            = 'Performance cuartil';
$lang['admin_operators_perform_presentismo']                        = 'Performance presentismo';
$lang['admin_operators_perform_prioridad_empleado']                 = 'Performance prioridad empleado';
$lang['admin_operators_perform_calidad']                            = 'Performance calidad';
$lang['main_aperturesite']                                          = 'APERT-SITE-MATRIZ';
$lang['main_aperturesite_report']                                   = 'APERT-SITE-REPORT';
$lang['main_aperturesite_rrhh']                                     = 'APERT-SITE-RRHH';
$lang['main_aperturesite_ops']                                      = 'APERT-SITE-OPS';
$lang['main_aperturesite_logis']                                    = 'APERT-SITE-LOGIS';
$lang['main_aperturesite_edit']                                     = 'Editar registro';