<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->database();
        $this->_error_prefix = '<p class="text-danger">';
        $this->_error_suffix = '</p>';
    }

    public function edit_unique($value, $params)
    {   
        list($table, $field, $current_id_name, $current_id, $is_logic_delete) = explode(".", $params);
        
        $databaseQuery = $this->CI->db->select()->from($table)->where($field, $value);
        
        if(($is_logic_delete == 'TRUE'))
            $databaseQuery->where('active', 1);

        $query = $databaseQuery->limit(1)->get();

        if ($query->row() && $query->row()->$current_id_name != $current_id)
        {
            $this->CI->form_validation->set_message('edit_unique', $this->CI->lang->line('is_unique'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function is_unique_active($value, $params)
    {
        list($table, $field) = explode(".", $params);
        $query = $this->CI->db->select()->from($table)->where($field, $value)->where('active', 1)->limit(1)->get();

        if ($query->row())
        {
            $this->CI->form_validation->set_message('is_unique_active', $this->CI->lang->line('is_unique'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function is_valid_date($value)
    {
        $date_components = explode('/', $value);

        if(count(explode('/', $value)) != 3 || !date_create_from_format('d/m/Y', $value)){
            $this->CI->form_validation->set_message('is_valid_date',$this->CI->lang->line('date_format_error'));
            return false;
        }
        return true;
    }

    public function exist_role($value)
    {
        $query = $this->CI->db->select()->from('roles')->where('roleId',$value)->limit(1)->get();
        $res = $query->row();

        if(isset($res))
        {
            return TRUE;
        }
        else{
            $this->CI->form_validation->set_message('exist_role',$this->CI->lang->line('role_error_role_not_exists'));
            return FALSE;
        }
    }
}
