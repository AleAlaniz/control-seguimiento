<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplies extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Supplies_model','Supplies');
        $this->load->model('Categories_model','Categories');
        $this->load->model('Sites_model','Sites');
        $this->load->model('Boxes_model','Boxes');
        $this->load->model('Owners_model','Owners');
        $this->load->model('Providers_model','Providers');
        $this->load->model('Brands_model','Brands');
        $this->load->model('Operators_model','Operators');
    }

    public function index()
    {   
        if($this->Identity_model->Validate('supplies/view')){
            $data = new StdClass();
            $data->categories = $this->Categories->getCategories();
            $data->sites = $this->Sites->getSites();
            $data->boxes = $this->Boxes->GetBoxesData();
            $data->owners = $this->Owners->getOwners();
            $data->states = getStates();
            $data->colorCounters = $this->Supplies->getColorCounters();

            $this->load->view('_shared/header');
            $this->load->view('supplies/index', $data);
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('supplies/admin')){
            $this->form_validation->set_rules('numero_serie'            ,'lang:admin_supplies_numero_serie'                  ,'required|max_length[40]|is_unique_active[supplies.numero_serie]');
            $this->form_validation->set_rules('categoryId'              ,'lang:admin_operators_categoryId'                   ,'required|max_length[40]');
            $this->form_validation->set_rules('nombre'                  ,'lang:general_name'                                 ,'required|max_length[40]|is_unique_active[supplies.nombre]');
            $this->form_validation->set_rules('purchaseDate'            ,'lang:admin_supplies_purchase_date'                 ,'max_length[40]');
            if($this->input->post('categoryId') == 9 || $this->input->post('categoryId') == 16){
                $this->form_validation->set_rules('disco'               ,'lang:admin_supplies_disco'                         ,'required|max_length[40]');
                $this->form_validation->set_rules('memoria'             ,'lang:admin_supplies_memoria'                       ,'required|max_length[40]');
                $this->form_validation->set_rules('procesador'          ,'lang:admin_supplies_procesador'                    ,'required|max_length[50]');    
            }
            else{
                $this->form_validation->set_rules('disco'               ,'lang:admin_supplies_disco'                         ,'max_length[40]');
                $this->form_validation->set_rules('memoria'             ,'lang:admin_supplies_memoria'                       ,'max_length[40]');
                $this->form_validation->set_rules('procesador'          ,'lang:admin_supplies_procesador'                    ,'max_length[50]');
            }
            $this->form_validation->set_rules('sophos_fecha'            ,'lang:admin_supplies_sophos_fecha'                  ,'max_length[10]');
            $this->form_validation->set_rules('id_teamviewer'           ,'lang:admin_supplies_id_teamviewer'                 ,'max_length[40]');
            $this->form_validation->set_rules('ownerId'                 ,'lang:main_owner'                                   ,'required|max_length[40]');
            $this->form_validation->set_rules('providerId'              ,'lang:main_provider'                                ,'required|max_length[40]');
            $this->form_validation->set_rules('siteId'                  ,'lang:site'                                         ,'required|max_length[40]');
            $this->form_validation->set_rules('boxId'                   ,'lang:admin_supplies_box'                           ,'required|max_length[40]');
            
            if ($this->form_validation->run() == FALSE) {
                // echo validation_errors();show_404();

                $data = new StdClass();
                $data->sites = $this->Sites->getSites();
                $data->categories = $this->Categories->getCategories();
                $data->discs = getDiscs();
                $data->memories = getMemories();
                $data->procesors = getProcesors();
                $data->owners = $this->Owners->getOwners();
                $data->brands = $this->Brands->getBrands();
                $data->providers = $this->Providers->getProviders();

                $this->load->view('_shared/header');
                $this->load->view('supplies/create',$data);
                $this->load->view('_shared/footer');

            }
            else{
                $supplyInsert = array(
                    'numero_serie'       => $this->input->post('numero_serie'),
                    'nombre'             => $this->input->post('nombre'),
                    'purchaseDate'       => $this->input->post('purchaseDate'),
                    'marca'              => $this->input->post('marca'),
                    'areacode'           => $this->input->post('areacode'),
                    'number'             => $this->input->post('number'),
                    'state'              => getStates()[0],
                    'sophos_fecha'       => $this->input->post('sophos_fecha'),
                    'disco'              => $this->input->post('disco'),
                    'memoria'            => $this->input->post('memoria'),
                    'procesador'         => $this->input->post('procesador'),
                    'imgs'               => '',
                    'id_teamviewer'      => $this->input->post('id_teamviewer'),
                    'ownerId'            => $this->input->post('ownerId'),
                    'providerId'         => $this->input->post('providerId'),
                    'categoryId'         => $this->input->post('categoryId'),
                    'boxId'              => $this->input->post('boxId')
                );

                if($_FILES['userFile']['name'][0] != "")
                {
                    $this->uploadFile($supplyInsert);
                }
                $res = $this->Supplies->Create($supplyInsert);

                if(isset($res))
                {
                    $this->session->set_flashdata('suppliesMessage', 'create');
                    header('Location:/'.FOLDERADD.'/supplies'); 
                }
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function edit()
    {
        if($this->uri->segment(3))
        {
            if($this->Identity_model->Validate('supplies/admin')){
                $url_id = $this->uri->segment(3);
                $data = new StdClass();
                $data->supply = $this->Supplies->GetSupplyById($this->uri->segment(3));
                
                if(isset($data->supply))
                {
                
                    $this->form_validation->set_rules('numero_serie'    ,'lang:admin_supplies_numero_serie'                ,'required|max_length[40]|edit_unique[supplies.numero_serie.supplyId.'.$url_id.'.TRUE]');
                    $this->form_validation->set_rules('categoryId'      ,'lang:admin_operators_categoryId'                 ,'required|max_length[40]');
                    $this->form_validation->set_rules('nombre'          ,'lang:general_name'                               ,'required|max_length[40]|edit_unique[supplies.nombre.supplyId.'.$url_id.'.TRUE]');
                    $this->form_validation->set_rules('purchaseDate'    ,'lang:admin_supplies_purchase_date'               ,'max_length[40]');
                    $this->form_validation->set_rules('marca'           ,'lang:admin_supplies_marca'                       ,'max_length[40]');
                    $this->form_validation->set_rules('state'           ,'lang:admin_supplies_state'                       ,'required|max_length[40]');
                    if($this->input->post('categoryId') == 9 || $this->input->post('categoryId') == 16){
                        $this->form_validation->set_rules('disco'           ,'lang:admin_supplies_disco'                       ,'required|max_length[40]');
                        $this->form_validation->set_rules('memoria'         ,'lang:admin_supplies_memoria'                     ,'required|max_length[40]');
                        $this->form_validation->set_rules('procesador'      ,'lang:admin_supplies_procesador'                  ,'required|max_length[50]');    
                    }
                    else{
                        $this->form_validation->set_rules('disco'           ,'lang:admin_supplies_disco'                       ,'max_length[40]');
                        $this->form_validation->set_rules('memoria'         ,'lang:admin_supplies_memoria'                     ,'max_length[40]');
                        $this->form_validation->set_rules('procesador'      ,'lang:admin_supplies_procesador'                  ,'max_length[50]');
                    }
                    $this->form_validation->set_rules('sophos_fecha'    ,'lang:admin_supplies_sophos_fecha'                ,'max_length[10]');
                    $this->form_validation->set_rules('id_teamviewer'   ,'lang:admin_supplies_id_teamviewer'               ,'max_length[40]');
                    $this->form_validation->set_rules('providerId'      ,'lang:main_owner'                                 ,'required|max_length[40]');
                    $this->form_validation->set_rules('siteId'          ,'lang:site'                                       ,'required|max_length[40]');
                    $this->form_validation->set_rules('boxId'           ,'lang:admin_supplies_box'                         ,'required|max_length[40]');
                    
                    if ($this->form_validation->run() == FALSE) {
                        // echo validation_errors();show_404();
                        $data->sites = $this->Sites->getSites();
                        $data->categories = $this->Categories->getCategories();
                        $data->discs = getDiscs();
                        $data->memories = getMemories();
                        $data->procesors = getProcesors();
                        $data->owners = $this->Owners->getOwners();
                        $data->brands = $this->Brands->getBrands();
                        $data->states = getStates();
                        $data->providers = $this->Providers->getProviders();
        
                        $this->load->view('_shared/header');
                        $this->load->view('supplies/edit',$data);
                        $this->load->view('_shared/footer');
        
                    }
                    else{
                        $supplyUpdate = array(
                            'supplyId'           => $this->uri->segment(3),
                            'numero_serie'       => $this->input->post('numero_serie'),
                            'nombre'             => $this->input->post('nombre'),
                            'purchaseDate'       => $this->input->post('purchaseDate'),
                            'state'              => $this->input->post('state'),
                            'marca'              => $this->input->post('marca'),
                            'sophos_fecha'       => $this->input->post('sophos_fecha'),
                            'disco'              => $this->input->post('disco'),
                            'memoria'            => $this->input->post('memoria'),
                            'procesador'         => $this->input->post('procesador'),
                            'imgs'               => '',
                            'id_teamviewer'      => $this->input->post('id_teamviewer'),
                            'ownerId'            => $this->input->post('ownerId'),
                            'providerId'         => $this->input->post('providerId'),
                            'categoryId'         => $this->input->post('categoryId'),
                            'boxId'              => $this->input->post('boxId')
                        );
                        
                        if($this->input->post('state') == getStates()[0]){
                            $supplyUpdate['stock'] = 1;
                        }
                        else{
                            $supplyUpdate['stock'] = 0;
                        }

                        if($_FILES['userFile']['name'][0] != "")
                        {
                            $this->uploadFile($supplyUpdate,$data->supply->imgs);
                        }
                        $res = $this->Supplies->Edit($supplyUpdate);
        
                        if(isset($res))
                        {
                            $this->session->set_flashdata('suppliesMessage', 'edit');
                            header('Location:/'.FOLDERADD.'/supplies'); 
                        }
                    }
                }
                else {
                    header('Location:/'.FOLDERADD);
                }
            }
            else {
                header('Location:/'.FOLDERADD);
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function view()
    {
        if($this->Identity_model->Validate('supplies/view')){
            $this->getViewDetailsOrDelete("view");
        }
        else {
            header('Location:'.base_url());
        }
    }

    function delete()
    {
        if($this->Identity_model->Validate('supplies/admin')){

            $this->getViewDetailsOrDelete("delete");
        }
        else {
            header('Location:'.base_url());
        }
    }

    private function getViewDetailsOrDelete($type)
    {
        if($this->uri->segment(3)){
            
            $supplyId = $this->uri->segment(3);

            $res = new StdClass();
            $res->supply = $this->Supplies->GetSupplyById($supplyId);
            
            if(isset($res->supply))
            {

                if($this->session->supplyId == $this->uri->segment(3) && $this->uri->segment(2) == "delete"){
                    $this->session->set_flashdata('deleteMessage', 'selfDelete');
                    header('Location:'.base_url().'supplies');
                }
                else{
                    if($this->input->post('supplyId') && $this->input->post('supplyId') == $res->supply->supplyId && $type == 'delete'){
                            
                        $supplyDelete = $res->supply->supplyId;
                        $res = $this->Supplies->Delete($supplyDelete);
                        if($res == "success")
                        {
                            $this->session->set_flashdata('suppliesMessage', 'delete');
                            header('Location:'.base_url().'supplies');
                        }
                    }
                    else {
                        $res->type = $type;
                        $res->records = $this->getsupplyrecord($supplyId);
                        $res->operator = $this->getoperatorbysupplyid($supplyId);
                        
                        $this->load->view('_shared/header');
                        $this->load->view('supplies/view-delete',$res);
                        $this->load->view('_shared/footer');
                    }
                }
                    
            }
            else {
                header('Location:'.base_url());
            }
        }
        else {
            header('Location:'.base_url());
        }
    }

    private function uploadFile(&$supplyInsert,$imgs = FALSE)
    {   
        //hacer el upload de las fotos
        $timestamp = time();
    
        $config['upload_path']          = './_uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 50480;

        $count = count($_FILES['userFile']['name']);
        
        for ($i=0; $i < $count; $i++) { 
            
            if (isset($_FILES['userFile']['name'][$i]) && $_FILES['userFile']['name'][$i] != NULL) {
               
                $_FILES['file']['name'] = $_FILES['userFile']['name'][$i];
                $_FILES['file']['type'] = $_FILES['userFile']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['userFile']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['userFile']['error'][$i];
                $_FILES['file']['size'] = $_FILES['userFile']['size'][$i];


                $name = $timestamp.'-'.$_FILES['file']['name'];
                $config['file_name']			= $name;
            }
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file'))
            {
                $response = new StdClass();
                $response->status  = $this->upload->display_errors();
                echo json_encode($response);
            }
            else{
                $upload_data = $this->upload->data();
                $supplyInsert['imgs']  .= $upload_data['file_name'].';';
            }
        }
        if($imgs){
            $imgsArray = explode(";",$imgs);
            $imgsC = count($imgsArray);
            for ($j=0; $j < $imgsC; $j++) { 
                unlink('./_uploads/'.$imgsArray[$j]);
            }
        }
    }

    public function getSupplies()
    {
        $filters  = new StdClass();

        $filters -> category = ($this->input->post('category') != 0)  ? $this->input->post('category') : null;
        $filters -> state    = ($this->input->post('state') != '0')   ? $this->input->post('state')    : null;
        $filters -> owner    = ($this->input->post('owner') != 0)     ? $this->input->post('owner')    : null;
        $filters -> site     = ($this->input->post('site') != 0)      ? $this->input->post('site')     : null;
        $filters -> box      = ($this->input->post('box') != 0)       ? $this->input->post('box')      : null;

        if (strlen($_POST['search']['value'])>0){

            $filters -> search_value   = $_POST['search']['value'];
        }
        else{

            $filters -> search_value   = null;
        }

        $this->Supplies->GetSuppliesWithFilters($filters);
    }

    public function getsuppliesbycategory()
    {
        if($this->Identity_model->Validate('shipments/admin'))
		{
			if($this->input->post('categoryId')){
				echo json_encode($this->Supplies->GetSuppliesByCategory($this->input->post('categoryId')));
			}
        }
        else{
            show_404();
        }
    }

    public function getsuppliesbyoperator()
    {
        if($this->Identity_model->Validate('shipments/admin'))
		{
			if($this->input->post('operatorId')){
				echo json_encode($this->Supplies->getSuppliesByOperator($this->input->post('operatorId')));
			}
        }
        else{
            show_404();
        }
    }

    public function getsupplyrecord($supplyId)
    {
        if($this->Identity_model->Validate('shipments/admin'))
        {
            return $this->Supplies->GetSupplyRecord($supplyId);
        }
        else{
            show_404();
        }
    }

    public function getoperatorbysupplyid($supplyId)
    {
        if($this->Identity_model->Validate('shipments/admin'))
        {
            return $this->Operators->GetOperatorBySupplyId($supplyId);
        }
        else{
            show_404();
        }
    }

    public function getColorCounters()
    {
        $categoryId = $this->input->post('categoryId');
        if($categoryId == 0){
            echo json_encode($this->Supplies->GetColorCounters());    
        }
        else{
            echo json_encode($this->Supplies->GetColorCounters($this->input->post('categoryId')));
        }
    }
    
    public function summarybsas()
    {
        if($this->Identity_model->Validate('supplies/admin')){
            $data = new StdClass();
            $data->generalCounters = $this->Supplies->GetGeneralCountersBsAs();
            $data->mainCategories = $this->Categories->getCategories();
            
            $this->load->view('_shared/header');
            $this->load->view('supplies/summarybsas', $data);
            $this->load->view('_shared/footer');
        }

        else{
            header('Location:/'.FOLDERADD);
        }
    }

    public function summarylp()
    {
        if($this->Identity_model->Validate('supplies/admin')){
            $data = new StdClass();
            $data->generalCounters = $this->Supplies->GetGeneralCountersLP();
            $data->mainCategories = $this->Categories->getCategories();

            $this->load->view('_shared/header');
            $this->load->view('supplies/summarylp', $data);
            $this->load->view('_shared/footer');
        }

        else{
            header('Location:/'.FOLDERADD);
        }
    }

    public function getinrepairsupplies()
    {
        if($this->Identity_model->Validate('shipments/admin'))
		{
            echo json_encode($this->Supplies->GetInRepairSupplies());
        }
        else{
            show_404();
        }
    }

    //scripts de carga/actualizacion masiva
    // public function UpdateSuppliesAndShipments()
    // {
    //     $query = "SELECT * from editar WHERE supplyId NOT IN (725,215,261,353,449)";
    //     $res = $this->db->query($query)->result();
        
    //     for ($i=0; $i < count($res); $i++) { 
    //         $supply = $res[$i];

    //         $queryC = "SELECT categoryId FROM categories WHERE name = ?";
    //         $category = $this->db->query($queryC,$supply->categoryId)->row();

    //         if(isset($category))
    //         {
    //             $this->db->set('numero_serie', $supply->numero_serie);
    //             $this->db->set('nombre', $supply->nombre);
    //             $this->db->set('purchaseDate', $supply->purchaseDate);
    //             $this->db->set('state', $supply->state);
    //             $this->db->set('marca', $supply->marca);
    //             $this->db->set('sophos_fecha', $supply->sophos_fecha);
    //             $this->db->set('disco', $supply->disco);
    //             $this->db->set('memoria', $supply->memoria);
    //             $this->db->set('procesador', $supply->procesador);
    //             $this->db->set('id_teamviewer', $supply->id_teamviewer);
    //             $this->db->set('imgs', $supply->imgs);
    //             $this->db->set('stock', $supply->stock);
    //             $this->db->set('ownerId', $supply->ownerId);
    //             $this->db->set('providerId', $supply->providerId);
    //             $this->db->set('categoryId', $category->categoryId);
    //             $this->db->where('supplyId', $supply->supplyId);
    //             $this->db->update('supplies');
                
    //             // print_r("updated:");print_r($supply);print_r("<br>");
    //         }  
    //         else{
    //             print_r("no se encontro categoria: $supply->categoryId");print_r("<br>");
    //         }

    //         //valida que la fila actual no este mandando un insumo sin asignar
    //         if(!is_null($supply->dni))
    //         {
    //             $query = "SELECT supplyId FROM assignedsupplies a JOIN operators o ON a.operatorId = o.operatorId WHERE dni = ? AND a.supplyId = ?";
    //             $assigned = $this->db->query($query,array($supply->dni,$supply->supplyId))->row();
    //             if(!isset($assigned))
    //             {
    //                 $query = "SELECT operatorId FROM operators WHERE dni = ?";
    //                 $operator = $this->db->query($query,$supply->dni)->row();
    //                 if(isset($operator))
    //                 {
    //                     $shipmentInsert = array(
    //                         'supplies'      => $supply->supplyId.",",
    //                         'type'          => getShipmentType()[0],
    //                         'state'         => getShipmentState()[0],
    //                         'operatorId'    => $operator->operatorId
    //                     );
    //                     $this->db->insert('shipments', $shipmentInsert);
                        
    //                     $objectInsert = array(
    //                         'operatorId'    => $operator->operatorId,
    //                         'supplyId'      => $supply->supplyId
    //                     );
    //                     $this->db->insert('assignedsupplies', $objectInsert);
    //                     print_r("insertado:");print_r($objectInsert);print_r("<br>");

    //                     $objectInsert['type'] = getShipmentType()[0];
    //                     $this->db->insert('suppliesrecords', $objectInsert);
    //                 }
    //                 else{
    //                     print_r("no se encontro dni:$supply->dni");print_r("<br>");
    //                 }
    //             }


    //         }
    //         else{
    //             $query = "SELECT assignedId FROM assignedsupplies WHERE supplyId = ?";
    //             $assignedDelete = $this->db->query($query,$supply->supplyId)->result();

    //             for ($j=0; $j < count($assignedDelete); $j++) {
    //                 $d = $assignedDelete[$j];

    //                 insert_audit_logs('assignedsupplies','DELETE',$d);
    //                 print_r("borrado si no tiene dni:");print_r($d);print_r("<br>");

    //                 $this->db->set('active', 0);
    //                 $this->db->where('assignedId', $d->assignedId);
    //                 $this->db->update('assignedsupplies');
    //             }
    //         }
    //     }

    //     $query = "SELECT distinct(dni) FROM editar";
    //     $res = $this->db->query($query)->result();

    //     for ($i=0; $i < count($res); $i++) { 
    //         $row = $res[$i];

    //         $query = "SELECT assignedId,supplyId,dni FROM assignedsupplies a JOIN operators o ON a.operatorId = o.operatorId
    //         WHERE o.dni = ?
    //         AND supplyId NOT IN (SELECT supplyId FROM editar where dni = ?)";

    //         $toDelete = $this->db->query($query,array($row->dni,$row->dni))->result();
    //         for ($j=0; $j < count($toDelete); $j++) { 
    //             $d = $toDelete[$j];

    //             $this->db->set('active', 0);
    //             $this->db->where('supplyId', $d->supplyId);
    //             $this->db->update('supplies');

    //             $this->db->set('active', 0);
    //             $this->db->where('assignedId', $d->assignedId);
    //             $this->db->update('assignedsupplies');

    //         }
    //     }
        
    //     echo "success";
    // }

    public function changeCampaings()
    {
        $query = "SELECT * from campaña_asociada where campaña <> ''";
        $campañas = $this->db->query($query)->result();

        foreach ($campañas as $campaña) {
            $customerId = $this->db->get_where('customers',array('name' => $campaña->campaña))->row();
            $operatorId = $this->db->get_where('operators',array('dni' => $campaña->dni))->row();
            if(!isset($customerId))
            {
                print_r("no se encontro campaña: $campaña->campaña<br>");
            }
            if(!isset($operatorId))
            {
                print_r("no se encontró dni: $campaña->dni <br>");
            }
            if(isset($operatorId) && isset($customerId))
            {
                $this->db->set('customerId', $customerId->customerId);
                $this->db->where('operatorId', $operatorId->operatorId);
                $this->db->update('operators');
            }
        }
    }

    // public function updateSuppliesRecords()
    // {
    //     $query = "SELECT * FROM `shipments` where type = 'Retiro de insumos' AND state = 'Entregado' AND refundreasonId = 3 AND active = 0";
    //     $shipments = $this->db->query($query)->result();

    //     foreach ($shipments as $shipment ) {
    //         $supplies = explode(',',$shipment->supplies);
    //         array_pop($supplies);
    //         for ($i=0; $i < count($supplies); $i++) { 
    //             $queryUpdate = "UPDATE suppliesrecords SET operatorId = {$shipment->operatorId} 
    //             WHERE type = 'Retiro de insumos'
    //             AND date = '{$shipment->deliveryDate}'
    //             AND supplyId = {$supplies[$i]}";
    //             print_r($queryUpdate);
    //             print_r("<br>");
    //         }
    //     }
    // }
}

/* End of file Supplies.php */


?>