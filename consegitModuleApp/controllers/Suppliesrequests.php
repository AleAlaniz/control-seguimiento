<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliesrequests extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Suppliesrequests_model','Suppliesrequests');
        $this->load->library('excel');
    }

    public function index()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){
            $data = new StdClass();
            $this->load->view('_shared/header');
            $this->load->view('suppliesrequests/index');
            $this->load->view('_shared/footer');
        }
        else{
            header('Location:'.base_url());
        }
    }

    public function getRequests()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){
            $date = date("H:i:s",strtotime('-3 hours'));
            $range = "";
            $day  = date("D");

            if($day == "Mon" || $day == "Sun"){
                $firstDate = date("Y-m-d 00:00:00",strtotime('-1 day'));
                $secondDate = date("Y-m-d 23:59:00",strtotime('-1 day'));
            }
            else{
                //primer periodo entrando desde 00 a 14:59
                if(strtotime($date) >= strtotime("00:00:00") && strtotime($date) <= strtotime("14:59:00")){
                    $firstDate = date("Y-m-d 15:00:00",strtotime('-1 day'));
                    $secondDate = date("Y-m-d 23:59:00",strtotime('-1 day'));
                }
                //segundo periodo entrando de 15 a 23:59
                else{
                    $firstDate = date("Y-m-d 00:00:00");
                    $secondDate = date("Y-m-d 14:59:00");
                }
            }

            if (strlen($_POST['search']['value'])>0){

                $search_value   = $_POST['search']['value'];
            }
            else{
    
                $search_value   = null;
            }
            $this->Suppliesrequests->GetRequests($firstDate,$secondDate,$search_value);
        }
        else{
            header('Location:'.base_url());
        }
    }

    public function edit()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){
            $state = $this->input->post("state");
            if(isset($state))
            {
                $state = $state == "acceptRequest" ? $this->lang->line('request_accepted') : $this->lang->line("request_rejected");
                echo $this->Suppliesrequests->Edit($this->input->post("requestId"),$state);
            }
            else{
                echo $this->lang->line("request_error_id_required");
            }
        }
        else{
            header('Location:'.base_url());
        }
    }

    public function getRequestsToSend()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){
            echo json_encode($this->Suppliesrequests->GetRequestsToSend());
        }
        else{
            show_404();
        }
    }

    public function uploadFile()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=2; $row<=$highestRow; $row++)
                    {
                        $data = array(
                            'dateFile' 		    =>	date('Y-m-d H:i:s',strtotime($worksheet->getCellByColumnAndRow(0, $row)->getValue())),
                            'stateFile'			=>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'priority'			=>	$worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'manager'			=>	$worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'adviser'			=>	$worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            // 'operatorId'		=>	$worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'supplies'			=>	$worksheet->getCellByColumnAndRow(19, $row)->getValue()
                        );
                        
                        $query = $this->db->get_where('suppliesrequests',$data)->result();
                        
                        if(count($query) == 0)
                        {
                            $data['operatorId'] = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                            $dataArray[] = $data;
                        }
                    }
                }
                if(count($dataArray)>0){
                    $this->Suppliesrequests->UploadFile($dataArray);
                }
                
                $query = "SELECT operatorId FROM suppliesrequests";
                $requests = $this->db->query($query)->result();
                foreach ($requests as $request) {
                    $operatorId = $this->db->get_where('operators',array('dni' => $request->operatorId))->row();
                    if(isset($operatorId))
                    {
                        $this->db->set('operatorId', $operatorId->operatorId);
                        $this->db->where('operatorId', $request->operatorId);
                        $this->db->update('suppliesrequests');
                    }
                }
                $this->session->set_flashdata('requestMessage', 'upload');
                echo "success";
            }	
        }
        else{
            show_404();
        }
    }

    public function searchRequests()
    {
        if($this->Identity_model->Validate('suppliesrequests/admin')){
            $requestData = new StdClass();

            $requestData->operatorId = $this->input->post('operatorId');
            $requestData->requestId = $this->input->post('requestId');

            echo json_encode($this->Suppliesrequests->SearchRequests($requestData));
        }
        else{
            show_404();
        }
    }
    
}

/* End of file Suppliesrequests.php */
