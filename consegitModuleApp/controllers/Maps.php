<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Maps extends CI_Controller {
    
        public function index()
        {
            if($this->Identity_model->Validate('maps/view')){
                $this->load->view('_shared/header');
                $this->load->view('maps/index');
                $this->load->view('_shared/footer');
            }
            else {
                header('Location:/'.FOLDERADD);
            }
        }

        public function getAddresses()
        {
            if($this->Identity_model->Validate('maps/view')){
                
                // $this->db->select('addressId,CONCAT_WS(",",calle, numero, localidad, provincia) as address');
                $this->db->select('lat,lang');
                $res =$this->db->get('addresses')->result();
                echo json_encode($res);
            }
            else{
                show_404();
            }
        }

        public function saveCoordinates()
        {
            if($this->Identity_model->Validate('maps/view')){
                $this->db->update_batch('addresses',$this->input->post("coordinates"),'addressId');
                echo "success";
            }
        }
    
    }
    
    /* End of file Maps.php */
    
?>