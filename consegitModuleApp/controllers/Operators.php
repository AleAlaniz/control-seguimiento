<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operators extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Operators_model','Operators');
        $this->load->model('Supplies_model','Supplies');
    }
    
    public function index()
    {
        if($this->Identity_model->Validate('operators/view')){
            $this->load->view('_shared/header');
            $this->load->view('operators/index');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getOperators()
    {
        $filters  = new StdClass();

        if (strlen($_POST['search']['value'])>0){

            $filters -> search_value   = $_POST['search']['value'];
        }
        else{

            $filters -> search_value   = null;
        }

        $this->Operators->GetOperators($filters);
    }

    public function create()
    {
        if($this->Identity_model->Validate('operators/admin')){
            $this->form_validation->set_rules('id_lote'         ,'lang:operator_id_lote'                    ,'max_length[40]');
            $this->form_validation->set_rules('id_interno'      ,'lang:operator_id_interno'                 ,'max_length[40]');
            $this->form_validation->set_rules('empresa'         ,'lang:admin_operators_empresa'             ,'required|max_length[40]');
            $this->form_validation->set_rules('legajo'          ,'lang:operator_legajo'                     ,'required|max_length[40]');
            $this->form_validation->set_rules('dni'             ,'lang:sales_dni'                           ,'required|max_length[11]|is_unique_active[operators.dni]');
            $this->form_validation->set_rules('cuenta'          ,'lang:operator_cuenta'                     ,'required|max_length[40]');
            $this->form_validation->set_rules('usuario'         ,'lang:operator_usuario'                    ,'max_length[40]');
            $this->form_validation->set_rules('nombre'          ,'lang:general_name'                        ,'required|max_length[50]');
            $this->form_validation->set_rules('apellido'        ,'lang:admin_users_lastName'                ,'required|max_length[40]');
            $this->form_validation->set_rules('celular'         ,'lang:admin_operators_celular'             ,'max_length[40]');
            $this->form_validation->set_rules('email'           ,'lang:admin_operators_email'               ,'max_length[40]');
            $this->form_validation->set_rules('province'        ,'lang:admin_operators_province'            ,'required|max_length[40]');
            $this->form_validation->set_rules('turno'           ,'lang:admin_users_turn'                    ,'max_length[40]|callback_check_turn');
            $this->form_validation->set_rules('contacto'        ,'lang:admin_operators_contacto'            ,'max_length[40]');
            $this->form_validation->set_rules('prioridad'       ,'lang:admin_operators_prioridad'           ,'max_length[40]');
            $this->form_validation->set_rules('pc_de_cat'       ,'lang:admin_operators_pc_de_cat'           ,'max_length[40]');
            
            if ($this->form_validation->run() == FALSE) {
                $states = array("Si","No");
                
                $data = new StdClass();
                $data->turns = getTurns();
                $data->provinces = getProvinces();
                $data->states = $states;

                $this->load->view('_shared/header');
                $this->load->view('operators/create',$data);
                $this->load->view('_shared/footer');

            }
            else{
                $operatorInsert = array(
                    'idlote'        => $this->input->post('id_lote'),
                    'idinterno'     => $this->input->post('id_interno'),
                    'empresa'       => $this->input->post('empresa'),
                    'legajo'        => $this->input->post('legajo'),
                    'dni'           => $this->input->post('dni'),
                    'cuenta'        => $this->input->post('cuenta'),
                    'usuario'       => $this->input->post('usuario'),
                    'nombre'        => $this->input->post('nombre'),
                    'apellido'      => $this->input->post('apellido'),
                    'celular'       => $this->input->post('celular'),
                    'email'         => $this->input->post('email'),
                    'provincia'     => $this->input->post('province'),
                    'localidad'     => $this->input->post('location'),
                    'calle'         => $this->input->post('street'),
                    'altura'        => $this->input->post('height'),
                    'codigo_postal' => $this->input->post('postal_code'),
                    'piso_depto'    => $this->input->post('floor_dept'),
                    'turno'         => $this->input->post('turno'),
                    'contacto'      => $this->input->post('contacto'),
                    'prioridad'     => $this->input->post('prioridad'),
                    'pc_de_cat'     => $this->input->post('pc_de_cat'),

                    'cuil'                           => $this->input->post('cuil'),
                    'fecha_nacimiento'               => $this->input->post('fecha_nacimiento'),
                    'site_original'                  => $this->input->post('site_original'),
                    'horario'                        => $this->input->post('horario'),
                    'puesto'                         => $this->input->post('puesto'),
                    'supervisor'                     => $this->input->post('supervisor'),
                    'team_leader'                    => $this->input->post('team_leader'),
                    'telet_estado_actual'            => $this->input->post('telet_estado_actual'),
                    'telet_persona_riesgo'           => $this->input->post('telet_persona_riesgo'),
                    'telet_fliares_cargo'            => $this->input->post('telet_fliares_cargo'),
                    'telet_pc_nb_propia'             => $this->input->post('telet_pc_nb_propia'),
                    'telet_elige_operar_site'        => $this->input->post('telet_elige_operar_site'),
                    'perform_cuartil'                => $this->input->post('perform_cuartil'),
                    'perform_presentismo'            => $this->input->post('perform_presentismo'),
                    'perform_prioridad_empleado'     => $this->input->post('perform_prioridad_empleado'),
                    'perform_calidad'                => $this->input->post('perform_calidad')
                );
                
                $res = $this->Operators->Create($operatorInsert);

                if(isset($res))
                {
                    $this->session->set_flashdata('operatorMessage', 'create');
                    header('Location:/'.FOLDERADD.'/operators'); 
                }
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function edit()
    {
        if($this->uri->segment(3))
        {
            $url_id = $this->uri->segment(3);
            $result = new StdClass();
            $states = array("Si","No");

            $result->states = $states;
            $result->operator = $this->Operators->GetOperatorById($url_id);

            if(isset($result))
            {
                
                $this->form_validation->set_rules('id_lote'         ,'lang:operator_id_lote'                    ,'max_length[40]');
                $this->form_validation->set_rules('id_interno'      ,'lang:operator_id_interno'                 ,'max_length[40]');
                $this->form_validation->set_rules('empresa'         ,'lang:admin_operators_empresa'             ,'required|max_length[40]');
                $this->form_validation->set_rules('legajo'          ,'lang:operator_legajo'                     ,'required|max_length[40]');
                $this->form_validation->set_rules('dni'             ,'lang:sales_dni'                           ,'required|max_length[11]|edit_unique[operators.dni.operatorId.'.$url_id.'.TRUE]');
                $this->form_validation->set_rules('cuenta'          ,'lang:operator_cuenta'                     ,'required|max_length[40]');
                $this->form_validation->set_rules('usuario'         ,'lang:operator_usuario'                    ,'max_length[40]');
                $this->form_validation->set_rules('nombre'          ,'lang:general_name'                        ,'required|max_length[50]');
                $this->form_validation->set_rules('apellido'        ,'lang:admin_users_lastName'                ,'required|max_length[40]');
                $this->form_validation->set_rules('celular'         ,'lang:admin_operators_celular'             ,'max_length[40]');
                $this->form_validation->set_rules('email'           ,'lang:admin_operators_email'               ,'max_length[40]');
                $this->form_validation->set_rules('province'        ,'lang:admin_operators_province'            ,'required|max_length[200]');
                $this->form_validation->set_rules('turno'           ,'lang:admin_users_turn'                    ,'max_length[40]|callback_check_turn');
                $this->form_validation->set_rules('contacto'        ,'lang:admin_operators_contacto'            ,'max_length[40]');
                $this->form_validation->set_rules('prioridad'       ,'lang:admin_operators_prioridad'           ,'max_length[40]');
                $this->form_validation->set_rules('pc_de_cat'       ,'lang:admin_operators_pc_de_cat'           ,'max_length[40]');

                if ($this->form_validation->run() == FALSE) {
                
                    $result->turns = getTurns();
                    $result->provinces = getProvinces();
                    $this->load->view('_shared/header');
                    $this->load->view('operators/edit',$result);
                    $this->load->view('_shared/footer');
                }

                else{
                    $operatorEdit = array(
                        'operatorId'    => $this->uri->segment(3),
                        'idlote'       => $this->input->post('id_lote'),
                        'idinterno'    => $this->input->post('id_interno'),
                        'empresa'       => $this->input->post('empresa'),
                        'legajo'        => $this->input->post('legajo'),
                        'dni'           => $this->input->post('dni'),
                        'cuenta'        => $this->input->post('cuenta'),
                        'usuario'       => $this->input->post('usuario'),
                        'nombre'        => $this->input->post('nombre'),
                        'apellido'      => $this->input->post('apellido'),
                        'celular'       => $this->input->post('celular'),
                        'email'         => $this->input->post('email'),
                        'provincia'     => $this->input->post('province'),
                        'localidad'     => $this->input->post('location'),
                        'calle'         => $this->input->post('street'),
                        'altura'        => $this->input->post('height'),
                        'codigo_postal' => $this->input->post('postal_code'),
                        'piso_depto'    => $this->input->post('floor_dept'),
                        'turno'         => $this->input->post('turno'),
                        'contacto'      => $this->input->post('contacto'),
                        'prioridad'     => $this->input->post('prioridad'),
                        'pc_de_cat'     => $this->input->post('pc_de_cat'),

                        'cuil'                           => $this->input->post('cuil'),
                        'fecha_nacimiento'               => $this->input->post('fecha_nacimiento'),
                        'site_original'                  => $this->input->post('site_original'),
                        'horario'                        => $this->input->post('horario'),
                        'puesto'                         => $this->input->post('puesto'),
                        'supervisor'                     => $this->input->post('supervisor'),
                        'team_leader'                    => $this->input->post('team_leader'),
                        'telet_estado_actual'            => $this->input->post('telet_estado_actual'),
                        'telet_persona_riesgo'           => $this->input->post('telet_persona_riesgo'),
                        'telet_fliares_cargo'            => $this->input->post('telet_fliares_cargo'),
                        'telet_pc_nb_propia'             => $this->input->post('telet_pc_nb_propia'),
                        'telet_elige_operar_site'        => $this->input->post('telet_elige_operar_site'),
                        'perform_cuartil'                => $this->input->post('perform_cuartil'),
                        'perform_presentismo'            => $this->input->post('perform_presentismo'),
                        'perform_prioridad_empleado'     => $this->input->post('perform_prioridad_empleado'),
                        'perform_calidad'                => $this->input->post('perform_calidad')
                    );
                    
                    $res = $this->Operators->Edit($operatorEdit);

                    if($res == "success"){
                        $this->session->set_flashdata('operatorMessage', 'edit');
                        header('Location:/'.FOLDERADD.'/operators'); 
                    }
                }
            }
            else {
                header('Location:/'.FOLDERADD);
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function check_turn()
    {
        $turns = getTurns();
        if(in_array($this->input->post('turno'),$turns))
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_turn',$this->lang->line('operators_unexisted_turn'));
            return FALSE;
        }
    }
    
    public function view()
    {
        if($this->Identity_model->Validate('operators/view')){
            $this->getViewDetailsOrDelete("view");
        }
        else {
            header('Location:'.base_url());
        }
    }

    function delete()
    {
        if($this->Identity_model->Validate('operators/admin')){

            $this->getViewDetailsOrDelete("delete");
        }
        else {
            header('Location:'.base_url());
        }
    }

    private function getViewDetailsOrDelete($type)
    {
        if($this->uri->segment(3)){
            if($this->Identity_model->Validate('operators/view')){
                $operatorId = $this->uri->segment(3);

                $res = new StdClass();
                $res->operator = $this->Operators->GetOperatorById($operatorId);

                if(isset($res->operator))
                {

                    if($this->session->operatorId == $this->uri->segment(3) && $this->uri->segment(2) == "delete"){
                        $this->session->set_flashdata('deleteMessage', 'selfDelete');
                        header('Location:'.base_url().'operators');
                    }
                    else{
                        if($this->input->post('operatorId') && $this->input->post('operatorId') == $res->operator->operatorId && $type == 'delete'){
                                
                            $operatorDelete = $res->operator->operatorId;
                            $res = $this->Operators->Delete($operatorDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('operatorMessage', 'delete');
                                header('Location:'.base_url().'operators');
                            }
                        }
                        else {
                            $res->type = $type;
                            $res->supplies = $this->Supplies->getSuppliesByOperator($operatorId);
                            $res->records = $this->getoperatorrecord($operatorId);

                            $this->load->view('_shared/header');
                            $this->load->view('operators/view-delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                       
                }
                else {
                    header('Location:'.base_url());
                }
            }
            else {
                header('Location:'.base_url());
            }
        }
        else {
            header('Location:'.base_url());
        }
    }

    public function searchOperators()
	{
		if ($this->Identity_model->Validate('shipments/admin') && $this->input->post('search')){

			if($this->input->post('search') == NULL){

				echo json_encode(array());
				return;
			}

			$searchString = strtolower(substr(trim($this->input->post('search', TRUE)), 0, 50));
			echo json_encode ($this->Operators->SearchOperators($searchString));

		}
		else{
			show_404();
		}
    }
    
    public function getoperatorswithsupplies()
	{
		if ($this->Identity_model->Validate('shipments/admin')){

			echo json_encode($this->Operators->GetOperatorsWithSupplies());

		}
		else{
			show_404();
		}
    }
    
    public function getoperatorrecord($operatorId)
    {
        if($this->Identity_model->Validate('shipments/admin'))
        {
            return $this->Operators->GetOperatorRecord($operatorId);
        }
        else{
            show_404();
        }
    }

}

/* End of file Operators.php */
