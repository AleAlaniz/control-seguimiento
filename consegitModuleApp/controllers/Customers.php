<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Customers extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Customers_model','Customers');
        }

        public function index()
        {
            if($this->Identity_model->Validate('customers/view')){
                $customers = $this->Customers->getCustomers();
                $this->load->view('_shared/header');
                $this->load->view('customers/index', array('customers' => $customers));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('customers/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[customers.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('customers/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $tlInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Customers->Create($tlInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('customerMessage', 'create');
                        header('Location:'.base_url().'customers'); 
                    }
                }
                
            }
            else{
                header('Location:'.base_url().'customers'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('customers/admin')){

                    $customerId = $this->uri->segment(3);
                    $sql = "SELECT * FROM customers WHERE customerId = ? AND active = 1";
                    
                    $res = new StdClass();
                    $res->customer = $this->db->query($sql,$customerId)->row();

                    if(isset($res->customer))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[customers.name.customerId.'.$customerId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $customerId;

                            $this->load->view('_shared/header');
                            $this->load->view('customers/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'customerId' => $customerId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Customers->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('customerMessage', 'edit');
                                header('Location:'.base_url().'customers'); 
                            }
                        }
                    }
                    else{
                        header('Location:'.base_url());     
                    }
                }
                else{
                    header('Location:'.base_url().'customers');     
                }
            }
            else {
                header('Location:'.base_url().'customers'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('customers/admin')){
                    $customerId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->customer = $this->Customers->getcustomer($customerId);
                    
                    if(isset($res->customer))
                    {
                        if($this->input->post('customerId') && $this->input->post('customerId') == $res->customer->customerId){
                            $customerDelete = array('customerId' => $res->customer->customerId);
                            $res = $this->Customers->Delete($customerDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('customerMessage', 'delete');
                                header('Location:'.base_url().'customers');
                            }
                        }
                        else {
                            $res->unique = $customerId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('customers/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }   
            else {
                header('Location:'.base_url());
            }
        }
    }

    /* End of file customers.php */
?>