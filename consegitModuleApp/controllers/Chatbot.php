<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Chatbot extends CI_Controller {

    	 public function __construct()
   		{
   		    parent::__construct();
   		}
    
        public function index()
        {	
            $this->load->view('_shared/header');
            $this->load->view('chatbot/index');
            $this->load->view('_shared/footer');
           
        }

        public function bot_bbva()
        { 
            $this->load->view('_shared/header');
            $this->load->view('chatbot/bot_bbva');
            $this->load->view('_shared/footer');
           
        }
    }
    
    /* End of file Chatbot.php */
    
?>