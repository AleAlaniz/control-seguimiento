<?php   
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','Users');
    }

    public function index()
    {   
        if($this->Identity_model->Validate('users/view')){
            $users = $this->Users->GetUsers();
            $this->load->view('_shared/header');
            $this->load->view('users/index', array('users' => $users));
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('users/admin')){

            $this->form_validation->set_rules('userName'    ,'lang:login_username'         ,'is_unique_active[users.userName]|required|max_length[50]');
            $this->form_validation->set_rules('password'    ,'lang:login_password'         ,'required|min_length[4]|max_length[128]');
            $this->form_validation->set_rules('confirmPassword', 'lang:admin_users_confirm_password', 'required|matches[password]',
                array(
                    'matches'     => $this->lang->line('admin_users_passwordmatch')
                ));
            $this->form_validation->set_rules('name'        ,'lang:general_name'           ,'required|max_length[50]');
            $this->form_validation->set_rules('lastName'    ,'lang:admin_users_lastName'   ,'required|max_length[50]');
            $this->form_validation->set_rules('role'        ,'lang:admin_users_role'        ,'exist_role');

            if ($this->form_validation->run() == FALSE) {

                $data = $this->Users->getFormData();

                $this->load->view('_shared/header');
                $this->load->view('users/create',$data);
                $this->load->view('_shared/footer');

            } 
            else 
            {

                //para validar si el id de role enviado está en la base de datos
                if($this->input->post('role') && $this->input->post('role') != "EMPTY"){

                    $roleId = $this->input->post('role') ? $this->input->post('role') : $this->input->post('roleH');

                    $sql = "SELECT * FROM roles WHERE roleId = ?";
                    $result = $this->db->query($sql,$roleId)->row();

                    if(!isset($result))
                    {
                        show_404();
                    }
                }
                
                $userInsert = array(
                    'userName'      => htmlspecialchars($this->input->post('userName')), 
                    'password'      => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
                    'name'          => htmlspecialchars($this->input->post('name')), 
                    'lastName'      => htmlspecialchars($this->input->post('lastName')), 
                    'createDate'    => date('Y-m-d H:i:s'),
                    'roleId'        => $this->input->post('role') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('role')) , 
                );
                $res = $this->Users->Create($userInsert);
                if(isset($res)){
                    $this->session->set_flashdata('userMessage', 'create');
                    header('Location:/'.FOLDERADD.'/users');    
                }

            }

        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }
    
    public function edit()
    {
        if($this->uri->segment(3))
        {
            $userId = $this->uri->segment(3);
            $sql = "SELECT * FROM users WHERE userId = ? AND active = 1";
            $res = $this->db->query($sql, $userId)->row();

            if(isset($res))
            {
                if($this->Identity_model->Validate('users/admin')){
                    
                    $this->form_validation->set_rules('userName'        ,'lang:login_username'         ,'required|max_length[30]|edit_unique[users.userName.userId.'.$userId.'.TRUE]');
                    $this->form_validation->set_rules('name'            ,'lang:general_name'           ,'required|max_length[50]');
                    $this->form_validation->set_rules('lastName'        ,'lang:admin_users_lastName'   ,'required|max_length[50]');
                    $this->form_validation->set_rules('role'            ,'lang:admin_users_role'       ,'exist_role');

                    if ($this->form_validation->run() == FALSE) {
                        $data = $this->Users->GetUserData($userId);
                        
                        $this->load->view('_shared/header');
                        $this->load->view('users/edit',$data);
                        $this->load->view('_shared/footer');

                    } 
                    else 
                    {
                        
                        //para validar si el id de role enviado está en la base de datos
                        if($this->input->post('role') && $this->input->post('role') != "EMPTY")
                        {
                            $roleId = $this->input->post('role') ? $this->input->post('role') : $this->input->post('roleH');

                            $sql = "SELECT * FROM roles WHERE roleId = ?";
                            $result = $this->db->query($sql,$roleId)->row();
                            
                            if(!isset($result))
                            {
                                show_404();
                            }
                        }

                        
                        $userEdit = array(
                            'userName'      => htmlspecialchars($this->input->post('userName')), 
                            'name'          => htmlspecialchars($this->input->post('name')), 
                            'lastName'      => htmlspecialchars($this->input->post('lastName')), 
                            'roleId'        => $this->input->post('role') == "EMPTY" ? NULL : htmlspecialchars($this->input->post('role'))  
                        );
                        // print_r($userEdit);show_404();
                        
                        $res = $this->Users->Edit($userId,$userEdit);
                        if($res == "success"){
                            $this->session->set_flashdata('userMessage', 'edit');
                            header('Location:/'.FOLDERADD.'/users');    
                        }
                    }
                }
                else {
                    header('Location:/'.FOLDERADD);
                } 
            }
            else {
                header('Location:/'.FOLDERADD);
            } 

        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    //aca igual que en edit
    public function delete()
    {
        if($this->Identity_model->Validate('users/admin')){

            $this->getViewDetailsOrDelete("delete");
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function view()
    {
        if($this->Identity_model->Validate('users/view')){
            $this->getViewDetailsOrDelete("view");
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    private function getViewDetailsOrDelete($type)
    {
        if($this->uri->segment(3)){
            if($this->Identity_model->Validate('users/view')){
                $userId = $this->uri->segment(3);

                $res = new StdClass();
                $res->user = $this->Users->GetDetailDeleteData($userId);

                if(isset($res->user))
                {
                    if($this->session->UserId == $this->uri->segment(3) && $this->uri->segment(2) == "delete"){
                        $this->session->set_flashdata('deleteMessage', 'selfDelete');
                        header('Location:/'.FOLDERADD.'/users');
                    }
                    else{
                        if($this->input->post('userId') && $this->input->post('userId') == $res->user->userId && $type == 'delete'){
                                
                            $userDelete = array('userId' => $res->user->userId);
                            $res = $this->Users->Delete($userDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('userMessage', 'delete');
                                header('Location:/'.FOLDERADD.'/users');
                            }
                        }
                        else {
                            $res->type = $type;
                            
                            $this->load->view('_shared/header');
                            $this->load->view('users/view-delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                   
                }
                else {
                    header('Location:/'.FOLDERADD);
                }
            }
            else {
                header('Location:/'.FOLDERADD);
            }
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function changePass()
    {
        if($this->Identity_model->Validate('users/changepass'))
        {
            $this->form_validation->set_rules('password'          , 'lang:login_password'                ,'required|min_length[4]|max_length[128]');
            $this->form_validation->set_rules('confirmPassword'   , 'lang:admin_users_confirm_password'  , 'required|matches[password]',
                array(
                    'matches'     => $this->lang->line('admin_users_passwordmatch')
                ));
            $this->form_validation->set_rules('oldpassword'       , 'lang:login_password'                ,'required|max_length[128]');

            if ($this->form_validation->run() == FALSE) 
            {
                $this->changePassView();
            } 
            else
            {
                $sql = 'SELECT userId, userName, password, roleId FROM users WHERE active = 1 AND userId = ?';
                $query = $this->db->query($sql, $this->session->UserId)->row();
                
                if (isset($query)) 
                {
                    if($this->input->post('oldpassword'))
                    {

                        if(password_verify($this->input->post('oldpassword'),$query->password))
                        {
                            $userEdit = array(
                                'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT)
                            );

                            $res = $this->Users->EditPass($query->userId,$userEdit);
                        }
                        else
                        {
                            $data['invalidPassword'] = TRUE;
                            $this->changePassView($data);
                            return;  
                        }
                    }

                    if($res == "success")
                    {
                        $this->session->set_flashdata('userMessage', 'edit');
                        header('Location:/'.FOLDERADD.'/users/changePass');    
                    }
                }
                else
                {
                    show_404();
                }
            }
        }
        else{
            header('Location:/'.FOLDERADD);
        }

    }

    private function changePassView($data = NULL)
    {
        $this->load->view('_shared/header');
        $this->load->view('users/changepass',$data);
        $this->load->view('_shared/footer');
    }
}

?>
