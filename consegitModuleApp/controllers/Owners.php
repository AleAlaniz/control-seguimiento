<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Owners extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Owners_model','Owners');
        }

        public function index()
        {
            if($this->Identity_model->Validate('owners/view')){
                $owners = $this->Owners->getOwners();
                $this->load->view('_shared/header');
                $this->load->view('owners/index', array('owners' => $owners));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('owners/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[owners.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('owners/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $tlInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Owners->Create($tlInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('ownerMessage', 'create');
                        header('Location:'.base_url().'owners'); 
                    }
                }
                
            }
            else{
                header('Location:'.base_url().'owners'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('owners/admin')){

                    $ownerId = $this->uri->segment(3);
                    $sql = "SELECT * FROM owners WHERE ownerId = ? AND active = 1";
                    
                    $res = new StdClass();
                    $res->owner = $this->db->query($sql,$ownerId)->row();

                    if(isset($res->owner))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[owners.name.ownerId.'.$ownerId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $ownerId;

                            $this->load->view('_shared/header');
                            $this->load->view('owners/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'ownerId' => $ownerId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Owners->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('ownerMessage', 'edit');
                                header('Location:'.base_url().'owners'); 
                            }
                        }
                    }
                    else{
                        header('Location:'.base_url());     
                    }
                }
                else{
                    header('Location:'.base_url().'owners');     
                }
            }
            else {
                header('Location:'.base_url().'owners'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('owners/admin')){
                    $ownerId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->owner = $this->Owners->getowner($ownerId);
                    
                    if(isset($res->owner))
                    {
                        if($this->input->post('ownerId') && $this->input->post('ownerId') == $res->owner->ownerId){
                            $ownerDelete = array('ownerId' => $res->owner->ownerId);
                            $res = $this->Owners->Delete($ownerDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('ownerMessage', 'delete');
                                header('Location:'.base_url().'owners');
                            }
                        }
                        else {
                            $res->unique = $ownerId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('owners/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }   
            else {
                header('Location:'.base_url());
            }
        }
    }

    /* End of file owners.php */
?>