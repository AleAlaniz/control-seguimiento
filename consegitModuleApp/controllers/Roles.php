<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Role_model','Role');
	}

	public function index()
	{	
		if($this->Identity_model->Validate('roles/view')){

			$roles = $this->Role->getRoles();

			$this->load->view('_shared/header');
			$this->load->view('roles/index', array('roles' => $roles));
			$this->load->view('_shared/footer');
		}
		else{

			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function create()
	{
		if($this->Identity_model->Validate('roles/admin')){

			$permissions = $this->Role->getPermissions();

			$this->load->view('_shared/header');
			$this->load->view('roles/create',array('permissions' => $permissions));
			$this->load->view('_shared/footer');

		}
		else{

			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function view()
	{
		if($this->Identity_model->Validate('roles/view')){

			$roleId = $this->uri->segment(3);
			$this->_showRoleInfo($roleId,'view');
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function edit()
	{
		if($this->Identity_model->Validate('roles/admin')){

			$roleId = $this->uri->segment(3);
			$this->_showRoleInfo($roleId,'edit');
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function delete($roleId)
	{
		if($this->Identity_model->Validate('roles/admin')){

			$roleId = $this->uri->segment(3);
			$this->_showRoleInfo($roleId,'delete');
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function createRole()
	{
		if($this->Identity_model->Validate('roles/admin')){

			$this->load->library('form_validation');
			$this->form_validation->set_rules('name',$this->lang->line('general_name'),'trim|required|max_length[100]|is_unique[roles.name]');
			
			if ($this->form_validation->run() == FALSE) {

				$this->create();
			}
			else{

				$this->Role->createRole($_POST);
				$this->session->set_flashdata('role', 'create');
				header('Location:/'.FOLDERADD.'/roles');
			}
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function editRole()
	{
		if($this->Identity_model->Validate('roles/admin')){

			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('name',$this->lang->line('general_name'),'trim|required|max_length[100]|edit_unique[roles.name.roleId.'.$_POST['roleId'].'.FALSE]');
			
			if ($this->form_validation->run() == FALSE) {
				
				$this->edit();
			}

			else{
				
				$this->Role->editRole($_POST);
				$this->session->set_flashdata('role', 'edit');
				header('Location:/'.FOLDERADD.'/roles');
			}
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function deleteRole()
	{
		if($this->Identity_model->Validate('roles/admin')){

			$this->Role->deleteRole($_POST);
			$this->session->set_flashdata('role', 'delete');
			header('Location:/'.FOLDERADD.'/roles');
		}
		else
		{
			header('Location:/'.FOLDERADD.'/');
		}
	}

	public function _showRoleInfo($roleId, $type)
	{

		if (isset($roleId) && is_numeric($roleId) && $this->Role->roleIdExists($roleId)){

			$role = $this->Role->getRole($roleId);
		
			$this->load->view('_shared/header');
			$this->load->view('roles/'.$type,array('role' => $role,'unique' => $roleId));
			$this->load->view('_shared/footer');
		}
		else
		{
			header('Location:/'.FOLDERADD.'/roles');
		}
		
	}
}
