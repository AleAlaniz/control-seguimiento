<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Remits extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
        $this->load->model('Remits_model','Remits');
        $this->load->model('Shipments_model','Shipments');
        $this->load->model('Deliveries_model','Deliveries');
    }

    public function index()
    {
        if($this->Identity_model->Validate('remits/admin')){
            $remits = $this->Remits->getRemits();
            $this->load->view('_shared/header');
            $this->load->view('remits/index', array('remits' => $remits));
            $this->load->view('_shared/footer');
        }
        else{
            header('Location:'.base_url());
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('remits/admin')){
            $data = new StdClass();
            $this->form_validation->set_rules('shipments[]'         ,'lang:main_shipment'                       ,'required');
            $this->form_validation->set_rules('name'                ,'lang:general_name'                        ,'required|max_length[50]');
            // $this->form_validation->set_rules('observation'         ,'lang:admin_shipments_observation'         ,'max_length[40]');
            $this->form_validation->set_rules('deliveryId'          ,'lang:admin_remits_delivery'               ,'required|max_length[50]');
            $this->form_validation->set_rules('extensionDate'       ,'lang:admin_remits_extensionDate'          ,'required|max_length[10]');
            
            if ($this->form_validation->run() == FALSE) {
                $data->deliveries = $this->Deliveries->getDeliveries();
                $this->load->view('_shared/header');
                $this->load->view('remits/create',$data);
                $this->load->view('_shared/footer');
            } 
            else 
            {
                $extensionDate = str_replace('/','-',$this->input->post('extensionDate'));
                $extensionDate = date("Y-m-d", strtotime($extensionDate));
                
                $remitInsert = array(
                    'deliveryDate'      => date("Y-m-d"),
                    'name'              => $this->input->post('name'),
                    'deliveryId'        => $this->input->post('deliveryId'),
                    'extensionDate'     => $extensionDate
                );
                
                $shipments = $this->input->post('shipments[]');
                if($this->Remits->Create($remitInsert,$shipments) == "success"){
                    $this->session->set_flashdata('remitMessage', 'create');
                    echo json_encode("success");
                }
            }
        }
        else{
            header('Location:'.base_url().'supplies'); 
        }
    }

    public function getRemitData()
    {
        if($this->Identity_model->Validate('remits/admin')){
            echo json_encode($this->Shipments->GetShipmentsInTransitWithSupplies());
        }
        else{
            show_404();
        }
    }

    public function getRemitById($remitId=0)
    {
        if($this->Identity_model->Validate('remits/admin')){
            return $this->Remits->GetRemitById($remitId);
        }
        else{
            header('Location:'.base_url());
        }
    }

    public function generatePdf()
    {
        if($this->uri->segment(3)){
            $pdf = new PDF('L','mm',array(297,210));

            $data = $this->getRemitById($this->uri->segment(3));
            if(isset($data->remit))
            {
                //imprimir la primer hoja apaisada
                $pdf->AddPage('L','Legal');
                $pdf->SetFont('Arial','',10);

                //imprimiendo las celdas independientes
                $deliveryDetail = "DETALLE DE ENTREGA ".$data->remit->deliveryDate;
                $pdf->Cell(70,7,$deliveryDetail,0,0,'C');
                $pdf->Ln(10);

                $name = iconv('UTF-8', 'windows-1252', $data->remit->name);
                $pdf->Cell(70,7,$name,0,0,'C');
                $pdf->Ln(12);

                $pdf->SetFont('Arial','bu',10);
                $pdf->Cell(70,7,"ENTREGADO",0,0,'C');
                $pdf->Ln(10);
                
                //headers de la tabla
                $pdf->SetFont('Arial','b',10);
                $headers = array(
                    // iconv('UTF-8', 'windows-1252', $this->lang->line('category')),
                    strtoupper($this->lang->line('main_supply')),
                    strtoupper($this->lang->line('sales_dni')),
                    strtoupper($this->lang->line('report_agent')),
                    strtoupper($this->lang->line('admin_operators_domicilio')),
                    strtoupper($this->lang->line('admin_operators_celular')),
                    strtoupper($this->lang->line('admin_remits_observations'))
                );
                $pdf->Cell(80,7,$headers[0],1,0,'C');
                $pdf->Cell(25,7,$headers[1],1,0,'C');
                $pdf->Cell(40,7,$headers[2],1,0,'C');
                $pdf->Cell(80,7,$headers[3],1,0,'C');
                $pdf->Cell(30,7,$headers[4],1,0,'C');
                $pdf->Cell(80,7,$headers[5],1,0,'C');
                $pdf->Ln();

                //recorriendo los datos para generar las filas
                $pdf->SetFont('Arial','',6);
                $cSupplies = 0;
                for ($i=0; $i < count($data->operators); $i++) { 
                    $operator = $data->operators[$i];

                    $sConcatenated = "";
                    $observation = "";
                    for ($j=0; $j < count($operator->shipments); $j++) { 
                        $shipment = $operator->shipments[$j];

                        for ($k=0; $k < count($shipment->supplies); $k++) { 
                            $supply = $shipment->supplies[$k];
                            if($shipment->type == getShipmentType()[0]){
                                $cSupplies++;
                                $sConcatenated.= $supply->category." ";
                                $sConcatenated.= empty($supply->numero_serie) ? " ".$supply->nombre."+ " : " ".$supply->numero_serie."+ ";
                            }
                            else{
                                $observation.= $supply->category;
                                $observation.= empty($supply->numero_serie) ? $supply->nombre."+" : $supply->numero_serie."+";
                            }
                        }
                        $observation.= $shipment->observation;
                    }

                    $x = $pdf->GetX();
                    $y = $pdf->GetY();

                    $pdf->MultiCell(80,7,$sConcatenated,1,'C');
                    $this->updateXY($pdf,$x,80,$y);
                    $pdf->MultiCell(25,7,$operator->dni,1,'C');
                    $this->updateXY($pdf,$x,25,$y);
                    $pdf->MultiCell(40,7,$operator->completeName,1,'C');
                    $this->updateXY($pdf,$x,40,$y);
                    $pdf->MultiCell(80,7,$operator->domicilio,1,'C');
                    $this->updateXY($pdf,$x,80,$y);
                    $pdf->MultiCell(30,7,$operator->celular,1,'C');
                    $this->updateXY($pdf,$x,30,$y);
                    $pdf->MultiCell(80,7,$observation,1,'C');
                    $observation = "";

                    $pdf->Ln();
                }
                $pdf->Ln(10);
                
                $pdf->SetFont('Arial','bu',10);
                $pdf->Cell(80,7,"TOTALES",0,0,'C');
                $pdf->Ln(10);
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(80,7,"INSUMOS RETIRADOS",0,0,'C');
                $pdf->Cell(25,7,$cSupplies,0,0,'C');
                $pdf->Ln(20);
                $pdf->SetFont('Arial','b',10);
                $pdf->Cell(80,7,"TOTALES",'T',0,'C');
                $pdf->SetFont('Arial','',10);
                $pdf->Cell(25,7,$cSupplies,'T',0,'C');

                $pdf->Ln(10);
                $pdf->Cell(80,7,"",0,0,'C');
                $txt = iconv('UTF-8', 'windows-1252', "Recibí conforme:");
                $pdf->Cell(80,7,$txt,0,0,'C');

                $this->PrintCells($pdf,"Firma:");
                $this->PrintCells($pdf,"Aclaracion:",160);
                $this->PrintCells($pdf,"Fecha:",160);

                //imprimir 3 hojas por cada usuario, nota de conformidad
                //SEGUNDA HOJA
                for ($i=0; $i < count($data->operators); $i++) {
                    $operator = $data->operators[$i];
                    
                    $this->Header($pdf,$operator->empresa);

                    $sConcatenated = "";
                    for ($j=0; $j < count($operator->shipments); $j++) { 
                        $shipment = $operator->shipments[$j];

                        for ($k=0; $k < count($shipment->supplies); $k++) { 
                            $supply = $shipment->supplies[$k];
                            
                            $sConcatenated.= $supply->category." ";
                            $sConcatenated.= empty($supply->numero_serie) ? " ".$supply->nombre."+ " : " ".$supply->numero_serie."+ ";
                        }
                    }

                    if($operator->empresa == 'CAT'){
                        $pdf->Ln(35);
                    }
                    else if($operator->empresa == 'CATCX'){
                        $pdf->Ln(20);
                    }

                    $txt = "<div align='justify'><u><b>ACUERDO EXTRAORDINARIO DE PRESTACION DE TAREAS POR TELETRABAJO EN EL MARCO DE LA EMERGENCIA SANITARIA POR COVID-19 (CORONAVIRUS)</b></u><br>En la Ciudad Autónoma de Buenos Aires, a los ____ días del mes de junio de 2020, comparecen por una parte ".$operator->completeName.", DNI ".$operator->dni.", en adelante EL EMPLEADO, y por la otra CAT TECHNOLOGIES ARGENTINA S.A., en adelante LA EMPRESA, y manifiestan: <br> <b>I.</b> Que atento a la situación e excepción que se encuentra atravesando el país como consecuencia de la pandemia COVID-19 Y en virtud de las disposiciones establecidas por el Gobierno hasta la fecha y las que pudiera establecer en el futuro, LA EMPRESA ha evaluado y puesto en funcionamiento diferentes medidas para garantizar la seguridad de quienes se desempeñan en el establecimiento. En el mismo sentido, se ha evaluado el impacto en la vida diaria de aquellos empleados que tienen a su cargo hijos en edad escolar, a quienes se les ha suspendido el dictado de clases por lo menos hasta el 28/06/2020. En dicho marco, y de conformidad con las recomendaciones dictadas por la autoridad de aplicación, se ha ofrecido a aquellos empleados que así lo deseen la posibilidad de prestar servicios en forma remota;<br> <b>II.</b> Que EL EMPLEADO se encuentra interesado en aceptar la propuesta de LA EMPRESA, lo que redundaría en un beneficio para su persona al evitar el traslado a las instalaciones de LA EMPRESA, y posibilitar además su presencia en su hogar para atender adecuadamente sus obligaciones familiares.<br> <b>III.</b> Por los motivos antes expuestos, LAS PARTES acuerdan que LA EMPRESA entregará al EMPLEADO un acceso remoto a los sistemas informáticos que se utilizan para prestar las tareas en el establecimiento. Dicho acceso remoto se utilizará en forma extraordinaria y sujeto a una evaluación dinámica sobre la evolución de las medidas a tomar por parte de los organismos de gobierno en relación a la situación de salud pública antes referenciada.<br> <b>IV.</b> El empleado deberá cumplir exactamente las mismas funciones y horario habituales, debiendo observar las normas de seguridad y confidencialidad de igual modo que cuando concurre a prestar tareas al establecimiento de LA EMPRESA. En particular, deberá garantizar a la empresa la confidencialidad de todos los datos y bases de datos a los que tenga acceso, debiendo disponer los recaudos necesarios para evitar la propagación de dicha información a los eventuales terceros que puedan tener acceso a la misma en su domicilio. LA EMPRESA recuerda en este sentido el compromiso de confidencialidad oportunamente asumido por el empleado, como así también que se encuentra expresamente prohibido almacenar de cualquier manera datos de las bases a las que se acceden para el cumplimiento de sus funciones.<br> <b>V.</b> EL EMPLEADO recibe de parte de LA EMPRESA los siguientes elementos, que deberán ser devueltos en idénticas condiciones de uso al finalizar el plazo de prestación de tareas en el domicilio del empleado <b>".$sConcatenated."</b> - Dichos elementos no podrán ser utilizados ni trasladados fuera del domicilio del EMPLEADO, con la única excepción del traslado desde y hacia el establecimiento de LA EMPRESA, y deberán ser utilizados exclusivamente para el cumplimiento de las tareas laborales. Por lo tanto, EL EMPLEADO se hace responsable por el cuidado y mantenimiento de los elementos entregados, debiendo responder en caso de no devolverlos en idénticas condiciones, facultando desde este momento al EMPLEADOR a retener de los importes que se le deba abonar por cualquier concepto el valor de los elementos de trabajo entregados en caso de falta de devolución o avería imputable.</div>";
                    $this->Body($pdf,$txt);            
                    $this->Footer($pdf,$operator->empresa);

                    //TERCER HOJA
                    $this->Header($pdf,$operator->empresa);
                    $txt = "A todo evento, EL EMPLEADO informa con carácter de declaración jurada que su domicilio es el siguiente: ".$operator->domicilio.".<br><b>VI. EL EMPLEADO</b> recibe la notebook previamente individualizada en el marco de la modalidad de teletrabajo acordada en forma excepcional con la empresa debido a la emergencia sanitaria declarada por el Estado Nacional a raíz del COVID-19 (CORONAVIRUS), dejándose expresa constancia que EL EMPLEADO será el único responsable de la guarda y cuidado de todos los elementos recibidos, debiendo reintegrar a LA EMPRESA cualquier importe que hubiere que abonar en caso de hurto, robo o rotura de los mismos por la causa que fuere. LA EMPRESA podrá requerir la inmediata devolución en el domicilio de la empresa sito en BARTOLOME MITRE 853-CABA, sin necesidad de justificar o invocar causa alguna, cuando así lo crea conveniente, de la notebook entregada en este acto a EL EMPLEADO, habida cuenta que es un derecho de LA EMPRESA y por motivos funcionales de esta que efectúa tal entrega. EL EMPLEADO no podrá reclamar daño o invocar derecho alguno en el caso de que, en el futuro, LA EMPRESA decida el retiro de tal notebook, sea que el retiro de la misma se haya producido en forma genérica (a todos los trabajadores) o al suscriptor o a un grupo de los mismos. Para ello, bastará con notificar a EL EMPLEADO que deberá entregar la notebook y demás elementos recibidos, dentro de las 48 horas de intimado, considerándose causal de injuria hacia LA EMPRESA que habilitará un despido con justa causa la falta de entrega de la misma. EL EMPLEADO recibe la notebook dejándose expresa constancia que la misma es entregada en forma extraordinaria, en el marco de la emergencia sanitaria vigente y declara bajo juramento no tener un equipo de computación similar ni compatible con los requisitos de sistemas de la empresa para poder realizar sus funciones por teletrabajo. EL EMPLEADO se notifica y consiente expresamente que la notebook entregada lo es exclusivamente para brindar su prestación laboral, debiendo abstenerse de utilizarla para cualquier otra finalidad, como así también garantiza que ninguna otra persona utilizará la misma. EL EMPLEADO tiene expresamente vedado instalar, desinstalar o modificar en forma alguna el software de la notebook, a excepción de las expresas instrucciones que pudiera recibir de parte de LA EMPRESA para la actualización en forma remota de los sistemas instalados. EL EMPLEADO deberá restituir la notebook en la forma y modalidad acordada en el acuerdo de prestación laboral por medio de teletrabajo. Para el caso en que así no lo hiciera, EL EMPLEADO autoriza a LA EMPRESA a retener los pagos que por cualquier causa le correspondieran el valor de reposición de la notebook de similares características y categoría. <br><br> <b>VII.</b> EL EMPLEADO deberá dar inmediato aviso en el caso en que por cualquier motivo se viera imposibilitado de prestar tareas desde su domicilio, a fin de evaluar conjuntamente con LA EMPRESA los pasos a seguir para reanudar la prestación laboral a la mayor brevedad, sea desde el domicilio de EL EMPLEADO o desde el establecimiento de LA EMPRESA. <br> <b>VIII.</b> EL EMPLEADO consiente y ratifica el cumplimiento integral de todas las normas de CONFIDENCIALIDAD existentes en el desarrollo de sus tareas y en forma específica declara bajo juramento que las tareas que desarrollará en su vivienda serán llevadas a cabo sin la presencia de otras personas y resguardando en forma absoluta todos los elementos de trabajo, manejo de las tareas, ratificando conocer que cualquier violación que pudiere existir respecto del manejo de la INFORMACION CONFIDENCIAL de los elementos y datos con los cuales desarrolla sus tareas, lo harán responsable de dicha violación."; 
                    $this->Body($pdf,$txt);
                    $this->Footer($pdf,$operator->empresa);

                    // CUARTA HOJA
                    $this->Header($pdf,$operator->empresa);
                    $txt = "<b>VIII.</b> AMBAS PARTES son contestes en que la presente constituye una medida de excepción frente a la emergencia sanitaria imperante, y que por lo tanto podrá ser revisada en cualquier momento. En el mismo sentido, AMBAS PARTES se comprometen a adoptar todas las medidas necesarias para permitir el cumplimiento de la prestación laboral en plena observancia de los principios de buena fe, cooperación y seguridad. En dicho marco, establecen como plazo tentativo de finalización de la presente el día ".$data->remit->extensionDate."; todo ello sin perjuicio de la posibilidad de prorrogar o adelantar su vencimiento en función de la evolución dinámica de la emergencia imperante. Las partes firman al presente en prueba de conformidad, en la locación y fecha indicadas en el proemio del presente.";
                    $this->Body($pdf,$txt);
                    
                    //seccion de firmas
                    $pdf->Ln(10);
                    $pdf->Cell(20,7,"Firma:",0,0,'C');
                    $pdf->Cell(40,7,"",'B',0,'C');
                    $pdf->Ln();

                    $pdf->Cell(20,7,"Aclaracion:",0,0,'C');
                    $pdf->Cell(40,7,"",'B',0,'C');
                    $pdf->Cell(70,7,"Firma y sello de la empresa:",'',0,'C');

                    if($operator->empresa == 'CAT'){
                        $pdf->image(base_url().'/_imgs/firma_cat.png',150,90,60);
                    }
                    else if($operator->empresa == 'CATCX'){
                        $pdf->image(base_url().'/_imgs/firma_cx.png',150,90,60);
                    }
                    $pdf->Ln();

                    $pdf->Cell(20,7,"Fecha:",0,0,'C');
                    $pdf->Cell(40,7,"",'B',0,'C');

                    $this->Footer($pdf,$operator->empresa);
                }
                $pdf->Output();
            }
            else{
                header('Location:'.base_url());
            }
        }
        else{
            header('Location:'.base_url());
        }
    }

    private function Header($pdf,$empresa)
    {
        $pdf->AddPage('P');
        $pdf->SetMargins(25,45);
        $pdf->SetFont('Arial','',10);
        
        if($empresa == 'CAT'){
            $pdf->image(base_url().'/_imgs/header_cat.png',70,10,85);
        }
        else if($empresa == 'CATCX'){
            $pdf->image(base_url().'/_imgs/header_cx.png',35,10,125);
        }
    }
    
    private function Body($pdf,$txt)
    {
        $txt = iconv('UTF-8', 'windows-1252', $txt);
        $pdf->WriteHTML($txt);
    }

    //footer condicional
    private function Footer($pdf,$empresa)
    {
        if($empresa == 'CAT'){
            $pdf->image(base_url().'/_imgs/footer_cat.png',45,270,125);
        }
        else if($empresa == 'CATCX'){
            $pdf->image(base_url().'/_imgs/footer_cx.png',45,270,125);
        }
    }

    private function updateXY($pdf,&$x,$xAdd,$y)
    {
        $x+=$xAdd;
        $pdf->SetXY($x,$y);
    }

    private function PrintCells($pdf,$txt,$extra=0)
    {
        if($extra != 0){
            $pdf->Cell($extra,7,"",0,0,'C');
        }
        $pdf->Cell(40,7,$txt,0,0,'C');
        $pdf->Cell(60,7,"",'B',0,'C');
        $pdf->Ln();
    }
    
}

?>