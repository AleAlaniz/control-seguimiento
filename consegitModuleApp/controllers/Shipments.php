<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Shipments extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Shipments_model','Shipments');
            $this->load->model('Categories_model','Categories');
            $this->load->model('Sites_model','Sites');
            $this->load->model('Refundreasons_model','Refundreasons');
            $this->load->model('Suppliesrequests_model','Suppliesrequests');
            
        }

        public function index()
        {
            if($this->Identity_model->Validate('shipments/view')){
                $states = getShipmentState();
                $this->load->view('_shared/header');
                $this->load->view('shipments/index',array('states'=>$states));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function searchShipments()
        {
            $filters = new StdClass();

            $filters -> state       = (strlen($this->input->post('state'))>1) ? $this->input->post('state') : null;
            $filters -> startDate   = (strlen($this->input->post('startDate'))>0)? $this->input->post('startDate') : null;
            $filters -> endDate     = (strlen($this->input->post('endDate'))>0)? $this->input->post('endDate') : null;

            $filters -> search_value = (strlen($_POST['search']['value'])>0)? $_POST['search']['value'] : null;

            $this->Shipments->searchShipments($filters);
        }

        public function create()
        {
            if($this->Identity_model->Validate('shipments/admin')){
                $this->form_validation->set_rules('type'                ,'lang:admin_shipments_type'                     ,'required');
                $this->form_validation->set_rules('user_operators[]'    ,'lang:admin_shipments_operators_assigned'       ,'callback_validate_input[user_operators[]]');
                $this->form_validation->set_rules('suppliesAdded[]'     ,'lang:main_supplies'                            ,'callback_validate_input[suppliesAdded[]]');

                if ($this->form_validation->run() == FALSE) {
                    $data = new StdClass();
                    $data->types = getShipmentType();
                    $data->sites = $this->Sites->getSites();
                    $data->categories = $this->Categories->getCategories();
                    $data->refundreasons = $this->Refundreasons->getRefundReasons();

                    $this->load->view('_shared/header');
                    $this->load->view('shipments/create',$data);
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $supplies = "";
                    for ($i=0; $i < count($this->input->post('suppliesAdded')); $i++) { 
                        $e = $this->input->post('suppliesAdded')[$i];
                        $supplies .= $e.",";
                    }
                    
                    $shipmentInsert = array(
                        'dateFile'      => $this->input->post('dateFile'),
                        'dateCheck'     => $this->input->post('dateCheck'),
                        'shipmentDate'  => date("Y-m-d H:i:s",strtotime('-3 hours')),
                        'supplies'      => $supplies,
                        'operatorId'    => $this->input->post('user_operators')[0],
                        'state'         => getShipmentState()[1],
                    );
                    
                    if($this->input->post('type') == getShipmentType()[1]){
                        $shipmentInsert['type'] = getShipmentType()[1];
                        $shipmentInsert['boxId'] = $this->input->post('boxId');
                        $shipmentInsert['observation'] = $this->input->post('observation');
                        $shipmentInsert['refundreasonId'] = $this->input->post('refundreasonId');

                        if($this->input->post('refundreasonId') == 3){
                            for ($i=0; $i < count($this->input->post('suppliesAdded')); $i++) { 
                                $e = $this->input->post('suppliesAdded')[$i];
                                $this->db->set('stock', 0);
                                $this->db->where('supplyId', $e);
                                $this->db->update('supplies');
                            }
                        }
                    }
                    else{
                        $shipmentInsert['type'] = getShipmentType()[0];
                    }
                    
                    $status = $this->Shipments->Create($shipmentInsert);
                    if ($status == "success") {
                        if($this->input->post('type') == getShipmentType()[0] && count($this->input->post("requestsAdded[]")) > 0){
                            $status = $this->Suppliesrequests->SetStateToSend($this->input->post("requestsAdded[]"));
                        }
                        $_SESSION['shipmentsMessage'] = 'create';
                    }
                    echo $status;
                }
                
            }
            else{
                header('Location:'.base_url().'shipments'); 
            }
        }

        public function edit()
        {   
            if($this->Identity_model->Validate('shipments/admin')){
                if($this->input->post('oldState') == getShipmentState()[1]){

                    $res = new StdClass();
    
                    $shipmentId = $this->input->post('shipmentId');
                    $state = $this->input->post('state');
                    $date = $this->input->post('date');
                    $operatorId = $this->input->post('operatorId');

                    if(!empty($date) && !empty($state))
                    {
                        $shipment = $this->Shipments->GetShipmentById($shipmentId);
                        $shipmentDate = strtotime($shipment->shipmentDate);
                        $deliveryDate = str_replace('/','-',$date);
                        $deliveryDate = date("Y-m-d H:i:s", strtotime($deliveryDate));
                        $deliveryDate = strtotime($deliveryDate);

                        if($shipmentDate < $deliveryDate){
                            $res->status = $this->Shipments->Edit($shipment,$state,$date,$operatorId);
            
                            if ($res->status == "success") {
                                $res->message = $this->lang->line('admin_shipments_edit_success');
                                $this->session->set_flashdata('shipmentsMessage', 'edit');
                            }
                            else{
                                $res->status = 'fail';
                                $res->message = 'El envío no fue editado debido a un error inesperado';
                            }
                        }
                        else{
                            $res->status = "fail";
                            $res->message = "La fecha de entrega no puede ser menor a la de envío";
                        }
                    }
                    else{
                        $res->status = "fail";
                        $res->message = "Completa todos los campos";
                    }
    
                    echo json_encode($res);
                }
                else{
                    header('Location:'.base_url().'shipments');    
                }
            }
            else{
                header('Location:'.base_url().'shipments');
            }
            
        }

        public function delete()
        {
            if($this->Identity_model->Validate('shipments/admin')){ 
                $res = "";
                if(isset($_POST['shipmentId']))
                {
                    $shipment = $this->Shipments->Exists($_POST['shipmentId']);
                    if(isset($shipment)){
                        $res = $this->Shipments->Delete($shipment);
                        if($res == "success"){
                            $this->session->set_flashdata('shipmentsMessage', 'delete');
                            echo $res; return;
                        }
                        echo $res; return;
                    }
                    else{
                        print_r("no existe");
                        show_404();
                    }
                }
                $res =  "error no shipmentId";
                echo $res; return;
            }
            else{
                header('Location:/'.FOLDERADD.'/');
            }
        }

        public function view()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('shipments/view')){
                    $shipmentId = $this->uri->segment(3);

                    $res = new StdClass();
                    $res->shipment = $this->Shipments->GetShipmentById($shipmentId);

                    if(isset($res->shipment))
                    {
                        $this->load->view('_shared/header');
                        $this->load->view('shipments/view',$res);
                        $this->load->view('_shared/footer');
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }
            else {
                header('Location:'.base_url());
            }
        }

        public function validate_input($value,$param)
        {
            $input = $this->input->post($param);
            if(isset($input)){
                return true;
            }
            $this->form_validation->set_message('validate_input', 'El campo {field} es requerido');
            return false;
        }

        public function process_data()
        {
            echo $this->Shipments->ProcessData();
        }

    }

    /* End of file shipments.php */
?>