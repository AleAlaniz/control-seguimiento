<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Refundreasons extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Refundreasons_model','Refundreasons');
        }

        public function index()
        {
            if($this->Identity_model->Validate('refundreasons/view')){
                $refundreasons = $this->Refundreasons->getRefundReasons();
                $this->load->view('_shared/header');
                $this->load->view('refundreasons/index', array('refundreasons' => $refundreasons));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('refundreasons/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[refundreasons.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('refundreasons/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $reasonInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Refundreasons->Create($reasonInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('refundreasonMessage', 'create');
                        header('Location:'.base_url().'refundreasons'); 
                    }
                }
                
            }
            else{
                header('Location:'.base_url().'refundreasons'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('refundreasons/admin')){

                    $refundreasonId = $this->uri->segment(3);

                    $res = new StdClass();
                    $res->refundreason = $this->Refundreasons->getRefundReason($refundreasonId);

                    if(isset($res->refundreason))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[refundreasons.name.refundreasonId.'.$refundreasonId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $refundreasonId;

                            $this->load->view('_shared/header');
                            $this->load->view('refundreasons/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'refundreasonId' => $refundreasonId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Refundreasons->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('refundreasonMessage', 'edit');
                                header('Location:'.base_url().'refundreasons'); 
                            }
                        }
                    }
                    else{
                        header('Location:'.base_url());     
                    }
                }
                else{
                    header('Location:'.base_url().'refundreasons');     
                }
            }
            else {
                header('Location:'.base_url().'refundreasons'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('refundreasons/admin')){
                    $refundreasonId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->refundreason = $this->Refundreasons->getRefundReason($refundreasonId);
                    
                    if(isset($res->refundreason))
                    {
                        if($this->input->post('refundreasonId') && $this->input->post('refundreasonId') == $res->refundreason->refundreasonId){
                            $refundreasonDelete = array('refundreasonId' => $res->refundreason->refundreasonId);
                            $res = $this->Refundreasons->Delete($refundreasonDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('refundreasonMessage', 'delete');
                                header('Location:'.base_url().'refundreasons');
                            }
                        }
                        else {
                            $res->unique = $refundreasonId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('refundreasons/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }   
            else {
                header('Location:'.base_url());
            }
        }
    }

    /* End of file refundreasons.php */
?>