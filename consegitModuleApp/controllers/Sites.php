<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Sites extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Sites_model','Sites');
        }

        public function index()
        {
            if($this->Identity_model->Validate('sites/view')){
                $sites = $this->Sites->getSites();
                $this->load->view('_shared/header');
                $this->load->view('sites/index', array('sites' => $sites));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('sites/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[sites.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('sites/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $tlInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Sites->Create($tlInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('siteMessage', 'create');
                        header('Location:'.base_url().'sites'); 
                    }
                }
                
            }
            else{
                header('Location:'.base_url().'sites'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('sites/admin')){

                    $siteId = $this->uri->segment(3);
                    $sql = "SELECT * FROM sites WHERE siteId = ? AND active = 1";
                    
                    $res = new StdClass();
                    $res->site = $this->db->query($sql,$siteId)->row();

                    if(isset($res->site))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[sites.name.siteId.'.$siteId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $siteId;

                            $this->load->view('_shared/header');
                            $this->load->view('sites/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'siteId' => $siteId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Sites->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('siteMessage', 'edit');
                                header('Location:'.base_url().'sites'); 
                            }
                        }
                    }
                    else{
                        header('Location:'.base_url());     
                    }
                }
                else{
                    header('Location:'.base_url().'sites');     
                }
            }
            else {
                header('Location:'.base_url().'sites'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('sites/admin')){
                    $siteId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->site = $this->Sites->getsite($siteId);
                    
                    if(isset($res->site))
                    {
                        if($this->input->post('siteId') && $this->input->post('siteId') == $res->site->siteId){
                            $siteDelete = array('siteId' => $res->site->siteId);
                            $res = $this->Sites->Delete($siteDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('siteMessage', 'delete');
                                header('Location:'.base_url().'sites');
                            }
                        }
                        else {
                            $res->unique = $siteId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('sites/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }   
            else {
                header('Location:'.base_url());
            }
        }
    }

    /* End of file sites.php */
?>