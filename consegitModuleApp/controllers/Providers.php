<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Providers extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Providers_model','Providers');
        }

        public function index()
        {
            if($this->Identity_model->Validate('providers/view')){
                $providers = $this->Providers->getProviders();
                $this->load->view('_shared/header');
                $this->load->view('providers/index', array('providers' => $providers));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:'.base_url());
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('providers/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[providers.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('providers/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $providerInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Providers->Create($providerInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('providerMessage', 'create');
                        header('Location:'.base_url().'providers'); 
                    }
                }
                
            }
            else{
                header('Location:'.base_url().'providers'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('providers/admin')){

                    $providerId = $this->uri->segment(3);
                    $sql = "SELECT * FROM providers WHERE providerId = ? AND active = 1";
                    
                    $res = new StdClass();
                    $res->provider = $this->db->query($sql,$providerId)->row();

                    if(isset($res->provider))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[providers.name.providerId.'.$providerId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $providerId;

                            $this->load->view('_shared/header');
                            $this->load->view('providers/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'providerId' => $providerId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Providers->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('providerMessage', 'edit');
                                header('Location:'.base_url().'providers'); 
                            }
                        }
                    }
                    else{
                        header('Location:'.base_url());     
                    }
                }
                else{
                    header('Location:'.base_url().'providers');     
                }
            }
            else {
                header('Location:'.base_url().'providers'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('providers/admin')){
                    $providerId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->provider = $this->Providers->getProvider($providerId);
                    
                    if(isset($res->provider))
                    {
                        if($this->input->post('providerId') && $this->input->post('providerId') == $res->provider->providerId){
                            $providerDelete = array('providerId' => $res->provider->providerId);
                            $res = $this->Providers->Delete($providerDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('providerMessage', 'delete');
                                header('Location:'.base_url().'providers');
                            }
                        }
                        else {
                            $res->unique = $providerId;
    
                            $this->load->view('_shared/header');
                            $this->load->view('providers/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:'.base_url());
                    }
                }
                else {
                    header('Location:'.base_url());
                }
            }   
            else {
                header('Location:'.base_url());
            }
        }
    }

    /* End of file providers.php */
?>