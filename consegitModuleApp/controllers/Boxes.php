<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Boxes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Boxes_model','Boxes');
		$this->load->model('Sites_model','Sites');
	}

	public function getboxesbysite()
	{
		if($this->Identity_model->Validate('supplies/view'))
		{
			if($this->input->post('siteId')){
				echo json_encode($this->Boxes->GetBoxesBySite($this->input->post('siteId')));
			}
		}
	}

	public function index()
	{	
		if($this->Identity_model->Validate('boxes/view'))
		{
			$this->load->view('_shared/header');
			$this->load->view('boxes/index', array('boxes' => $this->Boxes->GetBoxesData()));
			$this->load->view('_shared/footer');
		}
		else
		{
			header('Location:/'.FOLDERADD);
		}
	}

	

	public function create()
	{
		if($this->Identity_model->Validate('boxes/admin')){
			$this->form_validation->set_rules('box_name'    ,'lang:box_name'         ,'required|max_length[50]|is_unique_active[boxes.name]');
			$this->form_validation->set_rules('site_id'    ,'lang:site_name'         ,'required');
			
			if ($this->form_validation->run() == FALSE) {
				
				$this->load->view('_shared/header');
				$this->load->view('boxes/create', array('sites' => $this->Sites->getSites()));
				$this->load->view('_shared/footer');
			} 
			else 
			{
				$boxInsert = array(
					'name' => htmlspecialchars($this->input->post('box_name')),
					'active' => 1,
					'siteId' => $this->input->post('site_id')
				);
				$res = $this->Boxes->Create($boxInsert);
				if ($res == "success") {
					$this->session->set_flashdata('boxesMessage', 'create');
					header('Location:'.base_url().'boxes'); 
				}
			}
			
		}
		else{
			header('Location:'.base_url().'boxes'); 
		}
	}

	public function edit()
	{
		if($this->uri->segment(3)){
			if($this->Identity_model->Validate('boxes/admin')){

				$boxId = $this->uri->segment(3);
				
				$res = new StdClass();
				$res->box = $this->Boxes->GetBoxData($boxId);

				if(isset($res->box))
				{
					$this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[boxes.name.boxId.'.$boxId.'.TRUE]');
					$this->form_validation->set_rules('site_id'    ,'lang:site_name'         ,'required|exist_site');

					if ($this->form_validation->run() == FALSE) {
						$res->unique = $boxId;

						$res->sites = $this->Sites->getSites();

						$this->load->view('_shared/header');
						$this->load->view('boxes/edit',$res);
						$this->load->view('_shared/footer');
					} 
					else 
					{
						$boxUpdate = array(
							'boxId' 	   => $boxId,
							'name'         => htmlspecialchars($this->input->post('name')),
							'siteId'	   => $this->input->post('site_id')
						);

						$res = $this->Boxes->Edit($boxId,$boxUpdate);

						if ($res == "success") {
							$this->session->set_flashdata('boxesMessage', 'edit');
							header('Location:/'.FOLDERADD.'/boxes'); 
						}
					}
				}
				else{
					header('Location:/'.FOLDERADD.'/');     
				}
			}
			else{
				header('Location:/'.FOLDERADD.'/categories');     
			}
		}
		else {
			header('Location:/'.FOLDERADD.'/categories'); 
		}
	}

	public function delete()
	{
		if($this->uri->segment(3)){
			if($this->Identity_model->Validate('boxes/admin')){
				$boxId = $this->uri->segment(3);
				
				$res = new StdClass();
				$res->box = $this->Boxes->GetBoxData($boxId);
				
				if(isset($res->box))
				{
					if($this->input->post('boxId') && $this->input->post('boxId') == $res->box->boxId){
						$boxDelete = array('boxId' => $res->box->boxId);
						$res = $this->Boxes->Delete($boxDelete);
						if($res == "success")
						{
							$this->session->set_flashdata('boxesMessage', 'delete');
							header('Location:'.base_url().'boxes');
						}
					}
					else {
						$res->unique = $boxId;
						
						$this->load->view('_shared/header');
						$this->load->view('boxes/delete',$res);
						$this->load->view('_shared/footer');
					}
				}
				else {
					header('Location:'.base_url());
				}
			}
			else {
				header('Location:'.base_url());
			}
		}   
		else {
			header('Location:'.base_url());
		}
	}
}
