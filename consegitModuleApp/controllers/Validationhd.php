<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validationhd extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Validationhd_model','Validationhd');
        $this->load->model('Supplies_model','Supplies');
        
    }

    public function index()
    {
        if($this->Identity_model->Validate('validationhd/admin')){
            $this->load->view('_shared/header');
            $this->load->view('validationhd/index');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getSuppliesForValidation()
    {
        if($this->Identity_model->Validate('validationhd/admin')){
            $filters  = new StdClass();
            if (strlen($_POST['search']['value'])>0){

                $filters -> search_value   = $_POST['search']['value'];
            }
            else{

                $filters -> search_value   = null;
            }

            $this->Supplies->GetSuppliesForValidation($filters);
        }
        else{
            show_404();
        }
    }

    public function create()
    {
        if($this->Identity_model->Validate('validationhd/admin')){
            $operatorId = $this->db->get_where('operators',array('operatorId' => $this->input->post('operatorId')))->row();
            $supplyId = $this->db->get_where('supplies',array('supplyId' => $this->input->post('supplyId')))->row();
            if(isset($operatorId) && isset($supplyId))
            {
                $validationInsert = array(
                    'operatorId'        => $this->input->post('operatorId'),
                    'supplyId'          => $this->input->post('supplyId'),
                    'shipmentId'        => $this->input->post('shipmentId'),
                    'validationDate'    => date("Y-m-d H:i:s",strtotime('-3 hours')),
                    'state'             => $this->input->post('state'),
                    'userId'            => $this->session->UserId
                );
                echo $this->Validationhd->create($validationInsert);
            }
            else {
                show_404();
            }
        }
        else{
            show_404();
        }
    }

    public function report()
    {
        if($this->Identity_model->Validate('validationhd/admin')){
            $states = getValidationStates();
            $this->load->view('_shared/header');
            $this->load->view('validationhd/report',array('states'=>$states));
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getValidations()
    {
        if($this->Identity_model->Validate('validationhd/admin')){
            $filters = new StdClass();

            $filters->state       = (strlen($this->input->post('state'))>1) ? $this->input->post('state') : null;
            $filters->startDate   = (strlen($this->input->post('startDate'))>0)? $this->input->post('startDate') : null;
            $filters->endDate     = (strlen($this->input->post('endDate'))>0)? $this->input->post('endDate') : null;

            $filters->search_value = (strlen($_POST['search']['value'])>0)? $_POST['search']['value'] : null;

            $this->Validationhd->GetValidations($filters);
        }
        else{
            show_404();
        }
    }
}

/* End of file Validationhd.php */
    
?>