<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		{
			if($this->Identity_model->ValidateOne('validationhd/admin')){
				header('Location:/'.FOLDERADD.'/validationhd');
			}
			else if($this->Identity_model->ValidateOne('aperturesite/admin')){
				header('Location:/'.FOLDERADD.'/aperturesite');
			}
			else if($this->Identity_model->ValidateOne('aperturesite/report')){
				header('Location:/'.FOLDERADD.'/aperturesite/report');
			}
			else if($this->Identity_model->ValidateOne('aperturesite/rrhh')){
				header('Location:/'.FOLDERADD.'/aperturesite/rrhh');
			}
			else if($this->Identity_model->ValidateOne('aperturesite/ops')){
				header('Location:/'.FOLDERADD.'/aperturesite/ops');
			}
			else if($this->Identity_model->ValidateOne('aperturesite/logis')){
				header('Location:/'.FOLDERADD.'/aperturesite/logis');
			}
			else if($this->Identity_model->Validate('shipments/admin')){
				header('Location:/'.FOLDERADD.'/shipments');
			}
			else{
				header('Location:/'.FOLDERADD.'/supplies');
			}
		}
		else{
			$this->load->view('users/login');
		}
	}


	public function autenticate(){
		
		// if ($this->session->has_userdata('Loged') && $this->session->has_userdata('UserId'))
		// {
		// 	header('Location:/'.FOLDERADD);
		// }
		// else{
			$status = $this->Identity_model->Autenticate();
			
			if (strcmp('success', $status) === 0){
				header('Location:/'.FOLDERADD);
			}
			else{
				$this->session->set_flashdata('auth_status', $status);
				$this->load->view('users/login');
			}
		// }
		
	}

	public function logout()
	{
		$this->Identity_model->Logout();
	}
}
