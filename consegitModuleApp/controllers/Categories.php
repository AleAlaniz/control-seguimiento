<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Categories extends CI_Controller {

        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('Categories_model','Categories');
        }

        public function index()
        {
            if($this->Identity_model->Validate('categories/view')){
                $categories = $this->Categories->getCategories();
                $this->load->view('_shared/header');
                $this->load->view('categories/index', array('categories' => $categories));
                $this->load->view('_shared/footer');
            }
            else{
                header('Location:/'.FOLDERADD.'/');
            }
        }

        public function create()
        {
            if($this->Identity_model->Validate('categories/admin')){
                $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|is_unique_active[categories.name]');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('_shared/header');
                    $this->load->view('categories/create');
                    $this->load->view('_shared/footer');
                } 
                else 
                {
                    $tlInsert = array('name' => htmlspecialchars($this->input->post('name')));
                    $res = $this->Categories->Create($tlInsert);
                    if ($res == "success") {
                        $this->session->set_flashdata('categoriesMessage', 'create');
                        header('Location:/'.FOLDERADD.'/categories'); 
                    }
                }
                
            }
            else{
                header('Location:/'.FOLDERADD.'/categories'); 
            }
        }

        public function edit()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('categories/admin')){

                    $categoryId = $this->uri->segment(3);
                    $sql = "SELECT * FROM categories WHERE categoryId = ?";
                    
                    $res = new StdClass();
                    $res->category = $this->db->query($sql,$categoryId)->row();

                    if(isset($res->category))
                    {
                        $this->form_validation->set_rules('name'    ,'lang:general_name'         ,'required|max_length[50]|edit_unique[categories.name.categoryId.'.$categoryId.'.TRUE]');
                        if ($this->form_validation->run() == FALSE) {
                            $res->unique = $categoryId;

                            $this->load->view('_shared/header');
                            $this->load->view('categories/edit',$res);
                            $this->load->view('_shared/footer');
                        } 
                        else 
                        {
                            $tlInsert = array(
                                'categoryId' => $categoryId,
                                'name'         => htmlspecialchars($this->input->post('name'))
                            );

                            $res = $this->Categories->Edit($tlInsert);

                            if ($res == "success") {
                                $this->session->set_flashdata('categoriesMessage', 'edit');
                                header('Location:/'.FOLDERADD.'/categories'); 
                            }
                        }
                    }
                    else{
                        header('Location:/'.FOLDERADD.'/');     
                    }
                }
                else{
                    header('Location:/'.FOLDERADD.'/categories');     
                }
            }
            else {
                header('Location:/'.FOLDERADD.'/categories'); 
            }
        }

        public function delete()
        {
            if($this->uri->segment(3)){
                if($this->Identity_model->Validate('categories/admin')){
                    $categoryId = $this->uri->segment(3);
                    
                    $res = new StdClass();
                    $res->category = $this->Categories->getCategory($categoryId);
                    
                    if(isset($res->category))
                    {
                        if($this->input->post('categoryId') && $this->input->post('categoryId') == $res->category->categoryId){
                            $categoryDelete = array('categoryId' => $res->category->categoryId);
                            $res = $this->Categories->Delete($categoryDelete);
                            if($res == "success")
                            {
                                $this->session->set_flashdata('categoriesMessage', 'delete');
                                header('Location:/'.FOLDERADD.'/categories');
                            }
                        }
                        else {
                            $res->unique = $categoryId;
                            
                            $this->load->view('_shared/header');
                            $this->load->view('categories/delete',$res);
                            $this->load->view('_shared/footer');
                        }
                    }
                    else {
                        header('Location:/'.FOLDERADD.'/');
                    }
                }
                else {
                    header('Location:/'.FOLDERADD.'/');
                }
            }   
            else {
                header('Location:/'.FOLDERADD.'/');
            }
        }
    }

    /* End of file Teamleaders.php */
?>