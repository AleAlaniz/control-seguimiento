<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplies extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Brands_model','Brands');
    }

    public function getBrands()
    {
        return $this->Brands->getBrands();
    }
}