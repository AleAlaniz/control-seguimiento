<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aperturesite extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Operators_model','Operators');
        $this->load->model('Aperturesite_model','Aperturesite');
        $this->load->library('excel');
    }

    public function index()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            $states = array("Si","No");
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/index',array('states'=>$states));
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function uploadFile()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=5; $row<=$highestRow; $row++)
                    {
                        $dataArray[] = array(
                            'legajo'			                    =>	$worksheet->getCellByColumnAndRow(1, $row)->getValue(),
                            'dni'			                        =>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'empresa'   		                    =>	$worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'cliente'   		                    =>	$worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            // 'campana'		                        =>	$worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                            'site_original'		                    =>	$worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'puesto'     		                    =>	$worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'supervisor'		                    =>	$worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'team_leader'		                    =>	$worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'telet_estado_actual'			        =>	$worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'telet_conectividad_historica'	        =>	$worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'telet_persona_riesgo'   		        =>	$worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'telet_fliares_cargo'		            =>	$worksheet->getCellByColumnAndRow(13, $row)->getValue(),
                            'telet_pc_nb_propia'			        =>	$worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                            'telet_elige_operar_site'		        =>	$worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'telet_prioridad_campana'          		=>	$worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'perform_cuartil'          		        =>	$worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'perform_presentismo'		            =>	$worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'perform_prioridad_empleado'	        =>	$worksheet->getCellByColumnAndRow(19, $row)->getValue(),
                            'perform_calidad'   		            =>	$worksheet->getCellByColumnAndRow(20, $row)->getValue()
                        );
                        
                    }
                }
                
                if(count($dataArray)>0){
                    $this->Operators->UpdateByFile($dataArray);
                    $this->session->set_flashdata('aperturesiteMessage', 'upload');
                    echo "success";
                }
            }	
        }
        else{
            show_404();
        }
    }


    public function edit()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            $res = new StdClass();
            $editObject = [];
            foreach ($_POST as $key => $value) {
                $editObject[$key] = $value;
            }
            
            $res->status = $this->Operators->Edit($editObject);
            if($res->status == "success"){
                $this->session->set_flashdata('aperturesiteMessage', 'edit');
            }
            print_r(json_encode($res));
        }
        else {
            show_404();
        }
    }

    public function getOperatorsAperturesite()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            $filters = (array)json_decode($this->input->post('filters'));
            
            $this->Operators->GetOperatorsAperturesite($filters);
        }
        else{
            show_404();
        }
    }

    public function getFiltersData()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            echo json_encode($this->Aperturesite->GetFiltersData($this->input->post('filterName')));
        }
        else{
            show_404();
        }
    }

    // REPORT
    public function report()
    {
        if($this->Identity_model->Validate('aperturesite/report')){
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/report');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getOperatorsApertureReport()
    {
        if($this->Identity_model->Validate('aperturesite/report')){
            $this->Operators->GetOperatorsApertureReport();
        }
        else{
            show_404();
        }
    }

    public function uploadFileReport()
    {
        if($this->Identity_model->Validate('aperturesite/report')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=5; $row<=$highestRow; $row++)
                    {
                        $dataArray[] = array(
                            'dni'			                        =>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'telet_estado_actual'			        =>	$worksheet->getCellByColumnAndRow(10, $row)->getValue(),
                            'telet_conectividad_historica'	        =>	$worksheet->getCellByColumnAndRow(11, $row)->getValue(),
                            'perform_cuartil'          		        =>	$worksheet->getCellByColumnAndRow(17, $row)->getValue(),
                            'perform_presentismo'		            =>	$worksheet->getCellByColumnAndRow(18, $row)->getValue(),
                            'perform_calidad'   		            =>	$worksheet->getCellByColumnAndRow(20, $row)->getValue()
                        );
                        
                    }
                }
                
                if(count($dataArray)>0){
                    $this->Operators->UpdateByFile($dataArray);
                    $this->session->set_flashdata('aperturesiteMessage', 'upload');
                    echo "success";
                }
            }	
        }
        else{
            show_404();
        }
    }

    // RRHH
    public function rrhh()
    {
        if($this->Identity_model->Validate('aperturesite/rrhh')){
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/rrhh');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getOperatorsApertureRrhh()
    {
        if($this->Identity_model->Validate('aperturesite/rrhh')){
            $this->Operators->GetOperatorsApertureRrhh();
        }
        else{
            show_404();
        }
    }

    public function uploadFileRrhh()
    {
        if($this->Identity_model->Validate('aperturesite/rrhh')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=5; $row<=$highestRow; $row++)
                    {
                        $dataArray[] = array(
                            'dni'			                        =>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'telet_persona_riesgo'			        =>	$worksheet->getCellByColumnAndRow(12, $row)->getValue(),
                            'telet_fliares_cargo'   		        =>	$worksheet->getCellByColumnAndRow(13, $row)->getValue()
                        );
                        
                    }
                }
                
                if(count($dataArray)>0){
                    $this->Operators->UpdateByFile($dataArray);
                    $this->session->set_flashdata('aperturesiteMessage', 'upload');
                    echo "success";
                }
            }	
        }
        else{
            show_404();
        }
    }

    //OPS
    public function ops()
    {
        if($this->Identity_model->Validate('aperturesite/ops')){
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/ops');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getOperatorsApertureOps()
    {
        if($this->Identity_model->Validate('aperturesite/ops')){
            $this->Operators->GetOperatorsApertureOps();
        }
        else{
            show_404();
        }
    }

    public function uploadFileOps()
    {
        if($this->Identity_model->Validate('aperturesite/ops')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=5; $row<=$highestRow; $row++)
                    {
                        $dataArray[] = array(
                            'dni'			                            =>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'empresa'   		                        =>	$worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'cliente'			                        =>	$worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                            'site_original'   		                    =>	$worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                            'puesto'			                        =>	$worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                            'supervisor'   		                        =>	$worksheet->getCellByColumnAndRow(8, $row)->getValue(),
                            'team_leader'			                    =>	$worksheet->getCellByColumnAndRow(9, $row)->getValue(),
                            'telet_elige_operar_site'   		        =>	$worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                            'telet_prioridad_campana'   		        =>	$worksheet->getCellByColumnAndRow(16, $row)->getValue(),
                            'perform_prioridad_empleado'   		        =>	$worksheet->getCellByColumnAndRow(19, $row)->getValue()
                        );
                        
                    }
                }
                
                if(count($dataArray)>0){
                    $this->Operators->UpdateByFile($dataArray);
                    $this->session->set_flashdata('aperturesiteMessage', 'upload');
                    echo "success";
                }
            }	
        }
        else{
            show_404();
        }
    }

    //LOGIS
    public function logis()
    {
        if($this->Identity_model->Validate('aperturesite/logis')){
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/logis');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function getOperatorsApertureLogis()
    {
        if($this->Identity_model->Validate('aperturesite/logis')){
            $this->Operators->GetOperatorsApertureLogis();
        }
        else{
            show_404();
        }
    }

    public function uploadFileLogis()
    {
        if($this->Identity_model->Validate('aperturesite/logis')){

            if(isset($_FILES["file"]["name"]))
            {
                $path = $_FILES["file"]["tmp_name"];
                $object = PHPExcel_IOFactory::load($path);
                foreach($object->getWorksheetIterator() as $worksheet)
                {
                    $highestRow = $worksheet->getHighestRow();
                    $highestColumn = $worksheet->getHighestColumn();
                    $dataArray = [];

                    for($row=5; $row<=$highestRow; $row++)
                    {
                        $dataArray[] = array(
                            'dni'			                            =>	$worksheet->getCellByColumnAndRow(2, $row)->getValue(),
                            'telet_pc_nb_propia'   		                =>	$worksheet->getCellByColumnAndRow(14, $row)->getValue(),
                        );
                        
                    }
                }
                
                if(count($dataArray)>0){
                    $this->Operators->UpdateByFile($dataArray);
                    $this->session->set_flashdata('aperturesiteMessage', 'upload');
                    echo "success";
                }
            }	
        }
        else{
            show_404();
        }
    }

    public function map()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            $this->load->view('_shared/header');
            $this->load->view('aperturesite/map');
            $this->load->view('_shared/footer');
        }
        else {
            header('Location:/'.FOLDERADD);
        }
    }

    public function assignPosition()
    {
        if($this->Identity_model->Validate('aperturesite/admin')){
            $objectInsert = array(
                'operatorId' => $this->input->post('operatorId'),
                'positionId' => $this->input->post('positionId'),
                'turnId' => $this->input->post('turnId'),
                'startSchedule' => $this->input->post('startSchedule'),
                'endSchedule' => $this->input->post('endSchedule'),
                'startSchedule' => $this->input->post('startSchedule'),
                'endDate' => $this->input->post('endDate'),
            );

            if(!empty($objectInsert['operatorId'])&&
               !empty($objectInsert['positionId'])&&
               !empty($objectInsert['turnId'])&&
               !empty($objectInsert['startSchedule'])&&
               !empty($objectInsert['endSchedule'])&&
               !empty($objectInsert['startDate'])&&
               !empty($objectInsert['endDate'])
            ){
                echo $this->Aperturesite->AssignPosition($objectInsert);
            }   
            else{
                echo "error";
            }       
        }
    }
}

/* End of file Aperturesite.php */

?>